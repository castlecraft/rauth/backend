# Development Setup

Easy to use VSCode devcontainer setup is available that configures backing services and tools required for development

Clone the repo, copy example configs and re-open in vscode devcontainer

```shell
git clone https://gitlab.com/castlecraft/rauth/backend
cp -R backend/devcontainer-example backend/.devcontainer
cp -R backend/vscode-example backend/.vscode
code backend
```

Once opened in devcontainer:

```shell
yarn
cp .env.devcontainer .env
yarn start:debug
```

Run the setup setup script. Make sure you have started frontend on port `4210` as it will be the base url of the server.

```shell
node scripts/setup.js
```

Note:

- Default `ISSUER_URL` is `http://accounts.localhost:4210`.
- Admin user is `admin@example.com` and password is `14CharP@ssword`.
- In case you need to develop around kerberos use `https://accounts.example.com:4210` for `ISSUER_URL` and use `yarn start --ssl` to start frontend.
- In case the script is used to setup any external server, following are the available env variables:
  - `SERVER_URL`: Server URL used to make the request. Default: `http://localhost:3000`.
  - `FULL_NAME`: Default: `R-Auth Administrator`.
  - `ORGANIZATION_NAME`: Default: `Example Inc`.
  - `ADMIN_EMAIL`: Default: `admin@example.com`.
  - `ISSUER_URL`: Default: `http://accounts.localhost:4210`.
  - `ADMIN_PASSWORD`: Default: `14CharP@ssword`.
  - `ADMIN_PHONE`: Default: `+919876543210`.

For Kerberos related development:

Add LDAP Clients under R-Auth Admin dashboard.

**LDAP**

Following client hosts user account `posix.user`.

- URL: `ldap://ldap`
- Admin DN: `cn=admin,dc=example,dc=com`
- Admin Password: `admin`
- User Search Base: `dc=example,dc=com`
- Username Attribute: `uid`
- Email Attribute: `mail`
- Phone Attribute: `mobile`
- Full Name Attribute: `displayName`
- Timeout: `3000`
- Scope: `openid`
- Claim Map: `employeeID => employeeNumber`

**AD**

Following client hosts user account `ldap.user`.

- URL: `ldap://access-directory`
- Admin DN: `cn=admin,dc=example,dc=org`
- Admin Password: `admin`
- User Search Base: `dc=example,dc=org`
- Username Attribute: `uid`
- Email Attribute: `mail`
- Phone Attribute: `mobile`
- Full Name Attribute: `displayName`
- Timeout: `3000`
- Scope: `openid`
- Claim Map: `employeeID => employeeNumber`

Add principals for users from above clients. Run following command from host machine.

```shell
# for posix.user
docker exec -it backend_devcontainer-kerberos-ldap-1 bash /addprinc.sh
# for ldap.user
docker exec -it backend_devcontainer-kerberos-ad-1 bash /addprinc.sh
```

Note: change `backend_devcontainer-kerberos-ldap-1` and `backend_devcontainer-kerberos-ad-1` to appropriate container name as per your devcontainer project.

Generate keytab in development container for our app. Run following from the `development` devcontainer.

```shell
bash /addsvc.sh
```

Note:

- Setup LDAP Client using Admin UI.
- ldap:
  - access ui on http://localhost:8888
  - admin: `cn=admin,dc=example,dc=com`, password: `admin`
  - hosts `posix.user@EXAMPLE.COM`.
- access-directory:
  - access ui on http://localhost:9999
  - admin: `cn=admin,dc=example,dc=org`, password: `admin`
  - hosts `ldap.user@EXAMPLE.ORG`

## Non container setup

In this case it is assumed you are setting up the backing services separately, configure the `.env` file as per your hosts and start the development.

# Checks before contributing

Merge requests are tested for conventional commits, format, lint, unit and e2e tests.

## Code format with prettier

```shell
yarn format
```

## Code lint with eslint

```shell
yarn lint --fix
```

## Run Unit tests

```shell
yarn test:server
```

## Run e2e tests

To run e2e tests, first clean up the database.

```shell
node scripts/cleanup.js
```

Note:

- The above command is configured to run with devcontainer setup and cleans e2e test db.
- Set and use `AUTH_SOURCE=admin TEST_MONGO_URI=mongodb://root:admin@mongo:27017/authorization-server` to clean up development database.

Run e2e tests:

```shell
yarn test:e2e
```

## Write commit messages

Make sure to follow [Conventional Commits](https://conventionalcommits.org). The commitlint is used in pipeline to validate commit messages.

# Maintenance Guide

## Releases

Every time there are any merge request on `main` branch it will trigger automatic semantic release on gitlab.

## Dependency Upgrades

Run following command to upgrade dependencies to latest version.

```shell
yarn upgrade-interactive
```

Once the dependencies are upgraded run the commands from Lint, format and tests section to verify successful upgrade.

## Optimize Node Modules

```shell
yarn dedupe --strategy highest
```

## Upgrade yarn

```shell
yarn set version stable
yarn
```
