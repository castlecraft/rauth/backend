import { INestApplication } from '@nestjs/common';
import { ExpressAdapter } from '@nestjs/platform-express';
import { Test } from '@nestjs/testing';
import 'jest';
import request from 'supertest';

import { AppModule } from '../src/app.module';
import { OIDCKeyService } from '../src/auth/entities/oidc-key/oidc-key.service';
import { KeyPairGeneratorService } from '../src/auth/schedulers';
import { ClientService } from '../src/client-management/entities/client/client.service';
import { ConfigService } from '../src/config/config.service';
import { INFRASTRUCTURE_CONSOLE } from '../src/constants/app-strings';
import { ExpressServer } from '../src/express-server';
import {
  getUserToken,
  OIDCKey,
  stopServices,
  TEST_TIMEOUT,
} from './e2e-helpers';

jest.setTimeout(TEST_TIMEOUT);

describe('LDAP Authentication (e2e)', () => {
  let app: INestApplication;
  let moduleFixture;
  let clientId: string;
  let redirectUris: string[];
  let allowedScopes: string[];
  let clientService: ClientService;
  let accessToken: string;
  let ldapClient: string;
  let adClient: string;
  let kerberosClient: string;
  let appServer;
  const adminEmail = 'admin@example.com';
  const adminDomain = 'admin.admin';
  const adminPassword = '14CharP@ssword';
  const adminPhone = '+919876543210';
  const issuerUrl = 'http://accounts.localhost:3000';
  const ldapUser = 'Posix.User';
  const adUser = 'Ldap.User';
  const ldapPassword = 'password1';
  const authServer = new ExpressServer(new ConfigService());

  beforeAll(async () => {
    moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication(
      new ExpressAdapter(authServer.server),
    );
    authServer.setupSession(app);
    const keyPairService = moduleFixture.get(KeyPairGeneratorService);
    keyPairService.generateKeyPair = jest.fn(() => Promise.resolve());

    await app.init();
    const oidcKeyService = moduleFixture.get(OIDCKeyService);
    const oidcKey = await oidcKeyService.findOne({});
    if (!oidcKey) await oidcKeyService.save(OIDCKey);

    clientService = moduleFixture.get(ClientService);

    // Run POST /setup

    let res;
    let client;
    appServer = app.getHttpServer().listen(0);
    try {
      res = await request(appServer)
        .post('/setup')
        .send({
          fullName: 'R-Auth Administrator',
          organizationName: 'Example Inc',
          email: adminEmail,
          issuerUrl,
          adminPassword,
          phone: adminPhone,
        })
        .expect(201);
      clientId = res.body.clientId;
      client = await clientService.findOne({ clientId });
    } catch (error) {
      client = await clientService.findOne({ name: INFRASTRUCTURE_CONSOLE });
      clientId = client.clientId;
    }

    redirectUris = client.redirectUris;
    allowedScopes = client.allowedScopes;
    const token = await getUserToken(
      app,
      adminEmail,
      adminPassword,
      allowedScopes,
      clientId,
      redirectUris[0],
    );
    accessToken = token.accessToken;
  });

  it('/POST /ldap_client/v1/create (Create LDAP Client)', done => {
    request(appServer)
      .post('/ldap_client/v1/create')
      .set('Authorization', 'Bearer ' + accessToken)
      .send({
        name: 'ldap',
        disabled: null,
        description: 'ldap',
        url: 'ldap://ldap',
        adminDn: 'cn=admin,dc=example,dc=com',
        adminPassword: 'admin',
        userSearchBase: 'dc=example,dc=com',
        usernameAttribute: 'uid',
        emailAttribute: 'mail',
        phoneAttribute: 'mobile',
        fullNameAttribute: 'displayName',
        timeoutMs: 5000,
        allowedFailedLoginAttempts: 10,
        clientId: null,
        scope: 'openid',
        attributes: [
          {
            claim: 'employeeNumber',
            mapTo: 'employeeId',
          },
        ],
      })
      .expect(201)
      .end((err, res) => {
        if (err) return done(err);
        ldapClient = res.body.uuid;
        done();
      });
  });

  it('/POST /ldap_client/v1/create (Create AD Client)', done => {
    request(appServer)
      .post('/ldap_client/v1/create')
      .set('Authorization', 'Bearer ' + accessToken)
      .send({
        name: 'adUser',
        disabled: null,
        description: 'adUser',
        url: 'ldap://access-directory',
        adminDn: 'cn=admin,dc=example,dc=org',
        adminPassword: 'admin',
        userSearchBase: 'dc=example,dc=org',
        usernameAttribute: 'uid',
        emailAttribute: 'mail',
        phoneAttribute: 'mobile',
        fullNameAttribute: 'displayName',
        timeoutMs: 5500,
        allowedFailedLoginAttempts: 5,
        clientId: null,
        scope: 'openid',
        attributes: [
          {
            claim: 'employeeNumber',
            mapTo: 'employeeId',
          },
        ],
      })
      .expect(201)
      .end((err, res) => {
        if (err) return done(err);
        adClient = res.body.uuid;
        done();
      });
  });

  it('/POST /ldap_client/verify_user (Success Verify Ldap User)', done => {
    request(appServer)
      .post('/ldap_client/verify_user/' + ldapClient)
      .send({ username: ldapUser })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/POST /ldap_client/verify_user (Success Verify AD User)', done => {
    request(appServer)
      .post('/ldap_client/verify_user/' + adClient)
      .send({ username: adUser })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/POST /ldap_client/verify_user (Fail Verify AD User with Ldap Client)', done => {
    request(appServer)
      .post('/ldap_client/verify_user/' + ldapClient)
      .send({ username: adUser })
      .expect(401)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/POST /ldap_client/login (Success Login as Ldap User)', done => {
    request(appServer)
      .post('/ldap_client/login/' + ldapClient)
      .send({
        username: ldapUser,
        password: ldapPassword,
        redirect: null,
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/POST /ldap_client/login (Success Login as AD User)', done => {
    request(appServer)
      .post('/ldap_client/login/' + adClient)
      .send({
        username: adUser,
        password: ldapPassword,
        redirect: null,
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/POST /ldap_client/login (Fail Login Random User with Ldap Client)', done => {
    request(appServer)
      .post('/ldap_client/login/' + ldapClient)
      .send({
        username: adminDomain,
        password: adminPassword,
        redirect: null,
      })
      .expect(401)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/GET /ldap_client/v1/list_clients (List of Ldap Clients)', done => {
    request(appServer)
      .get('/ldap_client/v1/list_clients')
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/POST /ldap_client/v1/update (Success Update Ldap Client)', done => {
    request(appServer)
      .post('/ldap_client/v1/update/' + ldapClient)
      .set('Authorization', 'Bearer ' + accessToken)
      .send({
        name: 'ldap',
        disabled: null,
        description: 'ldap',
        url: 'ldap://ldap',
        adminDn: 'cn=admin,dc=example,dc=com',
        adminPassword: 'admin',
        userSearchBase: 'dc=example,dc=com',
        usernameAttribute: 'uid',
        emailAttribute: 'mail',
        phoneAttribute: 'mobile',
        fullNameAttribute: 'displayName',
        timeoutMs: 6000,
        allowedFailedLoginAttempts: 12,
        clientId: null,
        scope: 'openid',
        attributes: [
          {
            claim: 'employeeNumber',
            mapTo: 'employeeId',
          },
        ],
      })
      .expect(201)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/GET /ldap_client/v1/list (List LDAP Clients)', done => {
    request(appServer)
      .get('/ldap_client/v1/list?limit=10&offset=0&search=&sort=asc')
      .set('Authorization', 'Bearer ' + accessToken)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/POST /kerberos_realm/v1/create (Success Create Kerberos Realm)', done => {
    request(appServer)
      .post('/kerberos_realm/v1/create')
      .set('Authorization', 'Bearer ' + accessToken)
      .send({
        name: 'test',
        disabled: null,
        description: 'test',
        domain: 'admin',
        servicePrincipalName: 'admin',
        ldapClient,
      })
      .end((err, res) => {
        if (err) return done(err);
        kerberosClient = res.body.uuid;
        done();
      });
  });

  it('/GET /kerberos_realm/v1/get (Success Fetch Kerberos Realm)', done => {
    request(appServer)
      .get('/kerberos_realm/v1/get/' + kerberosClient)
      .set('Authorization', 'Bearer ' + accessToken)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/POST /kerberos_realm/v1/update (Success Update Kerberos Realm)', done => {
    request(appServer)
      .post('/kerberos_realm/v1/update/' + kerberosClient)
      .set('Authorization', 'Bearer ' + accessToken)
      .send({
        name: 'test',
        disabled: null,
        description: 'test2',
        domain: 'admin',
        servicePrincipalName: 'admin',
        ldapClient,
      })
      .expect(201)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/GET /kerberos_realm/v1/list_realms (Success Fetch Kerberos List)', done => {
    request(appServer)
      .get('/kerberos_realm/v1/list_realms')
      .end((err, res) => {
        if (err) return done(err);
        expect(res.statusCode).toEqual(200);
        done();
      });
  });

  it('/POST /kerberos_realm/v1/delete (Success Delete Kerberos Realm)', done => {
    request(appServer)
      .post('/kerberos_realm/v1/delete/' + kerberosClient)
      .set('Authorization', 'Bearer ' + accessToken)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/POST /ldap_client/v1/delete (Success Delete LDAP Client)', done => {
    request(appServer)
      .post('/ldap_client/v1/delete/' + ldapClient)
      .set('Authorization', 'Bearer ' + accessToken)
      .expect(201)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/POST /ldap_client/v1/delete (Success Delete AD Client)', done => {
    request(appServer)
      .post('/ldap_client/v1/delete/' + adClient)
      .set('Authorization', 'Bearer ' + accessToken)
      .expect(201)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  afterAll(done => {
    stopServices(app).then(() => done());
  });
});
