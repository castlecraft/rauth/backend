import { INestApplication } from '@nestjs/common';
import { ExpressAdapter } from '@nestjs/platform-express';
import { Test } from '@nestjs/testing';
import 'jest';
import request from 'supertest';

import { AppModule } from '../src/app.module';
import { OIDCKeyService } from '../src/auth/entities/oidc-key/oidc-key.service';
import { KeyPairGeneratorService } from '../src/auth/schedulers';
import { ClientAuthentication } from '../src/client-management/entities/client/client.interface';
import { ClientService } from '../src/client-management/entities/client/client.service';
import { Scope } from '../src/client-management/entities/scope/scope.interface';
import { ScopeService } from '../src/client-management/entities/scope/scope.service';
import { ConfigService } from '../src/config/config.service';
import {
  INFRASTRUCTURE_CONSOLE,
  SCOPE_EMAIL,
} from '../src/constants/app-strings';
import { ExpressServer } from '../src/express-server';
import {
  getUserToken,
  OIDCKey,
  stopServices,
  TEST_TIMEOUT,
} from './e2e-helpers';

jest.setTimeout(TEST_TIMEOUT);

describe('Client Operations (e2e)', () => {
  let app: INestApplication;
  let moduleFixture;
  let clientId: string;
  let redirectUris: string[];
  let allowedScopes: string[];
  let scopeService: ScopeService;
  let clientService: ClientService;
  let userAccessToken: string;
  let clientUuid: string;
  let emailScope: Scope;
  let appServer;
  const adminEmail = 'admin@example.com';
  const adminPassword = '14CharP@ssword';
  const adminPhone = '+919876543210';
  const issuerUrl = 'http://accounts.localhost:3000';
  const authServer = new ExpressServer(new ConfigService());

  beforeAll(async () => {
    moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication(
      new ExpressAdapter(authServer.server),
    );
    authServer.setupSession(app);
    const keyPairService = moduleFixture.get(KeyPairGeneratorService);
    keyPairService.generateKeyPair = jest.fn(() => Promise.resolve());

    await app.init();
    const oidcKeyService = moduleFixture.get(OIDCKeyService);
    const oidcKey = await oidcKeyService.findOne({});
    if (!oidcKey) await oidcKeyService.save(OIDCKey);

    scopeService = moduleFixture.get(ScopeService);
    clientService = moduleFixture.get(ClientService);

    // Run POST /setup

    let res;
    let client;
    appServer = app.getHttpServer().listen(0);
    try {
      res = await request(appServer)
        .post('/setup')
        .send({
          fullName: 'R-Auth Administrator',
          organizationName: 'Example Inc',
          email: adminEmail,
          issuerUrl,
          adminPassword,
          phone: adminPhone,
        })
        .expect(201);
      clientId = res.body.clientId;
      client = await clientService.findOne({ clientId });
    } catch (error) {
      client = await clientService.findOne({ name: INFRASTRUCTURE_CONSOLE });
      clientId = client.clientId;
    }

    redirectUris = client.redirectUris;
    allowedScopes = client.allowedScopes;
    emailScope = await scopeService.findOne({ name: SCOPE_EMAIL });
    const token = await getUserToken(
      app,
      adminEmail,
      adminPassword,
      allowedScopes,
      clientId,
      redirectUris[0],
    );
    userAccessToken = token.accessToken;
  });

  it('/POST /client/v1/create (Create Client)', done => {
    const clientReq = {
      allowedScopes: ['openid', 'email', 'roles', 'profile'],
      authenticationMethod: ClientAuthentication.PublicClient,
      autoApprove: true,
      isTrusted: '1',
      enforcePKCE: true,
      pkceMethods: ['s256', 'plain'],
      name: 'E2E Test Client',
      redirectUris: ['http://e2e.localhost:3000/index.html'],
    };
    request(appServer)
      .post('/client/v1/create')
      .set('Authorization', 'Bearer ' + userAccessToken)
      .send(clientReq)
      .expect(201)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/GET /client/v1/list (List Clients)', done => {
    request(appServer)
      .get('/client/v1/list')
      .set('Authorization', 'Bearer ' + userAccessToken)
      .expect(200)
      .end((err, res) => {
        expect(res.body.docs.length).toBeLessThanOrEqual(3);
        expect(res.body.length).toBeLessThanOrEqual(3);
        if (err) return done(err);
        done();
      });
  });

  it('/GET /client/v1/get_by_client_id (Retrieve Client)', done => {
    request(appServer)
      .get('/client/v1/get_by_client_id/' + clientId)
      .set('Authorization', 'Bearer ' + userAccessToken)
      .expect(200)
      .end((err, res) => {
        clientUuid = res.body.uuid;
        expect(res.body.clientId).toEqual(clientId);
        if (err) return done(err);
        done();
      });
  });

  it('/GET /client/v1/get (Retrieve Client by UUID)', done => {
    request(appServer)
      .get('/client/v1/get/' + clientUuid)
      .set('Authorization', 'Bearer ' + userAccessToken)
      .expect(200)
      .end((err, res) => {
        expect(res.body.clientId).toEqual(clientId);
        expect(res.body.uuid).toEqual(clientUuid);
        if (err) return done(err);
        done();
      });
  });

  it('/POST /scope/v1/create (Fail to add Scope with existing name)', done => {
    request(appServer)
      .post('/scope/v1/create')
      .set('Authorization', 'Bearer ' + userAccessToken)
      .send({ name: 'email' })
      .expect(400)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/POST /scope/v1/update (Fail update of client used scope name)', done => {
    request(appServer)
      .post('/scope/v1/update/' + emailScope.uuid)
      .set('Authorization', 'Bearer ' + userAccessToken)
      .send({ name: 'email_address' })
      .expect(400)
      .end((err, res) => {
        expect(res.body.existingClientsWithScope.length).toBeGreaterThan(0);
        if (err) return done(err);
        done();
      });
  });

  it('/POST /scope/v1/delete (Fail delete of client used scope name)', done => {
    request(appServer)
      .post('/scope/v1/delete/' + emailScope.name)
      .set('Authorization', 'Bearer ' + userAccessToken)
      .send({ name: 'admin' })
      .expect(400)
      .end((err, res) => {
        expect(res.body.cannotDeleteScope).toEqual(emailScope.name);
        if (err) return done(err);
        done();
      });
  });

  afterAll(done => {
    stopServices(app).then(() => done());
  });
});
