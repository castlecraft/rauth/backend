import { HttpStatus, INestApplication } from '@nestjs/common';
import { ExpressAdapter } from '@nestjs/platform-express';
import { Test, TestingModule } from '@nestjs/testing';
import 'jest';
import { authenticator, hotp } from 'otplib';
import request from 'supertest';

import { AppModule } from '../src/app.module';
import { OTPAggregateService } from '../src/auth/aggregates/otp-aggregate/otp-aggregate.service';
import { OIDCKeyService } from '../src/auth/entities/oidc-key/oidc-key.service';
import { KeyPairGeneratorService } from '../src/auth/schedulers';
import { ClientService } from '../src/client-management/entities/client/client.service';
import { ConfigService } from '../src/config/config.service';
import {
  ADMINISTRATOR,
  INFRASTRUCTURE_CONSOLE,
} from '../src/constants/app-strings';
import { ExpressServer } from '../src/express-server';
import { AuthDataType } from '../src/user-management/entities/auth-data/auth-data.interface';
import { AuthDataService } from '../src/user-management/entities/auth-data/auth-data.service';
import { Role } from '../src/user-management/entities/role/role.interface';
import { RoleService } from '../src/user-management/entities/role/role.service';
import { User } from '../src/user-management/entities/user/user.interface';
import { USER } from '../src/user-management/entities/user/user.schema';
import { UserService } from '../src/user-management/entities/user/user.service';
import {
  delay,
  getUserToken,
  OIDCKey,
  stopServices,
  TEST_TIMEOUT,
} from './e2e-helpers';

jest.setTimeout(TEST_TIMEOUT);

describe('Authentication (e2e)', () => {
  let app: INestApplication;
  let moduleFixture: TestingModule;
  let clientId: string;
  let redirectUris: string[];
  let allowedScopes: string[];
  let authDataService: AuthDataService;
  let roleService: RoleService;
  let userService: UserService;
  let clientService: ClientService;
  let otpAggregateService: OTPAggregateService;
  let userAccessToken: string;
  let adminRole: Role;
  let testUser: User;
  let adminUser: User;
  let sharedSecret: string;
  let forgottenPasswordVerificationCode: string;
  let systemEmailAccount: string;
  let appServer;
  const adminEmail = 'admin@example.com';
  const adminPassword = '14CharP@ssword';
  const adminPhone = '+919876543210';
  const issuerUrl = 'http://accounts.localhost:3000';
  const authServer = new ExpressServer(new ConfigService());

  beforeAll(async () => {
    moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication(
      new ExpressAdapter(authServer.server),
    );
    authServer.setupSession(app);
    const keyPairService = moduleFixture.get(KeyPairGeneratorService);
    keyPairService.generateKeyPair = jest.fn(() => Promise.resolve());

    await app.init();
    const oidcKeyService = moduleFixture.get(OIDCKeyService);
    const oidcKey = await oidcKeyService.findOne({});
    if (!oidcKey) await oidcKeyService.save(OIDCKey);

    userService = moduleFixture.get(UserService);
    roleService = moduleFixture.get(RoleService);
    authDataService = moduleFixture.get(AuthDataService);
    clientService = moduleFixture.get(ClientService);
    otpAggregateService = moduleFixture.get(OTPAggregateService);

    // Run POST /setup
    let res;
    let client;
    // https://github.com/ladjs/supertest/issues/709#issuecomment-1004883763
    appServer = app.getHttpServer().listen(0);
    try {
      res = await request(appServer)
        .post('/setup')
        .send({
          fullName: 'R-Auth Administrator',
          organizationName: 'Example Inc',
          email: adminEmail,
          issuerUrl,
          adminPassword,
          phone: adminPhone,
        })
        .expect(201);
      clientId = res.body.clientId;
      client = await clientService.findOne({ clientId });
      adminUser = await userService.findOne({ email: adminEmail });
    } catch (error) {
      client = await clientService.findOne({ name: INFRASTRUCTURE_CONSOLE });
      clientId = client.clientId;
      adminUser = await userService.findOne({ email: adminEmail });
    }

    redirectUris = client.redirectUris;
    allowedScopes = client.allowedScopes;
    adminRole = await roleService.findOne({ name: ADMINISTRATOR });
    const token = await getUserToken(
      app,
      adminEmail,
      adminPassword,
      allowedScopes,
      clientId,
      redirectUris[0],
    );
    userAccessToken = token.accessToken;
  });

  it('/POST /auth/login', done => {
    request(appServer)
      .post('/auth/login')
      .send({
        username: adminEmail,
        password: adminPassword,
        redirect: '/account',
      })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/POST /email/v1/create (Add Email Account)', async () => {
    const createEmailReq = request(appServer)
      .post('/email/v1/create')
      .set('Authorization', 'Bearer ' + userAccessToken)
      .send({
        name: 'Mail Pit',
        host: 'mailpit',
        user: 'admin',
        from: 'admin@example.com',
        pass: 'admin',
        port: '1025',
        disabled: true,
      });
    const emailRes = await createEmailReq;
    expect(emailRes.status).toEqual(HttpStatus.CREATED);
    systemEmailAccount = emailRes.body.uuid;
  });

  it('/POST /settings/v1/update (Update settings)', done => {
    request(appServer)
      .post('/settings/v1/update')
      .set('Authorization', 'Bearer ' + userAccessToken)
      .send({
        issuerUrl,
        authCodeExpiresInMinutes: 10,
        communicationServerClientId: clientId,
        allowedScopes,
        systemEmailAccount,
      })
      .expect(201)
      .end((error, response) => {
        if (error) done(error);
        done();
      });
  });

  it('/POST /role/v1/create (Fail to add Role with existing name)', done => {
    request(appServer)
      .post('/role/v1/create')
      .set('Authorization', 'Bearer ' + userAccessToken)
      .send({ name: 'administrator' })
      .expect(400)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/POST /role/v1/update (Fail update of user used role name)', done => {
    request(appServer)
      .post('/role/v1/update/' + adminRole.uuid)
      .set('Authorization', 'Bearer ' + userAccessToken)
      .send({ name: 'admin' })
      .expect(400)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/POST /role/v1/delete (Fail delete of user used role name)', done => {
    request(appServer)
      .post('/role/v1/delete/' + adminRole.name)
      .set('Authorization', 'Bearer ' + userAccessToken)
      .expect(400)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/POST /user/v1/delete (Fail delete of user with role "administrator")', async () => {
    const admin = await userService.findUserByIdentifier(adminEmail);
    return request(appServer)
      .post('/user/v1/delete/' + admin.uuid)
      .set('Authorization', 'Bearer ' + userAccessToken)
      .send({ name: 'admin' })
      .expect(400);
  });

  it('/POST /user/v1/create (Fail existing email creation)', done => {
    const userReq = {
      roles: ['administrator'],
      phone: '+919999999999',
      password: 'Br@ndNewP@ss1234',
      name: 'Tester',
      email: adminEmail,
    };
    request(appServer)
      .post('/user/v1/create')
      .set('Authorization', 'Bearer ' + userAccessToken)
      .send(userReq)
      .expect(400)
      .end((err, res) => {
        expect(res.body.message).toEqual('User already exists');
        if (err) return done(err);
        done();
      });
  });

  it('/POST /user/v1/create (Fail existing phone creation)', done => {
    const userReq = {
      roles: ['administrator'],
      phone: adminPhone,
      password: 'Br@ndNewP@ss1234',
      name: 'Tester',
      email: 'test@user.org',
    };
    request(appServer)
      .post('/user/v1/create')
      .set('Authorization', 'Bearer ' + userAccessToken)
      .send(userReq)
      .expect(400)
      .end((err, res) => {
        expect(res.body.message).toEqual('User already exists');
        if (err) return done(err);
        done();
      });
  });

  it('/POST /user/v1/create (Create User)', async () => {
    const userReq = {
      roles: [],
      phone: '+919999999999',
      password: 'Br@ndNewP@ss1234',
      name: 'Tester',
      email: 'test@user.org',
    };
    return (
      request(appServer)
        .post('/user/v1/create')
        .set('Authorization', 'Bearer ' + userAccessToken)
        .send(userReq)
        .expect(201)
        // delay 500ms before findUserByEmailOrPhone
        // async event may result in error: Invalid User
        .then(delay(500))
        .then(response => {
          return userService.findUserByIdentifier('test@user.org');
        })
        .then(user => (testUser = user))
    );
  });

  it('/POST /user/v1/update (Change Password)', async () => {
    const userReq = {
      roles: [],
      name: 'Tester',
      email: 'test@user.org',
      password: 'Br@ndNewP@ss4321',
    };
    return request(appServer)
      .post('/user/v1/update/' + testUser.uuid)
      .set('Authorization', 'Bearer ' + userAccessToken)
      .send(userReq)
      .expect(201);
  });

  it('/POST /user/v1/change_password (Self change password)', done => {
    request(appServer)
      .post('/user/v1/change_password')
      .set('Authorization', 'Bearer ' + userAccessToken)
      .send({ currentPassword: adminPassword, newPassword: '14Ch@rPassword' })
      .expect(201)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/POST /user/v1/initialize_2fa, /user/v1/verify_2fa, /auth/login (TOTP), /auth/login (HOTP) and /user/v1/disable_2fa', done => {
    let admin;
    request(appServer)
      .post('/user/v1/initialize_2fa?restart=true')
      .set('Authorization', 'Bearer ' + userAccessToken)
      .then(res => {
        sharedSecret = res.body.key;
        return authenticator.generate(sharedSecret);
      })
      .then(otp => {
        return request(appServer)
          .post('/user/v1/verify_2fa')
          .send({ otp })
          .set('Authorization', 'Bearer ' + userAccessToken);
      })
      .then(() => {
        // TOTP test
        const otp = authenticator.generate(sharedSecret);
        return request(appServer).post('/auth/login').send({
          username: adminEmail,
          password: '14Ch@rPassword',
          code: otp,
          redirect: '/account',
        });
      })
      .then(res => {
        expect(res.status).toBe(200);
      })
      .then(() => {
        return request(appServer).post('/auth/verify_password').send({
          username: adminEmail,
          password: '14Ch@rPassword',
        });
      })
      .then(loggedIn => {
        expect(loggedIn.status).toBe(201);
        return userService.findUserByIdentifier(adminEmail);
      })
      .then(user => (admin = user))
      .then(() => {
        return authDataService.findOne({
          entity: USER,
          entityUuid: admin.uuid,
          authDataType: AuthDataType.LoginOTP,
        });
      })
      .then(otpCounter => {
        if (!otpCounter) {
          return otpAggregateService.generateLoginOTP(admin);
        }
        return otpCounter;
      })
      .then(otpCounter => {
        // HOTP Test
        const otp = hotp.generate(
          otpCounter.metaData.secret as string,
          Number(otpCounter.metaData.counter),
        );
        return request(appServer).post('/auth/login').send({
          username: adminEmail,
          password: '14Ch@rPassword',
          code: otp,
          redirect: '/account',
        });
      })
      .then(res => {
        expect(res.status).toBe(200);
        return request(appServer)
          .post('/user/v1/disable_2fa')
          .set('Authorization', 'Bearer ' + userAccessToken)
          .send();
      })
      .then(res => {
        expect(res.status).toBe(201);
        done();
      })
      .catch(error => done(error));
  });

  it('/POST /user/v1/forgot_password (Reset Forgotten Password)', async () => {
    return request(appServer)
      .post('/user/v1/forgot_password')
      .send({ emailOrPhone: adminEmail })
      .expect(201)
      .then(res => {
        return userService.findUserByIdentifier(adminEmail);
      })
      .then(user => {
        return authDataService.findOne({
          entityUuid: user.uuid,
          authDataType: AuthDataType.VerificationCode,
          entity: USER,
        });
      })
      .then(verificationCode => {
        forgottenPasswordVerificationCode = verificationCode.password;
      });
  });

  it('/POST /user/v1/generate_password (Verify email and set password)', done => {
    request(appServer)
      .post('/user/v1/generate_password')
      .set('Authorization', 'Bearer ' + userAccessToken)
      .send({
        verificationCode: forgottenPasswordVerificationCode,
        password: adminPassword,
      })
      .expect(201)
      .end((err, res) => {
        if (err) return done(err);
        done();
      });
  });

  it('/POST /auth/revoke', async () => {
    const { accessToken } = await getUserToken(
      app,
      adminEmail,
      adminPassword,
      allowedScopes,
      clientId,
      redirectUris[0],
    );
    const revokeRequest = request(appServer)
      .post('/auth/revoke')
      .set('Authorization', 'Bearer ' + accessToken)
      .send({
        clientId,
        uuid: adminUser.uuid,
      });
    const revokeResponse = await revokeRequest;
    expect(revokeResponse.status).toBe(201);
  });

  it('/POST /user/v1/delete_me (Delete Me)', async () => {
    const { accessToken } = await getUserToken(
      app,
      'test@user.org',
      'Br@ndNewP@ss4321',
      allowedScopes,
      clientId,
      redirectUris[0],
    );
    const deleteRequest = request(appServer)
      .post('/user/v1/delete_me')
      .set('Authorization', 'Bearer ' + accessToken);
    const deleteResponse = await deleteRequest;
    expect(deleteResponse.status).toBe(201);
  });

  it('/GET /auth/logout', async () => {
    const { cookies } = await getUserToken(
      app,
      adminEmail,
      adminPassword,
      allowedScopes,
      clientId,
      redirectUris[0],
    );
    const req = request(appServer).get('/auth/logout');
    req.cookies = cookies;
    const res = await req;
    expect(res.status).toBe(200);
  });

  afterAll(done => {
    stopServices(app).then(() => done());
  });
});
