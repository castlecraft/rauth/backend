import { INestApplication } from '@nestjs/common';
import { ClientMqtt } from '@nestjs/microservices';
import { default as MongoStore } from 'connect-mongo';
import 'jest';
import { Mongoose } from 'mongoose';
import request from 'supertest';

import {
  MONGOOSE_CONNECTION,
  SESSION_CONNECTION,
} from '../src/common/database.provider';
import { BROADCAST_EVENT } from '../src/common/events-microservice.client';

export const TEST_TIMEOUT = 30000;

export function getParameterByName(url, name) {
  name = name.replace(/[\[\]]/g, '\\$&');
  const regex = new RegExp('[?&#]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

export function extractToken(hash) {
  const match = hash.match(/access_token=(\w+)/);
  return !!match && match[1];
}

/* eslint-disable max-len */

export const OIDCKey = {
  keyPair: {
    kty: 'RSA',
    kid: 'ZoQ6MA0Bex8qEQ3v7OzUA7mTRhMb0kpZRmw6gNnArxc',
    e: 'AQAB',
    n: 'kz7AAHs5ctl8uYX5MiH76OXCO9H1IP7D51gEx5CQ5f5gD5BJET6uWlOcUI6cTMLQoDn4l7vZ_IJDuVq3lG1n0JcqIUk3uOoKegiyMB4BqW9w3cxFV2KNJnDfgM89jhRXRICsJv6wsgzyMotLG0u9F4DhsbgZoxSn-tfyqIPJKTR1QH_hx6auRzIBu3qNjnlfoPgOl5GPyreqdg-J5oafiU7E0-go9ZbhbYyVtlhD8Fu4qmeb8qk2jnxfnHkj9PnB7AC5j4c6XUPcls36Ik1mhtXIfcBz4PFVrzOW16Ve8n9p1TEJx-C8rm1Q5PYLTFzzoMT0cVWLNJH7qOMgdE8Xxw',
    d: 'MveshD4jE5MeriUmrei_hs1I32X8oLAu5Xw9I0ryLPpRC_IYqKR2u4NnfybuCVAB4QRP_U2j1djNUnKJSxJXtgctKRpr9FDm0XkDHM62Ko0Nl2ims-nEDANUCgUyPGTkFC0p6dGNHgSHLWGz3L3ptSICfX7JOP7kalqnfxcm4WDj7g9MI3wNAMh1qRSMybob_3BnncG9xIUJFfbgw0JKtEvoqynwoHjWth2a2hg_6Ebm_ydXpvs13iY8Y6gLDSzlSyZH7zSiY2CSa73xu5QxXlhczE22lvNTy6ZDTfzCFIvBtJt4GibaM_CR-nPZwGWIorTNlndmu59td-TKCldhCQ',
    p: '5DUJysHO8S3iHLLB_AzY6sq5YmYBus3_pm12MTF-5vkWU2Bgba1mU9Qs45mPzxxbxUl6Bj7wLKgP8KM5KB21CJOuMdZes3Hen9SgdJdtSJNNNL5yyHCh0lgYGqMy-V3wyqre0Fujt4pZmVl4COD7uBg-Dk_p9yYs2lUBkM7gtbs',
    q: 'pS2BavaJw-IOOOjINfthtwdI6zUImRhEh5UtMbos7Y_0Oc3e94cks6n1jKwXCKILFf0y41m0OMBV5w71FqyT81xSc0ISmtlCzD5dcZLMYVEOJQi03F_NNUhBcspVWTVhoCpjDbKflEVTPbkhcpnKsL9vTHiYzmwDdSM9aQ-1X2U',
    dp: 'f-iOdXvNvvalvsoe2mRlDKzV3aYpIAgoW6MM1SPV6iYA8niZc7_2E9Rf2K4QodhWQ60cXPXX7l_Al3MVLTwBZS5JO5vY9qFDU7h8uvzI_x2473Azq88dlGVWFVAV2RljRmUhgA1tJQnBkKdKFUftLtE_rwvxqlpWV4W_2-doodM',
    dq: 'cKVF-0JC1ZmWhW0LDPVwwDdxnSY_xNht8-DiP2VuOlzP-5PQmRJLD1O7J8I8uyB3Wvmf-Lg2VfWlH7xtnJb5FyBBAmVu9rdv_IYTh97LDxsOAGedoCSdd9bc-4HNDtd-ypHdl3vXpHBawl881kDXoA4NwyMDYtL60KnFuZ6C3Jk',
    qi: 'EUrS_kW5fAzZRVYQKoqUaXAa43Oa7lINkRdR7QBfvAMhzBMQVw4L-M-oqwPRyNcXJzOcHFH5XHukTvAwSHWXfa0zng2nQDkszF5nqDQmBPYQ7_YfU1GAw0aElMxlnHak9qKLpg_HrlHMD8SYOPOh-MOT3dkpDv4qRjdIjWes6J4',
  },
};

/* eslint-enable max-len */

// https://stackoverflow.com/a/38956175
export const delay = (milliseconds: number) => promise =>
  new Promise(resolve => setTimeout(() => resolve(promise), milliseconds));

export async function stopServices(app: INestApplication) {
  app.get<ClientMqtt>(BROADCAST_EVENT).close();
  const database = app.get<Mongoose>(MONGOOSE_CONNECTION);
  const store = app.get<MongoStore>(SESSION_CONNECTION);
  await store.close();
  await database.connection.close();
  await database.disconnect();
  await app.close();
}

export async function getUserToken(
  app: INestApplication,
  username: string,
  password: string,
  allowedScopes: string[],
  clientId: string,
  redirectUri: string,
) {
  const login = await request(app.getHttpServer()).post('/auth/login').send({
    username,
    password,
    redirect: '/account',
  });
  const cookies = login.get('Set-Cookie').pop();
  const uri = `/oauth2/confirmation?scope=${allowedScopes.join(
    '%20',
  )}&response_type=code&client_id=${clientId}&redirect_uri=${
    redirectUri
  }&state=420&code_challenge_method=S256&code_challenge=21XaP8MJjpxCMRxgEzBP82sZ73PRLqkyBUta1R309J0`;
  const authRequest = request(app.getHttpServer()).get(uri);
  authRequest.cookies = cookies;
  const authResponse = await authRequest;
  const code = getParameterByName(authResponse.header.location, 'code');
  const codeExRequest = request(app.getHttpServer())
    .post('/oauth2/token')
    .send({
      grant_type: 'authorization_code',
      code,
      redirect_uri: redirectUri,
      client_id: clientId,
      scope: allowedScopes.join(' '),
      code_verifier: '420',
    });
  const codeExResponse = await codeExRequest;
  const accessToken = codeExResponse.body.access_token;
  const refreshToken = codeExResponse.body.refresh_token;
  return { accessToken, refreshToken, cookies };
}
