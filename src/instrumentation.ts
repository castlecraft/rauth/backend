import { getNodeAutoInstrumentations } from '@opentelemetry/auto-instrumentations-node';
import { NodeSDK, tracing } from '@opentelemetry/sdk-node';
import { ConsoleSpanExporter } from '@opentelemetry/sdk-trace-node';

const sdk = new NodeSDK({
  traceExporter: new ConsoleSpanExporter(),
  spanProcessor: new tracing.SimpleSpanProcessor(new ConsoleSpanExporter()),
  instrumentations: [
    getNodeAutoInstrumentations({
      '@opentelemetry/instrumentation-fs': {
        enabled: false,
      },
      '@opentelemetry/instrumentation-mongodb': {
        enhancedDatabaseReporting: true,
      },
      '@opentelemetry/instrumentation-express': {
        requestHook: (spanInfo, requestInfo) => {
          spanInfo.setAttribute(
            'express.body',
            JSON.stringify(requestInfo.request.body),
          );
        },
      },
      '@opentelemetry/instrumentation-http': {
        ignoreIncomingPaths: [/\/metrics/, /\/intake/],
      },
    }),
  ],
});

sdk.start();
