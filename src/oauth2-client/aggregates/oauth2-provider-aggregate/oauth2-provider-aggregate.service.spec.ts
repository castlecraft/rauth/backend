import { Test, TestingModule } from '@nestjs/testing';
import { Oauth2ProviderAggregateService } from './oauth2-provider-aggregate.service';
import { OAuth2ProviderService } from '../../entities/oauth2-provider/oauth2-provider.service';
import { PasswordCryptoService } from '../../../auth/policies/password-crypto/password-crypto.service';

describe('Oauth2ProviderAggregateService', () => {
  let service: Oauth2ProviderAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        Oauth2ProviderAggregateService,
        {
          provide: OAuth2ProviderService,
          useValue: {},
        },
        {
          provide: PasswordCryptoService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<Oauth2ProviderAggregateService>(
      Oauth2ProviderAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
