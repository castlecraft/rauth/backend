import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { v4 as uuidv4 } from 'uuid';
import { OAuth2ProviderDto } from '../../policies/oauth2-provider-dto/oauth2-provider.dto';
import { OAuth2ProviderAddedEvent } from '../../events/oauth2-provider-added/oauth2-provider-added.event';
import { OAuth2ProviderRemovedEvent } from '../../events/oauth2-provider-removed/oauth2-provider-removed.event';
import { OAuth2ProviderService } from '../../entities/oauth2-provider/oauth2-provider.service';
import { OAuth2Provider } from '../../entities/oauth2-provider/oauth2-provider.interface';
import { PasswordCryptoService } from '../../../auth/policies/password-crypto/password-crypto.service';

@Injectable()
export class Oauth2ProviderAggregateService extends AggregateRoot {
  constructor(
    private readonly oauth2Provider: OAuth2ProviderService,
    private readonly passwordCrypto: PasswordCryptoService,
  ) {
    super();
  }

  async addProvider(payload: OAuth2ProviderDto) {
    payload.clientSecret = this.passwordCrypto.encrypt(payload.clientSecret);
    const doc = Object.assign({ uuid: uuidv4() }, payload);
    this.apply(new OAuth2ProviderAddedEvent(doc as OAuth2Provider));
  }

  async removeProvider(uuid: string) {
    const provider = await this.oauth2Provider.findOne({ uuid });
    if (!provider) throw new NotFoundException({ uuid });
    this.apply(new OAuth2ProviderRemovedEvent(provider));
  }

  async retrieveProvider(uuid: string) {
    const provider = await this.oauth2Provider.findOne({ uuid });
    if (!provider) throw new NotFoundException({ uuid });
    provider.clientSecret = undefined;
    return provider;
  }

  async updateProvider(uuid: string, payload: OAuth2ProviderDto) {
    const provider = await this.oauth2Provider.findOne({ uuid });
    if (!provider) throw new NotFoundException({ uuid });
    payload.clientSecret = this.passwordCrypto.encrypt(payload.clientSecret);
    const updatedProvider = Object.assign(provider, payload);
    this.apply(new OAuth2ProviderAddedEvent(updatedProvider));
  }

  async list(
    offset: number = 0,
    limit: number = 20,
    search?: string,
    sort?: string,
  ) {
    offset = Number(offset);
    limit = Number(limit);
    const sortQuery = { name: sort || 'asc' };
    return this.oauth2Provider.list(offset, limit, search, {}, sortQuery);
  }
}
