import {
  Controller,
  Post,
  Get,
  Body,
  Param,
  Req,
  UseGuards,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { OAuth2ProviderDto } from '../../policies/oauth2-provider-dto/oauth2-provider.dto';
import { AddOAuth2ProviderCommand } from '../../commands/add-oauth2-provider/add-oauth2-provider.command';
import { RemoveOAuth2ProviderCommand } from '../../commands/remove-oauth2-provider/remove-oauth2-provider.command';
import { BearerTokenGuard } from '../../../auth/guards/bearer-token.guard';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { Roles } from '../../../auth/decorators/roles.decorator';
import { ADMINISTRATOR } from '../../../constants/app-strings';
import { UpdateOAuth2ProviderCommand } from '../../commands/update-oauth2-provider/update-oauth2-provider.command';
import { ListOAuth2ProviderQuery } from '../../queries/list-oauth2-providers/list-oauth2-providers.query';
import { RetrieveOAuth2ProviderQuery } from '../../queries/retrieve-oauth2-provider/retrieve-oauth2-provider.query';
import { ApiOperation } from '@nestjs/swagger';
import { i18n } from '../../../i18n/i18n.config';

@Controller('oauth2_provider')
export class Oauth2ProviderController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/add_provider')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @ApiOperation({
    summary: i18n.__('add oauth2 client'),
    description: i18n.__('add auth oauth2 provider as client'),
  })
  async addProvider(@Body() payload: OAuth2ProviderDto) {
    return await this.commandBus.execute(new AddOAuth2ProviderCommand(payload));
  }

  @Post('v1/remove_provider/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async removeProvider(@Param('uuid') uuid, @Req() req) {
    const user = req.token.sub;
    return await this.commandBus.execute(
      new RemoveOAuth2ProviderCommand(uuid, user),
    );
  }

  @Post('v1/update_provider/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @ApiOperation({
    summary: i18n.__('update oauth2 provider'),
    description: i18n.__('update oauth2 provider details'),
  })
  async updateProvider(
    @Param('uuid') uuid,
    @Body() payload: OAuth2ProviderDto,
  ) {
    return await this.commandBus.execute(
      new UpdateOAuth2ProviderCommand(uuid, payload),
    );
  }

  @Get('v1/retrieve_provider/:uuid')
  @UseGuards(BearerTokenGuard)
  async retrieveProvider(@Param('uuid') uuid) {
    return await this.queryBus.execute(new RetrieveOAuth2ProviderQuery(uuid));
  }

  @Get('v1/list')
  @UseGuards(BearerTokenGuard)
  async list(
    @Query('offset') offset: number,
    @Query('limit') limit: number,
    @Query('search') search?: string,
    @Query('sort') sort?: string,
  ) {
    return await this.queryBus.execute(
      new ListOAuth2ProviderQuery(offset, limit, search, sort),
    );
  }
}
