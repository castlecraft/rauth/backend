import { Test, TestingModule } from '@nestjs/testing';
import { HttpService } from '@nestjs/axios';
import { QueryBus, CommandBus } from '@nestjs/cqrs';
import { Reflector } from '@nestjs/core';
import { Oauth2ProviderController } from './oauth2-provider.controller';
import { BearerTokenGuard } from '../../../auth/guards/bearer-token.guard';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { BearerTokenService } from '../../../auth/entities/bearer-token/bearer-token.service';

describe('Oauth2Provider Controller', () => {
  let controller: Oauth2ProviderController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        { provide: CommandBus, useValue: {} },
        { provide: QueryBus, useValue: {} },
        { provide: Reflector, useValue: {} },
        { provide: HttpService, useValue: {} },
        { provide: ServerSettingsService, useValue: {} },
        { provide: BearerTokenService, useValue: {} },
      ],
      controllers: [Oauth2ProviderController],
    })
      .overrideGuard(BearerTokenGuard)
      .useValue({})
      .overrideGuard(RoleGuard)
      .useValue({})
      .compile();

    controller = module.get<Oauth2ProviderController>(Oauth2ProviderController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
