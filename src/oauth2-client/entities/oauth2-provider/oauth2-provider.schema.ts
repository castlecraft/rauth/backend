import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

export const OAUTH2_PROVIDER_COLLECTION = 'oauth2_provider';

export const schema = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4, unique: true, sparse: true },
    name: String,
    authServerURL: String,
    clientId: String,
    clientSecret: String,
    profileURL: String,
    tokenURL: String,
    introspectionURL: String,
    authorizationURL: String,
    revocationURL: String,
    disabled: { type: Boolean, default: false },
    scope: [String],
  },
  { collection: OAUTH2_PROVIDER_COLLECTION, versionKey: false },
);

export const OAuth2Provider = schema;

export const OAUTH2_PROVIDER = 'OAuth2Provider';

export const OAuth2ProviderModel = mongoose.model(
  OAUTH2_PROVIDER,
  OAuth2Provider,
);
