import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { OAuth2ProviderUpdatedEvent } from './oauth2-provider-updated.event';
import { OAuth2ProviderService } from '../../entities/oauth2-provider/oauth2-provider.service';

@EventsHandler(OAuth2ProviderUpdatedEvent)
export class OAuth2ProviderUpdatedHandler
  implements IEventHandler<OAuth2ProviderUpdatedEvent>
{
  constructor(private readonly provider: OAuth2ProviderService) {}

  handle(event: OAuth2ProviderUpdatedEvent) {
    const { provider } = event;
    this.provider
      .update({ uuid: provider.uuid }, provider)
      .then(success => {})
      .catch(error => {});
  }
}
