import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { OAuth2ProviderAddedEvent } from './oauth2-provider-added.event';
import { OAuth2ProviderService } from '../../entities/oauth2-provider/oauth2-provider.service';

@EventsHandler(OAuth2ProviderAddedEvent)
export class OAuth2ProviderAddedHandler
  implements IEventHandler<OAuth2ProviderAddedEvent>
{
  constructor(private readonly provider: OAuth2ProviderService) {}

  handle(event: OAuth2ProviderAddedEvent) {
    const { provider } = event;
    this.provider
      .save(provider)
      .then(success => {})
      .catch(error => {});
  }
}
