import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { Test } from '@nestjs/testing';
import { OAuth2ProviderService } from '../../entities/oauth2-provider/oauth2-provider.service';
import { Oauth2TokenModel } from '../../entities/oauth2-token/oauth2-token.schema';
import { OAuth2ProviderAddedEvent } from './oauth2-provider-added.event';
import { OAuth2ProviderAddedHandler } from './oauth2-provider-added.handler';

describe('Event: OAuth2ProviderAddedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: OAuth2ProviderAddedHandler;
  let service: OAuth2ProviderService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        OAuth2ProviderAddedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: OAuth2ProviderService,
          useValue: () => jest.fn(),
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<OAuth2ProviderAddedHandler>(
      OAuth2ProviderAddedHandler,
    );
    service = module.get<OAuth2ProviderService>(OAuth2ProviderService);
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should save OAuth2Provider', async () => {
    eventBus$.publish = jest.fn(() => {});
    const mockProvider = new Oauth2TokenModel({});
    service.save = jest.fn(mockProvider => Promise.resolve(mockProvider));
    await eventHandler.handle(new OAuth2ProviderAddedEvent(mockProvider));
    expect(service.save).toHaveBeenCalledTimes(1);
  });
});
