import { Injectable } from '@nestjs/common';
import { i18n } from './i18n/i18n.config';
import { ServerSettingsService } from './system-settings/entities/server-settings/server-settings.service';
import { ServiceMessage } from './common/interfaces/service-message.interface';

@Injectable()
export class AppService {
  constructor(private readonly serverSettings: ServerSettingsService) {}

  async info(req?) {
    const settings = await this.serverSettings.find();
    const message: ServiceMessage = {
      service: i18n.__('Authorization Server'),
      session: req.isAuthenticated(),
      communication: false,
      infrastructureConsoleClientId: settings.infrastructureConsoleClientId,
      issuerUrl: settings.issuerUrl,
      logoUrl: settings.logoUrl,
      faviconUrl: settings.faviconUrl,
      privacyUrl: settings.privacyUrl,
      helpUrl: settings.helpUrl,
      termsUrl: settings.termsUrl,
      copyrightMessage: settings.copyrightMessage,
      allowedScopes: settings.allowedScopes,
    };

    if (settings.systemEmailAccount) {
      message.communication = true;
    }

    message.disableSignup = settings?.disableSignup
      ? settings.disableSignup
      : false;
    message.enableChoosingAccount = settings.enableChoosingAccount;
    message.enableUserPhone = settings.enableUserPhone;

    return message;
  }

  async getIssuerUrl() {
    const settings = await this.serverSettings.find();
    return settings.issuerUrl;
  }
}

export enum ConnectedServiceNames {
  INFRASTRUCTURE_CONSOLE = 'infrastructure-console',
  COMMUNICATION_SERVER = 'communication-server',
  IDENTITY_PROVIDER = 'identity-provider',
}
