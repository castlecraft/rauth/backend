import {
  BadRequestException,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { randomBytes } from 'crypto';
import { authenticator, totp } from 'otplib';
import QRCode from 'qrcode';
import { v4 as uuidv4 } from 'uuid';
import { UnverifiedEmailAddedEvent } from '../../../auth/events/unverified-email-added/unverified-email-added.event';
import {
  CommunicationServerNotFoundException,
  EmailForVerificationNotFound,
  InvalidOTPException,
  InvalidUserException,
  InvalidVerificationCode,
  PasswordChangeRestrictedException,
  PasswordLessLoginAlreadyEnabledException,
  PasswordLessLoginNotEnabledException,
  PasswordlessLoginRequiresEnabled2FAOrOTPException,
  PasswordSameAsCurrentException,
  ProfileOTPAlreadyEnabledException,
  ProfileOTPNotEnabledException,
  TwoFactorEnabledException,
  TwoFactorNotEnabledException,
  UserAlreadyExistsException,
  VerificationExpiredOrInvalid,
} from '../../../common/filters/exceptions';
import { CryptographerService } from '../../../common/services/cryptographer/cryptographer.service';
import { ConfigService, NODE_ENV } from '../../../config/config.service';
import {
  DB,
  ADMINISTRATOR,
  RAUTH_SOURCE_TYPE,
  RAUTH_PASSWORD_CHANGE,
  SCOPE_SELF_SERVICE,
} from '../../../constants/app-strings';
import { i18n } from '../../../i18n/i18n.config';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { EmailVerifiedAndUpdatedEvent } from '../..//events/email-verified-and-updated/email-verified-and-updated.event';
import {
  AuthData,
  AuthDataType,
} from '../../entities/auth-data/auth-data.interface';
import { AuthDataService } from '../../entities/auth-data/auth-data.service';
import { UserAuthenticatorService } from '../../entities/user-authenticator/user-authenticator.service';
import { User, UserStatus } from '../../entities/user/user.interface';
import { USER } from '../../entities/user/user.schema';
import { UserService } from '../../entities/user/user.service';
import { AuthDataRemovedEvent } from '../../events/auth-data-removed/auth-data-removed.event';
import { EmailVerifiedAndPasswordSetEvent } from '../../events/email-verified-and-password-set/email-verified-and-password-set.event';
import { PasswordChangedEvent } from '../../events/password-changed/password-changed.event';
import { UserAccountModifiedEvent } from '../../events/user-account-modified/user-account-modified.event';
import {
  ChangePasswordDto,
  VerifyEmailDto,
  VerifyUpdatedEmailDto,
} from '../../policies';
import { PasswordPolicyService } from '../../policies/password-policy/password-policy.service';
import { UserClaimService } from '../../../auth/entities/user-claim/user-claim.service';
import { UserClaimsAddedEvent } from '../../../auth/events/user-claims-added/user-claims-added.event';
import { UserClaim } from '../../../auth/entities/user-claim/user-claim.interface';

@Injectable()
export class UserAggregateService extends AggregateRoot {
  constructor(
    private readonly user: UserService,
    private readonly authData: AuthDataService,
    private readonly settings: ServerSettingsService,
    private readonly crypto: CryptographerService,
    private readonly passwordPolicy: PasswordPolicyService,
    private readonly authenticator: UserAuthenticatorService,
    private readonly config: ConfigService,
    private readonly userClaim: UserClaimService,
  ) {
    super();
  }

  async initializeMfa(uuid: string, restart: boolean = false) {
    const user: User = await this.user.findOne({ uuid });
    const environment = this.config.get(NODE_ENV);
    const settings = await this.settings.find();

    // Disable 2FA for admin in env staging
    if (environment === 'staging' && user.roles.includes(ADMINISTRATOR)) {
      return { environment };
    }

    if ((settings && restart) || !user.enable2fa) {
      // Save secret on AuthData
      const secret = authenticator.generateSecret();

      // Find existing AuthData or create new
      let twoFactorTempSecret: AuthData & { _id?: any } =
        await this.checkLocalAuthData(
          user.uuid,
          AuthDataType.TwoFactorTempSecret,
        );
      if (!twoFactorTempSecret) {
        twoFactorTempSecret = this.getNewAuthData(
          user.uuid,
          AuthDataType.TwoFactorTempSecret,
        );
        user.twoFactorTempSecret = twoFactorTempSecret.uuid;
      }

      // Save generated base32 secret
      twoFactorTempSecret.password = secret;
      this.apply(new PasswordChangedEvent(twoFactorTempSecret));
      this.apply(new UserAccountModifiedEvent(user));

      const issuerUrl = settings && new URL(settings.issuerUrl).host;

      authenticator.options = { step: 30 };
      const otpAuthUrl = authenticator.keyuri(
        encodeURIComponent(user.email || user.phone),
        issuerUrl,
        secret,
      );
      const qrImage = await QRCode.toDataURL(otpAuthUrl);

      return {
        qrImage,
        // shared key from AuthData
        key: twoFactorTempSecret.password,
      };
    } else {
      throw new TwoFactorEnabledException();
    }
  }

  async getUserSaltedHashPassword(uuid: string) {
    const user = await this.user.findOne({ uuid });
    const authData = await this.authData.findOne({ uuid: user.password });
    return authData;
  }

  async verify2fa(uuid: string, otp: string) {
    const user = await this.user.findOne({ uuid });
    if (!otp) throw new InvalidOTPException();
    if (user.twoFactorTempSecret) {
      const twoFactorTempSecret = await this.checkLocalAuthData(
        user.uuid,
        AuthDataType.TwoFactorTempSecret,
      );
      const secret = twoFactorTempSecret.password;
      totp.options = { window: 2 };
      const verified = authenticator.verify({ secret, token: otp });
      if (verified) {
        const sharedSecret = await this.checkLocalAuthData(
          user.uuid,
          AuthDataType.SharedSecret,
        );
        if (sharedSecret) {
          this.apply(new AuthDataRemovedEvent(sharedSecret));
        }

        user.sharedSecret = twoFactorTempSecret.uuid;
        twoFactorTempSecret.authDataType = AuthDataType.SharedSecret;
        user.enable2fa = true;
        user.twoFactorTempSecret = undefined;
        this.apply(new PasswordChangedEvent(twoFactorTempSecret));
        this.apply(new UserAccountModifiedEvent(user));

        return {
          user: {
            uuid: user.uuid,
            email: user.email,
            phone: user.phone,
          },
        };
      } else {
        throw new TwoFactorNotEnabledException();
      }
    }
  }

  async disable2fa(uuid: string) {
    const user = await this.user.findOne({ uuid });
    if (!user.enable2fa) {
      throw new TwoFactorNotEnabledException();
    }
    user.enable2fa = false;

    const authenticators = await this.authenticator.find({
      userUuid: user.uuid,
    });
    if (authenticators.length === 0) {
      user.enablePasswordLess = false;
    }

    const sharedSecret = await this.checkLocalAuthData(
      user.uuid,
      AuthDataType.SharedSecret,
    );
    if (sharedSecret) {
      this.apply(new AuthDataRemovedEvent(sharedSecret));
    }

    const twoFactorTempSecret = await this.checkLocalAuthData(
      user.uuid,
      AuthDataType.SharedSecret,
    );
    if (twoFactorTempSecret) {
      this.apply(new AuthDataRemovedEvent(sharedSecret));
    }

    user.twoFactorTempSecret = undefined;
    user.sharedSecret = undefined;
    this.apply(new UserAccountModifiedEvent(user));
  }

  async verifyEmailAndSetPassword(payload: VerifyEmailDto) {
    const result = this.passwordPolicy.validatePassword(payload.password);
    if (result.errors.length > 0) {
      throw new BadRequestException(result.errors);
    }

    const verificationCode = await this.authData.findOne({
      password: payload.verificationCode,
      authDataType: AuthDataType.VerificationCode,
      entity: USER,
    });

    const verifiedUser = await this.user.findOne({
      uuid: verificationCode?.entityUuid,
    });

    if (!verifiedUser) {
      throw new VerificationExpiredOrInvalid();
    }

    const settings = await this.settings.find();
    const userSourceClaim = await this.userClaim.findOne({
      uuid: verificationCode?.entityUuid,
      name: RAUTH_SOURCE_TYPE,
    });
    if (
      !verifiedUser.roles.includes(ADMINISTRATOR) &&
      settings.passwordChangeRestricted &&
      userSourceClaim?.value !== DB
    ) {
      throw new PasswordChangeRestrictedException();
    }

    let userPassword = await this.authData.findOne({
      uuid: verifiedUser.password,
    });

    if (userPassword) {
      const isSamePassword = this.crypto.checkPassword(
        userPassword.password,
        payload.password,
      );
      if (isSamePassword) {
        throw new PasswordSameAsCurrentException();
      }
    }
    let addClaims = true;
    if (!userPassword) {
      userPassword = {} as AuthData & { _id: any };
      userPassword.authDataType = AuthDataType.Password;
      userPassword.uuid = uuidv4();

      addClaims = false;
    }

    userPassword.entity = USER;
    userPassword.entityUuid = verifiedUser.uuid;
    userPassword.password = this.crypto.hashPassword(payload.password);
    verifiedUser.password = userPassword.uuid;
    verifiedUser.disabled = false;
    verifiedUser.isEmailVerified = true;
    this.apply(
      new EmailVerifiedAndPasswordSetEvent(
        verifiedUser,
        userPassword,
        verificationCode,
      ),
    );

    if (addClaims) {
      await this.setPasswordChangeUserClaims(userPassword.entityUuid);
    }
  }

  async validatePassword(userUuid: string, passwordPayload: ChangePasswordDto) {
    const authData = await this.getUserSaltedHashPassword(userUuid);
    const validPassword = this.crypto.checkPassword(
      authData.password,
      passwordPayload.currentPassword,
    );
    const result = this.passwordPolicy.validatePassword(
      passwordPayload.newPassword,
    );
    if (validPassword && result.errors.length === 0) {
      authData.password = this.crypto.hashPassword(passwordPayload.newPassword);
      this.apply(new PasswordChangedEvent(authData));

      await this.setPasswordChangeUserClaims(userUuid);
    } else {
      const errors =
        result.errors.length > 0 ? result.errors : i18n.__('Invalid Password');
      throw new BadRequestException(errors);
    }
  }

  async setPasswordChangeUserClaims(uuid: string) {
    let claims = [];
    const getClaim = await this.userClaim.findOne({
      uuid,
      name: RAUTH_PASSWORD_CHANGE,
      scope: SCOPE_SELF_SERVICE,
    });
    if (getClaim) {
      getClaim.value = new Date();
      claims.push(getClaim);
    } else {
      claims = [
        {
          scope: SCOPE_SELF_SERVICE,
          name: RAUTH_PASSWORD_CHANGE,
          uuid,
          value: new Date(),
        } as UserClaim,
      ];
    }
    this.apply(new UserClaimsAddedEvent(claims));
  }

  async enablePasswordLessLogin(actorUuid: string, userUuid: string) {
    const settings = await this.settings.find();
    if (!settings.systemEmailAccount) {
      throw new CommunicationServerNotFoundException();
    }

    const user = await this.user.findOne({ uuid: userUuid });
    if (!user) throw new InvalidUserException();
    await this.validateAdminActor(actorUuid, userUuid);
    if (user.enablePasswordLess) {
      throw new PasswordLessLoginAlreadyEnabledException();
    }
    if (!user.enable2fa && !user.enableOTP) {
      throw new PasswordlessLoginRequiresEnabled2FAOrOTPException();
    }
    user.enablePasswordLess = true;
    this.apply(new UserAccountModifiedEvent(user));
    return user;
  }

  async disablePasswordLessLogin(actorUuid: string, userUuid: string) {
    const user = await this.user.findOne({ uuid: userUuid });
    if (!user) throw new InvalidUserException();
    await this.validateAdminActor(actorUuid, userUuid);
    if (!user.enablePasswordLess) {
      throw new PasswordLessLoginNotEnabledException();
    }
    user.enablePasswordLess = false;
    this.apply(new UserAccountModifiedEvent(user));
    return user;
  }

  async enableProfileOTP(actorUuid: string, userUuid: string) {
    const settings = await this.settings.find();
    if (!settings.systemEmailAccount) {
      throw new CommunicationServerNotFoundException();
    }

    const user = await this.user.findOne({ uuid: userUuid });
    if (!user) throw new InvalidUserException();
    await this.validateAdminActor(actorUuid, userUuid);
    if (user.enableOTP) {
      throw new ProfileOTPAlreadyEnabledException();
    }
    user.enableOTP = true;
    this.apply(new UserAccountModifiedEvent(user));
    return user;
  }

  async disableProfileOTP(actorUuid: string, userUuid: string) {
    const user = await this.user.findOne({ uuid: userUuid });
    if (!user) throw new InvalidUserException();
    await this.validateAdminActor(actorUuid, userUuid);
    if (!user.enableOTP) {
      throw new ProfileOTPNotEnabledException();
    }
    user.enableOTP = false;
    this.apply(new UserAccountModifiedEvent(user));
    return user;
  }

  async checkLocalAuthData(userEntityUuid: string, authDataType: AuthDataType) {
    return await this.authData.findOne({
      entity: USER,
      entityUuid: userEntityUuid,
      authDataType,
    });
  }

  getNewAuthData(userEntityUuid: string, authDataType: AuthDataType) {
    const authData = {} as AuthData;
    authData.uuid = uuidv4();
    authData.entityUuid = userEntityUuid;
    authData.entity = USER;
    authData.authDataType = authDataType;
    return authData;
  }

  async validateAdminActor(actorUuid: string, userUuid: string) {
    const isAdmin = await this.user.checkAdministrator(actorUuid);
    if (!isAdmin && actorUuid !== userUuid) {
      throw new ForbiddenException();
    }
  }

  async addUnverifiedEmail(userUuid: string, email: string) {
    email = email.trim().toLowerCase();
    await this.checkEmailAlreadyRegistered(email);

    const user = await this.user.findOne({ uuid: userUuid });
    if (!user) throw new InvalidUserException();

    const unverifiedEmail =
      (await this.authData.findOne({
        entity: USER,
        entityUuid: user.uuid,
        authDataType: AuthDataType.UnverifiedEmail,
      })) || ({} as AuthData);

    unverifiedEmail.password = email;
    unverifiedEmail.entity = USER;
    unverifiedEmail.entityUuid = user.uuid;
    unverifiedEmail.authDataType = AuthDataType.UnverifiedEmail;
    const expiry = new Date();
    expiry.setDate(expiry.getDate() + 1);
    unverifiedEmail.expiry = expiry;

    const verificationCode =
      (await this.authData.findOne({
        authDataType: AuthDataType.VerificationCode,
        entity: USER,
        entityUuid: user.uuid,
      })) || ({} as AuthData);

    verificationCode.password = randomBytes(32).toString('hex');
    verificationCode.entity = USER;
    verificationCode.entityUuid = user.uuid;
    verificationCode.expiry = new Date();
    verificationCode.authDataType = AuthDataType.VerificationCode;

    this.apply(
      new UnverifiedEmailAddedEvent(unverifiedEmail, verificationCode),
    );
  }

  async checkEmailAlreadyRegistered(email: string) {
    const user = await this.user.findOne({ email });
    if (user) {
      throw new UserAlreadyExistsException();
    }
  }

  async verifyAndUpdateEmail(payload: VerifyUpdatedEmailDto) {
    const verificationCode = await this.authData.findOne({
      password: payload.verificationCode,
      authDataType: AuthDataType.VerificationCode,
      entity: USER,
    });

    if (!verificationCode) {
      throw new InvalidVerificationCode();
    }

    const verifiedUser = await this.user.findOne({
      uuid: verificationCode?.entityUuid,
    });

    if (!verifiedUser) {
      throw new InvalidUserException();
    }

    const verifiedEmail = await this.authData.findOne({
      entity: USER,
      entityUuid: verifiedUser.uuid,
      authDataType: AuthDataType.UnverifiedEmail,
    });

    if (
      !verifiedEmail &&
      !verifiedUser.email &&
      !verifiedUser.isEmailVerified
    ) {
      throw new EmailForVerificationNotFound();
    }

    if (verifiedEmail?.password) {
      const user = await this.user.findOne({ email: verifiedEmail?.password });
      if (user) {
        throw new UserAlreadyExistsException();
      }
    }

    if (verifiedEmail?.password) {
      verifiedUser.email = verifiedEmail.password;
    }

    verifiedUser.isEmailVerified = true;
    verifiedUser.status = UserStatus.Verified;
    this.apply(
      new EmailVerifiedAndUpdatedEvent(
        verifiedUser,
        verifiedEmail,
        verificationCode,
      ),
    );
  }
}
