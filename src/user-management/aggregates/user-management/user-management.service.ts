import {
  BadRequestException,
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { AggregateRoot, CommandBus } from '@nestjs/cqrs';
import { randomBytes } from 'crypto';
import { v4 as uuidv4 } from 'uuid';

import { BearerTokenService } from '../../../auth/entities/bearer-token/bearer-token.service';
import { UserClaimService } from '../../../auth/entities/user-claim/user-claim.service';
import { ClientService } from '../../../client-management/entities/client/client.service';
import {
  CannotDeleteAdministratorException,
  InvalidClientException,
  InvalidRoleException,
  InvalidUserException,
  PasswordChangeRestrictedException,
  UserAlreadyDisabledException,
  UserAlreadyEnabledException,
  UserAlreadyExistsException,
  UserDeleteDisabled,
  UserOTPNotEnabledException,
} from '../../../common/filters/exceptions';
import { CryptographerService } from '../../../common/services/cryptographer/cryptographer.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import {
  ADMINISTRATOR,
  AVATAR_KEY,
  SCOPE_SELF_SERVICE,
  RAUTH_SOURCE_ID,
  RAUTH_SOURCE_TYPE,
  DB,
  ADMIN,
  EMAIL_DELAY_IN_SECONDS,
  RAUTH_PASSWORD_CHANGE,
} from '../../../constants/app-strings';
import { i18n } from '../../../i18n/i18n.config';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { SystemLogService } from '../../../system-settings/entities/system-log/system-log.service';
import { DeleteAvatarCommand } from '../../commands/delete-avatar/delete-avatar.command';
import {
  AuthData,
  AuthDataType,
} from '../../entities/auth-data/auth-data.interface';
import { AuthDataService } from '../../entities/auth-data/auth-data.service';
import { Role } from '../../entities/role/role.interface';
import { RoleService } from '../../entities/role/role.service';
import { UserAuthenticatorService } from '../../entities/user-authenticator/user-authenticator.service';
import { User } from '../../entities/user/user.interface';
import { USER } from '../../entities/user/user.schema';
import { UserService } from '../../entities/user/user.service';
import { AuthFailureLogsClearedEvent } from '../../events/auth-failure-logs-cleared/auth-failure-logs-cleared.event';
import { ForgottenPasswordGeneratedEvent } from '../../events/forgotten-password-generated/forgotten-password-generated.event';
import { PasswordChangedEvent } from '../../events/password-changed/password-changed.event';
import { UserAccountAddedEvent } from '../../events/user-account-added/user-account-added.event';
import { UserAccountModifiedEvent } from '../../events/user-account-modified/user-account-modified.event';
import { UserAccountRemovedEvent } from '../../events/user-account-removed/user-account-removed';
import { UserOTPEnabledEvent } from '../../events/user-otp-enabled/user-otp-enabled.event';
import { UserRoleAddedEvent } from '../../events/user-role-added/user-role-added.event';
import { UserRoleModifiedEvent } from '../../events/user-role-modified/user-role-modified.event';
import { UserRoleRemovedEvent } from '../../events/user-role-removed/user-role-removed.event';
import { UserAccountDto } from '../../policies';
import { PasswordPolicyService } from '../../policies/password-policy/password-policy.service';
import { RoleValidationPolicyService } from '../../policies/role-validation-policy/role-validation-policy.service';
import { UserClaim } from '../../../auth/entities/user-claim/user-claim.interface';
import { UserClaimsAddedEvent } from '../../../auth/events/user-claims-added/user-claims-added.event';
import { UserDisabledEvent } from '../../events/user-disabled/user-disabled.event';
import { UserSessionsDeletedEvent } from '../../../system-settings/events/user-sessions-deleted/user-sessions-deleted.event';
import { BearerTokensDeletedEvent } from '../../../system-settings/events/bearer-tokens-deleted/bearer-tokens-deleted.event';
import { PasswordExpiryAlertSentEvent } from '../../../user-management/events/password-expiry-alert-sent/password-expiry-alert-sent.event';
import {
  ConfigService,
  ADMIN_SET_USER_PWD,
  EnableDisable,
} from '../../../config/config.service';
import { ExternalAppRemovedEvent } from '../../../user-management/events/external-app-removed/external-app-removed.event';
@Injectable()
export class UserManagementService extends AggregateRoot {
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly userService: UserService,
    private readonly userClaimService: UserClaimService,
    private readonly authDataService: AuthDataService,
    private readonly clientService: ClientService,
    private readonly bearerTokenService: BearerTokenService,
    private readonly roleService: RoleService,
    private readonly crypto: CryptographerService,
    private readonly passwordPolicy: PasswordPolicyService,
    private readonly roleValidationPolicy: RoleValidationPolicyService,
    private readonly authenticator: UserAuthenticatorService,
    private readonly settings: ServerSettingsService,
    private readonly systemLog: SystemLogService,
    private readonly commandBus: CommandBus,
    private readonly config: ConfigService,
  ) {
    super();
  }

  async deleteUserByUser(uuid: string, actorUuid: string) {
    const settings = await this.settings.findWithoutError();
    if (settings?.isUserDeleteDisabled) {
      throw new UserDeleteDisabled();
    }
    const user = await this.fetchUserOrThrowException(uuid);
    if (
      !(await this.userService.checkAdministrator(actorUuid)) &&
      user.uuid !== actorUuid
    ) {
      throw new ForbiddenException();
    }
    await this.deleteUser(uuid);
  }

  async deleteUserByTrustedClient(uuid: string, clientId: string) {
    const client = await this.clientService.findOne({ clientId });
    if (!client) {
      throw new InvalidClientException();
    }
    if (!client.isTrusted) {
      throw new InvalidClientException();
    }
    await this.deleteUser(uuid);
  }

  async deleteUser(uuid: string) {
    const settings = await this.settings.find();
    if (settings.isUserDeleteDisabled) {
      throw new ForbiddenException({
        UserDeleteDisabled: settings.isUserDeleteDisabled,
      });
    }

    const user = await this.fetchUserOrThrowException(uuid);

    if (await this.userService.checkAdministrator(uuid)) {
      throw new CannotDeleteAdministratorException();
    }

    // Remove Auth Data
    const password = await this.authDataService.findOne({
      uuid: user.password,
    });
    const sharedSecret = await this.authDataService.findOne({
      uuid: user.sharedSecret,
    });
    const otpCounter = await this.authDataService.findOne({
      uuid: user.otpCounter,
    });
    const twoFactorTempSecret = await this.authDataService.findOne({
      uuid: user.twoFactorTempSecret,
    });

    // Remove non-trusted OAuth 2.0 Clients created by user
    await this.clientService.deleteClientsByUser(user.uuid);

    // Remove user's bearer tokens
    await this.bearerTokenService.deleteMany({ user: user.uuid });

    // Remove user's authenticator keys
    await this.authenticator.deleteMany({ userUuid: user.uuid });

    // Remove user's authenticator challenges
    await this.authDataService.deleteMany({
      authDataType: AuthDataType.Challenge,
      entity: USER,
      entityUuid: user.uuid,
    });

    // // Clear Logs relate to user
    await this.systemLog.deleteMany({
      entity: USER,
      entityId: user.uuid,
    });

    user.deleted = true;

    // delete uploaded Avatar
    if (settings.avatarBucket) {
      const avatarClaim = await this.userClaimService.findOne({
        name: AVATAR_KEY,
        scope: SCOPE_SELF_SERVICE,
        uuid: user.uuid,
      });

      if (avatarClaim) {
        try {
          await this.commandBus.execute(new DeleteAvatarCommand(user.uuid));
        } catch (error) {
          this.logger.error(error, this.constructor.name);
        }
      }
    }

    this.apply(
      new UserAccountRemovedEvent(
        user,
        password,
        sharedSecret,
        otpCounter,
        twoFactorTempSecret,
      ),
    );
  }

  async backupUser(uuid) {
    // TODO : Generate and Backup of user
  }

  async deleteRole(roleName: string, actorUuid: string) {
    const role = await this.roleService.findOne({ name: roleName });
    if (!role) throw new NotFoundException({ invalidRole: roleName });
    const usersWithRole = await this.userService.find({ roles: role.name });

    this.disallowChangeOfAdminRole(role);

    if (usersWithRole.length > 0) {
      throw new BadRequestException({
        usersWithRole: usersWithRole.map(user => ({
          name: user.name,
          email: user.email,
          phone: user.phone,
        })),
      });
    } else {
      this.apply(new UserRoleRemovedEvent(role, actorUuid));
    }
  }

  async generateForgottenPassword(emailOrPhone: string, redirect?: string) {
    const user = await this.userService.findUserByIdentifier(emailOrPhone);
    const settings = await this.settings.find();
    const userSourceClaim = await this.userClaimService.findOne({
      uuid: emailOrPhone,
      name: RAUTH_SOURCE_TYPE,
    });
    if (
      !user.roles.includes(ADMINISTRATOR) &&
      settings.passwordChangeRestricted &&
      userSourceClaim?.value !== DB
    ) {
      throw new PasswordChangeRestrictedException();
    }
    const verificationCode =
      (await this.authDataService.findOne({
        authDataType: AuthDataType.VerificationCode,
        entity: USER,
        entityUuid: user.uuid,
      })) || ({} as AuthData & { _id: any });
    verificationCode.authDataType = AuthDataType.VerificationCode;
    if (redirect) {
      verificationCode.metaData = { redirect };
    }
    verificationCode.password = randomBytes(32).toString('hex');
    verificationCode.entity = USER;
    verificationCode.entityUuid = user.uuid;
    const expiry = new Date();
    expiry.setDate(expiry.getDate() + 1);
    verificationCode.expiry = new Date();

    // Get the current time
    const currentTime = new Date();

    // Calculate the email delay in milliseconds
    const delayInMilliseconds =
      (settings.emailDelayInSeconds || EMAIL_DELAY_IN_SECONDS) * 1000;
    // Determine the earliest time a new request can be made
    const earliestRequestTime = new Date(
      currentTime.getTime() - delayInMilliseconds,
    );

    // Check if a recent request was made by comparing the creation timestamp
    const hasRecentRequest =
      verificationCode.creation &&
      verificationCode.creation >= earliestRequestTime;

    if (!hasRecentRequest) {
      // If no recent request, proceed with sending the email and update the creation timestamp
      verificationCode.creation = currentTime; // Set the creation timestamp to the current time
      this.apply(new ForgottenPasswordGeneratedEvent(user, verificationCode)); // Trigger the event for sending email
    }
  }

  async addUserAccount(userData: UserAccountDto, createdBy?: string) {
    await this.validateExistingUser(userData);
    const settings = await this.settings.find();

    if (settings.enableUserOTP && userData?.enableOTP !== true) {
      throw new UserOTPNotEnabledException();
    }

    const user = {} as User;
    user.uuid = uuidv4();
    user.email = userData.email;
    user.name = userData.name;
    user.phone = userData.phone;
    user.disabled = userData?.disabled;
    user.enableOTP = userData?.enableOTP;

    await this.validateRoles(userData.roles);

    user.roles = userData.roles;
    let authData = undefined as AuthData & { _id: any };
    if (
      userData.password &&
      this.config.get(ADMIN_SET_USER_PWD) === EnableDisable.ON
    ) {
      const result = this.passwordPolicy.validatePassword(userData.password);
      if (result.errors.length > 0) {
        throw new BadRequestException({
          errors: result.errors,
          message: i18n.__('Password not secure'),
        });
      }
      // create Password
      authData = {} as AuthData;
      authData.uuid = uuidv4();
      authData.password = this.crypto.hashPassword(userData.password);
      authData.authDataType = AuthDataType.Password;
      authData.entity = USER;
      authData.entityUuid = user.uuid;

      // link password with user
      user.password = authData.uuid;
    }
    user.createdBy = createdBy;
    user.creation = new Date();

    // delete mongodb _id
    user._id = undefined;
    this.apply(new UserAccountAddedEvent(user, authData));
    const claims = [
      {
        scope: SCOPE_SELF_SERVICE,
        name: RAUTH_SOURCE_TYPE,
        uuid: user.uuid,
        value: DB,
      } as UserClaim,
      {
        scope: SCOPE_SELF_SERVICE,
        name: RAUTH_SOURCE_ID,
        uuid: user.uuid,
        value: ADMIN,
      } as UserClaim,
      {
        scope: SCOPE_SELF_SERVICE,
        name: RAUTH_PASSWORD_CHANGE,
        uuid: user.uuid,
        value: new Date(),
      } as UserClaim,
    ];
    if (claims.length > 0) {
      this.apply(new UserClaimsAddedEvent(claims));
    }
    return user;
  }

  async updateUserAccount(
    uuid: string,
    payload: UserAccountDto,
    modifiedBy?: string,
  ) {
    const user = await this.userService.findOne({ uuid });
    if (!user) {
      throw new InvalidUserException();
    }

    await this.validateRoles(payload.roles);

    user.roles = payload.roles;
    user.name = payload.name;

    // Set modification details
    user.modifiedBy = modifiedBy;
    user.modified = new Date();

    // Set password if exists
    if (
      payload.password &&
      this.config.get(ADMIN_SET_USER_PWD) === EnableDisable.ON
    ) {
      const result = this.passwordPolicy.validatePassword(payload.password);
      if (result.errors.length > 0) {
        throw new BadRequestException({
          errors: result.errors,
          message: i18n.__('Password not secure'),
        });
      }

      let authData = await this.authDataService.findOne({
        uuid: user.password,
      });

      if (!authData) {
        authData = {} as AuthData & { _id: any };
        authData.uuid = uuidv4();
      }

      authData.entity = USER;
      authData.entityUuid = user.uuid;
      authData.authDataType = AuthDataType.Password;
      authData.password = this.crypto.hashPassword(payload.password);
      user.password = authData.uuid;
      this.apply(new PasswordChangedEvent(authData));
    }

    this.apply(new UserAccountModifiedEvent(user));
    return user;
  }

  async validateRoles(roles: string[]) {
    // Validate Roles
    const result = await this.roleValidationPolicy.validateRoles(roles);
    if (!result) {
      const validRoles = await this.roleValidationPolicy.getValidRoles(roles);
      throw new BadRequestException({
        message: i18n.__('Invalid Roles'),
        validRoles,
      });
    }
  }

  async addRole(name: string, actorUserUuid: string) {
    const role = { name } as Role;
    const localRole = await this.roleService.findOne({ name });
    if (localRole) throw new BadRequestException({ role });
    role.uuid = uuidv4();
    role.creation = new Date();
    role.createdBy = actorUserUuid;
    this.apply(new UserRoleAddedEvent(role, actorUserUuid));
    return role;
  }

  async modifyRole(uuid: string, name: string, actorUserUuid: string) {
    const role = await this.roleService.findOne({ uuid });
    if (!role) {
      throw new InvalidRoleException();
    }

    this.disallowChangeOfAdminRole(role);

    if (role.name !== name) {
      const existingUsersWithRole = await this.userService.find({
        roles: role.name,
      });

      const existingRole = await this.roleService.findOne({ name });
      if (existingRole) {
        throw new InvalidRoleException();
      }
      if (existingUsersWithRole.length > 0) {
        throw new BadRequestException({
          existingUsersWithRole: existingUsersWithRole.map(user => ({
            email: user.email,
            phone: user.phone,
            roles: user.roles,
            uuid: user.uuid,
          })),
        });
      }
    }

    role.name = name;
    role.modified = new Date();
    role.modifiedBy = actorUserUuid;
    this.apply(new UserRoleModifiedEvent(role, actorUserUuid));
    return role;
  }

  async updateUserFullName(fullName: string, actorUserUuid: string) {
    const user = await this.userService.findOne({ uuid: actorUserUuid });
    if (!user) {
      throw new NotFoundException({ userUuid: actorUserUuid });
    }
    user.name = fullName;
    this.apply(new UserAccountModifiedEvent(user));
    return user;
  }

  async validateExistingUser(userData: User | UserAccountDto) {
    let localUser: User;

    if (userData.email) {
      localUser = await this.userService.findOne({ email: userData.email });
      if (localUser) throw new UserAlreadyExistsException();
    }

    if (userData.phone) {
      localUser = await this.userService.findOne({ phone: userData.phone });
      if (localUser) throw new UserAlreadyExistsException();
    }
  }

  disallowChangeOfAdminRole(role: Role) {
    if (role.name === ADMINISTRATOR) {
      throw new BadRequestException({
        cannotChangeRole: role.name,
      });
    }
  }

  async fetchUserOrThrowException(uuid: string) {
    const user = await this.userService.findOne({ uuid });
    if (!user) throw new InvalidUserException();
    return user;
  }

  async findByEmail(email: string) {
    return await this.userService.findOne({ email });
  }

  async findByPhone(phone: string) {
    return await this.userService.findOne({ phone });
  }

  async clearAuthFailureLogs(uuid: string, actor: string) {
    this.apply(new AuthFailureLogsClearedEvent(uuid, actor));
  }

  async enableOTP(uuid: string, actor: string, enableOTP: boolean) {
    const user = await this.userService.findOne({ uuid });
    const isActorAdmin = await this.userService.checkAdministrator(actor);
    if (!user) {
      throw new InvalidUserException();
    }
    if (user.uuid !== uuid && !isActorAdmin) {
      throw new ForbiddenException();
    }
    this.apply(new UserOTPEnabledEvent(uuid, actor, enableOTP));
  }

  async disableUser(uuid: string, actor: string, disabled: boolean) {
    const user = await this.userService.findOne({ uuid });
    const isActorAdmin = await this.userService.checkAdministrator(actor);
    if (!user) {
      throw new InvalidUserException();
    }
    if (user.uuid !== uuid && !isActorAdmin) {
      throw new ForbiddenException();
    }
    if (user.disabled === true && disabled === true) {
      throw new UserAlreadyDisabledException();
    }
    if (user.disabled === false && disabled === false) {
      throw new UserAlreadyEnabledException();
    }
    this.apply(new UserDisabledEvent(uuid, actor, disabled));
    if (disabled) {
      this.apply(new UserSessionsDeletedEvent(actor, uuid));
      this.apply(new BearerTokensDeletedEvent(actor, null, uuid));
    }
  }

  async sendPasswordExpiryAlert(user: User) {
    this.apply(new PasswordExpiryAlertSentEvent(user));
  }

  async removeExternalConnection(userId: string, clientId: string) {
    const user = await this.userService.findOne({ uuid: userId });
    if (!user) {
      throw new InvalidUserException();
    }
    const client = await this.clientService.findOne({ clientId });
    if (!client) {
      throw new InvalidClientException();
    }

    const tokens = await this.bearerTokenService.find({
      client: clientId,
      user: userId,
    });

    this.apply(new ExternalAppRemovedEvent(clientId, userId, tokens));
  }
}
