import { Injectable } from '@nestjs/common';
import { handlebars } from 'hbs';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  COMPLETE_SIGNUP_MESSAGE,
  COMPLETE_SIGNUP_SUBJECT,
  FORGOT_PASSWORD_SUBJECT,
  FORGOT_ROUTE,
  GENERATE_NEW_PASSWORD_MESSAGE,
  LOGIN_OTP_MESSAGE,
  LOGIN_OTP_SUBJECT,
  PASSWORD_EXPIRY_MESSAGE,
  PASSWORD_EXPIRY_SUBJECT,
  SIGNUP_ROUTE,
  VERIFY_EXISTING_EMAIL_MESSAGE,
  VERIFY_EXISTING_EMAIL_SUBJECT,
  VERIFY_NEW_EMAIL_MESSAGE,
  VERIFY_NEW_EMAIL_SUBJECT,
  VERIFY_ROUTE,
} from '../../../constants/app-strings';
import { SendEmailService } from '../../../email/aggregates/send-email/send-email.service';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { User } from '../../../user-management/entities/user/user.interface';
import { AuthData } from '../../entities/auth-data/auth-data.interface';

@Injectable()
export class EmailRequestService {
  constructor(
    private readonly sendEmail: SendEmailService,
    private readonly settings: ServerSettingsService,
  ) {}

  emailForgottenPasswordVerificationCode(
    user: User,
    verificationCode: AuthData,
  ): Observable<unknown> {
    return from(this.settings.find()).pipe(
      map(settings => {
        const generateForgottenPasswordUrl =
          settings.issuerUrl +
          FORGOT_ROUTE +
          '/' +
          verificationCode.password +
          (verificationCode.metaData?.redirect
            ? '?redirect=' +
              encodeURIComponent(verificationCode.metaData?.redirect)
            : '');
        const htmlMessage = this.renderTemplate(
          settings.forgotPasswordMessage || GENERATE_NEW_PASSWORD_MESSAGE,
          { generateForgottenPasswordUrl },
        );
        const txtMessage = this.htmlToText(htmlMessage);

        return this.sendEmail.sendEmailWithSettings(settings, {
          emailTo: user.email,
          subject: this.renderTemplate(
            settings.forgotPasswordSubject || FORGOT_PASSWORD_SUBJECT,
            { email: user.email },
          ),
          text: txtMessage,
          html: htmlMessage,
        });
      }),
    );
  }

  sendPasswordExpiryAlert(user: User) {
    return from(this.settings.find()).pipe(
      map(settings => {
        return this.sendEmail.sendEmailWithSettings(settings, {
          emailTo: user.email,
          subject: settings.passwordExpirySubject || PASSWORD_EXPIRY_SUBJECT,
          text: settings.passwordExpiryMessage || PASSWORD_EXPIRY_MESSAGE,
          html: settings.passwordExpiryMessage || PASSWORD_EXPIRY_MESSAGE,
        });
      }),
    );
  }

  emailRequest(unverifiedUser: User, verificationCode: AuthData) {
    // Send Email
    return from(this.settings.find()).pipe(
      map(settings => {
        const verificationUrl =
          settings.issuerUrl +
          SIGNUP_ROUTE +
          '/' +
          verificationCode.password +
          (verificationCode.metaData?.redirect
            ? '?redirect=' +
              encodeURIComponent(verificationCode.metaData?.redirect)
            : '');

        const htmlMessage = this.renderTemplate(
          settings.completeSignupMessage || COMPLETE_SIGNUP_MESSAGE,
          { verificationUrl },
        );

        const txtMessage = this.htmlToText(htmlMessage);

        return this.sendEmail.sendEmailWithSettings(settings, {
          emailTo: unverifiedUser.email,
          subject: this.renderTemplate(
            settings.completeSignupSubject || COMPLETE_SIGNUP_SUBJECT,
            { email: unverifiedUser.email },
          ),
          text: txtMessage,
          html: htmlMessage,
        });
      }),
    );
  }

  renderTemplate(template: string, data: unknown) {
    const renderer = handlebars.compile(template);
    return renderer({ data });
  }

  verifyEmail(email: AuthData, code: AuthData) {
    // Send Email
    return from(this.settings.find()).pipe(
      map(settings => {
        const verificationUrl =
          settings.issuerUrl + VERIFY_ROUTE + '/' + code.password;

        const htmlMessage = this.renderTemplate(
          settings.verifyExistingEmailMessage || VERIFY_EXISTING_EMAIL_MESSAGE,
          { verificationUrl },
        );

        const txtMessage = this.htmlToText(htmlMessage);

        return this.sendEmail.sendEmailWithSettings(settings, {
          emailTo: email.password,
          subject: this.renderTemplate(
            settings.verifyExistingEmailSubject ||
              VERIFY_EXISTING_EMAIL_SUBJECT,
            { password: email.password },
          ),
          text: txtMessage,
          html: htmlMessage,
        });
      }),
    );
  }

  sendUnverifiedEmailVerificationCode(
    user: User,
    verificationCode: AuthData,
  ): Observable<unknown> {
    // Send Email
    return from(this.settings.find()).pipe(
      map(settings => {
        const generateForgottenPasswordUrl =
          settings.issuerUrl + VERIFY_ROUTE + '/' + verificationCode.password;

        const htmlMessage = this.renderTemplate(
          settings.verifyExistingEmailMessage || VERIFY_NEW_EMAIL_MESSAGE,
          { generateForgottenPasswordUrl },
        );

        const txtMessage = this.htmlToText(htmlMessage);

        return this.sendEmail.sendEmailWithSettings(settings, {
          emailTo: user.email,
          subject: this.renderTemplate(
            settings.verifyNewEmailSubject || VERIFY_NEW_EMAIL_SUBJECT,
            { email: user.email },
          ),
          text: txtMessage,
          html: htmlMessage,
        });
      }),
    );
  }

  sendOTP(user: User, hotp: string, otp: AuthData) {
    return from(this.settings.find()).pipe(
      map(settings => {
        const htmlMessage = this.renderTemplate(
          settings.loginOtpMessage || LOGIN_OTP_MESSAGE,
          { hotp, email: user.email, expiry: otp.expiry },
        );
        const txtMessage = this.htmlToText(htmlMessage);
        return this.sendEmail.sendEmailWithSettings(settings, {
          emailTo: user.email,
          subject: this.renderTemplate(
            settings.loginOtpSubject || LOGIN_OTP_SUBJECT,
            { email: user.email },
          ),
          text: txtMessage,
          html: htmlMessage,
        });
      }),
    );
  }

  htmlToText(htmlString: string) {
    // https://stackoverflow.com/a/65739861
    return htmlString
      .replace(/\n/gi, '')
      .replace(/<style[^>]*>[\s\S]*?<\/style[^>]*>/gi, '')
      .replace(/<head[^>]*>[\s\S]*?<\/head[^>]*>/gi, '')
      .replace(/<script[^>]*>[\s\S]*?<\/script[^>]*>/gi, '')
      .replace(/<\/\s*(?:p|div)>/gi, '\n')
      .replace(/<br[^>]*\/?>/gi, '\n')
      .replace(/<[^>]*>/gi, '')
      .replace('&nbsp;', ' ')
      .replace(/[^\S\r\n][^\S\r\n]+/gi, ' ');
  }
}
