import { BadRequestException, Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { v4 as uuidv4 } from 'uuid';
import { UserClaimDto } from '../../controllers/user-claim/user-claim.dto';
import { UserService } from '../../entities/user/user.service';
import { UserClaimService } from '../../../auth/entities/user-claim/user-claim.service';
import { UserClaimUpdatedEvent } from '../../events/user-claim-updated/user-claim-updated.event';
import { UserClaimAddedEvent } from '../../events/user-claim-added/user-claim-added.event';
import { UserClaim } from '../../../auth/entities/user-claim/user-claim.interface';
import { UserClaimRemovedEvent } from '../../events/user-claim-removed/user-claim-removed.event';
import { PICTURE, SCOPE_OPENID } from '../../../constants/app-strings';
import { OpenIDClaimsUpdatedEvent } from '../../events/openid-claims-updated/openid-claims-updated.event';
import { OpenIDClaimsDTO } from '../../policies/openid-claims/openid-claims-dto';

@Injectable()
export class UserClaimAggregateService extends AggregateRoot {
  constructor(
    private readonly userClaim: UserClaimService,
    private readonly user: UserService,
  ) {
    super();
  }

  async addUserClaim(payload: UserClaimDto) {
    await this.validateUser(payload.uuid);
    await this.checkIfClaimExists(payload.uuid, payload.name, payload.scope);
    payload.claimId = uuidv4();
    this.apply(new UserClaimAddedEvent(payload as UserClaim));

    return payload;
  }

  async addOrUpdateUserClaim(payload: UserClaimDto) {
    const claim = await this.userClaim.findOne({
      uuid: payload.uuid,
      name: payload.name,
      scope: payload.scope,
    });

    if (claim) {
      return await this.updateUserClaim(payload);
    }

    if (!claim) {
      return await this.addUserClaim(payload);
    }
  }

  async updateUserClaim(payload: UserClaimDto) {
    await this.validateUser(payload.uuid);

    const claim = await this.checkIfClaimDoesNotExists(
      payload.uuid,
      payload.name,
      payload.scope,
      payload.claimId,
    );

    // update claim
    claim.scope = payload.scope;
    claim.value = payload.value;

    this.apply(new UserClaimUpdatedEvent(claim));

    return claim;
  }

  async removeUserClaim(uuid: string, name: string, scope: string) {
    await this.validateUser(uuid);
    const claim = await this.checkIfClaimDoesNotExists(uuid, name, scope);

    this.apply(new UserClaimRemovedEvent(claim));
    return claim;
  }

  async validateUser(uuid) {
    const user = await this.user.findOne({ uuid });
    if (!user) {
      throw new BadRequestException({ uuid });
    }
    return user;
  }

  async checkIfClaimExists(uuid: string, name: string, scope: string) {
    const claim = await this.userClaim.findOne({ uuid, name, scope });
    if (claim) {
      throw new BadRequestException({ claim });
    }
  }

  async fetchClaim(params) {
    return await this.userClaim.findOne(params);
  }

  async checkIfClaimDoesNotExists(
    uuid: string,
    name: string,
    scope: string,
    claimId?: string,
  ) {
    const claim = await this.userClaim.findOne(
      Object.assign({ uuid, name, scope }, claimId ? { claimId } : {}),
    );
    if (!claim) {
      throw new BadRequestException({ claim });
    }
    return claim;
  }

  async updateOpenIDClaims(userUuid: string, claims: OpenIDClaimsDTO) {
    const userClaims = [];
    for (const claim of Object.keys(claims)) {
      const data = await this.userClaim.findOne({
        uuid: userUuid,
        scope: SCOPE_OPENID,
        name: claim,
      });
      if (!data) {
        userClaims.push({
          uuid: userUuid,
          scope: SCOPE_OPENID,
          name: claim,
          claimId: uuidv4(),
          value: claims[claim],
        } as UserClaim);
      } else if (data.name === PICTURE && data.value) {
        // Delete avatar before updating
      } else {
        data.value = claims[claim];
        userClaims.push(data);
      }
    }
    this.apply(new OpenIDClaimsUpdatedEvent(userClaims, claims));
  }
}
