import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiOperation } from '@nestjs/swagger';

import { AddUnverifiedEmailCommand } from '../../../auth/commands/add-unverified-email/add-unverified-phone.command';
import { AddUnverifiedMobileCommand } from '../../../auth/commands/add-unverified-phone/add-unverified-phone.command';
import { SendLoginOTPCommand } from '../../../auth/commands/send-login-otp/send-login-otp.command';
import { VerifyEmailCommand } from '../../../auth/commands/verify-email/verify-email.command';
import { VerifyPhoneCommand } from '../../../auth/commands/verify-phone/verify-phone.command';
import { Scope } from '../../../auth/decorators/scope.decorator';
import { BearerTokenGuard } from '../../../auth/guards/bearer-token.guard';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { ScopeGuard } from '../../../auth/guards/scope.guard';
import { Roles } from '../../../common/decorators/roles.decorator';
import { ListQueryDto } from '../../../common/policies/list-query/list-query';
import {
  ADMINISTRATOR,
  LOGOUT,
  SCOPE_SELF_SERVICE,
} from '../../../constants/app-strings';
import { i18n } from '../../../i18n/i18n.config';
import { AddUserAccountCommand } from '../../commands/add-user-account/add-user-account.command';
import { ChangePasswordCommand } from '../../commands/change-password/change-password.command';
import { ClearAuthFailureLogsCommand } from '../../commands/clear-auth-failure-logs/clear-auth-failure-logs.command';
import { DeleteAvatarCommand } from '../../commands/delete-avatar/delete-avatar.command';
import { Disable2FACommand } from '../../commands/disable-2fa/disable-2fa.command';
import { EmailVerificationCodeCommand } from '../../commands/email-verification-code/email-verification-code.command';
import { EnableOTPCommand } from '../../commands/enable-user-otp/enable-user-otp.command';
import { DisableUserCommand } from '../../commands/disable-user/disable-user.command';
import { GenerateForgottenPasswordCommand } from '../../commands/generate-forgotten-password/generate-forgotten-password.command';
import { Initialize2FACommand } from '../../commands/initialize-2fa/initialize-2fa.command';
import { ModifyUserAccountCommand } from '../../commands/modify-user-account/modify-user-account.command';
import { RemoveUserAccountCommand } from '../../commands/remove-user-account/remove-user-account.command';
import {
  TogglePasswordLessLogin,
  TogglePasswordLessLoginCommand,
} from '../../commands/toggle-password-less-login/toggle-password-less-login.command';
import { UpdateOpenIDClaimsCommand } from '../../commands/update-openid-claims/update-openid-claims.command';
import { UpdateUserFullNameCommand } from '../../commands/update-user-full-name/update-user-full-name.command';
import { UploadAvatarCommand } from '../../commands/upload-avatar/upload-avatar.command';
import { Verify2FACommand } from '../../commands/verify-2fa/verify-2fa.command';
import { VerifyEmailAndSetPasswordCommand } from '../../commands/verify-email-and-set-password/verify-email-and-set-password.command';
import { UserService } from '../../entities/user/user.service';
import {
  ChangePasswordDto,
  UnverifiedEmailDto,
  UnverifiedPhoneDto,
  UserAccountDto,
  VerifyEmailDto,
  VerifyPhoneDto,
  VerifySignupViaPhoneDto,
  VerifyUpdatedEmailDto,
} from '../../policies';
import { OpenIDClaimsDTO } from '../../policies/openid-claims/openid-claims-dto';
import { FetchUserForTrustedClientQuery } from '../../queries/fetch-user-for-trusted-client/fetch-user-for-trusted-client.query';
import { ListSessionUsersQuery } from '../../queries/list-session-users/list-session-users.query';
import { ListUserQuery } from '../../queries/list-user/list-user.query';
import { multerAvatarConnection } from './multer-avatar.connection';
import {
  ToggleProfileOTPCommand,
  ToggleProfileOTP,
} from '../../commands/toggle-profile-otp/toggle-profile-otp.command';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';
import { GetConnectedAppsQuery } from '../../queries/get-connected-apps/get-connected-apps.query';
import { RemoveConnectedAppCommand } from '../../commands/remove-connected-app/remove-connected-app.command';

@Controller('user')
export class UserController {
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly userService: UserService,
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
    private readonly config: ConfigService,
  ) {}

  @Post('v1/change_password')
  @Scope(SCOPE_SELF_SERVICE)
  @UseGuards(BearerTokenGuard, ScopeGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @ApiOperation({
    summary: i18n.__('change password'),
    description: i18n.__('change user password'),
  })
  async updatePassword(@Req() req, @Body() passwordPayload: ChangePasswordDto) {
    const userUuid = req.user.user;
    return await this.commandBus.execute(
      new ChangePasswordCommand(userUuid, passwordPayload),
    );
  }

  @Post('v1/update_full_name')
  @Scope(SCOPE_SELF_SERVICE)
  @UseGuards(BearerTokenGuard, ScopeGuard)
  async updateFullName(@Req() req, @Body('name') name) {
    const actorUserUuid = req.user.user;
    return await this.commandBus.execute(
      new UpdateUserFullNameCommand(actorUserUuid, name),
    );
  }

  @Post('v1/initialize_2fa')
  @UseGuards(BearerTokenGuard)
  async initialize2fa(@Req() req, @Query('restart') restart) {
    return await this.commandBus.execute(
      new Initialize2FACommand(req.user.user, restart || false),
    );
  }

  @Post('v1/verify_2fa')
  @UseGuards(BearerTokenGuard)
  async verify2fa(@Req() req, @Body('otp') otp: string) {
    return await this.commandBus.execute(
      new Verify2FACommand(req.user.user, otp),
    );
  }

  @Post('v1/disable_2fa')
  @UseGuards(BearerTokenGuard)
  async disable2fa(@Req() req) {
    return await this.commandBus.execute(new Disable2FACommand(req.user.user));
  }

  @Get('v1/get_user')
  @UseGuards(BearerTokenGuard)
  async getUser(@Req() req) {
    const actorUserUuid = req.user.user;
    return await this.userService.getAuthorizedUser(actorUserUuid);
  }

  @Post('v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  @ApiOperation({
    summary: i18n.__('Create user'),
    description: 'create user account',
  })
  async create(@Body() payload: UserAccountDto, @Req() req) {
    const createdBy = req.user.user;
    return await this.commandBus.execute(
      new AddUserAccountCommand(createdBy, payload),
    );
  }

  @Post('v1/update/:userUuidToBeModified')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  @ApiOperation({
    summary: i18n.__('Update user account'),
    description: i18n.__('update user account details'),
  })
  async update(
    @Param('userUuidToBeModified') userUuidToBeModified,
    @Body() payload: UserAccountDto,
    @Req() req,
  ) {
    const actorUserUuid = req.user.user;
    return await this.commandBus.execute(
      new ModifyUserAccountCommand(
        actorUserUuid,
        userUuidToBeModified,
        payload,
      ),
    );
  }

  @Post('v1/delete/:userUuidToBeDeleted')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async deleteUser(
    @Param('userUuidToBeDeleted') userUuidToBeDeleted,
    @Req() req,
  ) {
    const actorUserUuid = req.user.user;
    return await this.commandBus.execute(
      new RemoveUserAccountCommand(actorUserUuid, userUuidToBeDeleted),
    );
  }

  @Get('v1/list')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async list(@Query() query: ListQueryDto) {
    return await this.queryBus.execute(
      new ListUserQuery(query.offset, query.limit, query.search, query.sort),
    );
  }

  @Get('v1/get/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async findOne(@Param('uuid') uuid: string, @Req() req) {
    return await this.userService.getAuthorizedUser(uuid);
  }

  @Get('v1/fetch_for_trusted_client/:emailOrPhone')
  @UseGuards(BearerTokenGuard, RoleGuard)
  async findOneForTrustedClient(
    @Param('emailOrPhone') emailOrPhone: string,
    @Req() req,
  ) {
    return await this.queryBus.execute(
      new FetchUserForTrustedClientQuery(req?.token, emailOrPhone),
    );
  }

  @Post('v1/forgot_password')
  async forgotPassword(
    @Body('emailOrPhone') emailOrPhone: string,
    @Body('redirect') redirect?: string,
  ) {
    return await this.commandBus.execute(
      new GenerateForgottenPasswordCommand(emailOrPhone, redirect),
    );
  }

  @Post('v1/generate_password')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @ApiOperation({
    summary: i18n.__('generate password'),
    description: i18n.__('generate user password'),
  })
  async verifyEmailAndGeneratePassword(@Body() payload: VerifyEmailDto) {
    return await this.commandBus.execute(
      new VerifyEmailAndSetPasswordCommand(payload),
    );
  }

  @Post('v1/send_login_otp')
  async sendOTP(@Body('userIdentifier') userIdentifier) {
    const user = await this.userService.findUserByIdentifier(userIdentifier);
    return await this.commandBus.execute(new SendLoginOTPCommand(user));
  }

  @Post('v1/enable_password_less_login')
  @Scope(SCOPE_SELF_SERVICE)
  @UseGuards(BearerTokenGuard, ScopeGuard)
  async enablePasswordLess(@Req() req, @Body('userUuid') userUuid) {
    const actorUserUuid = req.user.user;
    await this.commandBus.execute(
      new TogglePasswordLessLoginCommand(
        actorUserUuid,
        userUuid,
        TogglePasswordLessLogin.Enable,
      ),
    );
  }

  @Post('v1/disable_password_less_login')
  @Scope(SCOPE_SELF_SERVICE)
  @UseGuards(BearerTokenGuard, ScopeGuard)
  async disablePasswordLess(@Req() req, @Body('userUuid') userUuid) {
    const actorUserUuid = req.user.user;
    await this.commandBus.execute(
      new TogglePasswordLessLoginCommand(
        actorUserUuid,
        userUuid,
        TogglePasswordLessLogin.Disable,
      ),
    );
  }

  @Post('v1/enable_profile_otp')
  @Scope(SCOPE_SELF_SERVICE)
  @UseGuards(BearerTokenGuard, ScopeGuard)
  async enableProfileOTP(@Req() req, @Body('userUuid') userUuid) {
    const actorUserUuid = req.user.user;
    await this.commandBus.execute(
      new ToggleProfileOTPCommand(
        actorUserUuid,
        userUuid,
        ToggleProfileOTP.Enable,
      ),
    );
  }

  @Post('v1/disable_profile_otp')
  @Scope(SCOPE_SELF_SERVICE)
  @UseGuards(BearerTokenGuard, ScopeGuard)
  async disableProfileOTP(@Req() req, @Body('userUuid') userUuid) {
    const actorUserUuid = req.user.user;
    await this.commandBus.execute(
      new ToggleProfileOTPCommand(
        actorUserUuid,
        userUuid,
        ToggleProfileOTP.Disable,
      ),
    );
  }

  @Post('v1/delete_me')
  @Scope(SCOPE_SELF_SERVICE)
  @UseGuards(BearerTokenGuard, ScopeGuard)
  async deleteMe(@Req() req) {
    const user = req.user.user;
    await this.commandBus.execute(new RemoveUserAccountCommand(user, user));
    req.logout(() => {
      this.logger.log(LOGOUT, this.constructor.name);
    });
  }

  @Get('v1/list_session_users')
  async listSessionUsers(@Req() req) {
    return await this.queryBus.execute(new ListSessionUsersQuery(req));
  }

  @Post('v1/add_unverified_phone')
  @Scope(SCOPE_SELF_SERVICE)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  @UseGuards(BearerTokenGuard, ScopeGuard)
  @ApiOperation({
    summary: i18n.__('Add unverified phone'),
    description: i18n.__('Add unverified phone for user'),
  })
  async addUnverifiedPhone(@Body() payload: UnverifiedPhoneDto, @Req() req) {
    const userUuid = req.user.user;
    const { unverifiedPhone } = payload;
    return await this.commandBus.execute(
      new AddUnverifiedMobileCommand(userUuid, unverifiedPhone),
    );
  }

  @Post('v1/add_unverified_email')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  @Scope(SCOPE_SELF_SERVICE)
  @UseGuards(BearerTokenGuard, ScopeGuard)
  @ApiOperation({
    summary: i18n.__('add unverified email'),
    description: i18n.__('add unverified email for user'),
  })
  async addUnverifiedEmail(@Body() payload: UnverifiedEmailDto, @Req() req) {
    const userUuid = req.user.user;
    const { unverifiedEmail } = payload;
    return await this.commandBus.execute(
      new AddUnverifiedEmailCommand(userUuid, unverifiedEmail),
    );
  }

  @Post('v1/verify_email')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  @ApiOperation({
    summary: i18n.__('verify email'),
    description: i18n.__('verify email for user'),
  })
  async verifyEmail(@Body() payload: VerifyUpdatedEmailDto) {
    const { verificationCode } = payload;
    return await this.commandBus.execute(
      new VerifyEmailCommand(verificationCode),
    );
  }

  @Post('v1/verify_phone')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  @UseGuards(BearerTokenGuard)
  @ApiOperation({
    summary: i18n.__('verify phone'),
    description: i18n.__('verify phone for user'),
  })
  async verifyPhone(@Body() payload: VerifyPhoneDto, @Req() req) {
    const userUuid = req.user.user;
    const { otp } = payload;
    return await this.commandBus.execute(new VerifyPhoneCommand(userUuid, otp));
  }

  @Post('v1/verify_phone_signup')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @ApiOperation({
    summary: i18n.__('verify signup via phone'),
    description: i18n.__('verify signup via phone for user'),
  })
  async verifySignupByPhone(
    @Body() payload: VerifySignupViaPhoneDto,
    @Req() request,
  ) {
    const user = await this.userService.findOne({
      unverifiedPhone: payload.unverifiedPhone,
    });
    return await this.commandBus.execute(
      new VerifyPhoneCommand(user?.uuid, payload.otp, request),
    );
  }

  @Post('v1/email_verification_code')
  @Scope(SCOPE_SELF_SERVICE)
  @UseGuards(BearerTokenGuard, ScopeGuard)
  @ApiOperation({
    summary: i18n.__('Send email verification'),
    description: i18n.__('Send email verification code'),
  })
  async emailVerificationCode(@Req() req) {
    const userUuid = req?.user?.user;
    return await this.commandBus.execute(
      new EmailVerificationCodeCommand(userUuid),
    );
  }

  @Post('v1/remove_user_account/:uuid')
  @UseGuards(BearerTokenGuard)
  async removeUserAccount(@Req() req, @Param('uuid') userUuidToDelete) {
    const client = req.token.client;
    await this.commandBus.execute(
      new RemoveUserAccountCommand(undefined, userUuidToDelete, client),
    );
    req.logout(() => {
      this.logger.log(LOGOUT, this.constructor.name);
    });
  }

  @Post('v1/update_openid_claims')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @Scope(SCOPE_SELF_SERVICE)
  @UseGuards(BearerTokenGuard, ScopeGuard)
  async updateOpenIDClaims(@Req() req, @Body() claims: OpenIDClaimsDTO) {
    const userUuid = req?.user?.user;
    return await this.commandBus.execute(
      new UpdateOpenIDClaimsCommand(userUuid, claims),
    );
  }

  @Post('v1/clear_auth_failure_logs/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async clearAuthFailureLogs(@Param('uuid') uuid: string, @Req() req) {
    const actor = req?.user?.user;
    return await this.commandBus.execute(
      new ClearAuthFailureLogsCommand(uuid, actor),
    );
  }

  @Post('v1/enable_otp/:uuid')
  @UseGuards(BearerTokenGuard)
  async enableOTP(
    @Param('uuid') uuid: string,
    @Body('enableOTP') enableOTP: boolean,
    @Req() req,
  ) {
    const actor = req?.user?.user;
    return await this.commandBus.execute(
      new EnableOTPCommand(uuid, actor, enableOTP),
    );
  }

  @Post('v1/disable_user/:uuid')
  @UseGuards(BearerTokenGuard)
  async disableUser(
    @Param('uuid') uuid: string,
    @Body('disabled') disabled: boolean,
    @Req() req,
  ) {
    const actor = req?.user?.user;
    return await this.commandBus.execute(
      new DisableUserCommand(uuid, actor, disabled),
    );
  }

  @Post('v1/upload_avatar')
  @Scope(SCOPE_SELF_SERVICE)
  @UseGuards(BearerTokenGuard, ScopeGuard)
  @UseInterceptors(FileInterceptor('file', multerAvatarConnection))
  async uploadAvatar(@Req() req, @UploadedFile('file') uploadedFile) {
    return await this.commandBus.execute(
      new UploadAvatarCommand(uploadedFile, req.user?.user),
    );
  }

  @Post('v1/delete_avatar')
  @Scope(SCOPE_SELF_SERVICE)
  @UseGuards(BearerTokenGuard, ScopeGuard)
  async deleteAvatar(@Req() req) {
    return await this.commandBus.execute(
      new DeleteAvatarCommand(req.user?.user),
    );
  }

  @Get('v1/get_connected_apps')
  @UseGuards(BearerTokenGuard)
  async getConnectedApps(@Req() req) {
    const userId = req.user?.user;
    return await this.queryBus.execute(new GetConnectedAppsQuery(userId));
  }

  @Post('v1/remove_connected_app')
  @UseGuards(BearerTokenGuard)
  async removeConnectedApp(@Req() req, @Body('clientId') clientId: string) {
    const userId = req.user?.user;
    return await this.commandBus.execute(
      new RemoveConnectedAppCommand(userId, clientId),
    );
  }
}
