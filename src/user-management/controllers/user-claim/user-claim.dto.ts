import { IsNotEmpty, IsOptional, IsString, IsUUID } from 'class-validator';
import { i18n } from '../../../i18n/i18n.config';
import { ApiProperty } from '@nestjs/swagger';
export class UserClaimDto {
  @IsUUID('4')
  @IsOptional()
  @ApiProperty({
    description: i18n.__('User Claim ID'),
    required: true,
    type: 'string',
  })
  claimId: string;

  @IsUUID('4')
  @ApiProperty({
    description: i18n.__('User UUID'),
    required: true,
    type: 'string',
  })
  uuid: string;

  @IsString()
  @ApiProperty({
    description: i18n.__('Name of User Claim'),
    required: true,
    type: 'string',
  })
  name: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('Value of User Claim'),
    required: true,
    type: 'string',
  })
  value: unknown | unknown[];

  @IsString()
  @ApiProperty({
    description: i18n.__('Scope for User Claim'),
    required: true,
    type: 'string',
  })
  scope: string;
}
