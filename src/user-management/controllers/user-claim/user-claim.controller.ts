import {
  Body,
  Controller,
  ForbiddenException,
  Get,
  Post,
  Query,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { Request } from 'express';

import { BasicClientCredentialsGuard } from '../../../auth/guards/basic-client-credentials.guard';
import { BearerTokenGuard } from '../../../auth/guards/bearer-token.guard';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { Client } from '../../../client-management/entities/client/client.interface';
import { Roles } from '../../../common/decorators/roles.decorator';
import { ADMINISTRATOR } from '../../../constants/app-strings';
import { AddUserClaimCommand } from '../../commands/add-user-claim/add-user-claim.command';
import { RemoveUserClaimCommand } from '../../commands/remove-user-claim/remove-user-claim.command';
import { UpdateUserClaimCommand } from '../../commands/update-user-claim/update-user-claim.command';
import { GetUserClaimsQuery } from '../../queries/get-user-claims/get-user-claims.query';
import { ListUserClaimsDto } from './list-user-claims.dto';
import { UserClaimDto } from './user-claim.dto';

@Controller('user_claim')
export class UserClaimController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/client/add_user_claim')
  @UseGuards(BasicClientCredentialsGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async addUserClaimHttpAsClient(
    @Body() payload: UserClaimDto,
    @Req() request: Request & { client: Client },
  ) {
    if (!request.client?.isTrusted) {
      throw new ForbiddenException();
    }
    return await this.commandBus.execute(new AddUserClaimCommand(payload));
  }

  @Post('v1/client/update_user_claim')
  @UseGuards(BasicClientCredentialsGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async updateUserClaimHttpAsClient(
    @Body() payload: UserClaimDto,
    @Req() request: Request & { client: Client },
  ) {
    if (!request.client?.isTrusted) {
      throw new ForbiddenException();
    }
    return await this.commandBus.execute(new UpdateUserClaimCommand(payload));
  }

  @Post('v1/client/remove_user_claim')
  @UseGuards(BasicClientCredentialsGuard)
  async removeUserClaimHttpAsClient(
    @Body() payload: { uuid: string; name: string; scope: string },
    @Req() request: Request & { client: Client },
  ) {
    if (!request.client?.isTrusted) {
      throw new ForbiddenException();
    }
    return await this.commandBus.execute(
      new RemoveUserClaimCommand(payload.uuid, payload.name, payload.scope),
    );
  }

  @Get('v1/retrieve_user_claims')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async listUserClaims(@Query() query: ListUserClaimsDto) {
    return await this.queryBus.execute(
      new GetUserClaimsQuery(query.uuid, query.offset, query.limit),
    );
  }

  @Get('v1/get_user_claims')
  @UseGuards(BearerTokenGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async getUserClaims(@Query() query: ListUserClaimsDto, @Req() req) {
    return await this.queryBus.execute(
      new GetUserClaimsQuery(req?.user?.user, query.offset, query.limit),
    );
  }
}
