import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { UploadFilesCloudBucketAggregateService } from '../../../cloud-storage/aggregates';
import {
  AVATAR_KEY,
  PICTURE,
  PUBLIC,
  SCOPE_OPENID,
  SCOPE_SELF_SERVICE,
} from '../../../constants/app-strings';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { UserClaimAggregateService } from '../../aggregates/user-claim-aggregate/user-claim-aggregate.service';
import { UploadAvatarCommand } from './upload-avatar.command';

@CommandHandler(UploadAvatarCommand)
export class UploadAvatarHandler
  implements ICommandHandler<UploadAvatarCommand>
{
  constructor(
    private readonly settings: ServerSettingsService,
    private readonly manager: UploadFilesCloudBucketAggregateService,
    private readonly userClaim: UserClaimAggregateService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: UploadAvatarCommand) {
    const { actor, uploadedFile } = command;
    const settings = await this.settings.find();
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const claimAggregate = this.publisher.mergeObjectContext(this.userClaim);

    const response = await aggregate.uploadFileToCloudBucket(
      uploadedFile,
      settings.avatarBucket,
      actor,
      PUBLIC,
    );
    aggregate.commit();

    await claimAggregate.addOrUpdateUserClaim({
      name: PICTURE,
      value: response.location,
      uuid: actor,
      scope: SCOPE_OPENID,
      claimId: undefined,
    });

    await claimAggregate.addOrUpdateUserClaim({
      name: AVATAR_KEY,
      value: response.key,
      uuid: actor,
      scope: SCOPE_SELF_SERVICE,
      claimId: undefined,
    });
    claimAggregate.commit();

    return response;
  }
}
