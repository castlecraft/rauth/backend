import { ICommand } from '@nestjs/cqrs';
import { MulterFile } from '../../../cloud-storage/aggregates/upload-files-cloud-bucket-aggregate/multer-file.interface';

export class UploadAvatarCommand implements ICommand {
  constructor(
    public readonly uploadedFile: MulterFile,
    public readonly actor: string,
  ) {}
}
