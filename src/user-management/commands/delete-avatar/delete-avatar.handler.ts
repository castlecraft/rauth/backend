import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';

import { UploadFilesCloudBucketAggregateService } from '../../../cloud-storage/aggregates';
import {
  AVATAR_KEY,
  PICTURE,
  SCOPE_OPENID,
  SCOPE_SELF_SERVICE,
} from '../../../constants/app-strings';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { UserClaimAggregateService } from '../../aggregates/user-claim-aggregate/user-claim-aggregate.service';
import { DeleteAvatarCommand } from './delete-avatar.command';

@CommandHandler(DeleteAvatarCommand)
export class DeleteAvatarHandler
  implements ICommandHandler<DeleteAvatarCommand>
{
  constructor(
    private readonly settings: ServerSettingsService,
    private readonly manager: UploadFilesCloudBucketAggregateService,
    private readonly userClaim: UserClaimAggregateService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: DeleteAvatarCommand) {
    const { actor } = command;
    const settings = await this.settings.find();
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const claimAggregate = this.publisher.mergeObjectContext(this.userClaim);
    const avatarKeyClaim = await claimAggregate.fetchClaim({
      scope: SCOPE_SELF_SERVICE,
      uuid: actor,
      name: AVATAR_KEY,
    });

    if (avatarKeyClaim) {
      await aggregate.deleteFileFromBucket(
        avatarKeyClaim.value as string,
        settings.avatarBucket,
        actor,
      );
      await claimAggregate.removeUserClaim(
        actor,
        AVATAR_KEY,
        SCOPE_SELF_SERVICE,
      );
    }
    aggregate.commit();

    await claimAggregate.removeUserClaim(actor, PICTURE, SCOPE_OPENID);
    claimAggregate.commit();

    return { deletedAvatarFor: actor };
  }
}
