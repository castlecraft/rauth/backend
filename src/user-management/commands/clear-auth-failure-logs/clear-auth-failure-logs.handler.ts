import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { ClearAuthFailureLogsCommand } from './clear-auth-failure-logs.command';
import { UserManagementService } from '../../aggregates/user-management/user-management.service';

@CommandHandler(ClearAuthFailureLogsCommand)
export class ClearAuthFailureLogsHandler
  implements ICommandHandler<ClearAuthFailureLogsCommand>
{
  constructor(
    private readonly manager: UserManagementService,
    private readonly eventPublisher: EventPublisher,
  ) {}

  async execute(command: ClearAuthFailureLogsCommand) {
    const { actorUuid, userUuid } = command;
    const aggregate = this.eventPublisher.mergeObjectContext(this.manager);

    await aggregate.clearAuthFailureLogs(userUuid, actorUuid);
    aggregate.commit();
    return { userUuid, cleared: true };
  }
}
