import { ICommand } from '@nestjs/cqrs';

export class EnableOTPCommand implements ICommand {
  constructor(
    public readonly userUuid: string,
    public readonly actorUuid: string,
    public readonly enableOTP: boolean,
  ) {}
}
