import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { EnableOTPCommand } from './enable-user-otp.command';
import { UserManagementService } from '../../aggregates/user-management/user-management.service';

@CommandHandler(EnableOTPCommand)
export class EnableOTPHandler implements ICommandHandler<EnableOTPCommand> {
  constructor(
    private readonly manager: UserManagementService,
    private readonly eventPublisher: EventPublisher,
  ) {}

  async execute(command: EnableOTPCommand) {
    const { actorUuid, userUuid, enableOTP } = command;
    const aggregate = this.eventPublisher.mergeObjectContext(this.manager);
    await aggregate.enableOTP(userUuid, actorUuid, enableOTP);
    aggregate.commit();
    return { userUuid, cleared: true };
  }
}
