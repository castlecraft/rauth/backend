import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveUnverifiedUserCommand } from './remove-unverified-user.command';
import { UserManagementService } from '../../aggregates/user-management/user-management.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@CommandHandler(RemoveUnverifiedUserCommand)
export class RemoveUnverifiedUserCommandHandler
  implements ICommandHandler<RemoveUnverifiedUserCommand>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly manager: UserManagementService,
    private readonly publisher: EventPublisher,
    private readonly config: ConfigService,
  ) {}

  async execute(command: RemoveUnverifiedUserCommand) {
    const { user } = command;

    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.deleteUser(user.uuid).catch(error => {
      this.logger.error(error, error, this.constructor.name);
    });
    aggregate.commit();
  }
}
