import { ICommand } from '@nestjs/cqrs';

export class DisableUserCommand implements ICommand {
  constructor(
    public readonly userUuid: string,
    public readonly actorUuid: string,
    public readonly disabled: boolean,
  ) {}
}
