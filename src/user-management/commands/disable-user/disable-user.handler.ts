import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { DisableUserCommand } from './disable-user.command';
import { UserManagementService } from '../../aggregates/user-management/user-management.service';

@CommandHandler(DisableUserCommand)
export class DisableUserHandler implements ICommandHandler<DisableUserCommand> {
  constructor(
    private readonly manager: UserManagementService,
    private readonly eventPublisher: EventPublisher,
  ) {}

  async execute(command: DisableUserCommand) {
    const { actorUuid, userUuid, disabled } = command;
    const aggregate = this.eventPublisher.mergeObjectContext(this.manager);
    await aggregate.disableUser(userUuid, actorUuid, disabled);
    aggregate.commit();
    return { userUuid, cleared: true };
  }
}
