import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { SendPasswordExpiryAlertCommand } from './send-password-expiry-alert.command';
import { UserManagementService } from '../../aggregates/user-management/user-management.service';

@CommandHandler(SendPasswordExpiryAlertCommand)
export class SendPasswordExpiryAlertHandler
  implements ICommandHandler<SendPasswordExpiryAlertCommand>
{
  constructor(
    private readonly manager: UserManagementService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: SendPasswordExpiryAlertCommand) {
    const { user } = command;

    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.sendPasswordExpiryAlert(user);
    aggregate.commit();
  }
}
