import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';

import { UserAggregateService } from '../../aggregates/user-aggregate/user-aggregate.service';
import {
  ToggleProfileOTP,
  ToggleProfileOTPCommand,
} from './toggle-profile-otp.command';

@CommandHandler(ToggleProfileOTPCommand)
export class ToggleProfileOTPHandler
  implements ICommandHandler<ToggleProfileOTPCommand>
{
  constructor(
    private readonly manager: UserAggregateService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: ToggleProfileOTPCommand) {
    const { actorUuid, userUuid, toggle } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    if (toggle === ToggleProfileOTP.Enable) {
      await aggregate.enableProfileOTP(
        actorUuid,
        userUuid ? userUuid : actorUuid,
      );
      aggregate.commit();
    } else if (toggle === ToggleProfileOTP.Disable) {
      await aggregate.disableProfileOTP(
        actorUuid,
        userUuid ? userUuid : actorUuid,
      );
      aggregate.commit();
    }
  }
}
