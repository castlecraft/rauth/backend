import { ICommand } from '@nestjs/cqrs';

export class ToggleProfileOTPCommand implements ICommand {
  constructor(
    public readonly actorUuid: string,
    public readonly userUuid: string,
    public readonly toggle: ToggleProfileOTP,
  ) {}
}

export enum ToggleProfileOTP {
  Enable = 'Enable',
  Disable = 'Disable',
}
