import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveConnectedAppCommand } from './remove-connected-app.command';
import { UserManagementService } from '../../../user-management/aggregates/user-management/user-management.service';
@CommandHandler(RemoveConnectedAppCommand)
export class RemoveConnectedAppHandler
  implements ICommandHandler<RemoveConnectedAppCommand>
{
  constructor(
    private readonly manager: UserManagementService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: RemoveConnectedAppCommand) {
    const { userId, clientId } = command;

    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.removeExternalConnection(userId, clientId);
    aggregate.commit();
  }
}
