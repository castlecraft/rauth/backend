import { ICommand } from '@nestjs/cqrs';

export class RemoveConnectedAppCommand implements ICommand {
  constructor(
    public readonly userId: string,
    public readonly clientId: string,
  ) {}
}
