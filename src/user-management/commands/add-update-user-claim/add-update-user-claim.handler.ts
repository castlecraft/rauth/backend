import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { UserClaimAggregateService } from '../../aggregates/user-claim-aggregate/user-claim-aggregate.service';
import { AddUpdateUserClaimCommand } from './add-update-user-claim.command';

@CommandHandler(AddUpdateUserClaimCommand)
export class AddUpdateUserClaimHandler
  implements ICommandHandler<AddUpdateUserClaimCommand>
{
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: UserClaimAggregateService,
  ) {}

  async execute(command: AddUpdateUserClaimCommand) {
    const { payload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const response = await aggregate.addOrUpdateUserClaim(payload);
    aggregate.commit();
    return response;
  }
}
