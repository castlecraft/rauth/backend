import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

export const AuthData = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4, unique: true, sparse: true },
    password: String,
    metaData: mongoose.Schema.Types.Mixed,
    entity: String, // Linked Schema
    entityUuid: String, // Linked instance's uuid
    expiry: Date,
    authDataType: String, // enum AuthDataType
    creation: { type: Date, default: Date.now },
  },
  { collection: 'auth_data', versionKey: false },
);

export const AUTH_DATA = 'AuthData';

export const AuthDataModel = mongoose.model(AUTH_DATA, AuthData);
