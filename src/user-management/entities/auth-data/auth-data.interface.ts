import { Document } from 'mongoose';

export interface AuthData extends Document {
  uuid?: string;
  password?: string;
  metaData?: AuthMetaData;
  entity?: string;
  entityUuid?: string;
  expiry?: Date;
  authDataType?: AuthDataType;
  creation?: Date;
}

export interface AuthMetaData {
  [key: string]: string | number | any;
}

export enum AuthDataType {
  Challenge = 'Challenge',
  LoginOTP = 'LoginOTP',
  Password = 'Password',
  PhoneVerificationCode = 'PhoneVerificationCode',
  SharedSecret = 'SharedSecret',
  TwoFactorTempSecret = 'TwoFactorTempSecret',
  VerificationCode = 'VerificationCode',
  UnverifiedEmail = 'UnverifiedEmail',
  MfaToken = 'MfaToken',
}
