import { Inject, Injectable } from '@nestjs/common';
import { isEmail, isUUID } from 'class-validator';
import { Model, RootFilterQuery } from 'mongoose';
import { isMobileE164 } from '../../../common/decorators/is-mobile-e164.decorator';
import { InvalidUserException } from '../../../common/filters/exceptions';
import { ADMINISTRATOR } from '../../../constants/app-strings';
import { i18n } from '../../../i18n/i18n.config';
import { User } from './user.interface';
import { USER } from './user.schema';

@Injectable()
export class UserService {
  constructor(@Inject(USER) public readonly model: Model<User>) {}

  public async save(params) {
    const createdUser = new this.model(params);
    return await createdUser.save();
  }

  public async update(user: User) {
    return await user.save();
  }

  async findAll(): Promise<User[]> {
    return await this.model.find().exec();
  }

  public async findOne(params): Promise<User> {
    return await this.model.findOne(params);
  }

  public async delete(params): Promise<any> {
    await this.model.deleteOne(params);
    return { message: i18n.__('User deleted') };
  }

  public async remove(user: User) {
    return await user.deleteOne();
  }

  public async find(params?) {
    return await this.model.find(params).exec();
  }

  public async deleteByEmail(email): Promise<any> {
    return await this.model.deleteOne({ email });
  }

  async findUserByEmail(email: string) {
    const user = await this.findOne({ email });
    if (!user) {
      throw new InvalidUserException();
    }
    return user;
  }

  async findUserByUuid(uuid: string) {
    const user = await this.findOne({ uuid });
    if (!user) {
      throw new InvalidUserException();
    }
    return user;
  }

  async findUserByPhone(phone: string) {
    const user = await this.findOne({ phone });
    if (!user) {
      throw new InvalidUserException();
    }
    return user;
  }

  /**
   * Find User by email, phone or uuid.
   * @param {string} userIdentifier email, phone or uuid to find the user.
   * @throws {InvalidUserException}
   * @returns {Promise<User>}
   */
  async findUserByIdentifier(userIdentifier: string): Promise<User> {
    if (isEmail(userIdentifier?.trim()?.toLowerCase())) {
      return await this.findUserByEmail(userIdentifier?.trim()?.toLowerCase());
    }
    if (isUUID(userIdentifier)) {
      return await this.findUserByUuid(userIdentifier);
    }
    if (isMobileE164(userIdentifier)) {
      return await this.findUserByPhone(userIdentifier);
    }
  }

  async checkAdministrator(uuid) {
    const user = (await this.findOne({ uuid })) || ({ roles: [] } as User);
    if (user.roles.includes(ADMINISTRATOR)) {
      return true;
    }
    return false;
  }

  async getAuthorizedUser(uuid: string) {
    const user = await this.findOne({ uuid });
    if (!user) {
      throw new InvalidUserException();
    }

    return {
      disabled: user.disabled,
      roles: user.roles,
      enable2fa: user.enable2fa,
      deleted: user.deleted,
      enablePasswordLess: user.enablePasswordLess,
      email: user.email,
      phone: user.phone,
      name: user.name,
      uuid: user.uuid,
      creation: user.creation,
      isPasswordSet: user.password ? true : false,
      enableOTP: user.enableOTP,
    };
  }

  getUserWithoutSecrets(user: User) {
    user.password = undefined;
    user.sharedSecret = undefined;
    user.twoFactorTempSecret = undefined;
    user.otpPeriod = undefined;
    return user;
  }

  getUserWithoutIdentity(user: User) {
    user.name = undefined;
    if (user.email) {
      user.email = this.maskEmail(user.email);
    }
    if (user.phone) {
      user.phone = this.maskPhone(user.phone);
    }
    user.roles = undefined;
    return user;
  }

  getUserWithoutMetaData(user: User) {
    user.deleted = undefined;
    user.creation = undefined;
    user.modified = undefined;
    user.modifiedBy = undefined;
    user._id = undefined;
    return user;
  }

  maskEmail(email: string): string {
    const [localPart, domain] = email.split('@');
    const maskedLocalPart = localPart.slice(0, 2).padEnd(localPart.length, '*');
    return `${maskedLocalPart}@${domain}`;
  }

  maskPhone(phone: string): string {
    const cleanPhone = phone.startsWith('+') ? phone.slice(1) : phone;
    const masked = '*'.repeat(cleanPhone.length - 3) + cleanPhone.slice(-3);
    return masked;
  }

  async list(
    offset: number = 0,
    limit: number = 10,
    search?: string,
    query?: any,
    sortQuery?: any,
  ) {
    if (search) {
      // Search through multiple keys
      // https://stackoverflow.com/a/41390870
      const nameExp = new RegExp(search, 'i');
      query.$or = ['email', 'phone', 'name', 'uuid'].map(field => {
        const out = {};
        out[field] = nameExp;
        return out;
      });
    }

    const data = this.model
      .find(query)
      .skip(Number(offset))
      .limit(Number(limit))
      .sort(sortQuery);

    return {
      docs: await data.exec(),
      length: await this.model.countDocuments(query),
      offset: Number(offset),
    };
  }

  async deleteMany(query: RootFilterQuery<User>): Promise<any> {
    return await this.model.deleteMany(query);
  }

  async updateOne(query, params): Promise<any> {
    return await this.model.updateOne(query, params);
  }
}
