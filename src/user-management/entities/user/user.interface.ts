import { Document } from 'mongoose';
import { IDTokenClaims } from '../../../auth/middlewares/interfaces';

export enum UserStatus {
  Verified = 'Verified',
  Unverified = 'Unverified',
}

export interface User extends Document {
  uuid?: string;
  creation?: Date;
  modified?: Date;
  createdBy?: string;
  modifiedBy?: string;
  disabled?: boolean;
  name?: string;
  phone?: string;
  email?: string;
  password?: string;
  roles?: string[];
  enable2fa?: boolean;
  sharedSecret?: string;
  otpPeriod?: number;
  otpCounter?: string;
  twoFactorTempSecret?: string;
  deleted?: boolean;
  enablePasswordLess?: boolean;
  unverifiedPhone?: string;
  isEmailVerified?: boolean;
  status?: UserStatus;
  enableOTP?: boolean;
  source_type: string;
  source_id: string;
  profile?: IDTokenClaims;
}
