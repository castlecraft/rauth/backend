import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AuthFailureLogsClearedEvent } from './auth-failure-logs-cleared.event';
import { SystemLogService } from '../../../system-settings/entities/system-log/system-log.service';
import {
  SystemLogCode,
  SystemLogType,
} from '../../../system-settings/entities/system-log/system-log.interface';
import { USER } from '../../entities/user/user.schema';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(AuthFailureLogsClearedEvent)
export class AuthFailureLogsClearedHandler
  implements IEventHandler<AuthFailureLogsClearedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly log: SystemLogService,
    private readonly config: ConfigService,
  ) {}
  handle(event: AuthFailureLogsClearedEvent) {
    const { userUuid } = event;
    this.log
      .deleteMany({
        entityId: userUuid,
        entity: USER,
        logType: SystemLogType.Authentication,
        logCode: SystemLogCode.Failure,
      })
      .then(success => {
        this.logger.log(
          `${AuthFailureLogsClearedEvent.name}`,
          this.constructor.name,
        );
      })
      .catch(err => {
        this.logger.error(err, err, this.constructor.name);
      });
  }
}
