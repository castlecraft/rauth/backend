import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AuthDataRemovedEvent } from './auth-data-removed.event';
import { from } from 'rxjs';
import { AuthDataService } from '../../entities/auth-data/auth-data.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(AuthDataRemovedEvent)
export class AuthDataRemovedHandler
  implements IEventHandler<AuthDataRemovedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly authData: AuthDataService,
    private readonly config: ConfigService,
  ) {}

  handle(event: AuthDataRemovedEvent) {
    const { authData } = event;
    from(this.authData.remove(authData)).subscribe({
      next: success => {
        this.logger.log(`${AuthDataRemovedEvent.name}`, this.constructor.name);
        this.logger.debug(
          `${AuthDataRemovedEvent.name}- uuid ${success.uuid} user ${success.entityUuid}`,
          this.constructor.name,
        );
      },
      error: error => {
        this.logger.error(error, error, this.constructor.name);
      },
    });
  }
}
