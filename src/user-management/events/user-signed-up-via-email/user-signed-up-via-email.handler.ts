import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { forkJoin, from } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { UserSignedUpViaEmailEvent } from './user-signed-up-via-email.event';
import { UserService } from '../../entities/user/user.service';
import { AuthDataService } from '../../entities/auth-data/auth-data.service';
import { EmailRequestService } from '../../aggregates/email-request/email-request.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(UserSignedUpViaEmailEvent)
export class UserSignedUpViaEmailHandler
  implements IEventHandler<UserSignedUpViaEmailEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly user: UserService,
    private readonly authData: AuthDataService,
    private readonly email: EmailRequestService,
    private readonly config: ConfigService,
  ) {}

  handle(event: UserSignedUpViaEmailEvent) {
    const { unverifiedUser, verificationCode } = event;

    forkJoin({
      user: from(this.user.save(unverifiedUser)),
      code: from(this.authData.save(verificationCode)),
    })
      .pipe(
        switchMap(({ user, code }) => {
          return this.email.emailRequest(user, code);
        }),
      )
      .subscribe({
        next: success => {
          this.logger.log(
            UserSignedUpViaEmailEvent.name,
            this.constructor.name,
          );
        },
        error: error => {
          this.logger.error(error, error, this.constructor.name);
        },
      });
  }
}
