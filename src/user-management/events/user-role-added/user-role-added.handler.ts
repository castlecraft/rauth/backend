import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { from } from 'rxjs';
import { UserRoleAddedEvent } from './user-role-added.event';
import { RoleService } from '../../entities/role/role.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';

@EventsHandler(UserRoleAddedEvent)
export class UserRoleAddedHandler implements IEventHandler<UserRoleAddedEvent> {
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly role: RoleService,
    private readonly config: ConfigService,
  ) {}

  handle(event: UserRoleAddedEvent) {
    const { role } = event;
    from(this.role.save(role)).subscribe({
      next: success => {
        this.logger.log(UserRoleAddedEvent.name, this.constructor.name);
        this.logger.debug(
          `${UserRoleAddedEvent.name}-${success.uuid}`,
          this.constructor.name,
        );
      },
      error: error => {
        this.logger.error(error, error, this.constructor.name);
      },
    });
  }
}
