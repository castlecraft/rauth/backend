import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { UserOTPEnabledEvent } from './user-otp-enabled.event';
import { UserService } from '../../entities/user/user.service';

@EventsHandler(UserOTPEnabledEvent)
export class UserOTPEnabledHandler
  implements IEventHandler<UserOTPEnabledEvent>
{
  constructor(private readonly user: UserService) {}
  async handle(event: UserOTPEnabledEvent) {
    const { userUuid, enableOTP, actorUuid } = event;
    await this.user.updateOne(
      { uuid: userUuid },
      { $set: { enableOTP, modifiedBy: actorUuid } },
    );
  }
}
