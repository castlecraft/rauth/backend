import { IEvent } from '@nestjs/cqrs';

export class UserOTPEnabledEvent implements IEvent {
  constructor(
    public readonly userUuid: string,
    public readonly actorUuid: string,
    public readonly enableOTP: boolean,
  ) {}
}
