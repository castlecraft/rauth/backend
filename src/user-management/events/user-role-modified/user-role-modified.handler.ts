import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { from } from 'rxjs';
import { UserRoleModifiedEvent } from './user-role-modified.event';
import { RoleService } from '../../entities/role/role.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(UserRoleModifiedEvent)
export class UserRoleModifiedHandler
  implements IEventHandler<UserRoleModifiedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly role: RoleService,
    private readonly config: ConfigService,
  ) {}

  handle(event: UserRoleModifiedEvent) {
    const { role } = event;
    from(this.role.save(role)).subscribe({
      next: success => {
        this.logger.log(UserRoleModifiedEvent.name, this.constructor.name);
        this.logger.debug(
          `${UserRoleModifiedEvent.name}-${success.uuid}`,
          this.constructor.name,
        );
      },
      error: error => {
        this.logger.error(error, error, this.constructor.name);
      },
    });
  }
}
