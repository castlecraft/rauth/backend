import { IEvent } from '@nestjs/cqrs';

export class UserDisabledEvent implements IEvent {
  constructor(
    public readonly userUuid: string,
    public readonly actorUuid: string,
    public readonly disabled: boolean,
  ) {}
}
