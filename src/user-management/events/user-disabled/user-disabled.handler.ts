import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { UserDisabledEvent } from './user-disabled.event';
import { UserService } from '../../entities/user/user.service';

@EventsHandler(UserDisabledEvent)
export class UserDisabledHandler implements IEventHandler<UserDisabledEvent> {
  constructor(private readonly user: UserService) {}
  async handle(event: UserDisabledEvent) {
    const { userUuid, disabled, actorUuid } = event;
    await this.user.updateOne(
      { uuid: userUuid },
      { $set: { disabled, modifiedBy: actorUuid } },
    );
  }
}
