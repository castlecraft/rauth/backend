import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { UserClaimService } from '../../../auth/entities/user-claim/user-claim.service';
import { UserClaimAddedEvent } from './user-claim-added.event';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';

@EventsHandler(UserClaimAddedEvent)
export class UserClaimAddedHandler
  implements IEventHandler<UserClaimAddedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly userClaim: UserClaimService,
    private readonly config: ConfigService,
  ) {}
  handle(event: UserClaimAddedEvent) {
    const { claim } = event;

    this.userClaim
      .save(claim)
      .then(updated => {
        this.logger.log(UserClaimAddedEvent.name, this.constructor.name);
        this.logger.debug(
          `${UserClaimAddedEvent.name}-${updated.uuid}`,
          this.constructor.name,
        );
      })
      .catch(error => {
        this.logger.error(error, error, this.constructor.name);
      });
  }
}
