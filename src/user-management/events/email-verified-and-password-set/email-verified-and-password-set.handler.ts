import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { EmailVerifiedAndPasswordSetEvent } from './email-verified-and-password-set.event';
import { forkJoin, from } from 'rxjs';
import { AuthDataService } from '../../entities/auth-data/auth-data.service';
import { UserService } from '../../entities/user/user.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(EmailVerifiedAndPasswordSetEvent)
export class EmailVerifiedAndPasswordSetHandler
  implements IEventHandler<EmailVerifiedAndPasswordSetEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly user: UserService,
    private readonly authData: AuthDataService,
    private readonly config: ConfigService,
  ) {}
  handle(event: EmailVerifiedAndPasswordSetEvent) {
    const { verifiedUser, userPassword, verificationCode } = event;
    forkJoin({
      user: from(this.user.update(verifiedUser)),
      authData: from(this.authData.save(userPassword)),
      verificationCode: from(this.authData.remove(verificationCode)),
    }).subscribe({
      next: success => {
        this.logger.log(
          `${EmailVerifiedAndPasswordSetEvent.name}`,
          this.constructor.name,
        );
      },
      error: error => {
        this.logger.error(error, error, this.constructor.name);
      },
    });
  }
}
