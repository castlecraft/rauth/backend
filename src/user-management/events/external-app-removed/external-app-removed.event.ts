import { IEvent } from '@nestjs/cqrs';
import { BearerToken } from '../../../auth/entities/bearer-token/bearer-token.interface';

export class ExternalAppRemovedEvent implements IEvent {
  constructor(
    public readonly clientId: string,
    public readonly userId: string,
    public readonly tokens: BearerToken[] = [],
  ) {}
}
