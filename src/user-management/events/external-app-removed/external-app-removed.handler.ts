import { HttpService } from '@nestjs/axios';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { stringify } from 'querystring';
import { EMPTY, filter, from, map, retry, switchMap } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';

import { BearerTokenService } from '../../../auth/entities/bearer-token/bearer-token.service';
import { OIDCKeyService } from '../../../auth/entities/oidc-key/oidc-key.service';
import { ClientService } from '../../../client-management/entities/client/client.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';
import { LOGOUT } from '../../../constants/app-strings';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { ExternalAppRemovedEvent } from './external-app-removed.event';

@EventsHandler(ExternalAppRemovedEvent)
export class ExternalAppRemovedHandler
  implements IEventHandler<ExternalAppRemovedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly http: HttpService,
    private readonly client: ClientService,
    private readonly token: BearerTokenService,
    private readonly settings: ServerSettingsService,
    private readonly oidcKey: OIDCKeyService,
    private readonly config: ConfigService,
  ) {}

  handle(event: ExternalAppRemovedEvent) {
    const { tokens } = event;
    from(tokens)
      .pipe(
        switchMap(token => {
          return from(this.token.remove(token)).pipe(
            switchMap(tokenRemoved => EMPTY),
            switchMap(() => this.signLogoutToken(token.client, token.user)),
            filter(logoutToken => !!logoutToken),
            switchMap(logoutToken =>
              this.informClient(token.client, logoutToken),
            ),
          );
        }),
      )
      .subscribe({
        next: loggedOut => {
          this.logger.log(LOGOUT, this.constructor.name);
        },
        error: error => {
          this.logger.error(error, this.constructor.name);
        },
      });
  }

  informClient(clientId: string, logoutToken: string) {
    return from(this.client.findOne({ clientId })).pipe(
      filter(client => !!client?.tokenDeleteEndpoint),
      switchMap(client => {
        return this.http
          .post(
            client.tokenDeleteEndpoint,
            stringify({ logout_token: logoutToken }),
            {
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
              },
            },
          )
          .pipe(retry(3));
      }),
    );
  }

  signLogoutToken(clientId: string, sub: string) {
    return from(this.settings.find()).pipe(
      map(settings => {
        const issuedAt = Date.parse(new Date().toString()) / 1000;
        const claims = {
          iss: settings.issuerUrl,
          aud: clientId,
          iat: Math.trunc(issuedAt),
          jti: uuidv4(),
          events: {
            'http://schemas.openid.net/event/backchannel-logout': {},
          },
          sub,
        };
        return claims;
      }),
      switchMap(claims => {
        return from(this.oidcKey.signWithLatestKey(JSON.stringify(claims)));
      }),
    );
  }
}
