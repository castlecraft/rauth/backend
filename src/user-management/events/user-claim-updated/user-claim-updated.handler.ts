import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { UserClaimService } from '../../../auth/entities/user-claim/user-claim.service';
import { UserClaimUpdatedEvent } from './user-claim-updated.event';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(UserClaimUpdatedEvent)
export class UserClaimUpdatedHandler
  implements IEventHandler<UserClaimUpdatedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly userClaim: UserClaimService,
    private readonly config: ConfigService,
  ) {}

  handle(event: UserClaimUpdatedEvent) {
    const { claim } = event;

    this.userClaim
      .updateOne(
        { uuid: claim.uuid, name: claim.name },
        { $set: { scope: claim.scope, value: claim.value } },
      )
      .then(updated => {
        this.logger.log(UserClaimUpdatedEvent.name, this.constructor.name);
      })
      .catch(error => {
        this.logger.error(error, error, this.constructor.name);
      });
  }
}
