import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { from } from 'rxjs';
import { PasswordChangedEvent } from './password-changed.event';
import { AuthDataService } from '../../entities/auth-data/auth-data.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';

@EventsHandler(PasswordChangedEvent)
export class PasswordChangedHandler
  implements IEventHandler<PasswordChangedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly authData: AuthDataService,
    private readonly config: ConfigService,
  ) {}
  handle(event: PasswordChangedEvent) {
    const { authData } = event;
    from(this.authData.save(authData)).subscribe({
      next: success => {
        this.logger.log(`${PasswordChangedEvent.name}`, this.constructor.name);
        this.logger.debug(
          `${PasswordChangedEvent.name}- uuid ${success.uuid}`,
          this.constructor.name,
        );
      },
      error: error => {
        this.logger.error(error, error, this.constructor.name);
      },
    });
  }
}
