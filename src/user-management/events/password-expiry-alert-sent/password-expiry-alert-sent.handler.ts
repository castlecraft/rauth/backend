import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { PasswordExpiryAlertSentEvent } from './password-expiry-alert-sent.event';
import { EmailRequestService } from '../../../user-management/aggregates/email-request/email-request.service';

@EventsHandler(PasswordExpiryAlertSentEvent)
export class PasswordExpiryAlertSentHandler
  implements IEventHandler<PasswordExpiryAlertSentEvent>
{
  constructor(private readonly email: EmailRequestService) {}
  handle(event: PasswordExpiryAlertSentEvent) {
    const { user } = event;
    this.email.sendPasswordExpiryAlert(user);
  }
}
