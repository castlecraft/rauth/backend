import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { UserClaimService } from '../../../auth/entities/user-claim/user-claim.service';
import { UserClaimRemovedEvent } from './user-claim-removed.event';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';

@EventsHandler(UserClaimRemovedEvent)
export class UserClaimRemovedHandler
  implements IEventHandler<UserClaimRemovedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly userClaim: UserClaimService,
    private readonly config: ConfigService,
  ) {}
  handle(event: UserClaimRemovedEvent) {
    const { claim } = event;

    this.userClaim
      .deleteOne({ uuid: claim.uuid, name: claim.name })
      .then(updated => {
        this.logger.log(UserClaimRemovedEvent.name, this.constructor.name);
        this.logger.debug(
          `${UserClaimRemovedEvent.name}-${updated.uuid}`,
          this.constructor.name,
        );
      })
      .catch(error => {
        this.logger.error(error, error, this.constructor.name);
      });
  }
}
