import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { ListUserQuery } from './list-user.query';
import { UserService } from '../../entities/user/user.service';

@QueryHandler(ListUserQuery)
export class ListUserHandler implements IQueryHandler {
  constructor(private readonly user: UserService) {}

  async execute(query: ListUserQuery) {
    const { offset, limit, search, sort } = query;
    const where = { deleted: { $eq: false } };
    const sortQuery = { name: sort || 'asc' };
    return await this.user.list(offset, limit, search, where, sortQuery);
  }
}
