import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { GetConnectedAppsQuery } from './get-connected-apps.query';
import { BearerTokenService } from '../../../auth/entities/bearer-token/bearer-token.service';
import { ClientService } from '../../../client-management/entities/client/client.service';
import {
  filter,
  forkJoin,
  from,
  lastValueFrom,
  mergeMap,
  of,
  switchMap,
  toArray,
} from 'rxjs';

@QueryHandler(GetConnectedAppsQuery)
export class GetConnectedAppsHandler implements IQueryHandler {
  constructor(
    private readonly tokenService: BearerTokenService,
    private readonly clientService: ClientService,
  ) {}

  async execute(query: GetConnectedAppsQuery) {
    return lastValueFrom(
      from(
        this.tokenService.distinct('client', {
          user: query.userId,
        }),
      ).pipe(
        filter(clients => !!clients),
        switchMap(clients => from(clients)),
        mergeMap(clientId => from(this.clientService.findOne({ clientId }))),
        filter(client => client && client.isTrusted === 0),
        mergeMap(client => {
          return forkJoin({
            token: from(
              this.tokenService.findOne(
                { client: client.clientId, user: query.userId },
                { creation: 1 },
              ),
            ),
            client: of(client),
          });
        }),
        mergeMap(({ token, client }) =>
          of({
            clientId: client.clientId,
            clientName: client.name,
            creation: token?.creation,
          }),
        ),
        toArray(),
      ),
    );
  }
}
