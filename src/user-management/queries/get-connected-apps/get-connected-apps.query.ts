import { IQuery } from '@nestjs/cqrs';

export class GetConnectedAppsQuery implements IQuery {
  constructor(public readonly userId: string) {}
}
