import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { i18n } from '../../../i18n/i18n.config';

export class VerifyUpdatedEmailDto {
  @IsString()
  @ApiProperty({
    description: i18n.__('verification code'),
    required: true,
    type: 'string',
  })
  verificationCode: string;
}
