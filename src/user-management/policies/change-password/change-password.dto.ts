import { IsString, IsNotEmpty } from 'class-validator';
import { i18n } from '../../../i18n/i18n.config';
import { ApiProperty } from '@nestjs/swagger';

export class ChangePasswordDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('your current password'),
    required: true,
    type: 'string',
  })
  currentPassword: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('your new password'),
    required: true,
    type: 'string',
  })
  newPassword: string;
}
