import { ApiProperty } from '@nestjs/swagger';
import { IsMobileE164 } from '../../../common/decorators/is-mobile-e164.decorator';
import { i18n } from '../../../i18n/i18n.config';

export class UnverifiedPhoneDto {
  @IsMobileE164()
  @ApiProperty({
    description: i18n.__('unverified phone'),
    required: true,
    type: 'string',
  })
  unverifiedPhone: string;
}
