import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { i18n } from '../../../i18n/i18n.config';

export class VerifyPhoneDto {
  @IsString()
  @ApiProperty({
    description: i18n.__('Verification OTP'),
    required: true,
    type: 'string',
  })
  otp: string;
}
