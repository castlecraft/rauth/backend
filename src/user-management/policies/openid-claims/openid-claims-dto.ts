import { IsString, IsDateString, IsOptional, IsUrl } from 'class-validator';

export class OpenIDClaimsDTO {
  @IsString()
  @IsOptional()
  given_name: string;

  @IsString()
  @IsOptional()
  middle_name: string;

  @IsString()
  @IsOptional()
  family_name: string;

  @IsString()
  @IsOptional()
  nickname: string;

  @IsString()
  @IsOptional()
  gender: string;

  @IsDateString()
  @IsOptional()
  birthdate: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  website: string;

  @IsString()
  @IsOptional()
  zoneinfo: string;

  @IsString()
  @IsOptional()
  locale: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  picture: string;
}
