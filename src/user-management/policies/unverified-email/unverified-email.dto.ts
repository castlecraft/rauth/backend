import { IsEmail } from 'class-validator';
import { i18n } from '../../../i18n/i18n.config';
import { ApiProperty } from '@nestjs/swagger';

export class UnverifiedEmailDto {
  @IsEmail()
  @ApiProperty({
    description: i18n.__('unverified email'),
    required: true,
    type: 'string',
  })
  unverifiedEmail: string;
}
