import { IsNotEmpty } from 'class-validator';
import { IsMobileE164 } from '../../../common/decorators/is-mobile-e164.decorator';
import { i18n } from '../../../i18n/i18n.config';
import { ApiProperty } from '@nestjs/swagger';

export class VerifySignupViaPhoneDto {
  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('Verification OTP'),
    required: true,
    type: 'string',
  })
  otp: string;

  @IsMobileE164()
  @ApiProperty({
    description: i18n.__('unverified phone'),
    required: true,
    type: 'string',
  })
  unverifiedPhone: string;
}
