import { IsEmail, IsNotEmpty, IsOptional, IsUrl } from 'class-validator';
import { i18n } from '../../../i18n/i18n.config';
import { ApiProperty } from '@nestjs/swagger';

export class SignupViaEmailDto {
  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('Full name of the user'),
    required: true,
  })
  name: string;

  @IsEmail({}, { message: 'Email must be an email' })
  email: string;
  @IsUrl({ require_tld: false })
  @IsOptional()
  @ApiProperty({
    description: i18n.__(
      'URL to which the user will be redirected after login',
    ),
    required: true,
    type: 'string',
  })
  redirect: string;
}
