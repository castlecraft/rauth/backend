import { IsNotEmpty } from 'class-validator';
import { IsMobileE164 } from '../../../common/decorators/is-mobile-e164.decorator';
import { i18n } from '../../../i18n/i18n.config';
import { ApiProperty } from '@nestjs/swagger';

export class SignupViaPhoneDto {
  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('Full name of the user'),
    required: true,
  })
  name: string;

  @IsMobileE164({ message: 'Phone number must be a valid phone number' })
  @ApiProperty({
    description: i18n.__('unverified phone number'),
    required: true,
    type: 'string',
  })
  unverifiedPhone: string;
}
