import { Injectable, Logger } from '@nestjs/common';
import dotenv from 'dotenv';
import Joi from 'joi';

export interface EnvConfig {
  [prop: string]: string;
}

export const NODE_ENV = 'NODE_ENV';
export const ADMIN_SET_USER_PWD = 'ADMIN_SET_USER_PWD';
export const AUTH_MAX_REQUESTS = 'AUTH_MAX_REQUESTS';
export const AUTH_WINDOW = 'AUTH_WINDOW';
export const COOKIE_MAX_AGE = 'COOKIE_MAX_AGE';
export const DB_HOST = 'DB_HOST';
export const DB_NAME = 'DB_NAME';
export const DB_PASSWORD = 'DB_PASSWORD';
export const DB_USER = 'DB_USER';
export const ENABLE_CORS = 'ENABLE_CORS';
export const ENABLE_CRON = 'ENABLE_CRON';
export const ENABLE_RATE_LIMIT = 'ENABLE_RATE_LIMIT';
export const ENABLE_SWAGGER = 'ENABLE_SWAGGER';
export const EVENTS_CLIENT_ID = 'EVENTS_CLIENT_ID';
export const EVENTS_GROUP = 'EVENTS_GROUP';
export const EVENTS_HEALTHCHECK = 'EVENTS_HEALTHCHECK';
export const EVENTS_HEALTHCHECK_TIMEOUT = 'EVENTS_HEALTHCHECK_TIMEOUT';
export const EVENTS_HOST = 'EVENTS_HOST';
export const EVENTS_PASSWORD = 'EVENTS_PASSWORD';
export const EVENTS_PORT = 'EVENTS_PORT';
export const EVENTS_PROTO = 'EVENTS_PROTO';
export const EVENTS_TOPIC = 'EVENTS_TOPIC';
export const EVENTS_USER = 'EVENTS_USER';
export const LOG_LEVEL = 'LOG_LEVEL';
export const MONGO_OPTIONS = 'MONGO_OPTIONS';
export const MONGO_URI_PREFIX = 'MONGO_URI_PREFIX';
export const PASSWORD_ENCRYPTION_KEY = 'PASSWORD_ENCRYPTION_KEY';
export const PROD_ENV_VALUE = 'PROD_ENV_VALUE';
export const SESSION_NAME = 'SESSION_NAME';
export const SESSION_SECRET = 'SESSION_SECRET';
export const TOKEN_LENGTH = 'TOKEN_LENGTH';
export const TOKEN_VALIDITY = 'TOKEN_VALIDITY';

export enum EnableDisable {
  ON = '1',
  OFF = '0',
}

export enum AllowedEventsProtocol {
  MQTT = 'mqtt',
  TCP = 'tcp',
  KAFKA = 'kafka',
}

export enum TokenLength {
  DEFAULT = 64,
  MINIMUM = 32,
  MAXIMUM = 2048,
}

export const AUTH_MAX_REQUESTS_DEFAULT = '10';
export const AUTH_WINDOW_DEFAULT = '60000';
export const HEALTHCHECK_TIMEOUT_DEFAULT = '10000';
export const LOG_LEVEL_DEFAULT = 'log';
export const MONGO_OPTIONS_DEFAULT = 'retryWrites=true';
export const PROD_ENV_DEFAULT = 'production';

@Injectable()
export class ConfigService {
  private readonly envConfig: EnvConfig;

  constructor() {
    const config = dotenv.config().parsed;
    this.envConfig = this.validateInput(config);
  }

  /**
   * Ensures all needed variables are set, and returns the validated JavaScript object
   * including the applied default values.
   */
  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      [NODE_ENV]: Joi.string().default('development'),
      [ADMIN_SET_USER_PWD]: Joi.string()
        .allow(EnableDisable.ON, EnableDisable.OFF)
        .default(EnableDisable.ON),
      [AUTH_MAX_REQUESTS]: Joi.string()
        .optional()
        .default(AUTH_MAX_REQUESTS_DEFAULT),
      [AUTH_WINDOW]: Joi.string().optional().default(AUTH_WINDOW_DEFAULT),
      [COOKIE_MAX_AGE]: Joi.number().required(),
      [DB_HOST]: Joi.string().required(),
      [DB_NAME]: Joi.string().required(),
      [DB_PASSWORD]: Joi.string().required(),
      [DB_USER]: Joi.string().required(),
      [ENABLE_CORS]: Joi.string()
        .allow(EnableDisable.ON, EnableDisable.OFF)
        .default(EnableDisable.ON),
      [ENABLE_CRON]: Joi.string()
        .allow(EnableDisable.ON, EnableDisable.OFF)
        .default(EnableDisable.ON),
      [ENABLE_RATE_LIMIT]: Joi.string()
        .allow(EnableDisable.ON, EnableDisable.OFF)
        .default(EnableDisable.ON),
      [ENABLE_SWAGGER]: Joi.string()
        .allow(EnableDisable.ON, EnableDisable.OFF)
        .default(EnableDisable.OFF),
      [EVENTS_CLIENT_ID]: Joi.string().optional(),
      [EVENTS_GROUP]: Joi.string().optional(),
      [EVENTS_HEALTHCHECK]: Joi.string()
        .allow(EnableDisable.ON, EnableDisable.OFF)
        .default(EnableDisable.OFF),
      [EVENTS_HEALTHCHECK_TIMEOUT]: Joi.string()
        .default(HEALTHCHECK_TIMEOUT_DEFAULT)
        .min(Number(HEALTHCHECK_TIMEOUT_DEFAULT)),
      [EVENTS_HOST]: Joi.string().optional(),
      [EVENTS_PASSWORD]: Joi.string().optional(),
      [EVENTS_PORT]: Joi.string().optional(),
      [EVENTS_PROTO]: Joi.string()
        .allow(
          AllowedEventsProtocol.TCP,
          AllowedEventsProtocol.MQTT,
          AllowedEventsProtocol.KAFKA,
        )
        .optional(),
      [EVENTS_TOPIC]: Joi.string().optional(),
      [EVENTS_USER]: Joi.string().optional(),
      [LOG_LEVEL]: Joi.string().optional().default(LOG_LEVEL_DEFAULT),
      [MONGO_OPTIONS]: Joi.string().optional().default(MONGO_OPTIONS_DEFAULT),
      [MONGO_URI_PREFIX]: Joi.string().optional(),
      [PASSWORD_ENCRYPTION_KEY]: Joi.string().required(),
      [PROD_ENV_VALUE]: Joi.string().optional().default(PROD_ENV_DEFAULT),
      [SESSION_NAME]: Joi.string().required(),
      [SESSION_SECRET]: Joi.string().required(),
      [TOKEN_LENGTH]: Joi.number()
        .optional()
        .default(TokenLength.DEFAULT)
        .max(TokenLength.MAXIMUM)
        .min(TokenLength.MINIMUM),
      [TOKEN_VALIDITY]: Joi.string().required(),
    });

    const { error, value: validatedEnvConfig } =
      envVarsSchema.validate(envConfig);
    if (error) {
      Logger.error(error, error.stack, this.constructor.name);
      process.exit(1);
    }
    return validatedEnvConfig;
  }

  get(key: string): string {
    switch (key) {
      case DB_NAME:
        return process.env.NODE_ENV === 'test-e2e'
          ? `test_${this.envConfig[key]}`
          : this.envConfig[key];
      default:
        return this.envConfig[key];
    }
  }
}
