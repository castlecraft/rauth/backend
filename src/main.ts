import { APP_PORT, getApp } from './express-server';

async function bootstrap() {
  const app = await getApp();
  await app.listen(APP_PORT);
}

bootstrap();
