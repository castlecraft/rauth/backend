import { Inject, Injectable } from '@nestjs/common';
import { DeleteResult } from 'mongodb';
import { Model } from 'mongoose';
import { CreateEmailDto } from '../../controllers/email/create-email-dto';
import { EmailAccount } from './email-account.interface';
import { EMAIL_ACCOUNT } from './email-account.schema';

@Injectable()
export class EmailAccountService {
  constructor(
    @Inject(EMAIL_ACCOUNT)
    private readonly model: Model<EmailAccount>,
  ) {}

  public async save(params: EmailAccount | CreateEmailDto) {
    const emailAccount = {};
    Object.assign(emailAccount, params);
    return await new this.model(emailAccount).save();
  }

  async findAll(): Promise<EmailAccount[]> {
    return await this.model.find();
  }

  public async findOne(params): Promise<EmailAccount> {
    return await this.model.findOne(params);
  }

  public async delete(params): Promise<DeleteResult> {
    return await this.model.deleteOne(params);
  }

  async list(
    offset: number,
    limit: number,
    search: string,
    query: any,
    sortQuery?: any,
  ) {
    if (search) {
      // Search through multiple keys
      // https://stackoverflow.com/a/41390870
      const nameExp = new RegExp(search, 'i');
      query.$or = ['from', 'host', 'name', 'uuid', 'user'].map(field => {
        const out = {};
        out[field] = nameExp;
        return out;
      });
    }

    const data = this.model
      .find(query, { pass: 0 })
      .skip(Number(offset))
      .limit(Number(limit))
      .sort(sortQuery);

    return {
      docs: await data.exec(),
      length: await this.model.countDocuments(query),
      offset: Number(offset),
    };
  }

  public async find(params) {
    return await this.model.find(params);
  }

  public async updateOne(query, params): Promise<any> {
    return await this.model.updateOne(query, params);
  }
}
