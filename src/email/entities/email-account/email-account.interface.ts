import { Document } from 'mongoose';

export interface EmailAccount extends Document {
  uuid?: string;
  name?: string;
  host?: string;
  user?: string;
  from?: string;
  pass?: string;
  secure?: boolean;
  port?: number;
  pool?: boolean;
  tlsRejectUnauthorized?: boolean;
  oauth2?: boolean; // set to true for OAuth2 Accounts
  owner?: string; // uuid of owner user
  sharedWithUsers?: string[];
  disabled?: boolean;
  creation?: Date;
  createdBy?: string;
  modified?: Date;
  modifiedBy?: string;
  saveLog?: boolean;
}
