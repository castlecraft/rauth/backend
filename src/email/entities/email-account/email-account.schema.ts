import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

const schema = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4, unique: true, sparse: true },
    name: String,
    host: String,
    user: String,
    from: String,
    pass: String,
    secure: Boolean,
    port: Number,
    pool: Boolean,
    tlsRejectUnauthorized: Boolean,
    oauth2: Boolean, // set to true for OAuth2 Accounts
    owner: String, // uuid of owner user
    sharedWithUsers: [String],
    disabled: { type: Boolean, default: false },
    creation: Date,
    createdBy: String,
    modified: Date,
    modifiedBy: String,
    saveLog: { type: Boolean, default: false },
  },
  { collection: 'email_account', versionKey: false },
);

export const EmailAccount = schema;

export const EMAIL_ACCOUNT = 'EmailAccount';

export const EmailAccountModel = mongoose.model(EMAIL_ACCOUNT, EmailAccount);
