import { IEvent } from '@nestjs/cqrs';

export class EmailDeletedEvent implements IEvent {
  constructor(public readonly emailAccountId: string) {}
}
