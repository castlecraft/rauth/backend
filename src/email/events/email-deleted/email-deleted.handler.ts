import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { EmailDeletedEvent } from './email-deleted.event';
import { EmailAccountService } from '../../../email/entities/email-account/email-account.service';

@EventsHandler(EmailDeletedEvent)
export class EmailDeletedHandler implements IEventHandler<EmailDeletedEvent> {
  constructor(private readonly emailAccount: EmailAccountService) {}
  handle(command: EmailDeletedEvent) {
    const { emailAccountId } = command;
    this.emailAccount
      .delete({ uuid: emailAccountId })
      .then(email => {})
      .catch(error => {});
  }
}
