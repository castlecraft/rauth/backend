import { IEvent } from '@nestjs/cqrs';
import { EmailAccount } from '../../../email/entities/email-account/email-account.interface';

export class EmailCreatedEvent implements IEvent {
  constructor(public readonly emailAccount: EmailAccount) {}
}
