import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { EmailCreatedEvent } from './email-created.event';
import { EmailAccountService } from '../../../email/entities/email-account/email-account.service';

@EventsHandler(EmailCreatedEvent)
export class EmailCreatedHandler implements IEventHandler<EmailCreatedEvent> {
  constructor(private readonly emailAccount: EmailAccountService) {}
  handle(command: EmailCreatedEvent) {
    const { emailAccount } = command;
    this.emailAccount
      .save(emailAccount)
      .then(email => {})
      .catch(error => {});
  }
}
