import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { EmailCreatedHandler } from './email-created.handler';
import { EmailAccount } from 'email/entities/email-account/email-account.interface';
import { EmailCreatedEvent } from './email-created.event';
import { EmailAccountService } from '../../../email/entities/email-account/email-account.service';

describe('Event: EmailCreatedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: EmailCreatedHandler;

  let emailAccount: EmailAccountService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        EmailCreatedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: EmailAccountService,
          useValue: () => jest.fn(),
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<EmailCreatedHandler>(EmailCreatedHandler);
    emailAccount = module.get<EmailAccountService>(EmailAccountService);
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should add EmailAccount using EmailAccountService', async () => {
    emailAccount.save = jest.fn((...args) =>
      Promise.resolve({} as EmailAccount & { _id: any }),
    );
    eventBus$.publish = jest.fn(() => {});
    await eventHandler.handle(new EmailCreatedEvent({} as EmailAccount));
    expect(emailAccount.save).toHaveBeenCalledTimes(1);
  });
});
