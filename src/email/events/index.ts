import { EmailCreatedHandler } from './email-created/email-created.handler';
import { EmailDeletedHandler } from './email-deleted/email-deleted.handler';
import { EmailUpdatedHandler } from './email-updated/email-updated.handler';
import { SystemEmailSentHandler } from './system-email-sent/system-email-sent.handler';

export const EmailEventHandlers = [
  SystemEmailSentHandler,
  EmailCreatedHandler,
  EmailDeletedHandler,
  EmailUpdatedHandler,
];
