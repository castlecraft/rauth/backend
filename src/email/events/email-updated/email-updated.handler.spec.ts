import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { EmailUpdatedHandler } from './email-updated.handler';
import { EmailAccountService } from '../../../email/entities/email-account/email-account.service';

describe('Event: EmailCreatedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: EmailUpdatedHandler;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        EmailUpdatedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: EmailAccountService,
          useValue: () => jest.fn(),
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<EmailUpdatedHandler>(EmailUpdatedHandler);
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });
});
