import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { EmailUpdatedEvent } from './email-updated.event';
import { EmailAccountService } from '../../../email/entities/email-account/email-account.service';
import { from } from 'rxjs';

@EventsHandler(EmailUpdatedEvent)
export class EmailUpdatedHandler implements IEventHandler<EmailUpdatedEvent> {
  constructor(public readonly emailAccount: EmailAccountService) {}
  handle(command: EmailUpdatedEvent) {
    const { email } = command;
    from(this.emailAccount.updateOne({ uuid: email.uuid }, email)).subscribe({
      next: success => {},
      error: error => {},
    });
  }
}
