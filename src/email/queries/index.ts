import { GetEmailByUuidHandler } from './get-email-by-uuid/get-email-by-uuid.handler';
import { GetEmailHandler } from './get-email/get-email.handler';
import { ListEmailHandler } from './list-email/list-email.handler';

export const EmailQueryHandlers = [
  ListEmailHandler,
  GetEmailHandler,
  GetEmailByUuidHandler,
];
