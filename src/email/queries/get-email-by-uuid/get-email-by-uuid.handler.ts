import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetEmailByUuidQuery } from './get-email-by-uuid.query';
import { EmailAccountService } from '../../../email/entities/email-account/email-account.service';

@QueryHandler(GetEmailByUuidQuery)
export class GetEmailByUuidHandler implements IQueryHandler {
  constructor(private readonly emailAccount: EmailAccountService) {}
  async execute(query: GetEmailByUuidQuery) {
    const { uuid } = query;
    return await this.emailAccount.findOne({ uuid });
  }
}
