import { IQuery } from '@nestjs/cqrs';

export class GetEmailByUuidQuery implements IQuery {
  constructor(public readonly uuid: string) {}
}
