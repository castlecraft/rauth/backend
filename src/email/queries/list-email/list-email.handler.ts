import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { ListEmailQuery } from './list-email.query';
import { EmailAccountService } from '../../../email/entities/email-account/email-account.service';

@QueryHandler(ListEmailQuery)
export class ListEmailHandler implements IQueryHandler<ListEmailQuery> {
  constructor(private readonly emailAccount: EmailAccountService) {}

  async execute(emailQuery: ListEmailQuery) {
    const { offset, limit, sort, query, search } = emailQuery;
    const skip = Number(offset);
    const take = Number(limit);
    const sortQuery = { name: sort || 'asc' };
    return await this.emailAccount.list(skip, take, search, query, sortQuery);
  }
}
