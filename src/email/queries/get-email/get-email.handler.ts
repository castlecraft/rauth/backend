import { CommandHandler, IQueryHandler } from '@nestjs/cqrs';
import { GetEmailQuery } from './get-email.query';
import { EmailAccountService } from '../../../email/entities/email-account/email-account.service';

@CommandHandler(GetEmailQuery)
export class GetEmailHandler implements IQueryHandler {
  constructor(private readonly emailAccount: EmailAccountService) {}
  async execute(query: GetEmailQuery) {
    return await this.emailAccount.findAll();
  }
}
