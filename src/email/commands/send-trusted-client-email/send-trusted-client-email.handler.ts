import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { SendTrustedClientEmailCommand } from './send-trusted-client-email.command';
import { SendEmailService } from '../../../email/aggregates/send-email/send-email.service';
import { ForbiddenException } from '@nestjs/common';

@CommandHandler(SendTrustedClientEmailCommand)
export class SendTrustedClientEmailHandler
  implements ICommandHandler<SendTrustedClientEmailCommand>
{
  constructor(
    private readonly manager: SendEmailService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: SendTrustedClientEmailCommand) {
    const { payload, token } = command;
    if (!token.trustedClient) {
      throw new ForbiddenException();
    }
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.sendEmail(payload);
    aggregate.commit();
  }
}
