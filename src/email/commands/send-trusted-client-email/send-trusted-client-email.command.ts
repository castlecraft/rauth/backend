import { ICommand } from '@nestjs/cqrs';
import { EmailMessageAuthServerDto } from '../../../email/controllers/email/email-message-authserver-dto';

export class SendTrustedClientEmailCommand implements ICommand {
  constructor(
    public readonly payload: EmailMessageAuthServerDto,
    public readonly token: unknown & { trustedClient?: boolean },
  ) {}
}
