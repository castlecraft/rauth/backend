import { SendSystemEmailHandler } from './send-system-email/send-system-email.handler';
import { CreateEmailHandler } from './create-email/create-email.handler';
import { UpdateEmailHandler } from './update-email/update-email.handler';
import { DeleteEmailHandler } from './delete-email/delete.email.handler';
import { SendTrustedClientEmailHandler } from './send-trusted-client-email/send-trusted-client-email.handler';

export const EmailCommandHandlers = [
  SendSystemEmailHandler,
  SendTrustedClientEmailHandler,
  CreateEmailHandler,
  UpdateEmailHandler,
  DeleteEmailHandler,
];
