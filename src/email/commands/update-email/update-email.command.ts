import { ICommand } from '@nestjs/cqrs';
import { CreateEmailDto } from '../../../email/controllers/email/create-email-dto';

export class UpdateEmailCommand implements ICommand {
  constructor(
    public readonly actorUuid: string,
    public readonly payload: CreateEmailDto,
  ) {}
}
