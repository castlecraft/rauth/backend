import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { UpdateEmailCommand } from './update-email.command';
import { EmailAggregateService } from '../../../email/aggregates/email-aggregate/email-aggregate.service';

@CommandHandler(UpdateEmailCommand)
export class UpdateEmailHandler implements ICommandHandler<UpdateEmailCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: EmailAggregateService,
  ) {}
  async execute(command: UpdateEmailCommand) {
    const { payload, actorUuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const email = await aggregate.updateEmail(payload, actorUuid);
    aggregate.commit();
    return email;
  }
}
