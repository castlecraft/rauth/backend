import { ICommand } from '@nestjs/cqrs';

export class DeleteEmailCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
