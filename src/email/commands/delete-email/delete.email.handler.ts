import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { DeleteEmailCommand } from './delete-email.command';
import { EmailAggregateService } from '../../../email/aggregates/email-aggregate/email-aggregate.service';

@CommandHandler(DeleteEmailCommand)
export class DeleteEmailHandler implements ICommandHandler {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: EmailAggregateService,
  ) {}
  async execute(command: DeleteEmailCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const email = await aggregate.deleteEmailAccount(uuid);
    aggregate.commit();
    return email;
  }
}
