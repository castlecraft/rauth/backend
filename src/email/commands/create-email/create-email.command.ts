import { ICommand } from '@nestjs/cqrs';
import { CreateEmailDto } from '../../../email/controllers/email/create-email-dto';

export class CreateEmailCommand implements ICommand {
  constructor(
    public readonly actorUuid: string,
    public readonly payload: CreateEmailDto,
    public readonly tokenSub: any,
  ) {}
}
