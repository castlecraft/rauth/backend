import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { CreateEmailCommand } from './create-email.command';
import { EmailAggregateService } from '../../../email/aggregates/email-aggregate/email-aggregate.service';

@CommandHandler(CreateEmailCommand)
export class CreateEmailHandler implements ICommandHandler {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: EmailAggregateService,
  ) {}

  async execute(command: CreateEmailCommand) {
    const { actorUuid, payload, tokenSub } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const email = await aggregate.createEmail(payload, actorUuid, tokenSub);
    aggregate.commit();
    return email;
  }
}
