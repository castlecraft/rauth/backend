import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  UseGuards,
  Req,
  Get,
  Query,
  Param,
} from '@nestjs/common';
import { Request } from 'express';
import { EmailMessageAuthServerDto } from './email-message-authserver-dto';
import { CreateEmailDto } from './create-email-dto';
import { ADMINISTRATOR } from '../../../constants/app-strings';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { Roles } from '../../../auth/decorators/roles.decorator';
import { AuthServerVerificationGuard } from '../../../auth/guards/authserver-verification.guard';
import { BearerTokenGuard } from '../../../auth/guards/bearer-token.guard';
import { ApiOperation } from '@nestjs/swagger';
import { i18n } from '../../../i18n/i18n.config';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { SendSystemEmailCommand } from '../../../email/commands/send-system-email/send-system-email.command';
import { CreateEmailCommand } from '../../../email/commands/create-email/create-email.command';
import { UpdateEmailCommand } from '../../../email/commands/update-email/update-email.command';
import { DeleteEmailCommand } from '../../../email/commands/delete-email/delete-email.command';
import { SendTrustedClientEmailCommand } from '../../../email/commands/send-trusted-client-email/send-trusted-client-email.command';
import { GetEmailByUuidQuery } from '../../../email/queries/get-email-by-uuid/get-email-by-uuid.query';
import { GetEmailQuery } from '../../../email/queries/get-email/get-email.query';
import { ListEmailQuery } from '../../../email/queries/list-email/list-email.query';

@Controller('email')
export class EmailController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/system')
  @UseGuards(AuthServerVerificationGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @ApiOperation({
    summary: i18n.__('send system email'),
    description: i18n.__('send system email'),
  })
  async sendSystemEmail(@Body() payload: EmailMessageAuthServerDto) {
    return await this.commandBus.execute(new SendSystemEmailCommand(payload));
  }

  @Post('v1/trusted_client')
  @UseGuards(BearerTokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @ApiOperation({
    summary: i18n.__('send trusted client system email'),
    description: i18n.__('send Trusted Client System Message'),
  })
  async sendTrustedClientEmail(
    @Body() payload: EmailMessageAuthServerDto,
    @Req() request: Request & { token: unknown },
  ) {
    const token = request.token;
    return await this.commandBus.execute(
      new SendTrustedClientEmailCommand(payload, token),
    );
  }

  @Post('v1/create')
  @UseGuards(BearerTokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @ApiOperation({
    summary: i18n.__('Create Email account'),
    description: 'create email service',
  })
  async create(@Req() req, @Body() payload: CreateEmailDto) {
    const tokenSub = req.token.sub;
    const actorUuid = req.user.user;
    return await this.commandBus.execute(
      new CreateEmailCommand(actorUuid, payload, tokenSub),
    );
  }

  @Get('v1/list')
  @UseGuards(BearerTokenGuard)
  async list(
    @Query('offset') offset: number,
    @Query('limit') limit: number,
    @Query('search') search?: string,
    @Query('sort') sort?: string,
  ) {
    const query = {};
    return await this.queryBus.execute(
      new ListEmailQuery(offset, limit, search, query, sort),
    );
  }

  @Post('v1/update')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  @ApiOperation({
    summary: i18n.__('update email account'),
    description: i18n.__('update email account details'),
  })
  async update(@Body() payload: CreateEmailDto, @Req() req) {
    const actorUuid = req.user.user;
    return await this.commandBus.execute(
      new UpdateEmailCommand(actorUuid, payload),
    );
  }

  @Get('v1/find')
  @UseGuards(BearerTokenGuard)
  async findAll() {
    return await this.queryBus.execute(new GetEmailQuery());
  }

  @Get('v1/get/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async findOne(@Param('uuid') uuid: string) {
    const email = await this.queryBus.execute(new GetEmailByUuidQuery(uuid));
    email.pass = undefined;
    return email;
  }

  @Post('v1/delete/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async deleteEmailAccount(@Param('uuid') uuid: string) {
    return await this.commandBus.execute(new DeleteEmailCommand(uuid));
  }
}
