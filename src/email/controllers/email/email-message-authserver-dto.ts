import { IsEmail, IsNotEmpty } from 'class-validator';
import { i18n } from '../../../i18n/i18n.config';
import { ApiProperty } from '@nestjs/swagger';

export class EmailMessageAuthServerDto {
  @IsEmail()
  @ApiProperty({
    description: i18n.__('receiver email'),
    required: true,
    example: 'joey@gmail.com',
  })
  emailTo: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('subject of the email'),
    required: true,
    type: 'string',
  })
  subject: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('text of the email'),
    required: true,
    type: 'string',
  })
  text: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('html for the email'),
    required: true,
    type: 'string',
  })
  html: string;
}
