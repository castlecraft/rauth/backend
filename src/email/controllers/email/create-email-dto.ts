import { ApiProperty } from '@nestjs/swagger';
import {
  IsUrl,
  IsNumberString,
  IsString,
  IsEmail,
  IsOptional,
  IsBoolean,
} from 'class-validator';
import { i18n } from '../../../i18n/i18n.config';

export class CreateEmailDto {
  uuid?: string;
  owner?: string;
  creation?: Date;
  createdBy?: string;

  @IsString()
  @ApiProperty({
    description: i18n.__('Email Account Name'),
    required: true,
    example: 'Mail Hog',
  })
  name: string;

  @IsUrl({ require_protocol: false, require_tld: false })
  @ApiProperty({
    description: i18n.__('Email Server Host'),
    required: true,
    example: '10.20.0.0',
  })
  host: string;

  @IsNumberString()
  @ApiProperty({
    description: i18n.__('Email Server Port'),
    required: true,
    example: '587',
  })
  port: number;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: i18n.__('Email Account User'),
    required: true,
    type: 'string',
    example: 'email_relay',
  })
  user: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: i18n.__('Email Account Password'),
    required: true,
    example: 'A@123678',
  })
  pass: string;

  @IsEmail()
  @ApiProperty({
    description: i18n.__('Email Account sender email'),
    required: true,
    example: 'john@example.com',
  })
  from: string;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({
    description: i18n.__('Email Account needs encryption or not'),
    required: true,
    example: true,
  })
  secure?: boolean;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({
    description: i18n.__('Disable Email Account'),
  })
  disabled?: boolean;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({
    description: i18n.__('Log success or failure'),
  })
  saveLog?: boolean;
}
