import {
  BadRequestException,
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { EmailAccountService } from '../../../email/entities/email-account/email-account.service';
import { EmailMessageAuthServerDto } from './email-message-authserver-dto';
import { SendSystemEmailCommand } from '../../../email/commands/send-system-email/send-system-email.command';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { CreateEmailDto } from './create-email-dto';

@Injectable()
export class EmailService {
  constructor(
    private readonly emailAccount: EmailAccountService,
    private readonly settings: ServerSettingsService,
    private readonly commandBus: CommandBus,
  ) {}

  async findOne(params) {
    return await this.emailAccount.findOne(params);
  }

  async sendSystemMessage(payload: EmailMessageAuthServerDto) {
    return await this.commandBus.execute(new SendSystemEmailCommand(payload));
  }

  async sendTrustedClientSystemMessage(
    payload: EmailMessageAuthServerDto,
    token: unknown & { trustedClient?: boolean },
  ) {
    if (!token.trustedClient) {
      throw new ForbiddenException();
    }

    return await this.commandBus.execute(new SendSystemEmailCommand(payload));
  }

  async deleteEmailAccount(uuid: string) {
    const email = await this.emailAccount.findOne({ uuid });
    if (!email) {
      throw new NotFoundException({ EmailAccountNotFound: uuid });
    }
    const settings = await this.settings.find();
    if (settings.systemEmailAccount === email.uuid) {
      throw new BadRequestException({ SystemEmailAccount: uuid });
    }
    await this.emailAccount.delete({ uuid });
    return { deleted: uuid };
  }

  async createEmail(req, payload: CreateEmailDto) {
    payload.owner = req.token.sub;
    payload.creation = new Date();
    payload.createdBy = req.user.user;
    const emailAccount = await this.emailAccount.save(payload);
    delete emailAccount._id;
    return emailAccount;
  }

  async updateEmail(payload: CreateEmailDto, actorUuid: string) {
    const email = await this.emailAccount.findOne({
      uuid: payload.uuid,
    });
    email.name = payload.name;
    email.host = payload.host;
    email.port = payload.port;
    email.from = payload.from;
    email.secure = payload.secure;
    email.disabled = payload.disabled;
    email.modified = new Date();
    email.modifiedBy = actorUuid;
    if (payload.user) {
      email.user = payload.user;
    }

    if (payload.pass) {
      email.pass = payload.pass;
    }
    await email.save();
    return { uuid: email.uuid };
  }

  async findAll() {
    return await this.emailAccount.findAll();
  }

  async list(
    offset: number,
    limit: number,
    search?: string,
    query?: unknown,
    sort?: string,
  ) {
    const skip = Number(offset);
    const take = Number(limit);
    const sortQuery = { name: sort || 'asc' };
    return await this.emailAccount.list(skip, take, search, query, sortQuery);
  }
}
