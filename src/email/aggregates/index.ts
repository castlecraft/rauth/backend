import { SendEmailService } from './send-email/send-email.service';
import { EmailAggregateService } from './email-aggregate/email-aggregate.service';

export const EmailAggregates = [SendEmailService, EmailAggregateService];
