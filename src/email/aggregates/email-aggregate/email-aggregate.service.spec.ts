import { Test, TestingModule } from '@nestjs/testing';
import { EmailAggregateService } from './email-aggregate.service';

describe('EmailAggregateService', () => {
  let service: EmailAggregateService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EmailAggregateService],
    })
      .overrideProvider(EmailAggregateService)
      .useFactory({ factory: (...args) => jest.fn() })
      .compile();
    service = module.get<EmailAggregateService>(EmailAggregateService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
