import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { v4 as uuidv4 } from 'uuid';
import { CreateEmailDto } from '../../../email/controllers/email/create-email-dto';
import { EmailAccount } from '../../../email/entities/email-account/email-account.interface';
import { EmailAccountService } from '../../../email/entities/email-account/email-account.service';
import { EmailCreatedEvent } from '../../../email/events/email-created/email-created.event';
import { EmailDeletedEvent } from '../../../email/events/email-deleted/email-deleted.event';
import { EmailUpdatedEvent } from '../../../email/events/email-updated/email-updated.event';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { PasswordCryptoService } from '../../../auth/policies/password-crypto/password-crypto.service';

@Injectable()
export class EmailAggregateService extends AggregateRoot {
  constructor(
    private readonly emailAccount: EmailAccountService,
    private readonly settings: ServerSettingsService,
    private readonly passwordCryptoService: PasswordCryptoService,
  ) {
    super();
  }

  async createEmail(payload: CreateEmailDto, actorUuid: string, tokenSub: any) {
    const params = Object.assign(
      {
        uuid: uuidv4(),
        owner: tokenSub,
        creation: new Date(),
        createdBy: actorUuid,
      },
      payload,
    );
    params.pass = undefined;

    if (payload.pass) {
      params.pass = this.passwordCryptoService.encrypt(payload.pass);
    }
    this.apply(new EmailCreatedEvent(params as EmailAccount));
    return params;
  }

  async deleteEmailAccount(uuid: string) {
    const email = await this.emailAccount.findOne({ uuid });
    if (!email) {
      throw new NotFoundException({ EmailAccountNotFound: uuid });
    }
    const settings = await this.settings.find();
    if (settings.systemEmailAccount === email.uuid) {
      throw new BadRequestException({ SystemEmailAccount: uuid });
    }
    this.apply(new EmailDeletedEvent(uuid));
    return { deleted: uuid };
  }

  async updateEmail(payload: CreateEmailDto, actorUuid: string) {
    const email = await this.emailAccount.findOne({
      uuid: payload.uuid,
    });
    if (!email) {
      throw new NotFoundException({ EmailAccountNotFound: payload.uuid });
    }
    email.name = payload.name;
    email.host = payload.host;
    email.port = payload.port;
    email.from = payload.from;
    email.secure = payload.secure;
    email.saveLog = payload.saveLog;
    email.disabled = payload.disabled;
    email.modified = new Date();
    email.modifiedBy = actorUuid;
    if (payload.user) {
      email.user = payload.user;
    }

    if (payload.pass) {
      email.pass = this.passwordCryptoService.encrypt(payload.pass);
    }
    this.apply(new EmailUpdatedEvent(email as EmailAccount));
    return { uuid: email.uuid };
  }
}
