import { Injectable, BadRequestException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as nodemailer from 'nodemailer';
import { EmailAccountService } from '../../entities/email-account/email-account.service';
import { SystemLogService } from '../../../system-settings/entities/system-log/system-log.service';
import { SEND_EMAIL } from '../../../constants/app-strings';
import { SystemEmailSentEvent } from '../../events/system-email-sent/system-email-sent.event';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { EmailMessageAuthServerDto } from '../../controllers/email/email-message-authserver-dto';
import { EmailAccount } from '../../entities/email-account/email-account.interface';
import { ServerSettings } from '../../../system-settings/entities/server-settings/server-settings.interface';
import {
  SystemLogCode,
  SystemLogType,
} from '../../../system-settings/entities/system-log/system-log.interface';
import { PasswordCryptoService } from '../../../auth/policies/password-crypto/password-crypto.service';

@Injectable()
export class SendEmailService extends AggregateRoot {
  constructor(
    private readonly emailAccount: EmailAccountService,
    private readonly systemLogService: SystemLogService,
    private readonly settingsService: ServerSettingsService,
    private readonly passwordCryptoService: PasswordCryptoService,
  ) {
    super();
  }

  async processMessage(
    payload: EmailMessageAuthServerDto,
    emailAccount: EmailAccount,
  ) {
    if (!emailAccount.disabled) {
      const message = {
        from: emailAccount.from,
        to: payload.emailTo,
        subject: payload.subject,
        text: payload.text,
        html: payload.html,
      };
      const transport = await this.getSMTPTransport(emailAccount);
      transport
        .sendMail(message)
        .then(success => {
          if (emailAccount.saveLog) {
            return this.systemLogService.save({
              data: {
                message,
                success,
                senderType: SEND_EMAIL,
                senderUuid: emailAccount.uuid,
              },
              logType: SystemLogType.Email,
              logCode: SystemLogCode.Success,
            });
          }
        })
        .catch(error => {
          if (emailAccount.saveLog) {
            return this.systemLogService.save({
              data: {
                message,
                error,
                senderType: SEND_EMAIL,
                senderUuid: emailAccount.uuid,
              },
              logType: SystemLogType.Email,
              logCode: SystemLogCode.Failure,
            });
          }
        });
    }
  }

  async getSMTPTransport(emailAccount: EmailAccount) {
    return nodemailer.createTransport({
      host: emailAccount.host,
      port: emailAccount.port,
      auth: {
        user: emailAccount.user,
        pass: emailAccount.pass
          ? this.passwordCryptoService.decrypt(emailAccount.pass)
          : emailAccount.pass,
      },
    });
  }

  async sendEmail(payload) {
    const settings = await this.settingsService.find();
    if (!settings.systemEmailAccount) {
      throw new BadRequestException({
        systemEmailAccount: 'NOT_SET',
      });
    }
    const emailAccountUuid = settings.systemEmailAccount;
    const emailAccount = await this.emailAccount.findOne({
      uuid: emailAccountUuid,
    });

    this.apply(new SystemEmailSentEvent(payload, emailAccount));
  }

  sendEmailWithSettings(settings: ServerSettings, payload) {
    if (!settings.systemEmailAccount) {
      throw new BadRequestException({
        systemEmailAccount: 'NOT_SET',
      });
    }
    const emailAccountUuid = settings.systemEmailAccount;
    this.emailAccount
      .findOne({
        uuid: emailAccountUuid,
      })
      .then(emailAccount => {
        if (emailAccount) {
          return this.processMessage(payload, emailAccount);
        }
      })
      .then(success => {})
      .catch(error => {});
  }
}
