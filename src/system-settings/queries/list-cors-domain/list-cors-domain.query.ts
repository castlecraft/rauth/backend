import { IQuery } from '@nestjs/cqrs';

export class ListCORSDomainQuery implements IQuery {
  constructor(
    public readonly offset: number = 0,
    public readonly limit: number = 10,
    public readonly search?: string,
    public readonly query?: unknown,
    public readonly sort?: string,
  ) {}
}
