import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { ListCORSDomainQuery } from './list-cors-domain.query';
import { CORSDomainService } from '../../entities/cors-domain/cors-domain.service';

@QueryHandler(ListCORSDomainQuery)
export class ListCORSDomainHandler
  implements IQueryHandler<ListCORSDomainQuery>
{
  constructor(private readonly CORSDomainService: CORSDomainService) {}

  async execute(emailQuery: ListCORSDomainQuery) {
    const { offset, limit, sort, query, search } = emailQuery;
    const skip = Number(offset);
    const take = Number(limit);
    const sortQuery = { name: sort || 'asc' };
    return await this.CORSDomainService.list(
      skip,
      take,
      search,
      query,
      sortQuery,
    );
  }
}
