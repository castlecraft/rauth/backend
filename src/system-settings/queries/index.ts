import { GetCORSDomainByUuidHandler } from './get-cors-domain-by-uuid/get-cors-domain-by-uuid.handler';
import { GetCORSDomainHandler } from './get-cors-domain/get-cors-domain.handler';
import { ListCORSDomainHandler } from './list-cors-domain/list-cors-domain.handler';

export const SystemSettingsQueryHandlers = [
  ListCORSDomainHandler,
  GetCORSDomainHandler,
  GetCORSDomainByUuidHandler,
];
