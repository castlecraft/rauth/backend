import { CommandHandler, IQueryHandler } from '@nestjs/cqrs';
import { GetCORSDomainQuery } from './get-cors-domain.query';
import { CORSDomainService } from '../../entities/cors-domain/cors-domain.service';

@CommandHandler(GetCORSDomainQuery)
export class GetCORSDomainHandler implements IQueryHandler {
  constructor(private readonly CORSDomainService: CORSDomainService) {}
  async execute(query: GetCORSDomainQuery) {
    return await this.CORSDomainService.findAll();
  }
}
