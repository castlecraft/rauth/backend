import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetCORSDomainByUuidQuery } from './get-cors-domain.query';
import { CORSDomainService } from '../../entities/cors-domain/cors-domain.service';

@QueryHandler(GetCORSDomainByUuidQuery)
export class GetCORSDomainByUuidHandler implements IQueryHandler {
  constructor(private readonly CORSDomainService: CORSDomainService) {}
  async execute(query: GetCORSDomainByUuidQuery) {
    const { uuid } = query;
    return await this.CORSDomainService.findOne({ uuid });
  }
}
