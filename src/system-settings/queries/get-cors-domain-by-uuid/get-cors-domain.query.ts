import { IQuery } from '@nestjs/cqrs';

export class GetCORSDomainByUuidQuery implements IQuery {
  constructor(public readonly uuid: string) {}
}
