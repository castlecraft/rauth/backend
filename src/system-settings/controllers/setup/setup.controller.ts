import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { SetupServerCommand } from '../../commands/setup-server/setup-server.command';
import { SetupFormDTO } from './setup-form-dto';
import { ApiOperation } from '@nestjs/swagger';
import { i18n } from '../../../i18n/i18n.config';

@Controller('setup')
export class SetupController {
  constructor(private readonly commandBus: CommandBus) {}

  @Post()
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  @ApiOperation({
    summary: i18n.__('Server Setup'),
    description: i18n.__('Setup Server using this run once endpoint'),
  })
  async setupInfrastructure(@Body() setupForm: SetupFormDTO) {
    return await this.commandBus.execute(new SetupServerCommand(setupForm));
  }
}
