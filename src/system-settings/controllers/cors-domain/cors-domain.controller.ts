import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ApiOperation } from '@nestjs/swagger';
import { BearerTokenGuard } from '../../../auth/guards/bearer-token.guard';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { Roles } from '../../../auth/decorators/roles.decorator';
import { ADMINISTRATOR } from '../../../constants/app-strings';
import { i18n } from '../../../i18n/i18n.config';
import { AddCORSDomainCommand } from '../../commands/add-cors-domain/add-cors-domain.command';
import { RemoveCORSDomainCommand } from '../../commands/remove-cors-domain/remove-cors-domain.command';
import { UpdateCORSDomainCommand } from '../../commands/update-cors-domain/update-cors-domain.command';
import { CreateCORSDomainDto } from '../../entities/cors-domain/create-cors-domain-dto';
import { GetCORSDomainByUuidQuery } from '../../queries/get-cors-domain-by-uuid/get-cors-domain.query';
import { GetCORSDomainQuery } from '../../queries/get-cors-domain/get-cors-domain.query';
import { ListCORSDomainQuery } from '../../queries/list-cors-domain/list-cors-domain.query';

@Controller('cors_domain')
export class CORSDomainController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Get('v1/list')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async list(
    @Query('offset') offset: number,
    @Query('limit') limit: number,
    @Query('search') search?: string,
    @Query('sort') sort?: string,
  ) {
    const query = {};
    return await this.queryBus.execute(
      new ListCORSDomainQuery(offset, limit, search, query, sort),
    );
  }

  @Get('v1/find')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async findAll() {
    return await this.queryBus.execute(new GetCORSDomainQuery());
  }

  @Get('v1/get/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async findOne(@Param('uuid') uuid: string) {
    const corsDomains = await this.queryBus.execute(
      new GetCORSDomainByUuidQuery(uuid),
    );
    corsDomains.pass = undefined;
    return corsDomains;
  }

  @Post('v1/add')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @ApiOperation({
    summary: i18n.__('Add CORS Domain'),
    description: 'Add a CORS Domain',
  })
  async create(@Req() req, @Body() payload: CreateCORSDomainDto) {
    const actorUuid = req.user.user;
    return await this.commandBus.execute(
      new AddCORSDomainCommand(actorUuid, payload),
    );
  }

  @Post('v1/update/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @ApiOperation({
    summary: i18n.__('Update CORS Domain'),
    description: i18n.__('Update a CORS Domain'),
  })
  async update(
    @Param('uuid') uuid,
    @Body() payload: CreateCORSDomainDto,
    @Req() req,
  ) {
    const actorUuid = req.user.user;
    return await this.commandBus.execute(
      new UpdateCORSDomainCommand(actorUuid, uuid, payload),
    );
  }

  @Post('v1/remove/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async delete(@Param('uuid') uuid: string, @Req() req) {
    const actorUuid = req.user.user;
    return await this.commandBus.execute(
      new RemoveCORSDomainCommand(actorUuid, uuid),
    );
  }
}
