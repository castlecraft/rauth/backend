import { HttpService } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { CORSDomainController } from './cors-domain.controller';
import { BearerTokenGuard } from '../../../auth/guards/bearer-token.guard';
import { AuthServerVerificationGuard } from '../../../auth/guards/authserver-verification.guard';
import { BearerTokenService } from '../../../auth/entities/bearer-token/bearer-token.service';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { UserService } from '../../../user-management/entities/user/user.service';

describe('CorsDomainController', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [CORSDomainController],
      providers: [
        {
          provide: CommandBus,
          useFactory: (...args) => jest.fn(),
        },
        {
          provide: QueryBus,
          useFactory: (...args) => jest.fn(),
        },
        {
          provide: UserService,
          useFactory: (...args) => jest.fn(),
        },
        {
          provide: AuthServerVerificationGuard,
          useValue: {},
        },
        {
          provide: BearerTokenService,
          useValue: {},
        },
        {
          provide: HttpService,
          useFactory: (...args) => jest.fn(),
        },
      ],
    })
      .overrideGuard(BearerTokenGuard)
      .useValue({})
      .overrideGuard(AuthServerVerificationGuard)
      .useValue({})
      .compile();
  });
  it('should be defined', () => {
    const controller: CORSDomainController =
      module.get<CORSDomainController>(CORSDomainController);
    expect(controller).toBeDefined();
  });
});
