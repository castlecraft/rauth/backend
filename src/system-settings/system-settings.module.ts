import { Module, Global } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { TerminusModule } from '@nestjs/terminus';
import { SystemSettingsEntitiesModule } from './entities/entities.module';
import { SetupService } from './controllers/setup/setup.service';
import { SetupController } from './controllers/setup/setup.controller';
import { ServerSettingsController } from './controllers/server-settings/server-settings.controller';
import { CORSDomainController } from './controllers/cors-domain/cors-domain.controller';
import { SystemSettingsManagementService } from './aggregates/system-settings-management/system-settings-management.service';
import { SystemSettingsCommandHandlers } from './commands';
import { SystemSettingsEventHandlers } from './events';
import { AuthEntitiesModule } from '../auth/entities/entities.module';
import { HealthController } from './controllers/health/health.controller';
import { HealthCheckAggregateService } from './aggregates/health-check/health-check.service';
import { DatabaseHealthIndicatorService } from './aggregates/database-health-indicator/database-health-indicator.service';
import { SystemSettingsQueryHandlers } from './queries';
import { CORSDomainAggregateService } from './aggregates/cors-domain/cors-domain.aggregate.service';

@Global()
@Module({
  imports: [
    SystemSettingsEntitiesModule,
    AuthEntitiesModule,
    CqrsModule,
    TerminusModule,
  ],
  exports: [SystemSettingsEntitiesModule],
  providers: [
    SetupService,
    HealthCheckAggregateService,
    SystemSettingsManagementService,
    CORSDomainAggregateService,
    ...SystemSettingsCommandHandlers,
    ...SystemSettingsEventHandlers,
    ...SystemSettingsQueryHandlers,
    DatabaseHealthIndicatorService,
  ],
  controllers: [
    SetupController,
    ServerSettingsController,
    HealthController,
    CORSDomainController,
  ],
})
export class SystemSettingsModule {}
