import { ICommand } from '@nestjs/cqrs';
import { SystemLog } from '../../entities/system-log/system-log.interface';

export class AppendSystemLogCommand implements ICommand {
  constructor(
    public readonly log: SystemLog,
    public readonly saveLog: boolean,
  ) {}
}
