import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { AppendSystemLogCommand } from './append-system-log.command';
import { SystemSettingsManagementService } from '../../aggregates';

@CommandHandler(AppendSystemLogCommand)
export class AppendSystemLogHandler
  implements ICommandHandler<AppendSystemLogCommand>
{
  constructor(
    private readonly manager: SystemSettingsManagementService,
    private readonly publisher: EventPublisher,
  ) {}
  async execute(command: AppendSystemLogCommand) {
    const { log, saveLog } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    aggregate.appendSystemLog(log, saveLog);
    aggregate.commit();
    return log;
  }
}
