import { AppendSystemLogHandler } from './append-system-log/append-system-log.handler';
import { ChangeServerSettingsHandler } from './change-server-settings/change-server-settings.handler';
import { DeleteBearerTokensHandler } from './delete-bearer-tokens/delete-bearer-tokens.handler';
import { DeleteUserSessionsHandler } from './delete-user-sessions/delete-user-sessions.handler';
import { SetupServerHandler } from './setup-server/setup-server.handler';
import { AddCORSDomainHandler } from './add-cors-domain/add-cors-domain.handler';
import { RemoveCORSDomainHandler } from './remove-cors-domain/remove-cors-domain.handler';
import { UpdateCORSDomainHandler } from './update-cors-domain/update-cors-domain.handler';

export const SystemSettingsCommandHandlers = [
  ChangeServerSettingsHandler,
  DeleteBearerTokensHandler,
  DeleteUserSessionsHandler,
  SetupServerHandler,
  AppendSystemLogHandler,
  AddCORSDomainHandler,
  UpdateCORSDomainHandler,
  RemoveCORSDomainHandler,
];
