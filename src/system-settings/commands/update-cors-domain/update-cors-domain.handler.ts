import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { UpdateCORSDomainCommand } from './update-cors-domain.command';
import { CORSDomainAggregateService } from '../../aggregates/cors-domain/cors-domain.aggregate.service';

@CommandHandler(UpdateCORSDomainCommand)
export class UpdateCORSDomainHandler
  implements ICommandHandler<UpdateCORSDomainCommand>
{
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: CORSDomainAggregateService,
  ) {}
  async execute(command: UpdateCORSDomainCommand) {
    const { payload, actorUuid, uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const domain = await aggregate.updateCORSDomain(payload, actorUuid, uuid);
    aggregate.commit();
    return domain;
  }
}
