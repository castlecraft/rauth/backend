import { ICommand } from '@nestjs/cqrs';
import { CreateCORSDomainDto } from '../../entities/cors-domain/create-cors-domain-dto';

export class UpdateCORSDomainCommand implements ICommand {
  constructor(
    public readonly actorUuid: string,
    public readonly uuid: string,
    public readonly payload: CreateCORSDomainDto,
  ) {}
}
