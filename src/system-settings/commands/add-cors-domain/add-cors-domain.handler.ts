import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddCORSDomainCommand } from './add-cors-domain.command';
import { CORSDomainAggregateService } from '../../aggregates/cors-domain/cors-domain.aggregate.service';

@CommandHandler(AddCORSDomainCommand)
export class AddCORSDomainHandler implements ICommandHandler {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: CORSDomainAggregateService,
  ) {}

  async execute(command: AddCORSDomainCommand) {
    const { actorUuid, payload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const domain = await aggregate.addCORSDomain(payload, actorUuid);
    aggregate.commit();
    return domain;
  }
}
