import { ICommand } from '@nestjs/cqrs';
import { CreateCORSDomainDto } from '../../entities/cors-domain/create-cors-domain-dto';

export class AddCORSDomainCommand implements ICommand {
  constructor(
    public readonly actorUuid: string,
    public readonly payload: CreateCORSDomainDto,
  ) {}
}
