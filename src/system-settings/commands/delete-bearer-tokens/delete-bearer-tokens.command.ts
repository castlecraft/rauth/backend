import { ICommand } from '@nestjs/cqrs';

export class DeleteBearerTokensCommand implements ICommand {
  constructor(
    public readonly actorUuid: string,
    public readonly clientId: string,
    public readonly user: string,
  ) {}
}
