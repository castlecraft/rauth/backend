import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { RemoveCORSDomainCommand } from './remove-cors-domain.command';
import { CORSDomainAggregateService } from '../../aggregates/cors-domain/cors-domain.aggregate.service';

@CommandHandler(RemoveCORSDomainCommand)
export class RemoveCORSDomainHandler implements ICommandHandler {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: CORSDomainAggregateService,
  ) {}
  async execute(command: RemoveCORSDomainCommand) {
    const { actorUuid, uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const domain = await aggregate.removeCORSDomain(actorUuid, uuid);
    aggregate.commit();
    return domain;
  }
}
