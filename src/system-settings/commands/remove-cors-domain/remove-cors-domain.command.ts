import { ICommand } from '@nestjs/cqrs';

export class RemoveCORSDomainCommand implements ICommand {
  constructor(
    public readonly actorUuid: string,
    public readonly uuid: string,
  ) {}
}
