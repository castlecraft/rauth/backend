import {
  IsUrl,
  IsOptional,
  IsUUID,
  IsBoolean,
  IsNumber,
  Min,
  IsString,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { ONE_NUMBER, ZERO_NUMBER } from '../../../constants/app-strings';

export class ServerSettingDto {
  @IsUrl({ require_tld: false })
  @ApiProperty({
    description: 'The URL of the server.',
    type: 'string',
    required: true,
  })
  issuerUrl: string;

  @IsOptional()
  @IsUUID()
  @ApiProperty({
    description: 'OAuth 2.0 Client ID for Infrastructure Console',
    type: 'string',
  })
  infrastructureConsoleClientId?: string;

  @IsOptional()
  @IsUUID()
  @ApiProperty({
    description: 'Cloud Storage Bucket',
    type: 'string',
  })
  backupBucket?: string;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({
    description: 'disable signup or not',
    example: false,
  })
  disableSignup?: boolean;

  @IsOptional()
  @IsNumber()
  @ApiProperty({
    description: 'otp expiry number',
    type: 'number',
  })
  otpExpiry?: number;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({
    description: 'Clicking resendOTP generates a new OTP each time.',
    type: 'string',
  })
  regenerateOTP: boolean;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({
    description: 'enable choosing account or not',
    example: false,
  })
  enableChoosingAccount?: boolean;

  @IsOptional()
  @IsNumber()
  @Min(ONE_NUMBER)
  @ApiProperty({
    description: 'refresh token expiry hours',
    type: 'string',
  })
  refreshTokenExpiresInHours?: number;

  @IsOptional()
  @IsNumber()
  @Min(ONE_NUMBER)
  @ApiProperty({
    description: 'auth code expiry minutes',
    type: 'string',
  })
  authCodeExpiresInMinutes?: number;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'The name of host organization.',
    type: 'string',
  })
  organizationName: string;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({
    description: 'Allow client to show custom login page',
    type: 'string',
  })
  enableCustomLogin: boolean;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({
    description: 'Allow users to register phone',
    type: 'boolean',
  })
  enableUserPhone: boolean;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({
    description: 'Disallow user to be deleted',
    type: 'boolean',
  })
  isUserDeleteDisabled: boolean;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({
    description: "Don't allow user to set or change password",
    type: 'boolean',
  })
  passwordChangeRestricted: boolean;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({
    description: 'Enable User OTP',
    type: 'boolean',
  })
  enableUserOTP: boolean;

  @IsUUID()
  @IsOptional()
  @ApiProperty({
    description: 'system email account',
    type: 'string',
  })
  systemEmailAccount: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  @ApiProperty({
    description: 'url of logo',
    type: 'string',
  })
  logoUrl?: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  @ApiProperty({
    description: 'url of favicon',
    type: 'string',
  })
  faviconUrl?: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  @ApiProperty({
    description: 'privacy url',
    type: 'string',
  })
  privacyUrl?: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  @ApiProperty({
    description: 'help url',
    type: 'string',
  })
  helpUrl?: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  @ApiProperty({
    description: 'teams url',
    type: 'string',
  })
  termsUrl?: string;

  @IsOptional()
  @ApiProperty({
    description: 'copyright message',
    type: 'string',
  })
  copyrightMessage?: string;

  @IsOptional()
  @IsUUID()
  @ApiProperty({
    description: 'Cloud Storage Bucket for avatars',
    type: 'string',
  })
  avatarBucket?: string;

  @IsOptional()
  @IsNumber()
  @Min(ZERO_NUMBER)
  @ApiProperty({
    description: 'Disallow Login after number of failed attempts',
    type: 'number',
  })
  allowedFailedLoginAttempts?: number;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Forget password Message',
    type: 'string',
  })
  forgotPasswordMessage?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Forget password Subject',
    type: 'string',
  })
  forgotPasswordSubject?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Complete Signup Message',
    type: 'string',
  })
  completeSignupMessage?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Complete Signup Subject',
    type: 'string',
  })
  completeSignupSubject?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Verify Existing Email Message',
    type: 'string',
  })
  verifyExistingEmailMessage?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Verify Existing Email Subject',
    type: 'string',
  })
  verifyExistingEmailSubject?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Verify New Email Message',
    type: 'string',
  })
  verifyNewEmailMessage?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Verify New Email Subject',
    type: 'string',
  })
  verifyNewEmailSubject?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Login OTP Message',
    type: 'string',
  })
  loginOtpMessage?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Login OTP Subject',
    type: 'string',
  })
  loginOtpSubject?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Password Expiry Message',
    type: 'string',
  })
  passwordExpiryMessage?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Password Expiry Subject',
    type: 'string',
  })
  passwordExpirySubject?: string;

  @ApiProperty({
    description: 'Allowed Scopes for settings',
  })
  @IsString({ each: true })
  allowedScopes: string[];

  @IsOptional()
  @IsNumber()
  @Min(60)
  @ApiProperty({
    description: 'Delay in seconds for sending emails.',
    type: 'number',
    default: 120,
  })
  emailDelayInSeconds?: number;

  @IsOptional()
  @IsNumber()
  @Min(0)
  @ApiProperty({
    description: 'Interval in days for mandatory password reset.',
    type: 'number',
    default: 0,
  })
  passwordResetIntervalDays?: number;

  @IsOptional()
  @IsNumber()
  @Min(0)
  @ApiProperty({
    description: 'OTP Resend Delay (seconds)',
    type: 'number',
    default: 0,
  })
  resendOTPDelayInSec?: number;
}
