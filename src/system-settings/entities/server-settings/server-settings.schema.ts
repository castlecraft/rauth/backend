import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';
import {
  SERVICE,
  THIRTY_DAYS_IN_HOURS,
  THIRTY_NUMBER,
} from '../../../constants/app-strings';

export const SERVER_SETTINGS_COLLECTION_NAME = 'server_settings';

export const ServerSettings = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4, unique: true, sparse: true },
    issuerUrl: String,
    organizationName: String,
    service: { type: String, default: SERVICE },
    infrastructureConsoleClientId: String,
    backupBucket: String,
    disableSignup: Boolean,
    otpExpiry: { type: Number, default: 5 },
    regenerateOTP: Boolean,
    enableChoosingAccount: Boolean,
    enableCustomLogin: Boolean,
    refreshTokenExpiresInHours: { type: Number, default: THIRTY_DAYS_IN_HOURS },
    authCodeExpiresInMinutes: { type: Number, default: THIRTY_NUMBER },
    enableUserPhone: { type: Boolean, default: false },
    isUserDeleteDisabled: { type: Boolean, default: false },
    passwordChangeRestricted: { type: Boolean, default: false },
    enableUserOTP: { type: Boolean, default: false },
    systemEmailAccount: String,
    logoUrl: String,
    faviconUrl: String,
    privacyUrl: String,
    helpUrl: String,
    termsUrl: String,
    copyrightMessage: String,
    avatarBucket: String,
    modifiedBy: String,
    modified: Date,
    allowedFailedLoginAttempts: { type: Number, default: 10 },
    forgotPasswordMessage: String,
    forgotPasswordSubject: String,
    completeSignupMessage: String,
    completeSignupSubject: String,
    verifyExistingEmailMessage: String,
    verifyExistingEmailSubject: String,
    verifyNewEmailMessage: String,
    verifyNewEmailSubject: String,
    loginOtpMessage: String,
    loginOtpSubject: String,
    passwordExpiryMessage: String,
    passwordExpirySubject: String,
    allowedScopes: [String],
    emailDelayInSeconds: { type: Number, default: 120 },
    passwordResetIntervalDays: { type: Number, default: 0 },
    resendOTPDelayInSec: { type: Number, default: 60 },
  },
  { collection: SERVER_SETTINGS_COLLECTION_NAME, versionKey: false },
);

export const SERVER_SETTINGS = 'ServerSettings';

export const ServerSettingsModel = mongoose.model(
  SERVER_SETTINGS,
  ServerSettings,
);
