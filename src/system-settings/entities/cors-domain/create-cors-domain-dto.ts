import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsUrl, IsUUID } from 'class-validator';
import { i18n } from '../../../i18n/i18n.config';

export class CreateCORSDomainDto {
  creation?: Date;
  createdBy?: string;

  @IsOptional()
  @IsUUID()
  uuid?: string;

  @IsUrl({ require_protocol: true, require_host: true })
  @ApiProperty({
    description: i18n.__('Domain Name'),
    required: true,
    example: 'https://www.google.com',
  })
  domain: string;
}
