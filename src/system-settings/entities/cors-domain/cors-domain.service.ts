import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CORS_DOMAIN } from './cors-domain.schema';
import { CORSDomain } from './cors-domain.interface';

@Injectable()
export class CORSDomainService {
  constructor(
    @Inject(CORS_DOMAIN)
    private readonly model: Model<CORSDomain>,
  ) {}

  public async save(params: CORSDomain) {
    const corsDomain = {};
    Object.assign(corsDomain, params);
    return await new this.model(corsDomain).save();
  }

  async findAll(): Promise<CORSDomain[]> {
    return await this.model.find();
  }

  public async findOne(params): Promise<any> {
    return await this.model.findOne(params);
  }

  public async delete(params): Promise<any> {
    return await this.model.deleteOne(params);
  }

  public async find(params) {
    return await this.model.find(params);
  }

  public async updateOne(query, params): Promise<any> {
    return await this.model.updateOne(query, params);
  }

  async list(
    offset: number,
    limit: number,
    search: string,
    query: any,
    sortQuery?: any,
  ) {
    if (search) {
      // Search through multiple keys
      // https://stackoverflow.com/a/41390870
      const nameExp = new RegExp(search, 'i');
      query.$or = ['domain', 'uuid'].map(field => {
        const out = {};
        out[field] = nameExp;
        return out;
      });
    }

    const data = this.model
      .find(query, { pass: 0 })
      .skip(Number(offset))
      .limit(Number(limit))
      .sort(sortQuery);

    return {
      docs: await data.exec(),
      length: await this.model.countDocuments(query),
      offset: Number(offset),
    };
  }
}
