import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

export const CORS_DOMAIN = 'CORSDomain';

export const CORS_DOMAIN_COLLECTION = 'cors_domain';

export const CORSDomain = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4 },
    domain: String,
    creation: Date,
    modified: Date,
    createdBy: String,
    modifiedBy: String,
  },
  { collection: CORS_DOMAIN_COLLECTION, versionKey: false, strict: false },
);

export const CORSDomainModel = mongoose.model(CORS_DOMAIN, CORSDomain);
