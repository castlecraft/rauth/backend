import { Test, TestingModule } from '@nestjs/testing';
import { CORSDomainService } from './cors-domain.service';
import { CORS_DOMAIN } from './cors-domain.schema';

describe('CORSDomain', () => {
  let service: CORSDomainService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CORSDomainService,
        {
          provide: CORS_DOMAIN,
          useValue: {}, // provide mock values
        },
      ],
    }).compile();
    service = module.get<CORSDomainService>(CORSDomainService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
