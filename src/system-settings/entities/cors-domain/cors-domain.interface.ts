import { Document } from 'mongoose';

export interface CORSDomain extends Document {
  uuid?: string;
  domain?: string;
  creation?: Date;
  modified?: Date;
  createdBy?: string;
  modifiedBy?: string;
}
