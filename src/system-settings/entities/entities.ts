import { Connection } from 'mongoose';
import {
  SERVER_SETTINGS,
  ServerSettings,
  SERVER_SETTINGS_COLLECTION_NAME,
} from './server-settings/server-settings.schema';
import { MONGOOSE_CONNECTION } from '../../common/database.provider';
import {
  SYSTEM_LOG,
  SYSTEM_LOG_COLLECTION,
  SystemLog,
} from './system-log/system-log.schema';
import {
  CORS_DOMAIN,
  CORS_DOMAIN_COLLECTION,
  CORSDomain,
} from './cors-domain/cors-domain.schema';

export const SystemSettingsModuleEntities = [
  {
    provide: SERVER_SETTINGS,
    useFactory: (connection: Connection) =>
      connection.model(
        SERVER_SETTINGS,
        ServerSettings,
        SERVER_SETTINGS_COLLECTION_NAME,
        { overwriteModels: process.env.NODE_ENV === 'test-e2e' },
      ),
    inject: [MONGOOSE_CONNECTION],
  },
  {
    provide: SYSTEM_LOG,
    useFactory: (connection: Connection) => {
      // Added an index to improve performance during load testing, as it was degrading without index
      SystemLog.index({ entityId: 1, entity: 1, logType: 1, logCode: 1 });
      return connection.model(SYSTEM_LOG, SystemLog, SYSTEM_LOG_COLLECTION);
    },
    inject: [MONGOOSE_CONNECTION],
  },
  {
    provide: CORS_DOMAIN,
    useFactory: (connection: Connection) => {
      return connection.model(CORS_DOMAIN, CORSDomain, CORS_DOMAIN_COLLECTION);
    },
    inject: [MONGOOSE_CONNECTION],
  },
];
