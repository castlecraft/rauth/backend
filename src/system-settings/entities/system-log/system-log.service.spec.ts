import { Test, TestingModule } from '@nestjs/testing';
import { SystemLogService } from './system-log.service';
import { SYSTEM_LOG } from './system-log.schema';

describe('SystemLogService', () => {
  let service: SystemLogService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SystemLogService,
        {
          provide: SYSTEM_LOG,
          useValue: {},
        },
      ],
    }).compile();
    service = module.get<SystemLogService>(SystemLogService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
