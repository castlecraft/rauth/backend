import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { SYSTEM_LOG } from './system-log.schema';
import { SystemLog } from './system-log.interface';
import { DeleteResult } from 'mongodb';

@Injectable()
export class SystemLogService {
  constructor(
    @Inject(SYSTEM_LOG)
    private readonly model: Model<SystemLog>,
  ) {}

  async save(params) {
    const doc = new this.model(params);
    return await doc.save();
  }

  async update(doc: SystemLog) {
    return await doc.save();
  }

  async find(params?): Promise<SystemLog[]> {
    return await this.model.find(params);
  }

  async findOne(params) {
    return await this.model.findOne(params).exec();
  }

  async countDocuments(params?) {
    return await this.model.countDocuments(params);
  }

  async deleteMany(params?): Promise<DeleteResult> {
    return await this.model.deleteMany(params);
  }
}
