import { Document } from 'mongoose';

export class SystemLog extends Document {
  uuid?: string;
  data?: SystemLogData;
  logType?: SystemLogType;
  entity?: string;
  entityId?: string;
  logCode?: SystemLogCode;
  reason?: string;
}

export enum SystemLogType {
  Authentication = 'Authentication',
  Email = 'Email',
}

export enum SystemLoginType {
  Kerberos = 'Kerberos',
  LDAP = 'LDAP',
  PasswordLess = 'PasswordLess',
  Database = 'Database',
}

export enum SystemLogCode {
  Success = 'Success',
  Failure = 'Failure',
}

export interface SystemLogData {
  success?: unknown;
  error?: unknown;
  loginType?: SystemLoginType;
  [key: string]: unknown;
}

export enum AccessType {
  LDAP = 'ldap',
  Kerberos = 'kerberos',
}
