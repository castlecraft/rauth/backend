import { Module } from '@nestjs/common';
import { ServerSettingsService } from './server-settings/server-settings.service';
import { SystemSettingsModuleEntities } from './entities';
import { SystemLogService } from './system-log/system-log.service';
import { CORSDomainService } from './cors-domain/cors-domain.service';

@Module({
  providers: [
    ...SystemSettingsModuleEntities,
    ServerSettingsService,
    SystemLogService,
    CORSDomainService,
  ],
  exports: [
    ...SystemSettingsModuleEntities,
    ServerSettingsService,
    SystemLogService,
    CORSDomainService,
  ],
})
export class SystemSettingsEntitiesModule {}
