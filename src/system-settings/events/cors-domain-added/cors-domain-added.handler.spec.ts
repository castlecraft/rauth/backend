import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { CORSDomainAddedHandler } from './cors-domain-added.handler';
import { CORSDomain } from '../../entities/cors-domain/cors-domain.interface';
import { CORSDomainAddedEvent } from './cors-domain-added.event';
import { CORSDomainService } from '../../entities/cors-domain/cors-domain.service';
import { ConfigService } from '../../../config/config.service';

describe('Event: CORSDomainAddedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: CORSDomainAddedHandler;

  let domainService: CORSDomainService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        CORSDomainAddedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: CORSDomainService,
          useValue: () => jest.fn(),
        },
        {
          provide: ConfigService,
          useValue: {
            get: (key: string) => key,
          },
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<CORSDomainAddedHandler>(CORSDomainAddedHandler);
    domainService = module.get<CORSDomainService>(CORSDomainService);
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should add CORSDomain using CORSDomainService', async () => {
    domainService.save = jest.fn((...args) =>
      Promise.resolve({} as CORSDomain & { _id: any }),
    );
    eventBus$.publish = jest.fn(() => {});
    await eventHandler.handle(new CORSDomainAddedEvent({} as CORSDomain));
    expect(domainService.save).toHaveBeenCalledTimes(1);
  });
});
