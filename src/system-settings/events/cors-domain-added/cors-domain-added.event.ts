import { IEvent } from '@nestjs/cqrs';
import { CORSDomain } from '../../entities/cors-domain/cors-domain.interface';

export class CORSDomainAddedEvent implements IEvent {
  constructor(public readonly domain: CORSDomain) {}
}
