import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { CORSDomainAddedEvent } from './cors-domain-added.event';
import { CORSDomainService } from '../../entities/cors-domain/cors-domain.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';

@EventsHandler(CORSDomainAddedEvent)
export class CORSDomainAddedHandler
  implements IEventHandler<CORSDomainAddedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly domainService: CORSDomainService,
    private readonly config: ConfigService,
  ) {}
  handle(command: CORSDomainAddedEvent) {
    const { domain } = command;
    this.domainService
      .save(domain)
      .then(domain => {
        this.logger.log(CORSDomainAddedEvent.name, this.constructor.name);
        this.logger.debug(domain.uuid, this.constructor.name);
      })
      .catch(error => {
        this.logger.error(error, error, this.constructor.name);
      });
  }
}
