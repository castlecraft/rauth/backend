import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { HttpService } from '@nestjs/axios';
import { BearerTokensDeletedEvent } from './bearer-tokens-deleted.event';
import { BearerTokenService } from '../../../auth/entities/bearer-token/bearer-token.service';
import { ClientService } from '../../../client-management/entities/client/client.service';
import { TOKEN_DELETE_QUEUE } from '../../../auth/schedulers/token-schedule/token-schedule.service';
import { retry } from 'rxjs/operators';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(BearerTokensDeletedEvent)
export class BearerTokensDeletedHandler
  implements IEventHandler<BearerTokensDeletedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly bearerToken: BearerTokenService,
    private readonly client: ClientService,
    private readonly http: HttpService,
    private readonly config: ConfigService,
  ) {}

  handle(event: BearerTokensDeletedEvent) {
    const { clientId, user } = event;
    this.deleteTokens(clientId, user)
      .then(success => {
        this.logger.log(BearerTokensDeletedEvent.name, this.constructor.name);
      })
      .catch(error => {
        this.logger.error(error, error, this.constructor.name);
      });
  }

  async deleteTokens(clientId?: string, user?: string) {
    let tokens: any[];
    if (clientId) {
      tokens = await this.bearerToken.find({ client: clientId });
    } else if (user) {
      tokens = await this.bearerToken.find({ user });
    } else {
      tokens = await this.bearerToken.getAll();
    }

    for (const token of tokens) {
      await this.bearerToken.remove(token);
      await this.informClients(token.accessToken);
    }
  }

  async informClients(accessToken: string) {
    const clients = await this.client.findAll();
    for (const client of clients) {
      if (client.tokenDeleteEndpoint) {
        this.http
          .post(
            client.tokenDeleteEndpoint,
            {
              message: TOKEN_DELETE_QUEUE,
              accessToken,
            },
            {
              auth: {
                username: client.clientId,
                password: client.clientSecret,
              },
            },
          )
          .pipe(retry(3))
          .subscribe({
            error: error => {
              // TODO: Log Error
            },
          });
      }
    }
  }
}
