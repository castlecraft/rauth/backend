import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { CORSDomainUpdatedHandler } from './cors-domain-updated.handler';
import { CORSDomainService } from '../../entities/cors-domain/cors-domain.service';
import { ConfigService } from '../../../config/config.service';

describe('Event: CORSDomainUpdatedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: CORSDomainUpdatedHandler;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        CORSDomainUpdatedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: CORSDomainService,
          useValue: () => jest.fn(),
        },
        {
          provide: ConfigService,
          useValue: {
            get: (key: string) => key,
          },
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<CORSDomainUpdatedHandler>(
      CORSDomainUpdatedHandler,
    );
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });
});
