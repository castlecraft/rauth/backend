import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { CORSDomainUpdatedEvent } from './cors-domain-updated.event';
import { from } from 'rxjs';
import { CORSDomainService } from '../../entities/cors-domain/cors-domain.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(CORSDomainUpdatedEvent)
export class CORSDomainUpdatedHandler
  implements IEventHandler<CORSDomainUpdatedEvent>
{
  private readonly logger = new RAuthLogger(this.config);

  constructor(
    public readonly domainService: CORSDomainService,
    private readonly config: ConfigService,
  ) {}
  handle(command: CORSDomainUpdatedEvent) {
    const { domain } = command;
    from(this.domainService.updateOne({ uuid: domain.uuid }, domain)).subscribe(
      {
        next: success => {
          this.logger.log(CORSDomainUpdatedEvent.name, this.constructor.name);
        },
        error: error => {
          this.logger.error(error, error, this.constructor.name);
        },
      },
    );
  }
}
