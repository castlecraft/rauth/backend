import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { UserSessionsDeletedHandler } from './user-sessions-deleted.handler';
import { UserSessionsDeletedEvent } from './user-sessions-deleted.event';
import { SessionService } from '../../../auth/entities/session/session.service';
import { ConfigService } from '../../../config/config.service';

describe('Event: UserSessionsDeletedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: UserSessionsDeletedHandler;
  let sessionService: SessionService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        UserSessionsDeletedHandler,
        { provide: SessionService, useValue: {} },
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: ConfigService,
          useValue: {
            get: (key: string) => key,
          },
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<UserSessionsDeletedHandler>(
      UserSessionsDeletedHandler,
    );
    sessionService = module.get<SessionService>(SessionService);
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should clear sessions using Mongoose', async () => {
    eventBus$.publish = jest.fn(() => {});
    sessionService.deleteMany = jest.fn(() => Promise.resolve());
    await eventHandler.handle(
      new UserSessionsDeletedEvent('uuid-here', 'user-here'),
    );
    expect(sessionService.deleteMany).toHaveBeenCalledTimes(1);
  });
});
