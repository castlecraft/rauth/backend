import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { UserSessionsDeletedEvent } from './user-sessions-deleted.event';
import { SessionService } from '../../../auth/entities/session/session.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(UserSessionsDeletedEvent)
export class UserSessionsDeletedHandler
  implements IEventHandler<UserSessionsDeletedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly session: SessionService,
    private readonly config: ConfigService,
  ) {}

  handle(event: UserSessionsDeletedEvent) {
    const { user } = event;
    this.deleteSessions(user)
      .then(success => {
        this.logger.log(UserSessionsDeletedEvent.name, this.constructor.name);
      })
      .catch(error => {
        this.logger.error(error, error, this.constructor.name);
      });
  }

  async deleteSessions(user: string) {
    if (user) {
      // Delete sessions where the user's UUID matches
      await this.session.deleteMany({ 'session.users.uuid': user });
    } else {
      // Clear all sessions
      await this.session.clear();
    }
  }
}
