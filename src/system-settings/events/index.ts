import { SystemSettingsChangedHandler } from './server-settings-changed/server-settings-changed.handler';
import { BearerTokensDeletedHandler } from './bearer-tokens-deleted/bearer-tokens-deleted.handler';
import { UserSessionsDeletedHandler } from './user-sessions-deleted/user-sessions-deleted.handler';
import { SystemLogAppendedHandler } from './system-log-appended/system-log-appended.handler';
import { CORSDomainAddedHandler } from './cors-domain-added/cors-domain-added.handler';
import { CORSDomainUpdatedHandler } from './cors-domain-updated/cors-domain-updated.handler';
import { CORSDomainRemovedHandler } from './cors-domain-removed/cors-domain-removed.handler';

export const SystemSettingsEventHandlers = [
  SystemSettingsChangedHandler,
  BearerTokensDeletedHandler,
  UserSessionsDeletedHandler,
  SystemLogAppendedHandler,
  CORSDomainAddedHandler,
  CORSDomainUpdatedHandler,
  CORSDomainRemovedHandler,
];
