import { IEvent } from '@nestjs/cqrs';
import { SystemLog } from '../../entities/system-log/system-log.interface';

export class SystemLogAppendedEvent implements IEvent {
  constructor(
    public readonly log: SystemLog,
    public readonly saveLog: boolean,
  ) {}
}
