import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { SystemLogAppendedHandler } from './system-log-appended.handler';
import { SystemLogAppendedEvent } from './system-log-appended.event';
import { SystemLog } from '../../entities/system-log/system-log.interface';
import { SystemLogService } from '../../entities/system-log/system-log.service';
import { ConfigService } from '../../../config/config.service';

describe('Event: SystemLogAppendedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: SystemLogAppendedHandler;
  let systemLog: SystemLogService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        SystemLogAppendedHandler,
        { provide: SystemLogService, useValue: {} },
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: ConfigService,
          useValue: {
            get: (key: string) => key,
          },
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<SystemLogAppendedHandler>(
      SystemLogAppendedHandler,
    );
    systemLog = module.get<SystemLogService>(SystemLogService);
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should append logs', async () => {
    eventBus$.publish = jest.fn(() => {});
    systemLog.save = jest.fn(() => Promise.resolve({} as any));
    await eventHandler.handle(
      new SystemLogAppendedEvent({} as SystemLog, true),
    );
    expect(systemLog.save).toHaveBeenCalledTimes(1);
  });
});
