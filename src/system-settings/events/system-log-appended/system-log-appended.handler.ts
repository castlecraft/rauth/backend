import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { SystemLogAppendedEvent } from './system-log-appended.event';
import { SystemLogService } from '../../entities/system-log/system-log.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(SystemLogAppendedEvent)
export class SystemLogAppendedHandler
  implements IEventHandler<SystemLogAppendedEvent>
{
  private readonly logger = new RAuthLogger(this.config);

  constructor(
    private readonly log: SystemLogService,
    private readonly config: ConfigService,
  ) {}

  handle(event: SystemLogAppendedEvent) {
    const { saveLog } = event;
    if (saveLog) {
      this.log
        .save(event.log)
        .then(success => {
          this.logger.log(SystemLogAppendedEvent.name, this.constructor.name);
        })
        .catch(error => {
          this.logger.error(error, error, this.constructor.name);
        });
    }
  }
}
