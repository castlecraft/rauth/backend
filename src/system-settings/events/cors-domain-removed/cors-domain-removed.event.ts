import { IEvent } from '@nestjs/cqrs';
import { CORSDomain } from '../../entities/cors-domain/cors-domain.interface';

export class CORSDomainRemovedEvent implements IEvent {
  constructor(
    public readonly actorUuid: string,
    public readonly domain: CORSDomain,
  ) {}
}
