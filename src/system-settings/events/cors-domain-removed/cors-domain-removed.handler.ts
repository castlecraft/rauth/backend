import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { CORSDomainRemovedEvent } from './cors-domain-removed.event';
import { CORSDomainService } from '../../entities/cors-domain/cors-domain.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(CORSDomainRemovedEvent)
export class CORSDomainRemovedHandler
  implements IEventHandler<CORSDomainRemovedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly domainService: CORSDomainService,
    private readonly config: ConfigService,
  ) {}
  handle(command: CORSDomainRemovedEvent) {
    const { domain } = command;
    this.domainService
      .delete({ uuid: domain.uuid })
      .then(email => {
        this.logger.log(CORSDomainRemovedEvent.name, this.constructor.name);
      })
      .catch(error => {
        this.logger.error(error, error, this.constructor.name);
      });
  }
}
