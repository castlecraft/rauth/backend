import { ForbiddenException, Injectable } from '@nestjs/common';
import { from } from 'rxjs';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { ClientService } from '../../../client-management/entities/client/client.service';
import { InvalidClientException } from '../../../common/filters/exceptions';
import { AggregateRoot } from '@nestjs/cqrs';
import { SystemSettingsChangedEvent } from '../../events/server-settings-changed/server-settings-changed.event';
import { ServerSettingDto } from '../../entities/server-settings/server-setting.dto';
import { ServerSettings } from '../../entities/server-settings/server-settings.interface';
import { BearerTokensDeletedEvent } from '../../events/bearer-tokens-deleted/bearer-tokens-deleted.event';
import { UserSessionsDeletedEvent } from '../../events/user-sessions-deleted/user-sessions-deleted.event';
import { StorageService } from '../../../cloud-storage/entities/storage/storage.service';
import { EmailAccountService } from '../../../email/entities/email-account/email-account.service';
import { SystemLog } from '../../entities/system-log/system-log.interface';
import { SystemLogAppendedEvent } from '../../events/system-log-appended/system-log-appended.event';

@Injectable()
export class SystemSettingsManagementService extends AggregateRoot {
  constructor(
    private readonly settingsService: ServerSettingsService,
    private readonly clientService: ClientService,
    private readonly storage: StorageService,
    private readonly email: EmailAccountService,
  ) {
    super();
  }

  async updateSettings(actorUserUuid: string, payload: ServerSettingDto) {
    const settings: ServerSettings =
      await this.settingsService.findWithoutError();

    if (settings && payload.issuerUrl) {
      settings.issuerUrl = payload.issuerUrl;
    }

    if (settings && payload.disableSignup) {
      settings.disableSignup = payload.disableSignup;
    }

    if (settings && payload.infrastructureConsoleClientId) {
      await this.checkValidClientId(payload.infrastructureConsoleClientId);
      settings.infrastructureConsoleClientId =
        payload.infrastructureConsoleClientId;
    }

    if (settings && payload.avatarBucket) {
      await this.checkValidStorage(payload.avatarBucket);
      settings.avatarBucket = payload.avatarBucket;
    }

    if (settings && payload.backupBucket) {
      await this.checkValidStorage(payload.backupBucket);
      settings.backupBucket = payload.backupBucket;
    }

    if (settings && payload.backupBucket) {
      await this.checkValidStorage(payload.backupBucket);
      settings.backupBucket = payload.backupBucket;
    }

    if (settings && payload.systemEmailAccount) {
      await this.checkValidEmailAccount(payload.systemEmailAccount);
      settings.systemEmailAccount = payload.systemEmailAccount;
    }

    if (settings) {
      Object.assign(settings, payload);
    }
    settings.modifiedBy = actorUserUuid;
    settings.modified = new Date();
    this.apply(new SystemSettingsChangedEvent(actorUserUuid, settings));
  }

  getSettings() {
    return from(this.settingsService.find());
  }

  async checkValidClientId(clientId: string) {
    if (!(await this.clientService.findOne({ clientId }))) {
      throw new InvalidClientException();
    }
  }

  async checkValidStorage(uuid: string) {
    if (!(await this.storage.findOne({ uuid }))) {
      throw new ForbiddenException({ InvalidStorageUUID: uuid });
    }
  }

  async checkValidEmailAccount(uuid: string) {
    if (!(await this.email.findOne({ uuid }))) {
      throw new ForbiddenException({ InvalidEmailAccountUUID: uuid });
    }
  }

  async deleteUserSessions(userUuid: string, user: string) {
    this.apply(new UserSessionsDeletedEvent(userUuid, user));
  }

  async deleteBearerTokens(userUuid: string, clientId: string, user: string) {
    this.apply(new BearerTokensDeletedEvent(userUuid, clientId, user));
  }

  appendSystemLog(log: SystemLog, saveLog: boolean) {
    this.apply(new SystemLogAppendedEvent(log, saveLog));
  }
}
