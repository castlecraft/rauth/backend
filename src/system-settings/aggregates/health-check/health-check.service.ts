import { Injectable } from '@nestjs/common';
import { Transport } from '@nestjs/microservices';
import {
  HealthIndicatorFunction,
  MicroserviceHealthIndicator,
} from '@nestjs/terminus';
import { getEventsClientProvider } from '../../../common/events-microservice.client';
import {
  AllowedEventsProtocol,
  ConfigService,
  EnableDisable,
  EVENTS_HEALTHCHECK,
  EVENTS_HEALTHCHECK_TIMEOUT,
  EVENTS_PROTO,
} from '../../../config/config.service';
import { DatabaseHealthIndicatorService } from '../database-health-indicator/database-health-indicator.service';

export const HEALTHCHECK_TIMEOUT = 10000;

@Injectable()
export class HealthCheckAggregateService {
  constructor(
    private readonly microservice: MicroserviceHealthIndicator,
    private readonly database: DatabaseHealthIndicatorService,
    private readonly config: ConfigService,
  ) {}

  createTerminusOptions(): HealthIndicatorFunction[] {
    const healthEndpoints: HealthIndicatorFunction[] = [
      async () => this.database.isHealthy(),
    ];

    if (this.config.get(EVENTS_HEALTHCHECK) === EnableDisable.ON) {
      const { options } = getEventsClientProvider(this.config);

      healthEndpoints.push(async () => {
        return this.microservice.pingCheck('events', {
          transport: this.getEventsTransport(),
          options,
          timeout: Number(this.config.get(EVENTS_HEALTHCHECK_TIMEOUT)),
        });
      });
    }

    return healthEndpoints;
  }

  getEventsTransport() {
    if (this.config.get(EVENTS_PROTO) === AllowedEventsProtocol.MQTT) {
      return Transport.MQTT;
    }
    if (this.config.get(EVENTS_PROTO) === AllowedEventsProtocol.TCP) {
      return Transport.TCP;
    }
    if (this.config.get(EVENTS_PROTO) === AllowedEventsProtocol.KAFKA) {
      return Transport.KAFKA;
    }
  }
}
