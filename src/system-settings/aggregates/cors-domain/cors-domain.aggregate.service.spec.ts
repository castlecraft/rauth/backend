import { Test, TestingModule } from '@nestjs/testing';
import { CORSDomainAggregateService } from './cors-domain.aggregate.service';

describe('CORSDomainAggregateService', () => {
  let service: CORSDomainAggregateService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CORSDomainAggregateService],
    })
      .overrideProvider(CORSDomainAggregateService)
      .useFactory({ factory: (...args) => jest.fn() })
      .compile();
    service = module.get<CORSDomainAggregateService>(
      CORSDomainAggregateService,
    );
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
