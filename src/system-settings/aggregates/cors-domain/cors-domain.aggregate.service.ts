import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { v4 as uuidv4 } from 'uuid';

import { CORSDomain } from '../../entities/cors-domain/cors-domain.interface';
import { CreateCORSDomainDto } from '../../entities/cors-domain/create-cors-domain-dto';
import { CORSDomainAddedEvent } from '../../events/cors-domain-added/cors-domain-added.event';
import { CORSDomainUpdatedEvent } from '../../events/cors-domain-updated/cors-domain-updated.event';
import { CORSDomainService } from '../../entities/cors-domain/cors-domain.service';
import { CORSDomainRemovedEvent } from '../../events/cors-domain-removed/cors-domain-removed.event';
import { InvalidCORSDomainException } from '../../../common/filters/exceptions';

@Injectable()
export class CORSDomainAggregateService extends AggregateRoot {
  constructor(private readonly domainService: CORSDomainService) {
    super();
  }

  async addCORSDomain(payload: CreateCORSDomainDto, actorUuid: string) {
    if (payload.domain !== this.setDomain(payload?.domain)) {
      throw new InvalidCORSDomainException(payload.domain);
    }

    const checkDomain = await this.domainService.findOne({
      domain: payload?.domain,
    });
    if (checkDomain) {
      throw new InvalidCORSDomainException();
    }
    payload.creation = new Date();
    payload.createdBy = actorUuid;
    payload.uuid = uuidv4();
    this.apply(new CORSDomainAddedEvent(payload as CORSDomain));
    return payload;
  }

  async updateCORSDomain(
    payload: CreateCORSDomainDto,
    actorUuid: string,
    uuid: string,
  ) {
    if (payload.domain !== this.setDomain(payload?.domain)) {
      throw new InvalidCORSDomainException(payload.domain);
    }

    const domain = await this.domainService.findOne({
      uuid,
    });

    if (!domain) {
      throw new NotFoundException({ DomainNotFound: payload?.uuid });
    }
    if (payload.domain !== domain.domain) {
      const checkDomain = await this.domainService.findOne({
        domain: payload?.domain,
      });
      if (checkDomain && checkDomain.uuid !== uuid) {
        throw new InvalidCORSDomainException();
      }
    }
    domain.domain = payload.domain;
    domain.modified = new Date();
    domain.modifiedBy = actorUuid;
    this.apply(new CORSDomainUpdatedEvent(domain as CORSDomain));
    return { uuid: domain.uuid };
  }

  async removeCORSDomain(actorUuid: string, uuid: string) {
    const domain = await this.domainService.findOne({ uuid });
    if (!domain) {
      throw new NotFoundException({ DomainNotFound: uuid });
    }
    this.apply(new CORSDomainRemovedEvent(actorUuid, domain));
    return { deleted: uuid };
  }

  setDomain(url: string) {
    try {
      return new URL(url?.toLowerCase()?.trim()).origin;
    } catch (error) {
      return url?.toLowerCase()?.trim();
    }
  }
}
