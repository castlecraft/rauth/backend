import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { ClientProxy } from '@nestjs/microservices';
import { v4 as uuidv4 } from 'uuid';
import { ConfigService, EVENTS_TOPIC } from '../../../config/config.service';
import { SERVICE } from '../../../constants/app-strings';
import { BROADCAST_EVENT } from '../../events-microservice.client';
import { SaveEventCommand } from './save-event.command';

@CommandHandler(SaveEventCommand)
export class SaveEventHandler implements ICommandHandler<SaveEventCommand> {
  constructor(
    @Inject(BROADCAST_EVENT)
    private readonly publisher: ClientProxy,
    private readonly config: ConfigService,
  ) {}

  async execute(command: SaveEventCommand) {
    const { event } = command;
    const payload = {
      eventId: uuidv4(),
      eventName: event.constructor.name,
      eventFromService: SERVICE,
      eventDateTime: new Date(),
      eventData: event,
    };

    if (this.config.get(EVENTS_TOPIC)) {
      return this.publisher.emit(this.config.get(EVENTS_TOPIC), payload);
    }

    return this.publisher.emit(event.constructor.name, payload);
  }
}
