import {
  Transport,
  KafkaOptions,
  ClientProvider,
  ClientsProviderAsyncOptions,
} from '@nestjs/microservices';
import { Partitioners } from 'kafkajs';
import { v4 as uuidv4 } from 'uuid';

import {
  AllowedEventsProtocol,
  ConfigService,
  EVENTS_HOST,
  EVENTS_PASSWORD,
  EVENTS_PORT,
  EVENTS_PROTO,
  EVENTS_USER,
  EVENTS_CLIENT_ID,
  EVENTS_GROUP,
} from '../config/config.service';
import { SCRAM_SHA_256 } from '../constants/app-strings';

export const BROADCAST_EVENT = 'BROADCAST_EVENT';

export function getEventsClientProvider(config: ConfigService): ClientProvider {
  if (
    config.get(EVENTS_PROTO) === AllowedEventsProtocol.MQTT &&
    config.get(EVENTS_HOST) &&
    config.get(EVENTS_PORT) &&
    config.get(EVENTS_USER) &&
    config.get(EVENTS_PASSWORD)
  ) {
    const url = `${config.get(EVENTS_PROTO)}://${config.get(EVENTS_USER)}:${config.get(
      EVENTS_PASSWORD,
    )}@${config.get(EVENTS_HOST)}:${config.get(EVENTS_PORT)}`;
    return {
      transport: Transport.MQTT,
      options: { url, clientId: uuidv4(), protocolVersion: 5 },
    };
  }

  if (
    config.get(EVENTS_PROTO) === AllowedEventsProtocol.TCP &&
    config.get(EVENTS_HOST) &&
    config.get(EVENTS_PORT)
  ) {
    return {
      transport: Transport.TCP,
      options: {
        host: config.get(EVENTS_HOST),
        port: Number(config.get(EVENTS_PORT)),
      },
    };
  }

  if (
    config.get(EVENTS_PROTO) === AllowedEventsProtocol.KAFKA &&
    config.get(EVENTS_HOST) &&
    config.get(EVENTS_USER) &&
    config.get(EVENTS_PASSWORD) &&
    config.get(EVENTS_CLIENT_ID)
  ) {
    const brokers = config.get(EVENTS_HOST)?.replace(/,\s*$/, '').split(',');
    const options: KafkaOptions['options'] = {
      client: {
        clientId: config.get(EVENTS_CLIENT_ID),
        brokers,
        ssl: { rejectUnauthorized: false },
        sasl: {
          mechanism: SCRAM_SHA_256,
          username: config.get(EVENTS_USER),
          password: config.get(EVENTS_PASSWORD),
        },
      },
      producer: { createPartitioner: Partitioners.DefaultPartitioner },
    };

    if (config.get(EVENTS_GROUP)) {
      options.consumer = { groupId: config.get(EVENTS_GROUP) };
    }

    return {
      transport: Transport.KAFKA,
      options,
    };
  }

  return {};
}

export const eventsClient: ClientsProviderAsyncOptions = {
  useFactory: (config: ConfigService) => getEventsClientProvider(config),
  name: BROADCAST_EVENT,
  inject: [ConfigService],
};
