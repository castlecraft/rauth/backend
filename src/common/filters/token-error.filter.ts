import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { TokenError } from 'oauth2orize';
import { MfaTokenError } from './exceptions';

@Catch(TokenError)
export class TokenErrorFilter implements ExceptionFilter {
  catch(error: TokenError | MfaTokenError, host: ArgumentsHost) {
    const response = host.switchToHttp().getResponse();
    const eRes = {
      error: error.code,
      error_description: error.message,
      mfa_token: undefined,
    };

    if (error.mfaToken) {
      eRes.mfa_token = error.mfaToken;
    }

    return response.status(error.status).json(eRes);
  }
}
