import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { Request, Response } from 'express';
import { AuthorizationError } from 'oauth2orize';

@Catch(AuthorizationError)
export class AuthorizationErrorFilter implements ExceptionFilter {
  catch(error: AuthorizationError, host: ArgumentsHost) {
    const response = host.switchToHttp().getResponse();
    const request = host.switchToHttp().getRequest();

    if (request.query.redirect_uri) {
      return this.redirect(error, request, response);
    }
    return response.status(error.status).json({ error: error.message });
  }

  redirect(error: AuthorizationError, request: Request, response: Response) {
    const code = error.code ? '?error=' + error.code : '';
    const state = request.query.state ? '&state=' + request.query.state : '';
    const description = error.message
      ? '&error_description=' + error.message
      : '';
    const redirectParams = code + description + state;
    return response.redirect(request.query.redirect_uri + redirectParams);
  }
}
