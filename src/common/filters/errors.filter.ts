import {
  ExceptionFilter,
  Catch,
  HttpException,
  ArgumentsHost,
  HttpStatus,
} from '@nestjs/common';
import { ServerSettingsService } from '../../system-settings/entities/server-settings/server-settings.service';
import {
  LOGIN_ROUTE,
  NEGOTIATE,
  RESPONSE,
  WWW_NEGOTIATE_HEADER_KEY,
} from '../../constants/app-strings';

@Catch()
export class ErrorFilter implements ExceptionFilter {
  constructor(private readonly settings: ServerSettingsService) {}

  async catch(error: Error, host: ArgumentsHost) {
    const request = host.switchToHttp().getRequest();
    const response = host.switchToHttp().getResponse();
    const settings = await this.settings.find();
    const status: HttpStatus =
      error instanceof HttpException
        ? error.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    if (status === HttpStatus.FORBIDDEN) {
      return response.redirect(
        settings.issuerUrl +
          LOGIN_ROUTE +
          '?redirect=' +
          encodeURIComponent(settings.issuerUrl + request.originalUrl),
      );
    }

    if (status === HttpStatus.NOT_FOUND) {
      return response
        .status(status)
        .json({ message: error.message, statusCode: status });
    }

    if (status === HttpStatus.UNAUTHORIZED && error.message === NEGOTIATE) {
      response.header(WWW_NEGOTIATE_HEADER_KEY, NEGOTIATE);
      return response
        .status(status)
        .json({ message: error.message, statusCode: status });
    }

    if (status === HttpStatus.INTERNAL_SERVER_ERROR) {
      const message = error.message;
      return response.status(status).json({ error: message });
    }

    const message =
      error instanceof HttpException ? error.getResponse() : error.message;

    if (message && message[RESPONSE]) {
      message[RESPONSE] = undefined;
      return response.status(status).json(message);
    }

    return response.status(status).json({ message });
  }
}
