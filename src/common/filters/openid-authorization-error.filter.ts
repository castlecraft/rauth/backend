import { Catch } from '@nestjs/common';
import AuthorizationError from 'oauth2orize-openid/lib/errors/authorizationerror';
import { AuthorizationErrorFilter } from './authorization-error.filter';

@Catch(AuthorizationError)
export class OpenIDAuthorizationErrorFilter extends AuthorizationErrorFilter {}
