import {
  HttpException,
  HttpStatus,
  InternalServerErrorException,
} from '@nestjs/common';
import { TokenError } from 'oauth2orize';
import {
  CANNOT_DELETE_ADMINISTRATOR,
  DELETE_USER_DISABLED,
  EMAIL_FOR_VERIFICATION_NOT_FOUND,
  EVENTS_NOT_CONNECTED,
  EXPIRED_VERIFICATION_CODE_USED,
  INVALID_ADMIN_PASSWORD,
  INVALID_AUTH_CODE,
  INVALID_CLIENT,
  INVALID_CODE_CHALLENGE,
  INVALID_CORS_DOMAIN,
  INVALID_OTP,
  INVALID_ROLE,
  INVALID_SCOPE,
  INVALID_USER,
  INVALID_VERIFICATION_CODE,
  JWKS_NOT_FOUND,
  NEW_PASSWORD_CANNOT_BE_SAME_AS_CURRENT_PASSWORD,
  PASSWORD_LESS_ALREADY_ENABLED,
  PASSWORD_LESS_NOT_ENABLED,
  PASSWORDLESS_LOGIN_REQUIRES_2FA_OR_OTP_TO_BE_ENABLED,
  PHONE_ALREADY_REGISTERED,
  PHONE_REGISTRATION_NOT_ALLOWED,
  PKCE_NOT_ENABLED_FOR_PUBLIC_CLIENT,
  PROFILE_OTP_ALREADY_ENABLED,
  PROFILE_OTP_NOT_ENABLED,
  SET_OR_CHANGE_PASSWORD_IS_RESTRICTED_FOR_YOUR_ACCOUNT,
  SETTINGS_NOT_FOUND,
  SYSTEM_EMAIL_ACCOUNT_NOT_FOUND,
  TWO_FACTOR_ALREADY_ENABLED,
  TWO_FACTOR_NOT_ENABLED,
  USER_ALREADY_DISABLED,
  USER_ALREADY_ENABLED,
  USER_ALREADY_EXISTS,
  USER_OTP_MUST_BE_ENABLED,
} from '../../constants/messages';
import { OAuth2ErrorMessage } from './oauth2-error.messages';

export class InvalidScopeException extends HttpException {
  constructor() {
    super(INVALID_SCOPE, HttpStatus.BAD_REQUEST);
  }
}

export class InvalidClientException extends HttpException {
  constructor() {
    super(INVALID_CLIENT, HttpStatus.BAD_REQUEST);
  }
}

export class InvalidAuthorizationCodeException extends HttpException {
  constructor() {
    super(INVALID_AUTH_CODE, HttpStatus.BAD_REQUEST);
  }
}

export class UserAlreadyExistsException extends HttpException {
  constructor() {
    super(USER_ALREADY_EXISTS, HttpStatus.BAD_REQUEST);
  }
}

export class InvalidUserException extends HttpException {
  constructor() {
    super(INVALID_USER, HttpStatus.BAD_REQUEST);
  }
}

export class PasswordChangeRestrictedException extends HttpException {
  constructor() {
    super(
      SET_OR_CHANGE_PASSWORD_IS_RESTRICTED_FOR_YOUR_ACCOUNT,
      HttpStatus.FORBIDDEN,
    );
  }
}

export class TwoFactorEnabledException extends HttpException {
  constructor() {
    super(TWO_FACTOR_ALREADY_ENABLED, HttpStatus.BAD_REQUEST);
  }
}

export class TwoFactorNotEnabledException extends HttpException {
  constructor() {
    super(TWO_FACTOR_NOT_ENABLED, HttpStatus.BAD_REQUEST);
  }
}

export class InvalidOTPException extends HttpException {
  constructor() {
    super(INVALID_OTP, HttpStatus.BAD_REQUEST);
  }
}

export class UserOTPNotEnabledException extends HttpException {
  constructor() {
    super(USER_OTP_MUST_BE_ENABLED, HttpStatus.BAD_REQUEST);
  }
}

export class SettingsNotFoundException extends HttpException {
  constructor() {
    super(SETTINGS_NOT_FOUND, HttpStatus.BAD_REQUEST);
  }
}

export class JWKSNotFoundException extends HttpException {
  constructor() {
    super(JWKS_NOT_FOUND, HttpStatus.BAD_REQUEST);
  }
}

export class InvalidRoleException extends HttpException {
  constructor() {
    super(INVALID_ROLE, HttpStatus.BAD_REQUEST);
  }
}

export class CannotDeleteAdministratorException extends HttpException {
  constructor() {
    super(CANNOT_DELETE_ADMINISTRATOR, HttpStatus.BAD_REQUEST);
  }
}

export class InvalidCodeChallengeException extends HttpException {
  constructor() {
    super(INVALID_CODE_CHALLENGE, HttpStatus.BAD_REQUEST);
  }
}

export class PasswordLessLoginNotEnabledException extends HttpException {
  constructor() {
    super(PASSWORD_LESS_NOT_ENABLED, HttpStatus.BAD_REQUEST);
  }
}

export class PasswordLessLoginAlreadyEnabledException extends HttpException {
  constructor() {
    super(PASSWORD_LESS_ALREADY_ENABLED, HttpStatus.BAD_REQUEST);
  }
}

export class PasswordlessLoginRequiresEnabled2FAOrOTPException extends HttpException {
  constructor() {
    super(
      PASSWORDLESS_LOGIN_REQUIRES_2FA_OR_OTP_TO_BE_ENABLED,
      HttpStatus.BAD_REQUEST,
    );
  }
}

export class ProfileOTPAlreadyEnabledException extends HttpException {
  constructor() {
    super(PROFILE_OTP_ALREADY_ENABLED, HttpStatus.BAD_REQUEST);
  }
}

export class ProfileOTPNotEnabledException extends HttpException {
  constructor() {
    super(PROFILE_OTP_NOT_ENABLED, HttpStatus.BAD_REQUEST);
  }
}

export class UserAlreadyEnabledException extends HttpException {
  constructor() {
    super(USER_ALREADY_ENABLED, HttpStatus.BAD_REQUEST);
  }
}

export class UserAlreadyDisabledException extends HttpException {
  constructor() {
    super(USER_ALREADY_DISABLED, HttpStatus.BAD_REQUEST);
  }
}

export class CommunicationServerNotFoundException extends HttpException {
  constructor() {
    super(SYSTEM_EMAIL_ACCOUNT_NOT_FOUND, HttpStatus.BAD_GATEWAY);
  }
}
// Passwordless login requires 2FA or OTP to be enabled
export class PhoneAlreadyRegisteredException extends HttpException {
  constructor() {
    super(PHONE_ALREADY_REGISTERED, HttpStatus.BAD_REQUEST);
  }
}

export class EventsNotConnectedException extends HttpException {
  constructor() {
    super(EVENTS_NOT_CONNECTED, HttpStatus.BAD_REQUEST);
  }
}

export class PhoneRegistrationNotAllowedException extends HttpException {
  constructor() {
    super(PHONE_REGISTRATION_NOT_ALLOWED, HttpStatus.BAD_REQUEST);
  }
}

export class InvalidVerificationCode extends HttpException {
  constructor() {
    super(INVALID_VERIFICATION_CODE, HttpStatus.BAD_REQUEST);
  }
}

export class EmailForVerificationNotFound extends HttpException {
  constructor() {
    super(EMAIL_FOR_VERIFICATION_NOT_FOUND, HttpStatus.BAD_REQUEST);
  }
}

export class UserDeleteDisabled extends HttpException {
  constructor() {
    super(DELETE_USER_DISABLED, HttpStatus.BAD_REQUEST);
  }
}

export class InvalidRequestToDeleteUser extends HttpException {
  constructor() {
    super(JWKS_NOT_FOUND, HttpStatus.BAD_REQUEST);
  }
}

export class VerificationExpiredOrInvalid extends HttpException {
  constructor() {
    super(EXPIRED_VERIFICATION_CODE_USED, HttpStatus.BAD_REQUEST);
  }
}

export class PasswordSameAsCurrentException extends HttpException {
  constructor() {
    super(
      NEW_PASSWORD_CANNOT_BE_SAME_AS_CURRENT_PASSWORD,
      HttpStatus.BAD_REQUEST,
    );
  }
}

export class AdminPasswordException extends HttpException {
  constructor() {
    super(INVALID_ADMIN_PASSWORD, HttpStatus.BAD_REQUEST);
  }
}

export class InvalidCORSDomainException extends HttpException {
  constructor(domain = '') {
    super((INVALID_CORS_DOMAIN + domain).trim(), HttpStatus.BAD_REQUEST);
  }
}

export class PKCEEnforcementError extends HttpException {
  constructor() {
    super(PKCE_NOT_ENABLED_FOR_PUBLIC_CLIENT, HttpStatus.BAD_REQUEST);
  }
}

export class MfaTokenError extends TokenError {
  mfaToken = '';
  constructor(
    mfaToken: string,
    message?: string,
    code?: OAuth2ErrorMessage,
    uri?: string,
    status?: HttpStatus,
  ) {
    super(message, code, uri, status);
    this.mfaToken = mfaToken;
  }
}

export class PasswordDecryptionException extends InternalServerErrorException {
  constructor(error?: unknown) {
    super(error);
  }
}
