export enum OAuth2ErrorMessage {
  InvalidRequest = 'invalid_request',
  UnauthorizedClient = 'unauthorized_client',
  AccessDenied = 'access_denied',
  UnsupportedResponseType = 'unsupported_response_type',
  InvalidScope = 'invalid_scope',
  TemporarilyUnavailable = 'temporarily_unavailable',
  InvalidClient = 'invalid_client',
  InvalidGrant = 'invalid_grant',
  UnsupportedGrantType = 'unsupported_grant_type',
  ServerError = 'server_error',
  MfaRequired = 'mfa_required',
}
