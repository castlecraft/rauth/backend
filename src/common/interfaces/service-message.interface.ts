import { ConnectedService } from './connected-service.interface';

export interface ServiceMessage {
  service: string;
  session: boolean;
  communication: boolean;
  services?: ConnectedService[];
  enableChoosingAccount?: boolean;
  enableUserPhone?: boolean;
  infrastructureConsoleClientId: string;
  issuerUrl?: string;
  logoUrl?: string;
  faviconUrl?: string;
  privacyUrl?: string;
  helpUrl?: string;
  termsUrl?: string;
  copyrightMessage?: string;
  forgotPasswordMessage?: string;
  forgotPasswordSubject?: string;
  completeSignupMessage?: string;
  completeSignupSubject?: string;
  verifyExistingEmailMessage?: string;
  verifyExistingEmailSubject?: string;
  verifyNewEmailMessage?: string;
  verifyNewEmailSubject?: string;
  loginOtpMessage?: string;
  loginOtpSubject?: string;
  passwordExpiryMessage?: string;
  passwordExpirySubject?: string;
  allowedScopes?: string[];
  disableSignup?: boolean;
}
