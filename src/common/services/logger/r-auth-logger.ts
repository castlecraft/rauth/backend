import { ConsoleLogger, LogLevel } from '@nestjs/common';
import { ConfigService, LOG_LEVEL } from '../../../config/config.service';
import { APP_NAME } from '../../../constants/app-strings';

export class RAuthLogger extends ConsoleLogger {
  appName = APP_NAME;

  constructor(readonly config: ConfigService) {
    super();
    const logLevels = (config.get(LOG_LEVEL)?.split(/\s*,\s*/) ||
      []) as LogLevel[];
    this.context = this.appName;
    this.options = { logLevels };
  }

  override formatPid(pid: number): string {
    return `[${this.appName}] ${pid}  - `;
  }

  override setLogLevels(levels: LogLevel[] = ['verbose']): void {
    this.options.logLevels = levels;
  }
}
