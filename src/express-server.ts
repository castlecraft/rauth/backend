import { ForbiddenException, INestApplication } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { ExpressAdapter } from '@nestjs/platform-express';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import MongoStore from 'connect-mongo';
import cookieParser from 'cookie-parser';
import express from 'express';
import expressSession from 'express-session';
import fs from 'fs';
import helmet from 'helmet';
import passport from 'passport';
import { join } from 'path';

import { AppModule } from './app.module';
import {
  AuthDataScheduleService,
  KeyPairGeneratorService,
  TokenSchedulerService,
} from './auth/schedulers';
import { AuthCodeSchedulerService } from './auth/schedulers/auth-code-scheduler/auth-code-scheduler.service';
import { DeleteUnverifiedUsersService } from './auth/schedulers/delete-unverified-users/delete-unverified-users.service';
import { PasswordChangePolicyService } from './auth/schedulers/password-change-policy/password-change-policy.service';
import { SESSION_CONNECTION } from './common/database.provider';
import { RAuthLogger } from './common/services/logger/r-auth-logger';
import {
  COOKIE_MAX_AGE,
  ConfigService,
  ENABLE_CORS,
  ENABLE_CRON,
  ENABLE_SWAGGER,
  EnableDisable,
  NODE_ENV,
  PROD_ENV_VALUE,
  SESSION_NAME,
  SESSION_SECRET,
} from './config/config.service';
import { API, SWAGGER_ROUTE, VIEWS_DIR } from './constants/app-strings';
import { CORS_DOMAIN_NOT_ALLOWED } from './constants/messages';
import {
  JWKS_ENDPOINT,
  OPENID_CONFIGURATION_ENDPOINT,
} from './constants/url-strings';
import { i18n } from './i18n/i18n.config';
import { CORSDomainService } from './system-settings/entities/cors-domain/cors-domain.service';

export const APP_PORT = Number(process.env.PORT || '3000');

export class ExpressServer {
  public server: express.Express;

  constructor(private config: ConfigService) {
    this.server = express();
  }

  setupSecurity() {
    // Helmet
    this.server.use(helmet());

    // Enable Trust Proxy for session to work
    // https://github.com/expressjs/session/issues/281
    this.server.set('trust proxy', 1);
  }

  setupSession(app: INestApplication) {
    this.server.use(cookieParser(this.config.get(SESSION_SECRET)));

    const cookie = {
      maxAge: Number(this.config.get(COOKIE_MAX_AGE)),
      httpOnly: true,
      secure: true,
    };

    if (this.config.get(NODE_ENV) !== this.config.get(PROD_ENV_VALUE)) {
      cookie.secure = false;
      cookie.httpOnly = false;
    }

    const store = app.get<MongoStore>(SESSION_CONNECTION);

    const sessionConfig = {
      name: this.config.get(SESSION_NAME),
      secret: this.config.get(SESSION_SECRET),
      store,
      cookie,
      saveUninitialized: false,
      resave: false,
      proxy: true, // https://github.com/expressjs/session/issues/281
      // unset: 'destroy'
    };

    this.server.use(expressSession(sessionConfig));
    this.server.use(passport.initialize());
    this.server.use(passport.session());
  }

  setupI18n() {
    this.server.use(i18n.init);
  }

  setupSwagger(app: INestApplication) {
    if (this.config.get(ENABLE_SWAGGER) === EnableDisable.ON) {
      const version = JSON.parse(
        fs.readFileSync(join(process.cwd(), 'package.json'), 'utf-8'),
      ).version;

      const options = new DocumentBuilder()
        .setTitle('R-Auth')
        .setVersion(version)
        .setDescription(
          i18n.__('OAuth 2.0 OpenID Connect Authorization Server'),
        )
        .build();
      const document = SwaggerModule.createDocument(app, options);
      SwaggerModule.setup(SWAGGER_ROUTE, app, document);
    }
  }

  setupCORS(app: INestApplication) {
    if (this.config.get(ENABLE_CORS) === EnableDisable.OFF) {
      const corsDomain = app.get<CORSDomainService>(CORSDomainService);
      app.enableCors({
        origin: (origin, callback) => {
          corsDomain.findOne({ domain: origin }).then(domain => {
            if (domain || origin === undefined) {
              return callback(null, domain);
            }
            return callback(new ForbiddenException(CORS_DOMAIN_NOT_ALLOWED));
          });
        },
      });
    } else {
      app.enableCors();
    }
  }

  static setupViewEngine(app: INestApplication) {
    app.getHttpAdapter().setBaseViewsDir(VIEWS_DIR);
    app.getHttpAdapter().setViewEngine('hbs');
  }

  static setupCron(config: ConfigService, app: INestApplication) {
    if (config.get(ENABLE_CRON) === EnableDisable.ON) {
      const authCodeCron = app.get(AuthCodeSchedulerService);
      authCodeCron.start();

      const authDataCron = app.get(AuthDataScheduleService);
      authDataCron.start();

      const unverifiedUserCron = app.get(DeleteUnverifiedUsersService);
      unverifiedUserCron.start();

      const passwordPolicyCron = app.get(PasswordChangePolicyService);
      passwordPolicyCron.start();

      const keyPairCron = app.get(KeyPairGeneratorService);
      keyPairCron.start();

      const tokenCron = app.get(TokenSchedulerService);
      tokenCron.start();
    }
  }
}

export async function getApp() {
  const config = new ConfigService();
  const authServer = new ExpressServer(config);
  authServer.setupSecurity();
  authServer.setupI18n();

  const app = await NestFactory.create(
    AppModule,
    new ExpressAdapter(authServer.server),
    { logger: new RAuthLogger(config) },
  );

  // Set api prefix
  app.setGlobalPrefix(API, {
    exclude: [OPENID_CONFIGURATION_ENDPOINT, JWKS_ENDPOINT],
  });

  // Enable CORS
  authServer.setupCORS(app);

  // Setup Swagger
  authServer.setupSwagger(app);

  // Setup Session
  authServer.setupSession(app);

  // Handlebars View engine
  ExpressServer.setupViewEngine(app);

  // Setup Cron
  ExpressServer.setupCron(config, app);

  return app;
}
