import { Document } from 'mongoose';

export interface Scope extends Document {
  uuid?: string;
  name?: string;
  description?: string;
  creation?: Date;
  createdBy?: string;
  modified?: Date;
  modifiedBy?: string;
}
