import { Document } from 'mongoose';

export interface Client extends Document {
  uuid?: string;
  creation?: Date;
  modified?: Date;
  createdBy?: string;
  modifiedBy?: string;
  name?: string;
  clientId?: string;
  clientSecret?: string;
  isTrusted?: number;
  autoApprove?: boolean;
  redirectUris?: string[];
  allowedScopes?: string[];
  userDeleteEndpoint?: string;
  tokenDeleteEndpoint?: string;
  changedClientSecret?: string;
  customLoginRoute?: string;
  authenticationMethod?: ClientAuthentication;
  disabled?: boolean;
  accessTokenValidity?: number;
  refreshTokenExpiresInHours?: number;
  restrictTokenUsage?: boolean;
  is2FAMandated?: boolean;
  authCodeExpiresInMinutes?: number;
  revokeOnRefresh?: boolean;
  pkceMethods?: PKCEMethods[];
}

export enum ClientAuthentication {
  BasicHeader = 'BASIC_HEADER',
  BodyParam = 'BODY_PARAM',
  PublicClient = 'PUBLIC_CLIENT',
}

export enum PKCEMethods {
  S256 = 's256',
  PLAIN = 'plain',
}
