import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';
import { randomBytes } from 'crypto';
import { ClientAuthentication, PKCEMethods } from './client.interface';

const schema = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4, unique: true, sparse: true },
    creation: Date,
    modified: Date,
    createdBy: String,
    modifiedBy: String,
    name: String,
    clientId: { type: String, default: uuidv4, unique: true, sparse: true },
    clientSecret: { type: String, default: randomBytes32 },
    isTrusted: Number,
    autoApprove: Boolean,
    redirectUris: [String],
    allowedScopes: [String],
    userDeleteEndpoint: String,
    tokenDeleteEndpoint: String,
    changedClientSecret: String,
    customLoginRoute: String,
    authenticationMethod: {
      type: String,
      default: ClientAuthentication.PublicClient,
    },
    disabled: { type: Boolean, default: false },
    accessTokenValidity: Number,
    refreshTokenExpiresInHours: Number,
    restrictTokenUsage: { type: Boolean, default: false },
    is2FAMandated: { type: Boolean, default: false },
    authCodeExpiresInMinutes: Number,
    revokeOnRefresh: { type: Boolean, default: false },
    enforcePKCE: { type: Boolean, default: true },
    pkceMethods: { type: [String], default: [PKCEMethods.S256] },
  },
  { collection: 'client', versionKey: false },
);

export const Client = schema;

export const CLIENT = 'Client';

export const ClientModel = mongoose.model(CLIENT, Client);

export function randomBytes32() {
  return randomBytes(32).toString('hex');
}
