import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { ClientSecretUpdatedEvent } from './client-secret-updated.event';
import { ClientService } from '../../entities/client/client.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(ClientSecretUpdatedEvent)
export class ClientSecretUpdatedHandler
  implements IEventHandler<ClientSecretUpdatedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly client: ClientService,
    private readonly config: ConfigService,
  ) {}

  handle(command: ClientSecretUpdatedEvent) {
    const { client } = command;
    this.client
      .save(client)
      .then(secretUpdated => {
        this.logger.log(ClientSecretUpdatedEvent.name, this.constructor.name);
      })
      .catch(error => {
        this.logger.error(error, error, this.constructor.name);
      });
  }
}
