import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { ClientModifiedEvent } from './client-modified.event';
import { ClientService } from '../../entities/client/client.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';

@EventsHandler(ClientModifiedEvent)
export class ClientModifiedHandler
  implements IEventHandler<ClientModifiedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly client: ClientService,
    private readonly config: ConfigService,
  ) {}

  handle(command: ClientModifiedEvent) {
    const { client } = command;
    this.client
      .save(client)
      .then(added => {
        this.logger.log(ClientModifiedEvent.name, this.constructor.name);
        this.logger.debug(
          `${ClientModifiedEvent.name} client ${added.uuid}`,
          this.constructor.name,
        );
      })
      .catch(error => {
        this.logger.error(error, error, this.constructor.name);
      });
  }
}
