import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { from } from 'rxjs';
import { OAuth2ScopeRemovedEvent } from './oauth2scope-removed.event';
import { ScopeService } from '../../entities/scope/scope.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(OAuth2ScopeRemovedEvent)
export class OAuth2ScopeRemovedHandler
  implements IEventHandler<OAuth2ScopeRemovedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly scope: ScopeService,
    private readonly config: ConfigService,
  ) {}

  handle(event: OAuth2ScopeRemovedEvent) {
    const { scope } = event;
    from(this.scope.remove(scope)).subscribe({
      next: success => {
        this.logger.log(OAuth2ScopeRemovedEvent.name, this.constructor.name);
      },
      error: error => {
        this.logger.error(error, error, this.constructor.name);
      },
    });
  }
}
