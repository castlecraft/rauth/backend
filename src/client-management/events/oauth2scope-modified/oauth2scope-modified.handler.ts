import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { from } from 'rxjs';
import { OAuth2ScopeModifiedEvent } from './oauth2scope-modified.event';
import { ScopeService } from '../../entities/scope/scope.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(OAuth2ScopeModifiedEvent)
export class OAuth2ScopeModifiedHandler
  implements IEventHandler<OAuth2ScopeModifiedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly scope: ScopeService,
    private readonly config: ConfigService,
  ) {}

  handle(event: OAuth2ScopeModifiedEvent) {
    const { scope } = event;
    from(this.scope.save(scope)).subscribe({
      next: success => {
        this.logger.log(OAuth2ScopeModifiedEvent.name, this.constructor.name);
      },
      error: error => {
        this.logger.error(error, error, this.constructor.name);
      },
    });
  }
}
