import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { OAuth2ClientRemovedEvent } from './oauth2client-removed.event';
import { from } from 'rxjs';
import { ClientService } from '../../entities/client/client.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';

@EventsHandler(OAuth2ClientRemovedEvent)
export class OAuth2ClientRemovedHandler
  implements IEventHandler<OAuth2ClientRemovedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly client: ClientService,
    private readonly config: ConfigService,
  ) {}

  handle(event: OAuth2ClientRemovedEvent) {
    const { client } = event;
    from(this.client.deleteByClientId(client.clientId)).subscribe({
      next: success => {
        this.logger.log(OAuth2ClientRemovedEvent.name, this.constructor.name);
      },
      error: error => {
        this.logger.error(error, error, this.constructor.name);
      },
    });
  }
}
