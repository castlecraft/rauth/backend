import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { from } from 'rxjs';
import { OAuth2ScopeAddedEvent } from './oauth2scope-added.event';
import { ScopeService } from '../../entities/scope/scope.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(OAuth2ScopeAddedEvent)
export class OAuth2ScopeAddedHandler
  implements IEventHandler<OAuth2ScopeAddedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly scope: ScopeService,
    private readonly config: ConfigService,
  ) {}

  handle(event: OAuth2ScopeAddedEvent) {
    const { scope } = event;
    from(this.scope.save(scope)).subscribe({
      next: success => {
        this.logger.log(OAuth2ScopeAddedEvent.name, this.constructor.name);
      },
      error: error => {
        this.logger.error(error, error, this.constructor.name);
      },
    });
  }
}
