import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { ClientSecretVerifiedEvent } from './client-secret-verified.event';
import { ClientService } from '../../entities/client/client.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(ClientSecretVerifiedEvent)
export class ClientSecretVerifiedHandler
  implements IEventHandler<ClientSecretVerifiedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly client: ClientService,
    private readonly config: ConfigService,
  ) {}

  handle(command: ClientSecretVerifiedEvent) {
    const { client } = command;
    this.client
      .save(client)
      .then(secretVerified => {
        return this.client.updateOne(
          { clientId: client.clientId },
          { $unset: { changedClientSecret: 1 } },
        );
      })
      .then(clientUpdated => {
        this.logger.log(ClientSecretVerifiedEvent.name, this.constructor.name);
      })
      .catch(error => {
        this.logger.error(error, error, this.constructor.name);
      });
  }
}
