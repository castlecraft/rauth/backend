import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { ClientAddedEvent } from './client-added.event';
import { ClientService } from '../../entities/client/client.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(ClientAddedEvent)
export class ClientAddedHandler implements IEventHandler<ClientAddedEvent> {
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly client: ClientService,
    private readonly config: ConfigService,
  ) {}
  handle(command: ClientAddedEvent) {
    const { client } = command;
    this.client
      .save(client)
      .then(modified => {
        this.logger.log(ClientAddedEvent.name, this.constructor.name);
        this.logger.debug(
          `${ClientAddedEvent.name} client ${modified.uuid}`,
          this.constructor.name,
        );
      })
      .catch(error => {
        this.logger.error(error, error, this.constructor.name);
      });
  }
}
