import { DeleteUnverifiedUsersService } from '../auth/schedulers/delete-unverified-users/delete-unverified-users.service';
import { APP_PORT, getApp } from '../express-server';

async function start() {
  const app = await getApp();
  const cron = app.get(DeleteUnverifiedUsersService);
  cron.start();
  await app.listen(APP_PORT);
}

start();
