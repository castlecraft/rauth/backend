import { KeyPairGeneratorService } from '../auth/schedulers';
import { APP_PORT, getApp } from '../express-server';

async function start() {
  const app = await getApp();
  const cron = app.get(KeyPairGeneratorService);
  cron.start();
  await app.listen(APP_PORT);
}

start();
