import { PasswordChangePolicyService } from '../auth/schedulers/password-change-policy/password-change-policy.service';
import { APP_PORT, getApp } from '../express-server';

async function start() {
  const app = await getApp();
  const cron = app.get(PasswordChangePolicyService);
  cron.start();
  await app.listen(APP_PORT);
}

start();
