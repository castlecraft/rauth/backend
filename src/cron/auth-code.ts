import { AuthCodeSchedulerService } from '../auth/schedulers/auth-code-scheduler/auth-code-scheduler.service';
import { APP_PORT, getApp } from '../express-server';

async function start() {
  const app = await getApp();
  const cron = app.get(AuthCodeSchedulerService);
  cron.start();
  await app.listen(APP_PORT);
}

start();
