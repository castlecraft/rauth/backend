import { TokenSchedulerService } from '../auth/schedulers';
import { APP_PORT, getApp } from '../express-server';

async function start() {
  const app = await getApp();
  const cron = app.get(TokenSchedulerService);
  cron.start();
  await app.listen(APP_PORT);
}

start();
