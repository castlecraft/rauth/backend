import { i18n } from '../i18n/i18n.config';

export const SETUP_ALREADY_COMPLETE = 'Setup already complete';
export const COMMUNICATION_SERVER = 'Communication Server';
export const PLEASE_RUN_SETUP = 'Please Run Setup';
export const DELETED = 'Resource deleted';
export const INVALID_FILE_OR_STORAGE_UUID = 'Invalid Request bad file or uuid';
export const USER_ALREADY_EXISTS = i18n.__('User already exists');
export const INVALID_CLIENT = i18n.__('Invalid Client');
export const CLIENT_DISABLED = i18n.__('Client Disabled');
export const INVALID_USER = i18n.__('Invalid User');
export const SET_OR_CHANGE_PASSWORD_IS_RESTRICTED_FOR_YOUR_ACCOUNT = i18n.__(
  'Set or Change Password is restricted for your account.',
);
export const INVALID_SCOPE = i18n.__('Invalid Scope');
export const INVALID_ADMIN_PASSWORD = i18n.__('Invalid Admin Password');
export const INVALID_AUTH_CODE = i18n.__('Invalid Authorization Code');
export const MFA_REQUIRED = i18n.__('Multifactor authentication required');
export const INVALID_MFA_TOKEN = i18n.__('Invalid mfa_token');
export const CLIENT_REQUIRES_2FA_FOR_USER = i18n.__(
  'Client requires 2FA enabled for the user',
);
export const INVALID_CODE_CHALLENGE_METHOD = i18n.__(
  'Invalid Code Challenge Method',
);
export const INVALID_CODE_CHALLENGE = i18n.__('Invalid Code Challenge');

export const TWO_FACTOR_ALREADY_ENABLED = i18n.__('2FA already enabled');
export const TWO_FACTOR_NOT_ENABLED = i18n.__('2FA not enabled');
export const INVALID_OTP = i18n.__('Invalid OTP');
export const USER_OTP_MUST_BE_ENABLED = i18n.__('User OTP must be enabled');
export const SETTINGS_NOT_FOUND = i18n.__('Settings not found');
export const JWKS_NOT_FOUND = i18n.__('JWKS not found');
export const INVALID_ROLE = i18n.__('Invalid Role');
export const CANNOT_DELETE_ADMINISTRATOR = i18n.__(
  'Cannot Delete Administrators',
);
export const PASSWORD_LESS_NOT_ENABLED = i18n.__(
  'Password less login is not enabled',
);
export const PASSWORD_LESS_ALREADY_ENABLED = i18n.__(
  'Password less login is already enabled',
);
export const PASSWORDLESS_LOGIN_REQUIRES_2FA_OR_OTP_TO_BE_ENABLED = i18n.__(
  'Passwordless login requires 2FA or OTP to be enabled',
);
export const PROFILE_OTP_ALREADY_ENABLED = i18n.__(
  'Profile OTP is already enabled',
);
export const PROFILE_OTP_NOT_ENABLED = i18n.__('Profile OTP is not enabled');
export const USER_ALREADY_ENABLED = i18n.__('User already enabled');
export const USER_ALREADY_DISABLED = i18n.__('User already disabled');
export const SYSTEM_EMAIL_ACCOUNT_NOT_FOUND = i18n.__(
  'System email account not found',
);
export const PHONE_ALREADY_REGISTERED = i18n.__(
  'Phone number already registered',
);
export const EVENTS_NOT_CONNECTED = i18n.__('Events service not connected');
export const PHONE_REGISTRATION_NOT_ALLOWED = i18n.__(
  'Server does not allow phone registration',
);
export const INVALID_VERIFICATION_CODE = i18n.__('Invalid Verification Code');
export const EMAIL_FOR_VERIFICATION_NOT_FOUND = i18n.__(
  'Email for verification not found',
);
export const DELETE_USER_DISABLED = i18n.__(
  'Deleting user account is disabled',
);
export const USER_OR_CLIENT_MISSING = i18n.__('user or client missing');
export const EXPIRED_VERIFICATION_CODE_USED = i18n.__(
  'Invalid or expired verification code used. Please signup again or reset password for the user.',
);
export const NEW_PASSWORD_CANNOT_BE_SAME_AS_CURRENT_PASSWORD =
  'New Password Cannot be same as Current Password';
export const MFA_ENABLED_FOR_USER = i18n.__('MFA enabled for the user');
export const INVALID_CORS_DOMAIN = i18n.__('Invalid CORS Domain ');
export const CORS_DOMAIN_NOT_ALLOWED = i18n.__('CORS Domain not allowed');
export const PKCE_NOT_ENABLED_FOR_PUBLIC_CLIENT = i18n.__(
  'PKCE Must be Enforced For a Public Client',
);
