import { join } from 'path';

export const ACCOUNT_CHOOSE_ROUTE = '/account/choose';
export const ACCOUNTS_ROUTE = '/account'; // html5 route
export const ACL_PUBLIC_PERMISSION = 'public-read';
export const ADMIN = 'admin';
export const ADMINISTRATOR = 'administrator';
export const API = 'api';
export const APP_NAME = 'R-Auth';
export const APPLICATION_XML = 'application/xml';
export const AUTH_DATA_REMOVED = 'auth data removed';
export const AUTHORIZATION = 'authorization';
export const AVATAR_KEY = 'avatar_key';
export const CALLBACK_ROUTE = '/callback';
export const COMPLETE_SIGNUP_MESSAGE =
  'To complete signup <a href="{{ data.verificationUrl }}">Click here</a>';
export const COMPLETE_SIGNUP_SUBJECT =
  'Please complete sign up for {{ data.email }}';
export const CONTENT_TYPE_HEADER = 'Content-Type';
export const DB = 'db';
export const EMAIL = 'email';
export const FIVE_THOUSAND_MS = 5000;
export const FORGOT_PASSWORD_SUBJECT =
  'Forgot Password? Generate new password for {{ data.email }}';
export const FORGOT_ROUTE = '/forgot';
export const GENERATE_NEW_PASSWORD_MESSAGE =
  'To generate new password <a href="{{ data.generateForgottenPasswordUrl }}">click here</a>';
export const PASSWORD_EXPIRY_SUBJECT = 'Password Expiry Alert';
export const PASSWORD_EXPIRY_MESSAGE =
  'Your password is going to expire soon. Please change it to avoid being logged out.';
export const HOUR_IN_SECONDS = 3600;
export const INFRASTRUCTURE_CONSOLE = 'Infrastructure Console';
export const KERBEROS = 'kerberos';
export const LDAP = 'ldap';
export const LOGIN = 'login';
export const LOGIN_OTP_MESSAGE =
  'OTP for {{ data.email }} is {{ data.hotp }}. OTP expires at {{ data.expiry }}. Do not share the OTP';
export const LOGIN_OTP_SUBJECT = 'Login OTP for {{ data.email }}';
export const LOGIN_ROUTE = '/login';
export const LOGOUT = 'logout successful';
export const NEGOTIATE = 'Negotiate';
export const NONE = 'none';
export const ONE_NUMBER = 1;
export const ONE_THOUSAND_MS = 1000;
export const OTP_ENABLED = 'OTP is enabled';
export const OTPCOUNTER = 'otpCounter';
export const OTP_VERIFIED = 'OTP Verified';
export const PASSWORD = 'password';
export const PASSWORDLESS = 'passwordless';
export const PHONE = 'phone';
export const PHONE_NUMBER = 'phone_number';
export const PICTURE = 'picture';
export const PUBLIC = 'public';
export const RAUTH_SOURCE_ID = 'rauth_source_id';
export const RAUTH_SOURCE_TYPE = 'rauth_source_type';
export const RAUTH_PASSWORD_CHANGE = 'rauth_password_change';
export const RAUTH_EMAIL_SENT = 'rauth_email_sent';
export const REDIRECT_PATH = 'redirectPath';
export const RESPONSE = 'response';
export const ROLES = 'roles';
export const SCOPE_EMAIL = 'email';
export const SCOPE_OPENID = 'openid';
export const SCOPE_PHONE = 'phone';
export const SCOPE_PROFILE = 'profile';
export const SCOPE_ROLES = 'roles';
export const SCOPE_SELF_SERVICE = 'self_service';
export const SELECT_ACCOUNT = 'select_account';
export const SEND_EMAIL = 'send_email';
export const SERVICE = 'authorization-server';
export const SHAREDSECRET = 'sharedSecret';
export const SIGNUP_ROUTE = '/signup';
export const SOCIAL_LOGIN = 'social_login';
export const SUCCESS = 'Success';
export const SWAGGER_ROUTE = 'api-docs';
export const TEN_NUMBER = 10;
export const THIRTY_DAYS_IN_HOURS = 720;
export const THIRTY_NUMBER = 30;
export const TOKEN = 'token';
export const TRUE = 'true';
export const TWENTY_FOUR_NUMBER = 24;
export const TWOFACTORTEMPSECRET = 'twoFactorTempSecret';
export const USER_ADDED = 'user created successfully';
export const USER_CLAIMS = 'Claim added';
export const USER_CLAIMS_ADDED = 'Claim added successfully';
export const USER_CLAIMS_REMOVED = 'Claim removed successfully';
export const USER_PROPERTIES = [
  { field: 'uuid', mapTo: 'Uuid' },
  { field: 'disabled', mapTo: 'Disabled' },
  { field: 'name', mapTo: 'Name' },
  { field: 'phone', mapTo: 'Phone' },
  { field: 'email', mapTo: 'Email' },
  { field: 'roles', mapTo: 'Roles' },
  { field: 'enable2fa', mapTo: 'Enable2Fa' },
  { field: 'deleted', mapTo: 'Deleted' },
  { field: 'enablePasswordLess', mapTo: 'Enablepasswordless' },
  { field: 'enableOTP', mapTo: 'Enableotp' },
];
export const USER_REMOVED = 'user removed';
export const VERIFY_EXISTING_EMAIL_MESSAGE =
  'To complete verification <a href="{{ data.verificationUrl }}">click here</a>';
export const VERIFY_EXISTING_EMAIL_SUBJECT =
  'Please complete verification for {{ data.password }}';
export const VERIFY_NEW_EMAIL_MESSAGE =
  'To verify email <a href="{{ data.generateForgottenPasswordUrl }}">click here</a>';
export const VERIFY_NEW_EMAIL_SUBJECT =
  'Complete email verification for {{ data.email }}';
export const VERIFY_PASSWORD = 'verify_password';
export const VERIFY_ROUTE = '/verify';
export const VERIFY_USER = 'verify_user';
export const VIEWS_DIR = join(process.cwd(), '/src/views');
export const WWW_NEGOTIATE_HEADER_KEY = 'WWW-Authenticate';
export const ZERO_NUMBER = 0;
export const EMAIL_DELAY_IN_SECONDS = 60;
export const SCRAM_SHA_256 = 'scram-sha-256';
