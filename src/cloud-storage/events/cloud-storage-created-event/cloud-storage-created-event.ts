import { IEvent } from '@nestjs/cqrs';
import { Storage } from '../../entities/storage/storage.interface';

export class CloudStorageCreatedEvent implements IEvent {
  constructor(public readonly cloudStoragePayload: Storage) {}
}
