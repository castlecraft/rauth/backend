import { IEvent } from '@nestjs/cqrs';
import { Storage } from '../../entities/storage/storage.interface';

export class BucketFileDeletedEvent implements IEvent {
  constructor(
    public readonly filename: string,
    public readonly storage: Storage,
    public readonly actor: string,
  ) {}
}
