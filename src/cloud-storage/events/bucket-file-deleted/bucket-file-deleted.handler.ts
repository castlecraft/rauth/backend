import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BucketFileDeletedEvent } from './bucket-file-deleted.event';

@EventsHandler(BucketFileDeletedEvent)
export class BucketFileDeletedHandler
  implements IEventHandler<BucketFileDeletedEvent>
{
  handle(event: BucketFileDeletedEvent) {}
}
