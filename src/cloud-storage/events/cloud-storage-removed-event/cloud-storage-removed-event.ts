import { IEvent } from '@nestjs/cqrs';
import { Storage } from '../../entities/storage/storage.interface';

export class CloudStorageRemovedEvent implements IEvent {
  constructor(public readonly storage: Storage) {}
}
