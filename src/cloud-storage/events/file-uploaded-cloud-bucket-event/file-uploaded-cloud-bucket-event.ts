import { IEvent } from '@nestjs/cqrs';
import { MulterFile } from '../../aggregates/upload-files-cloud-bucket-aggregate/multer-file.interface';
import { Storage } from '../../entities/storage/storage.interface';

export class FileUploadedCloudBucketEvent implements IEvent {
  constructor(
    public readonly uploadedFile: MulterFile,
    public readonly storage: Storage,
    public readonly actor: string,
    public readonly fileACLPermission: string,
  ) {}
}
