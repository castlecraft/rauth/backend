import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { FileUploadedCloudBucketEvent } from './file-uploaded-cloud-bucket-event';

@EventsHandler(FileUploadedCloudBucketEvent)
export class FileUploadedCloudBucketHandler
  implements IEventHandler<FileUploadedCloudBucketEvent>
{
  async handle(event: FileUploadedCloudBucketEvent) {}
}
