import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { ModifyCloudStorageAggregateService } from '../../aggregates/modify-cloud-storage-aggregate/modify-cloud-storage-aggregate.service';
import { ModifyCloudStorageCommand } from './modify-cloud-storage.command';

@CommandHandler(ModifyCloudStorageCommand)
export class ModifyCloudStorageHandler
  implements ICommandHandler<ModifyCloudStorageCommand>
{
  constructor(
    private readonly manager: ModifyCloudStorageAggregateService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: ModifyCloudStorageCommand) {
    const { uuid, modifyCloudParams } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.modifyCloudStorage(uuid, modifyCloudParams);
    aggregate.commit();
  }
}
