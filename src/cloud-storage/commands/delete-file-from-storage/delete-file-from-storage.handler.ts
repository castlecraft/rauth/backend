import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { ClientService } from '../../../client-management/entities/client/client.service';
import { InvalidClientException } from '../../../common/filters/exceptions';
import { UploadFilesCloudBucketAggregateService } from '../../aggregates';
import { DeleteFileFromStorageCommand } from './delete-file-from-storage.command';

@CommandHandler(DeleteFileFromStorageCommand)
export class DeleteFileFromStorageHandler
  implements ICommandHandler<DeleteFileFromStorageCommand>
{
  constructor(
    private readonly client: ClientService,
    private readonly manager: UploadFilesCloudBucketAggregateService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: DeleteFileFromStorageCommand) {
    const { filename, storageUuid, actor } = command;
    const client = await this.client.findOne({ clientId: actor });
    if (!client.isTrusted) {
      throw new InvalidClientException();
    }

    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const response = await aggregate.deleteFileFromBucket(
      filename,
      storageUuid,
      actor,
    );
    aggregate.commit();
    return response;
  }
}
