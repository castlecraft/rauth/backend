import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { ClientService } from '../../../client-management/entities/client/client.service';
import { InvalidClientException } from '../../../common/filters/exceptions';
import { UploadFilesCloudBucketAggregateService } from '../../aggregates/index';
import { UploadFilesCloudBucketCommand } from './upload-files-cloud-bucket.command';

@CommandHandler(UploadFilesCloudBucketCommand)
export class UploadFilesCloudBucketHandler
  implements ICommandHandler<UploadFilesCloudBucketCommand>
{
  constructor(
    private readonly client: ClientService,
    private readonly manager: UploadFilesCloudBucketAggregateService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: UploadFilesCloudBucketCommand) {
    const { uploadedFile, storageUuid, actor, fileACLPermission } = command;

    const client = await this.client.findOne({ clientId: actor });
    if (!client.isTrusted) {
      throw new InvalidClientException();
    }

    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const response = await aggregate.uploadFileToCloudBucket(
      uploadedFile,
      storageUuid,
      actor,
      fileACLPermission,
    );
    aggregate.commit();
    return response;
  }
}
