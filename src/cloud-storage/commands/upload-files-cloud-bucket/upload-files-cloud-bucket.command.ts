import { IEvent } from '@nestjs/cqrs';
import { MulterFile } from '../../aggregates/upload-files-cloud-bucket-aggregate/multer-file.interface';

export class UploadFilesCloudBucketCommand implements IEvent {
  constructor(
    public readonly uploadedFile: MulterFile,
    public readonly storageUuid: string,
    public readonly actor: string,
    public readonly fileACLPermission: string,
  ) {}
}
