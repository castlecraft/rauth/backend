import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  IsOptional,
  IsUrl,
  IsNotEmpty,
  IsBoolean,
} from 'class-validator';
import { i18n } from '../../../i18n/i18n.config';

export class ModifyStorageDto {
  @IsString()
  @IsOptional()
  @ApiProperty({
    description: i18n.__('Cloud Storage API Version'),
    required: true,
    example: '7.1',
  })
  version: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: i18n.__('Cloud Storage Name'),
    required: true,
    example: 'google',
  })
  name: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: i18n.__('Cloud Storage S3 Region'),
    required: true,
    type: 'string',
  })
  region: string;

  @IsString()
  @IsOptional()
  @IsUrl({ require_tld: false })
  @ApiProperty({
    description: i18n.__('Cloud Storage S3 Endpoint'),
    required: true,
    type: 'string',
  })
  endpoint: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: i18n.__('Cloud Storage S3 Access Key'),
    required: true,
    type: 'string',
  })
  accessKey: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: i18n.__('Cloud Storage S3 Secret Key'),
    required: true,
    type: 'string',
  })
  secretKey: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: i18n.__('Cloud Storage Bucket'),
    required: true,
    type: 'string',
  })
  bucket: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('Cloud Storage Base Path in bucket'),
    required: true,
    type: 'string',
  })
  basePath: string;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({
    description: i18n.__('Disable Cloud Storage'),
  })
  disabled?: boolean;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({
    description: i18n.__('Storage uses path-style'),
  })
  isPathStyle: boolean;
}
