import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { STORAGE } from './storage.schema';
import { Model } from 'mongoose';
import { Storage } from './storage.interface';

@Injectable()
export class StorageService {
  constructor(
    @Inject(STORAGE)
    private readonly model: Model<Storage>,
  ) {}

  public async save(params: Storage) {
    const doc = await this.findOne({ uuid: params.uuid });
    if (doc) throw new BadRequestException({ storage: params.uuid });
    const storage = new this.model(params);
    return await storage.save();
  }

  async findAll(): Promise<Storage[]> {
    return await this.model.find();
  }

  public async findOne(params): Promise<Storage> {
    return await this.model.findOne(params);
  }

  public async delete(params): Promise<any> {
    const doc = await this.findOne({ uuid: params.uuid });
    if (!doc) {
      throw new BadRequestException({ storage: params.uuid });
    }
    return await doc.deleteOne();
  }

  async list(
    offset: number = 0,
    limit: number = 10,
    search?: string,
    query?: any,
    sortQuery?: any,
  ) {
    if (search) {
      // Search through multiple keys
      // https://stackoverflow.com/a/41390870
      const nameExp = new RegExp(search, 'i');
      query.$or = ['name', 'uuid'].map(field => {
        const out = {};
        out[field] = nameExp;
        return out;
      });
    }

    const data = this.model
      .find(query, { accessKey: 0, secretKey: 0 })
      .skip(Number(offset))
      .limit(Number(limit))
      .sort(sortQuery);

    return {
      docs: await data.exec(),
      length: await this.model.countDocuments(query),
      offset: Number(offset),
    };
  }

  public async updateOne(uuid, updateQuery) {
    return await this.model.updateOne(uuid, updateQuery);
  }

  public async find(params) {
    return await this.model.find(params);
  }
}
