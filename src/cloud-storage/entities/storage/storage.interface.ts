import { Document } from 'mongoose';

export interface Storage extends Document {
  uuid?: string;
  version?: string;
  name?: string;
  region?: string;
  endpoint?: string;
  accessKey?: string;
  secretKey?: string;
  bucket?: string;
  basePath?: string;
  disabled?: boolean;
  isPathStyle?: boolean;
  creation?: Date;
  createdBy?: string;
  modified?: Date;
  modifiedBy?: string;
}
