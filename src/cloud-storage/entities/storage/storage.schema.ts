import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

const schema = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4, unique: true, sparse: true },
    version: String,
    name: String,
    region: String,
    endpoint: String,
    accessKey: String,
    secretKey: String,
    bucket: String,
    basePath: String,
    creation: Date,
    createdBy: String,
    modified: Date,
    modifiedBy: String,
    disabled: { type: Boolean, default: false },
    isPathStyle: { type: Boolean, default: false },
  },
  { collection: 'object_storage', versionKey: false },
);

export const Storage = schema;

export const STORAGE = 'Storage';

export const StorageModel = mongoose.model(STORAGE, Storage);
