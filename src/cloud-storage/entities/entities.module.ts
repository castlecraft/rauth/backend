import { Module } from '@nestjs/common';
import { StorageService } from './storage/storage.service';
import { CloudStorageModuleEntities } from './entities';
@Module({
  providers: [...CloudStorageModuleEntities, StorageService],
  exports: [...CloudStorageModuleEntities, StorageService],
})
export class CloudStorageEntitiesModule {}
