import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiOperation } from '@nestjs/swagger';

import { Roles } from '../../../auth/decorators/roles.decorator';
import { AuthServerVerificationGuard } from '../../../auth/guards/authserver-verification.guard';
import { BearerTokenGuard } from '../../../auth/guards/bearer-token.guard';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { ListQueryDto } from '../../../common/policies/list-query/list-query';
import { ADMINISTRATOR } from '../../../constants/app-strings';
import { INVALID_FILE_OR_STORAGE_UUID } from '../../../constants/messages';
import { i18n } from '../../../i18n/i18n.config';
import { MulterFile } from '../../aggregates/upload-files-cloud-bucket-aggregate/multer-file.interface';
import { AddCloudStorageCommand } from '../../commands/add-cloud-storage/add-cloud-storage.command';
import { DeleteFileFromStorageCommand } from '../../commands/delete-file-from-storage/delete-file-from-storage.command';
import { ModifyCloudStorageCommand } from '../../commands/modify-cloud-storage/modify-cloud-storage.command';
import { RemoveCloudStorageCommand } from '../../commands/remove-cloud-storage/remove-cloud-storage.command';
import { UploadFilesCloudBucketCommand } from '../../commands/upload-files-cloud-bucket/upload-files-cloud-bucket.command';
import { Storage } from '../../entities/storage/storage.interface';
import { StorageService } from '../../entities/storage/storage.service';
import { StorageValidationDto } from '../../policies';
import { ModifyStorageDto } from '../../policies/modify-cloud-storage-dto/modify-cloud-storage-dto';
import { RetrieveFileDto } from '../../policies/retrieve-file/retrieve-file.dto';
import { ListStorageQuery } from '../../queries/list-storage/list-storage.query';
import { RetrieveFileQuery } from '../../queries/retrieve-file/retrieve-file.query';

@Controller('storage')
export class CloudStorageController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
    private readonly storage: StorageService,
  ) {}

  @Get('v1/list')
  @UseGuards(BearerTokenGuard)
  @UsePipes(new ValidationPipe({ transform: true }))
  async list(@Query() query: ListQueryDto) {
    return await this.queryBus.execute(
      new ListStorageQuery(query.offset, query.limit, query.search, query.sort),
    );
  }

  @Get('v1/get/:uuid')
  @UseGuards(BearerTokenGuard)
  async findOne(@Param('uuid') uuid: string) {
    const storage: Storage = await this.storage.findOne({ uuid });
    if (!storage) throw new BadRequestException(INVALID_FILE_OR_STORAGE_UUID);
    storage.accessKey = undefined;
    storage.secretKey = undefined;
    return storage;
  }

  @Post('v1/add')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  @ApiOperation({
    summary: i18n.__('add cloud storage bucket'),
    description: i18n.__('add s3 api cloud storage'),
  })
  async addStorage(@Body() payload: StorageValidationDto, @Req() req) {
    const actorUuid = req.user?.user;
    return await this.commandBus.execute(
      new AddCloudStorageCommand(payload, actorUuid),
    );
  }

  @Post('v1/modify/:uuid')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  @ApiOperation({
    summary: i18n.__('update cloud storage'),
    description: i18n.__('update cloud storage details'),
  })
  async modifyStorage(@Body() payload: ModifyStorageDto, @Param('uuid') uuid) {
    return await this.commandBus.execute(
      new ModifyCloudStorageCommand(uuid, payload),
    );
  }

  @Post('v1/remove/:uuid')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async removeStorage(@Param() params, @Req() req) {
    const actorUuid = req.user?.user;
    return await this.commandBus.execute(
      new RemoveCloudStorageCommand(actorUuid, params.uuid),
    );
  }

  @Post('v1/upload_file/:uuid')
  @UseGuards(AuthServerVerificationGuard)
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(
    @UploadedFile('file') file: MulterFile,
    @Req() req,
    @Body('permission') permission,
    @Param('uuid') storageUuid,
  ) {
    const actorUuid = req.user?.user;
    return await this.commandBus.execute(
      new UploadFilesCloudBucketCommand(
        file,
        storageUuid,
        actorUuid,
        permission,
      ),
    );
  }

  @Post('v1/delete_file/:uuid')
  @UseGuards(AuthServerVerificationGuard)
  async deleteFile(
    @Body('filename') filename,
    @Req() req,
    @Param('uuid') storageUuid,
  ) {
    return await this.commandBus.execute(
      new DeleteFileFromStorageCommand(filename, storageUuid, req),
    );
  }

  @Get('v1/retrieve_file/:uuid')
  @UseGuards(BearerTokenGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async retrieveFile(
    @Query() query: RetrieveFileDto,
    @Param('uuid') uuid: string,
    @Req() req,
  ) {
    const { filename, expiry } = query;
    return await this.queryBus.execute(
      new RetrieveFileQuery(filename, uuid, req, Number(expiry)),
    );
  }
}
