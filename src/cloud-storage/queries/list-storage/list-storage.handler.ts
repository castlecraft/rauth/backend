import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { ListStorageQuery } from './list-storage.query';
import { StorageService } from '../../entities/storage/storage.service';

@QueryHandler(ListStorageQuery)
export class ListStorageHandler implements IQueryHandler {
  constructor(private readonly storage: StorageService) {}

  async execute(query: ListStorageQuery) {
    const { offset, limit, search, sort } = query;
    const where = {};
    const sortQuery = { name: sort || 'asc' };
    return await this.storage.list(offset, limit, search, where, sortQuery);
  }
}
