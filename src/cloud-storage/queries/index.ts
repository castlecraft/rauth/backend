import { ListStorageHandler } from './list-storage/list-storage.handler';
import { RetrieveFileHandler } from './retrieve-file/retrieve-file.handler';

export const CloudStorageQueries = [RetrieveFileHandler, ListStorageHandler];
