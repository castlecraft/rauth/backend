import {
  Injectable,
  NotFoundException,
  BadRequestException,
  ForbiddenException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { v4 as uuidv4 } from 'uuid';
import { S3 } from '@aws-sdk/client-s3';
import { from, of, switchMap, throwError } from 'rxjs';
import { CloudStorageCreatedEvent } from '../../events/cloud-storage-created-event/cloud-storage-created-event';
import { StorageService } from '../../entities/storage/storage.service';
import {
  INVALID_FILE_OR_STORAGE_UUID,
  INVALID_CLIENT,
} from '../../../constants/messages';
import { CloudStorageRemovedEvent } from '../../events/cloud-storage-removed-event/cloud-storage-removed-event';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { PasswordCryptoService } from '../../../auth/policies/password-crypto/password-crypto.service';

@Injectable()
export class CloudStorageAggregateService extends AggregateRoot {
  constructor(
    private readonly storage: StorageService,
    private readonly settings: ServerSettingsService,
    private readonly passwordCrypto: PasswordCryptoService,
  ) {
    super();
  }
  async addCloudStorage(cloudStoragePayload, actorUuid) {
    cloudStoragePayload.secretKey = this.passwordCrypto.encrypt(
      cloudStoragePayload.secretKey,
    );
    cloudStoragePayload.uuid = uuidv4();
    cloudStoragePayload.creation = new Date();
    cloudStoragePayload.createdBy = actorUuid;
    return await this.apply(new CloudStorageCreatedEvent(cloudStoragePayload));
  }

  async retrieveFile(
    fileName: string,
    storageUuid: string,
    req: unknown,
    expiry = 300000,
  ) {
    this.validateAuthorizedRequest(req);
    const storage = await this.storage.findOne({ uuid: storageUuid });
    if (!storage) throw new NotFoundException({ storageUuid });
    if (!fileName) throw new BadRequestException(INVALID_FILE_OR_STORAGE_UUID);

    const s3 = new S3({
      region: storage.region,
      endpoint: storage.endpoint,
      credentials: {
        accessKeyId: storage.accessKey,
        secretAccessKey: this.passwordCrypto.decrypt(storage.secretKey),
      },
    });

    return s3.getObject({
      Bucket: storage.bucket,
      Key: `${storage.basePath}/${fileName}`,
      ResponseExpires: new Date(new Date().getTime() + expiry), // default 5 minutes
    });
  }

  validateAuthorizedRequest(req) {
    if (!req.token.trustedClient || req.token.sub) {
      throw new ForbiddenException(INVALID_CLIENT);
    }
  }

  removeStorage(uuid: string) {
    return from(this.settings.find()).pipe(
      switchMap(settings => {
        if ([settings.avatarBucket, settings.backupBucket].includes(uuid)) {
          return throwError(() => {
            return new ForbiddenException({
              DeleteBucket: uuid,
              AvatarBucket: settings.avatarBucket,
              BackupBucket: settings.backupBucket,
            });
          });
        }
        return from(this.storage.findOne({ uuid }));
      }),
      switchMap(storage => {
        if (storage) {
          this.apply(new CloudStorageRemovedEvent(storage));
          return of({ deleted: storage.uuid });
        }
        return throwError(
          () => new NotFoundException({ CloudStorageNotFound: uuid }),
        );
      }),
    );
  }
}
