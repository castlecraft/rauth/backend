import { BadRequestException, Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { PasswordCryptoService } from '../../../auth/policies/password-crypto/password-crypto.service';
import { StorageValidationDto } from '../../../cloud-storage/policies';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { CloudStorageModifiedEvent } from '../../events/cloud-storage-modified-event/cloud-storage-modified-event';

@Injectable()
export class ModifyCloudStorageAggregateService extends AggregateRoot {
  constructor(
    private readonly settings: ServerSettingsService,
    private readonly password: PasswordCryptoService,
  ) {
    super();
  }
  async modifyCloudStorage(
    uuid: string,
    cloudStoragePayload: StorageValidationDto,
  ) {
    const settings = await this.settings.find();
    if (cloudStoragePayload.disabled && uuid === settings.backupBucket) {
      throw new BadRequestException({ CannotDisableBackupBucket: uuid });
    }
    if (cloudStoragePayload.disabled && uuid === settings.avatarBucket) {
      throw new BadRequestException({ CannotDisableAvatarBucket: uuid });
    }
    if (cloudStoragePayload.secretKey) {
      cloudStoragePayload.secretKey = this.password.encrypt(
        cloudStoragePayload.secretKey,
      );
    }
    return await this.apply(
      new CloudStorageModifiedEvent(uuid, cloudStoragePayload),
    );
  }
}
