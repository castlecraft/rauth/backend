import { PutObjectRequest, S3 } from '@aws-sdk/client-s3';
import { BadRequestException, Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { isURL } from 'class-validator';

import { ACL_PUBLIC_PERMISSION, PUBLIC } from '../../../constants/app-strings';
import { INVALID_FILE_OR_STORAGE_UUID } from '../../../constants/messages';
import { Storage } from '../../entities/storage/storage.interface';
import { StorageService } from '../../entities/storage/storage.service';
import { BucketFileDeletedEvent } from '../../events/bucket-file-deleted/bucket-file-deleted.event';
import { FileUploadedCloudBucketEvent } from '../../events/file-uploaded-cloud-bucket-event/file-uploaded-cloud-bucket-event';
import { MulterFile } from './multer-file.interface';
import { PasswordCryptoService } from '../../../auth/policies/password-crypto/password-crypto.service';

@Injectable()
export class UploadFilesCloudBucketAggregateService extends AggregateRoot {
  constructor(
    private readonly storage: StorageService,
    private readonly passwordCrypto: PasswordCryptoService,
  ) {
    super();
  }

  async uploadFileToCloudBucket(
    uploadedFile: MulterFile,
    storageUuid: string,
    actor: string,
    fileACLPermission: string,
  ) {
    const storage = await this.validateBucket(storageUuid, uploadedFile);

    const s3 = this.getS3Client(storage);
    const params: PutObjectRequest = {
      ACL: fileACLPermission === PUBLIC ? ACL_PUBLIC_PERMISSION : undefined,
      Body: uploadedFile.buffer,
      Bucket: storage.bucket,
      Key: storage.basePath + '/' + uploadedFile.originalname,
    };

    if (uploadedFile.mimetype) {
      params.ContentType = uploadedFile.mimetype;
    }

    try {
      await s3.putObject(params);
      this.apply(
        new FileUploadedCloudBucketEvent(
          uploadedFile,
          storage,
          actor,
          fileACLPermission,
        ),
      );

      return {
        key: params.Key,
        location: this.getFileLocation(storage, params.Key),
      };
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async deleteFileFromBucket(
    filename: string,
    storageUuid: string,
    actor: string,
  ) {
    const storage = await this.validateBucket(storageUuid, filename);

    const s3 = this.getS3Client(storage);

    // remove a file from bucket
    const params = {
      Bucket: storage.bucket,
      Key: filename,
    };

    try {
      await s3.deleteObject(params);
      this.apply(new BucketFileDeletedEvent(filename, storage, actor));
      return { deletedFile: filename };
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async validateBucket(uuid: string, uploadedFile: MulterFile | string) {
    const storage = await this.storage.findOne({
      uuid,
    });

    if (!uploadedFile || !storage) {
      throw new BadRequestException(INVALID_FILE_OR_STORAGE_UUID);
    }

    return storage;
  }

  getS3Client(storage: Storage) {
    return new S3({
      apiVersion: storage.version,
      region: storage.region,
      endpoint: storage.endpoint,
      forcePathStyle: storage.isPathStyle,
      credentials: {
        accessKeyId: storage.accessKey,
        secretAccessKey: this.passwordCrypto.decrypt(storage.secretKey),
      },
    });
  }

  getFileLocation(storage: Storage, fileKey: string) {
    if (storage.isPathStyle) {
      return storage.endpoint + '/' + storage.bucket + '/' + fileKey;
    }

    if (
      isURL(storage.endpoint, { require_tld: false, require_protocol: true })
    ) {
      const url = new URL(storage.endpoint);
      return `${url.protocol}//${storage.bucket}.${url.host}/${fileKey}`;
    }
    return `${storage.bucket}.${storage.endpoint}/${fileKey}`;
  }
}
