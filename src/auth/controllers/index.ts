import { PasswordCryptoService } from '../../auth/policies/password-crypto/password-crypto.service';
import { AuthController } from './auth/auth.controller';
import { AuthService } from './auth/auth.service';
import { KerberosController } from './kerberos/kerberos.controller';
import { LDAPClientController } from './ldap-client/ldap-client.controller';
import { OAuth2Controller } from './oauth2/oauth2.controller';
import { OAuth2Service } from './oauth2/oauth2.service';
import { SamlAppController } from './saml-app/saml-app.controller';
import { SocialLoginController } from './social-login/social-login.controller';
import { WebAuthnController } from './webauthn/webauthn.controller';
import { WellKnownController } from './well-known/well-known.controller';
import { WellKnownService } from './well-known/well-known.service';

export const authControllers = [
  AuthController,
  KerberosController,
  LDAPClientController,
  OAuth2Controller,
  SamlAppController,
  SocialLoginController,
  WebAuthnController,
  WellKnownController,
];

export const authServices = [
  AuthService,
  OAuth2Service,
  WellKnownService,
  PasswordCryptoService,
];
