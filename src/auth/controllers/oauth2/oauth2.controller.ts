import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Req,
  Res,
  UseFilters,
  UseGuards,
} from '@nestjs/common';
import { ApiExcludeEndpoint, ApiOperation } from '@nestjs/swagger';
import { randomBytes } from 'crypto';

import { ErrorFilter } from '../../../common/filters/errors.filter';
import { TWENTY_FOUR_NUMBER } from '../../../constants/app-strings';
import { OAUTH2_AUTHORIZE_ENDPOINT } from '../../../constants/url-strings';
import { i18n } from '../../../i18n/i18n.config';
import { BasicClientCredentialsGuard } from '../../guards/basic-client-credentials.guard';
import { BearerTokenGuard } from '../../guards/bearer-token.guard';
import { EnsureLoginGuard } from '../../guards/ensure-login.guard';
import { OAuth2Service } from './oauth2.service';

@Controller('oauth2')
export class OAuth2Controller {
  constructor(private readonly oauth2Service: OAuth2Service) {}

  @Get('confirmation')
  @UseGuards(EnsureLoginGuard)
  @UseFilters(ErrorFilter)
  @ApiExcludeEndpoint()
  async confirmation(@Req() request, @Res() response) {
    const issuerUrl = await this.oauth2Service.getIssuerUrl();
    const nonce = randomBytes(TWENTY_FOUR_NUMBER).toString('base64');
    response.set('Content-Security-Policy', `default-src 'nonce-${nonce}'`);
    return response.render('oauth2-dialog', {
      transactionId: request.oauth2.transactionID,
      email: request.user.email,
      client: request.oauth2.client,
      action: issuerUrl + OAUTH2_AUTHORIZE_ENDPOINT,
      nonce,
    });
  }

  @Post('authorize')
  @UseGuards(EnsureLoginGuard)
  @ApiOperation({
    summary: i18n.__('Authorize'),
    description: i18n.__('Authorize access to resource'),
  })
  async authorize() {}

  @Post('token')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({
    summary: i18n.__('Token'),
    description: i18n.__('OAuth2.0 flow: Return a bearer token'),
  })
  token() {}

  @Get('profile')
  @UseGuards(BearerTokenGuard)
  profile(@Req() req) {
    return this.oauth2Service.getProfile(req);
  }

  @Post('revoke')
  @HttpCode(HttpStatus.OK)
  @UseGuards(BasicClientCredentialsGuard)
  @ApiOperation({
    summary: i18n.__('Revoke'),
    description: i18n.__('OAuth2.0 flow: Revoke a token explicitly'),
  })
  async tokenRevoke(@Body('token') token) {
    return await this.oauth2Service.tokenRevoke(token);
  }

  @Post('introspection')
  @HttpCode(HttpStatus.OK)
  @UseGuards(BasicClientCredentialsGuard)
  @ApiOperation({
    summary: i18n.__('Introspection'),
    description: i18n.__('Introspect token validity'),
  })
  async tokenIntrospection(@Body('token') token) {
    return await this.oauth2Service.tokenIntrospection(token);
  }
}
