import { ForbiddenException, Injectable } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { forkJoin, from, of, throwError } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { BearerTokenService } from '../../../auth/entities/bearer-token/bearer-token.service';
import { ClientService } from '../../../client-management/entities/client/client.service';
import { InvalidClientException } from '../../../common/filters/exceptions';
import {
  ROLES,
  SCOPE_EMAIL,
  SCOPE_PHONE,
  SCOPE_PROFILE,
} from '../../../constants/app-strings';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { User } from '../../../user-management/entities/user/user.interface';
import { UserService } from '../../../user-management/entities/user/user.service';
import { RemoveBearerTokenCommand } from '../../commands/remove-bearer-token/remove-bearer-token.command';
import { BearerToken } from '../../entities/bearer-token/bearer-token.interface';
import { UserClaimService } from '../../entities/user-claim/user-claim.service';
import { IDTokenClaims } from '../../middlewares/interfaces';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { INVALID_CLIENT } from '../../../constants/messages';

@Injectable()
export class OAuth2Service {
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly bearerTokenService: BearerTokenService,
    private readonly userService: UserService,
    private readonly settings: ServerSettingsService,
    private readonly clientService: ClientService,
    private readonly userClaimService: UserClaimService,
    private readonly commandBus: CommandBus,
    private readonly config: ConfigService,
  ) {}

  async tokenRevoke(token) {
    return await this.commandBus.execute(new RemoveBearerTokenCommand(token));
  }

  async tokenIntrospection(token) {
    const bearerToken = await this.bearerTokenService.findOne({
      accessToken: token,
    });

    let tokenData: any = { active: false };
    if (bearerToken) {
      /**
       * exp Integer timestamp, measured in the number of seconds
       * since January 1 1970 UTC, indicating when this token will expire
       * https://tools.ietf.org/html/rfc7662#section-2.2
       */
      const exp = new Date(
        bearerToken.creation.getTime() + bearerToken.expiresIn * 1000,
      );
      this.logger.log(`exp ${exp}`, this.constructor.name);
      const client = await this.clientService.findOne({
        clientId: bearerToken.client,
      });
      this.logger.log(
        `TokenClient ${bearerToken.client}`,
        this.constructor.name,
      );
      this.logger.log(`clientId ${client.clientId}`, this.constructor.name);
      if (!client) {
        this.logger.log(INVALID_CLIENT, this.constructor.name);
        throw new InvalidClientException();
      }

      tokenData = {
        client_id: bearerToken.client,
        trusted_client: client.isTrusted,
        active: this.unixTime(exp) > this.unixTime(new Date()),
        exp: this.unixTime(exp),
      };

      if (bearerToken.user) tokenData.sub = bearerToken.user;

      const user = await this.userService.findOne({
        uuid: tokenData.sub,
      });

      if (bearerToken.scope) {
        tokenData.scope = bearerToken.scope;
        tokenData = this.appendUserInfo(user, bearerToken, tokenData);
      }
    }
    return tokenData;
  }

  getProfile(req) {
    const uuid = req.user.user;
    return this.observeLocalProfile(uuid, req.token);
  }

  getAccessToken(request) {
    if (!request.headers.authorization) {
      if (!request.query.access_token) return null;
    }
    return (
      request.query.access_token ||
      request.headers.authorization.split(' ')[1] ||
      null
    );
  }

  observeLocalProfile(uuid: string, token: BearerToken) {
    return forkJoin({
      settings: from(this.settings.find()),
      user: from(this.userService.findOne({ uuid })),
    }).pipe(
      switchMap(({ settings, user }) => {
        if (!token) {
          return throwError(() => new ForbiddenException());
        }

        if (!user) {
          return throwError(() => new ForbiddenException());
        }

        const claims: IDTokenClaims = {
          aud: token.client,
          iss: settings.issuerUrl,
          sub: user.uuid,
        };

        if (token.scope.includes(ROLES)) {
          claims.roles = user.roles;
        }
        if (token.scope.includes(SCOPE_EMAIL)) {
          claims.email = user.email;
          claims.email_verified = user.isEmailVerified;
        }
        if (token.scope.includes(SCOPE_PROFILE)) {
          claims.name = user.name;
        }
        if (token.scope.includes(SCOPE_PHONE) && user.phone) {
          claims.phone_number = user.phone;
          claims.phone_number_verified = !user.unverifiedPhone;
        }

        return forkJoin({
          requestClaims: of(claims),
          userClaims: from(
            this.userClaimService.find({
              uuid: user.uuid,
              scope: { $in: token.scope },
            }),
          ),
        });
      }),
      switchMap(({ requestClaims, userClaims }) => {
        const claims = requestClaims;

        if (userClaims && userClaims.length > 0) {
          userClaims.forEach(claim => {
            claims[claim.name] = claim.value;
          });
        }
        return of(claims);
      }),
    );
  }

  appendUserInfo(
    user: User,
    bearerToken: BearerToken,
    tokenData: IDTokenClaims,
  ) {
    if (user) {
      if (bearerToken.scope.includes(ROLES)) {
        tokenData.roles = user.roles;
      }

      if (bearerToken.scope.includes(SCOPE_EMAIL)) {
        tokenData.email = user.email;
        tokenData.email_verified = user.isEmailVerified;
      }

      if (bearerToken.scope.includes(SCOPE_PROFILE)) {
        tokenData.name = user.name;
      }

      if (bearerToken.scope.includes(SCOPE_PHONE) && user.phone) {
        tokenData.phone_number = user.phone;
        tokenData.phone_number_verified = !user.unverifiedPhone;
      }
    }

    return tokenData;
  }

  /**
   * unixTime Returns the stored time value in seconds since midnight, January 1, 1970 UTC.
   * @param date
   */
  unixTime(date: Date) {
    return Math.floor(date.valueOf() / 1000);
  }

  async getIssuerUrl() {
    const settings = await this.settings.find();
    return settings.issuerUrl;
  }
}
