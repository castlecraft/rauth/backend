import { CqrsModule } from '@nestjs/cqrs';
import { Test, TestingModule } from '@nestjs/testing';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { UserService } from '../../../user-management/entities/user/user.service';
import { BearerTokenService } from '../../entities/bearer-token/bearer-token.service';
import { LDAPClientService } from '../../entities/ldap-client/ldap-client.service';
import { LDAPClientController } from './ldap-client.controller';

describe('LDAPClient Controller', () => {
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [CqrsModule],
      controllers: [LDAPClientController],
      providers: [
        {
          provide: LDAPClientService,
          useValue: {},
        },
        {
          provide: UserService,
          useValue: {},
        },
        {
          provide: BearerTokenService,
          useValue: {},
        },
        {
          provide: ServerSettingsService,
          useValue: {},
        },
      ],
    }).compile();
  });
  it('should be defined', () => {
    const controller: LDAPClientController =
      module.get<LDAPClientController>(LDAPClientController);
    expect(controller).toBeDefined();
  });
});
