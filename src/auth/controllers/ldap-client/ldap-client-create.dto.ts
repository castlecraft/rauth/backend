import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsArray,
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  IsUUID,
  Min,
  ValidateNested,
} from 'class-validator';

import { FIVE_THOUSAND_MS, ZERO_NUMBER } from '../../../constants/app-strings';
import { i18n } from '../../../i18n/i18n.config';

export class LdapAttribute {
  @IsString()
  @ApiProperty({ description: 'user claim' })
  claim: string;

  @IsString()
  @ApiProperty({ description: 'map to user claim' })
  mapTo: string;
}

export class CreateLDAPClientDto {
  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('Identifies a LDAP client uniquely'),
    required: true,
    example: 'Microsoft AD',
  })
  name: string;

  @IsOptional()
  @ApiProperty({
    description: i18n.__('LDAP Client description'),
    required: true,
    example: 'Microsoft AD Client',
  })
  description: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('LDAP Server URL'),
    required: true,
    type: 'string',
    example: 'ldap://ldap.example.com',
  })
  url?: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('Admin domain name'),
    required: true,
    example: 'IN/john.k',
  })
  adminDn?: string;

  @IsOptional()
  @ApiProperty({
    description: i18n.__('LDAP Admin DN password'),
    required: true,
    example: 'h4cv_4%b2#D:-)',
  })
  adminPassword?: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('LDAP User search base'),
    required: true,
    type: 'string',
  })
  userSearchBase?: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('LDAP User name attribute'),
    required: true,
    example: 'sn',
  })
  usernameAttribute?: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('LDAP email attribute'),
    required: true,
    example: 'main',
  })
  emailAttribute?: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('LDAP phone attribute'),
    required: true,
    example: 'mobile',
  })
  phoneAttribute?: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('LDAP full name attribute'),
    required: true,
    example: 'cn',
  })
  fullNameAttribute?: string;

  @IsUUID()
  @IsOptional()
  @ApiProperty({
    description: i18n.__('OAuth2 Client ID link'),
    type: 'string',
  })
  clientId: string;

  @ApiProperty({
    description: i18n.__('Additional user attributes to save as claims'),
    type: [LdapAttribute],
  })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => LdapAttribute)
  attributes: LdapAttribute[];

  @IsString()
  @ApiProperty({
    description: i18n.__('Scope to store user claims'),
    required: true,
    type: 'string',
  })
  scope: string;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({
    description: i18n.__('Disabled LDAP Client'),
  })
  disabled?: boolean;

  @IsOptional()
  @IsNumber()
  @ApiProperty({
    description: 'Timeout for ldap',
    default: FIVE_THOUSAND_MS,
  })
  timeoutMs?: number = FIVE_THOUSAND_MS;

  @IsOptional()
  @IsNumber()
  @Min(ZERO_NUMBER)
  @ApiProperty({
    description: 'Allowed failed login attempts before blocking user',
  })
  allowedFailedLoginAttempts?: number;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({
    description: 'Skip mobile verification',
  })
  skipMobileVerification?: boolean;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({
    description: 'Disable creation of user',
  })
  disableUserCreation?: boolean;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({
    description: 'is Secure',
  })
  isSecure?: boolean;

  @IsOptional()
  @IsString()
  @ApiProperty({
    description: 'cert',
  })
  cert?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    description: 'key',
  })
  key?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    description: 'key passphrase',
  })
  keyPassphrase?: string;

  @IsOptional()
  @IsString({ each: true })
  @ApiProperty({
    description: 'ca',
  })
  ca?: string[];
}
