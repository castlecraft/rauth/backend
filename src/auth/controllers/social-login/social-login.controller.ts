import {
  Body,
  Controller,
  ForbiddenException,
  Get,
  Param,
  Post,
  Query,
  Req,
  Res,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ApiOperation } from '@nestjs/swagger';
import { RemoveSocialLoginCommand } from '../../../auth/commands/remove-social-login/remove-social-login.command';
import { SocialLoginService } from '../../../auth/entities/social-login/social-login.service';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { Roles } from '../../../common/decorators/roles.decorator';
import { ListQueryDto } from '../../../common/policies/list-query/list-query';
import { ADMINISTRATOR } from '../../../constants/app-strings';
import { i18n } from '../../../i18n/i18n.config';
import { UserService } from '../../../user-management/entities/user/user.service';
import { AddSocialLoginCommand } from '../../commands/add-social-login/add-social-login.command';
import { ModifySocialLoginCommand } from '../../commands/modify-social-login/modify-social-login.command';
import { BearerTokenGuard } from '../../guards/bearer-token.guard';
import { OAuth2ClientGuard } from '../../guards/oauth2-client.guard';
import { ListSocialLoginQuery } from '../../queries/list-social-login/list-social-login.query';
import { CreateSocialLoginDto } from './social-login-create.dto';

@Controller('social_login')
export class SocialLoginController {
  constructor(
    private readonly socialLoginService: SocialLoginService,
    private readonly userService: UserService,
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}
  @Post('v1/create')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @ApiOperation({
    summary: i18n.__('Create social login'),
    description: 'create account with social login',
  })
  async create(@Body() body: CreateSocialLoginDto, @Req() req) {
    const createdBy = req.user.user;
    return await this.commandBus.execute(
      new AddSocialLoginCommand(body, createdBy),
    );
  }

  @Post('v1/update/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  @ApiOperation({
    summary: i18n.__('update social login'),
    description: i18n.__('update social login details'),
  })
  async update(
    @Body() payload: CreateSocialLoginDto,
    @Param('uuid') uuid: string,
  ) {
    return await this.commandBus.execute(
      new ModifySocialLoginCommand(payload, uuid),
    );
  }

  @Get('v1/list')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async list(@Query() query: ListQueryDto) {
    const { offset, limit, search, sort } = query;
    return await this.queryBus.execute(
      new ListSocialLoginQuery(offset, limit, search, sort),
    );
  }

  @Get('v1/get/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async findOne(@Param('uuid') uuid: string, @Req() req) {
    let socialLogin;
    if (await this.userService.checkAdministrator(req.user.user)) {
      socialLogin = await this.socialLoginService.findOne({ uuid });
    } else {
      socialLogin = await this.socialLoginService.findOne({
        uuid,
        createdBy: req.user.user,
      });
    }
    if (!socialLogin) throw new ForbiddenException();
    socialLogin.clientSecret = undefined;
    return socialLogin;
  }

  @Post('v1/delete/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async deleteByUUID(@Param('uuid') uuid, @Req() req) {
    const userUuid = req.user.user;
    return await this.commandBus.execute<RemoveSocialLoginCommand>(
      new RemoveSocialLoginCommand(userUuid, uuid),
    );
  }

  @Get('callback/:socialLogin')
  @UseGuards(OAuth2ClientGuard)
  oauth2Callback(@Req() req, @Res() res) {
    const parsedState = JSON.parse(
      Buffer.from(req.session.state, 'base64').toString(),
    );
    res.redirect(parsedState.redirect);
  }

  @Get('v1/list_logins')
  async listLogins() {
    const logins = await this.socialLoginService.getAllWithClientId();
    return logins.map(login => ({
      name: login.name,
      uuid: login.uuid,
    }));
  }
}
