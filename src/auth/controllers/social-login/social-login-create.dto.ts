import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsArray,
  IsBoolean,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUrl,
  ValidateNested,
} from 'class-validator';
import { i18n } from '../../../i18n/i18n.config';

export class SocialLoginClaimDto {
  @IsString()
  @ApiProperty({ description: 'user claim' })
  claim: string;

  @IsString()
  @ApiProperty({ description: 'map to user claim' })
  mapTo: string;
}

export class CreateSocialLoginDto {
  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('Identifies a Social Login uniquely'),
    required: true,
    example: 'Google',
  })
  name: string;

  @IsOptional()
  @ApiProperty({
    description: i18n.__('Social Login description'),
    required: true,
    example: 'Login with Google',
  })
  description: string;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({
    description: i18n.__('Disable User Creation'),
  })
  disableUserCreation?: boolean;

  @IsOptional()
  @ApiProperty({
    description: i18n.__('Social Login OAuth 2 Client ID'),
    required: true,
    example: '7884a-yyr23-u87g53',
  })
  clientId: string;

  @IsOptional()
  @ApiProperty({
    description: i18n.__('Social Login OAuth 2 Client Secret'),
    required: true,
    example: '7jj73884a-yyr776gg23-u87ijh65g53',
  })
  clientSecret: string;

  @IsUrl({ require_tld: false })
  @ApiProperty({
    description: i18n.__('Social Login OAuth 2 Authorization URL'),
    required: true,
    example: 'https://api.example.com/oauth2/authorize',
  })
  authorizationURL: string;

  @IsUrl({ require_tld: false })
  @ApiProperty({
    description: i18n.__('Social Login OAuth 2 Token URL'),
    required: true,
    example: 'https://api.example.com/oauth2/token',
  })
  tokenURL: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  @ApiProperty({
    description: i18n.__('Social Login OAuth 2 Introspection URL'),
    required: true,
    example: 'https://api.example.com/oauth2/tokeninfo',
  })
  introspectionURL: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  @ApiProperty({
    description: i18n.__('Social Login OAuth 2 Base URL'),
    required: true,
    example: 'https://api.example.com',
  })
  baseURL: string;

  @IsUrl({ require_tld: false })
  @ApiProperty({
    description: i18n.__('Social Login OAuth 2 User Profile URL'),
    required: true,
    example: 'https://api.example.com/oauth2/userinfo',
  })
  profileURL: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  @ApiProperty({
    description: i18n.__('Social Login OAuth 2 Revocation URL'),
    required: true,
    example: 'https://api.example.com/oauth2/revoke',
  })
  revocationURL: string;

  @IsOptional()
  @ApiProperty({
    description: i18n.__('Social Login Email'),
  })
  emailAttribute?: string;

  @IsOptional()
  @ApiProperty({
    description: i18n.__('Social Login Phone'),
  })
  phoneAttribute?: string;

  @IsNotEmpty({ each: true })
  @ApiProperty({
    description: i18n.__('Social Login Array of OAuth 2 scopes'),
    required: true,
    example: ['openid', 'profile', 'email', 'phone', 'roles'],
  })
  scope: string[];

  @IsString()
  @ApiProperty({
    description: i18n.__('Scope to store user claims'),
    required: true,
    type: 'string',
  })
  claimsScope: string;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({
    description: i18n.__('Disable Social Login'),
  })
  disabled?: boolean;

  @ApiProperty({
    description: i18n.__('Additional openid claims to save as user claims'),
    type: [SocialLoginClaimDto],
  })
  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => SocialLoginClaimDto)
  claimsMap?: SocialLoginClaimDto[];
}
