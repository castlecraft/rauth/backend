import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ApiOperation } from '@nestjs/swagger';

import { Scope } from '../../../auth/decorators/scope.decorator';
import { ScopeGuard } from '../../../auth/guards/scope.guard';
import { SCOPE_SELF_SERVICE } from '../../../constants/app-strings';
import { i18n } from '../../../i18n/i18n.config';
import { RegisterWebAuthnKeyCommand } from '../../commands/register-webauthn-key/register-webauthn-key.command';
import { RemoveUserAuthenticatorCommand } from '../../commands/remove-user-authenticator/remove-user-authenticator.command';
import { RenameUserAuthenticatorCommand } from '../../commands/rename-user-authenticator/rename-user-authenticator.command';
import { RequestWebAuthnKeyRegistrationCommand } from '../../commands/request-webauthn-key-registration/request-webauthn-key-registration.command';
import { WebAuthnLoginCommand } from '../../commands/webauthn-login/webauthn-login.command';
import { WebAuthnRequestLoginCommand } from '../../commands/webauthn-request-login/webauthn-request-login.command';
import { BearerTokenGuard } from '../../guards/bearer-token.guard';
import { RequestLogin } from '../../policies/login-user/request-login.dto';
import { RegisterDeviceDto } from '../../policies/register-device/register-device.dto';
import { FindUserAuthenticatorsQuery } from '../../queries/find-user-authenticators/find-user-authenticators.query';

@Controller('webauthn')
export class WebAuthnController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/request_register')
  @UseGuards(BearerTokenGuard)
  @ApiOperation({
    summary: i18n.__('request auth key registration'),
    description: i18n.__('Request authentication key registration'),
  })
  async requestRegister(@Req() req, @Body() body: RegisterDeviceDto) {
    const { userUuid } = body;
    const actorUuid = req.user.user;

    return await this.commandBus.execute(
      new RequestWebAuthnKeyRegistrationCommand(actorUuid, userUuid),
    );
  }

  @Post('v1/register')
  @UseGuards(BearerTokenGuard)
  async register(@Req() req, @Body() body) {
    const userUuid = body.userUuid;
    return await this.commandBus.execute(
      new RegisterWebAuthnKeyCommand(body.credentials, userUuid),
    );
  }

  @Post('v1/login')
  @HttpCode(HttpStatus.OK)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  @ApiOperation({
    summary: i18n.__('Webauthn Login'),
    description: i18n.__('login with webauthn'),
  })
  async login(@Body() body: RequestLogin) {
    return await this.commandBus.execute(
      new WebAuthnRequestLoginCommand(body.username),
    );
  }

  @Post('v1/login_challenge')
  @HttpCode(HttpStatus.OK)
  async loginChallenge(@Req() req) {
    return await this.commandBus.execute(new WebAuthnLoginCommand(req));
  }

  @Get('v1/authenticators/:userUuid')
  @UseGuards(BearerTokenGuard)
  async findAuthenticators(@Param('userUuid') userUuid: string, @Req() req) {
    const actorUuid = req.user.user;
    return await this.queryBus.execute(
      new FindUserAuthenticatorsQuery(actorUuid, userUuid),
    );
  }

  @Post('v1/remove_authenticator/:uuid')
  @Scope(SCOPE_SELF_SERVICE)
  @UseGuards(BearerTokenGuard, ScopeGuard)
  async removeAuthenticator(
    @Param('uuid') uuid: string,
    @Body('userUuid') userUuid: string,
    @Req() req,
  ) {
    const actorUuid = req.user.user;
    return await this.commandBus.execute(
      new RemoveUserAuthenticatorCommand(uuid, actorUuid, userUuid),
    );
  }

  @Post('v1/rename_authenticator/:uuid')
  @Scope(SCOPE_SELF_SERVICE)
  @UseGuards(BearerTokenGuard, ScopeGuard)
  async renameAuthenticator(
    @Param('uuid') uuid: string,
    @Body('name') name: string,
    @Req() req,
  ) {
    const actorUuid = req.user.user;
    return await this.commandBus.execute(
      new RenameUserAuthenticatorCommand(uuid, name, actorUuid),
    );
  }
}
