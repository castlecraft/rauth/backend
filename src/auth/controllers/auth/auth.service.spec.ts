import { CommandBus } from '@nestjs/cqrs';
import { Test, TestingModule } from '@nestjs/testing';
import { CryptographerService } from '../../../common/services/cryptographer/cryptographer.service';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { SystemLogService } from '../../../system-settings/entities/system-log/system-log.service';
import { AuthDataService } from '../../../user-management/entities/auth-data/auth-data.service';
import { UserService } from '../../../user-management/entities/user/user.service';
import { PasswordPolicyService } from '../../../user-management/policies/password-policy/password-policy.service';
import { UserClaimService } from '../../entities/user-claim/user-claim.service';
import { AuthService } from './auth.service';
import { ConfigService } from '../../../config/config.service';

describe('AuthService', () => {
  let service: AuthService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: CryptographerService,
          useValue: {},
        },
        {
          provide: UserService,
          useValue: {},
        },
        {
          provide: AuthDataService,
          useValue: {},
        },
        {
          provide: ServerSettingsService,
          useValue: {},
        },
        {
          provide: SystemLogService,
          useValue: {},
        },
        {
          provide: PasswordPolicyService,
          useValue: {},
        },
        { provide: UserClaimService, useValue: {} },
        {
          provide: CommandBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: ConfigService,
          useValue: {
            get: (key: string) => key,
          },
        },
      ],
    }).compile();
    service = module.get<AuthService>(AuthService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
