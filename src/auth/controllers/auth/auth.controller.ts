import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Req,
  Res,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiOperation } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { i18n } from '../../../i18n/i18n.config';
import { BearerTokenGuard } from '../../guards/bearer-token.guard';
import { LocalUserGuard } from '../../guards/local-user.guard';
import { LoginUserDto } from '../../policies/login-user/login-user.dto';
import { PasswordLessDto } from '../../policies/password-less/password-less.dto';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  @HttpCode(HttpStatus.OK)
  @UseGuards(LocalUserGuard)
  @ApiOperation({
    summary: i18n.__('Login'),
    description: 'Login with email or mobile phone',
  })
  login(@Body() body: LoginUserDto, @Req() req, @Res() res) {
    const out: { user: string; path?: string } = {
      user: req.user.email || req.user.phone,
    };
    if (body.redirect) {
      out.path = body.redirect;
      // https://stackoverflow.com/a/36885704
      // return res.redirect(out.path);
    }
    return res.json(out);
  }

  @Get('logout')
  @ApiOperation({
    summary: i18n.__('Logout'),
    description: i18n.__('Logout of the session'),
  })
  logout(@Req() req: Request, @Res() res: Response) {
    return this.authService.logout(req, res);
  }

  @Get('logout/:uuid')
  @ApiOperation({
    summary: i18n.__('Logout'),
    description: i18n.__('Logout of the session'),
  })
  logoutUuid(@Param('uuid') uuid, @Req() req, @Res() res) {
    this.authService.logoutUuid(uuid, req, res);
  }

  @Post('verify_user')
  @ApiOperation({
    summary: i18n.__('Verify User'),
    description: i18n.__('Check whether the user exists and retrieve a record'),
  })
  async verifyUser(
    @Body('username') username: string,
    @Body('password') password?: string,
  ) {
    return await this.authService.verifyUser(username, password);
  }

  @Post('verify_password')
  async verifyPassword(@Body('username') username, @Body('password') password) {
    const response = await this.authService.verifyPassword(username, password);

    return response?.resendOTPInfo
      ? {
          verified: response?.checkPassword,
          resendOTPInfo: response?.resendOTPInfo,
        }
      : { verified: response?.checkPassword };
  }

  @Post('password_less')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async passwordLess(@Body() payload: PasswordLessDto, @Req() req) {
    return await this.authService.passwordLess(payload, req);
  }

  @Post('choose_user')
  @HttpCode(HttpStatus.OK)
  async chooseUser(
    @Body('uuid') uuid: string,
    @Req() req,
    @Body('redirect') redirect,
  ) {
    const user = await this.authService.chooseUser(req, uuid);
    return {
      uuid: user.uuid,
      email: user.email,
      phone: user.phone,
      path: redirect,
    };
  }

  @Post('/revoke')
  @ApiOperation({
    summary: i18n.__('Revoke token'),
    description: i18n.__('revoke Logout of the session'),
  })
  @UseGuards(BearerTokenGuard)
  async revoke(
    @Req() req,
    @Body('clientId') clientId?: string,
    @Body('uuid') uuid?: string,
  ) {
    return await this.authService.revokeToken(req, clientId, uuid);
  }
}
