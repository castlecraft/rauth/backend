import {
  BadRequestException,
  ForbiddenException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { Request } from 'express';
import { authenticator, hotp, totp } from 'otplib';
import { v4 as uuidv4 } from 'uuid';
import { RemoveBearerTokenCommand } from '../../../auth/commands/remove-bearer-token/remove-bearer-token.command';
import {
  InvalidClientException,
  InvalidOTPException,
  InvalidUserException,
  PasswordLessLoginNotEnabledException,
  UserAlreadyExistsException,
} from '../../../common/filters/exceptions';
import { CryptographerService } from '../../../common/services/cryptographer/cryptographer.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';
import {
  LOGIN,
  OTP_ENABLED,
  OTP_VERIFIED,
  PASSWORDLESS,
  RAUTH_SOURCE_ID,
  RAUTH_SOURCE_TYPE,
  SCOPE_SELF_SERVICE,
  SUCCESS,
  USER_CLAIMS,
  VERIFY_PASSWORD,
  VERIFY_USER,
} from '../../../constants/app-strings';
import { i18n } from '../../../i18n/i18n.config';
import { AppendSystemLogCommand } from '../../../system-settings/commands/append-system-log/append-system-log.command';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import {
  SystemLog,
  SystemLogCode,
  SystemLogData,
  SystemLoginType,
  SystemLogType,
} from '../../../system-settings/entities/system-log/system-log.interface';
import { SystemLogService } from '../../../system-settings/entities/system-log/system-log.service';
import {
  AuthData,
  AuthDataType,
} from '../../../user-management/entities/auth-data/auth-data.interface';
import { AuthDataService } from '../../../user-management/entities/auth-data/auth-data.service';
import { Role } from '../../../user-management/entities/role/role.interface';
import { User } from '../../../user-management/entities/user/user.interface';
import { USER } from '../../../user-management/entities/user/user.schema';
import { UserService } from '../../../user-management/entities/user/user.service';
import { UserAccountDto } from '../../../user-management/policies';
import { PasswordPolicyService } from '../../../user-management/policies/password-policy/password-policy.service';
import { SendLoginOTPCommand } from '../../commands/send-login-otp/send-login-otp.command';
import { BearerToken } from '../../entities/bearer-token/bearer-token.interface';
import { UserClaim } from '../../entities/user-claim/user-claim.interface';
import { UserClaimService } from '../../entities/user-claim/user-claim.service';
import { addSessionUser, defaultOptions } from '../../guards/guard.utils';
import { PasswordLessDto } from '../../policies/password-less/password-less.dto';

@Injectable()
export class AuthService {
  loginOTP: AuthData;
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly userService: UserService,
    private readonly authDataService: AuthDataService,
    private readonly cryptoService: CryptographerService,
    private readonly passwordPolicy: PasswordPolicyService,
    private readonly settings: ServerSettingsService,
    private readonly log: SystemLogService,
    private readonly commandBus: CommandBus,
    private readonly userClaimService: UserClaimService,
    private readonly config: ConfigService,
  ) {}

  async setupAdministrator(user: UserAccountDto, roles: Role[]) {
    this.validatePassword(user);
    return this.saveUser(user, roles);
  }

  /**
   * Returns User if credentials match
   * @param email
   * @param password
   */
  public async logIn(username, password, code?) {
    const user: User = await this.userService.findUserByIdentifier(username);
    this.logger.log(`${LOGIN}-${user.uuid}`, this.constructor.name);

    let failedAttempts: number;
    if (!code) {
      failedAttempts = await this.validateLoginAttempts(user);
    }
    if (user.disabled) {
      throw new UnauthorizedException(i18n.__('User Disabled'));
    }

    if (!user.password) {
      throw new UnauthorizedException(i18n.__('Unregistered User'));
    }

    const userPassword = await this.authDataService.findOne({
      uuid: user.password,
    });

    const passwordVerified = await this.cryptoService.checkPassword(
      userPassword.password,
      password,
    );

    if (!passwordVerified) {
      const error = new UnauthorizedException(i18n.__('Invalid Password'));
      await this.saveAuthLog(
        user.uuid,
        SystemLogCode.Failure,
        { loginType: SystemLoginType.Database },
        error.message,
      );
      throw error;
    }

    if (user.enable2fa || user.enableOTP) {
      this.logger.log(
        `${LOGIN}-${user.uuid}`,
        `${OTP_ENABLED}`,
        this.constructor.name,
      );
      await this.validate2FaCode(user, code, SystemLoginType.Database);
      await this.removeLoginHOTP(user);
    }

    if (failedAttempts) {
      await this.clearAuthFailLogs(user.uuid);
    }

    await this.saveAuthLog(
      user.uuid,
      SystemLogCode.Success,
      { loginType: SystemLoginType.Database },
      SUCCESS,
      false,
    );
    return user;
  }

  async validate2FaCode(
    user: User,
    code: string,
    loginType: SystemLoginType,
    isEnforced = false,
  ) {
    const { isTOTPVerified, isHOTPVerified } = await this.verifyOTP(user, code);

    // Invalid OTP will not save system_log. It will not lockout user.
    if (isEnforced && !isTOTPVerified && !isHOTPVerified) {
      const error = new InvalidOTPException();
      this.logger.error(`${LOGIN}- ${user.uuid}`, error, this.constructor.name);
      await this.saveAuthLog(
        user.uuid,
        SystemLogCode.Failure,
        { loginType, user },
        error.message,
        false,
      );
      throw error;
    }

    if (user.enableOTP && !isHOTPVerified && !user.enable2fa) {
      const error = new InvalidOTPException();
      this.logger.error(`${LOGIN}- ${user.uuid}`, error, this.constructor.name);
      await this.saveAuthLog(
        user.uuid,
        SystemLogCode.Failure,
        { loginType, user },
        error.message,
        false,
      );
      throw error;
    }

    if (user.enable2fa && !isTOTPVerified && !isHOTPVerified) {
      const error = new InvalidOTPException();
      this.logger.error(`${LOGIN}- ${user.uuid}`, error, this.constructor.name);
      await this.saveAuthLog(
        user.uuid,
        SystemLogCode.Failure,
        { loginType, user },
        error.message,
        false,
      );
      throw error;
    }

    await this.saveAuthLog(
      user.uuid,
      SystemLogCode.Success,
      { loginType, user, isTOTPVerified, isHOTPVerified },
      OTP_VERIFIED,
      false,
    );
  }

  async verifyOTP(user: User, code: string) {
    const sharedSecret = await this.authDataService.findOne({
      uuid: user.sharedSecret,
    });

    const isTOTPVerified = this.isUserTOTPValid(sharedSecret, code);
    const verifiedHOTP = await this.getUserHOTP(user);
    const isHOTPVerified = verifiedHOTP && verifiedHOTP === code;

    return { isTOTPVerified, isHOTPVerified };
  }

  async checkExistingUser(user: User) {
    const userByEmail = await this.userService.findOne({ email: user.email });
    const userByPhone = await this.userService.findOne({ phone: user.phone });
    if (userByEmail) {
      return true;
    } else if (userByPhone) {
      return true;
    }
    return false;
  }

  /**
   * Returns User by phone
   * @param phone
   */
  async getUserByPhone(phone: string) {
    return await this.userService.findOne({ phone });
  }

  /**
   * Returns User by email
   * @param email
   */
  async getUserByEmail(email: string) {
    return await this.userService.findOne({ email });
  }

  async verifyPassword(emailOrPhone: string, password: string) {
    const user = await this.userService.findUserByIdentifier(emailOrPhone);

    await this.validateLoginAttempts(user);

    const passwordData = await this.authDataService.findOne({
      uuid: user.password,
    });
    this.logger.debug(
      `${VERIFY_PASSWORD}-${user.uuid}`,
      `${password}`,
      this.constructor.name,
    );
    this.logger.debug(
      `${VERIFY_PASSWORD}-${user.uuid}`,
      `${passwordData.password}`,
      this.constructor.name,
    );
    if (!passwordData) throw new InvalidUserException();
    const checkPassword = this.cryptoService.checkPassword(
      passwordData.password,
      password,
    );
    if (!checkPassword)
      throw new UnauthorizedException(i18n.__('Invalid Password'));

    let resendOTPInfo;
    if (user.enable2fa || user.enableOTP) {
      resendOTPInfo = await this.commandBus.execute(
        new SendLoginOTPCommand(user),
      );
    }

    return { checkPassword, resendOTPInfo };
  }

  async verifyUser(username: string, password?: string) {
    username = username.trim().toLocaleLowerCase();
    let user: User = await this.userService.findUserByIdentifier(username);
    if (!user) {
      throw new InvalidUserException();
    }
    this.logger.log(`${VERIFY_USER} ${user.uuid}`, this.constructor.name);
    if (password) {
      user = await this.logIn(username, password);
      if (!user) throw new UnauthorizedException(i18n.__('Invalid Password'));
    }

    const userclaim: UserClaim[] = await this.userClaimService.find({
      uuid: user.uuid,
      scope: SCOPE_SELF_SERVICE,
    });
    const rauthSourceTypeClaim = userclaim.find(
      c => c.name === RAUTH_SOURCE_TYPE,
    );
    user.source_type = rauthSourceTypeClaim
      ? (rauthSourceTypeClaim.value as string)
      : undefined;
    const rauthSourceIdClaim = userclaim.find(c => c.name === RAUTH_SOURCE_ID);
    user.source_id = rauthSourceIdClaim
      ? (rauthSourceIdClaim.value as string)
      : undefined;
    this.logger.log(
      `${USER_CLAIMS}-${rauthSourceTypeClaim} ${rauthSourceIdClaim}`,
      this.constructor.name,
    );
    user = this.sanitizeUser(user);
    return {
      user: {
        ...user.toObject(),
        roles: undefined,
        _id: undefined,
        source_type: user.source_type,
        source_id: user.source_id,
      },
    };
  }

  sanitizeUser(user) {
    return this.userService.getUserWithoutSecrets(
      this.userService.getUserWithoutIdentity(
        this.userService.getUserWithoutMetaData(user),
      ),
    );
  }

  async getUserHOTP(user: User) {
    this.loginOTP = await this.authDataService.findOne({
      entity: USER,
      entityUuid: user.uuid,
      authDataType: AuthDataType.LoginOTP,
    });

    if (this.loginOTP && this.loginOTP.expiry > new Date()) {
      return hotp.generate(
        this.loginOTP.metaData.secret as string,
        Number(this.loginOTP.metaData.counter),
      );
    }
  }

  isUserTOTPValid(sharedSecret: AuthData, code: string) {
    if (sharedSecret) {
      totp.options = { window: 2 };
      return authenticator.verify({
        secret: sharedSecret.password,
        token: code,
      });
    }
    return false;
  }

  async passwordLessLogin(payload: PasswordLessDto) {
    const user = await this.userService.findUserByIdentifier(payload.username);

    await this.validateLoginAttempts(user);

    if (!user.enablePasswordLess) {
      throw new PasswordLessLoginNotEnabledException();
    }

    if (user.disabled) {
      throw new ForbiddenException(i18n.__('User Disabled'));
    }

    const failedAttempts = await this.validateLoginAttempts(user);

    await this.validate2FaCode(user, payload.code, SystemLoginType.Database);
    await this.removeLoginHOTP(user);

    if (failedAttempts) {
      await this.clearAuthFailLogs(user.uuid);
    }

    await this.saveAuthLog(
      user.uuid,
      SystemLogCode.Success,
      { loginType: SystemLoginType.PasswordLess },
      SUCCESS,
    );
    return user;
  }

  async saveUser(user: UserAccountDto, roles?: Role[]) {
    const userEntity = {} as User;
    userEntity.uuid = uuidv4();
    userEntity.name = user.name;

    // process email field
    userEntity.email = user.email.toLowerCase().trim();
    userEntity.phone = user.phone;
    const authData = await this.authDataService.save({
      uuid: uuidv4(),
      password: this.cryptoService.hashPassword(user.password),
      authDataType: AuthDataType.Password,
      entity: USER,
      entityUuid: userEntity.uuid,
    });
    userEntity.password = authData.uuid;

    if (roles && roles.length) {
      userEntity.roles = roles.map(r => r.name);
    }

    const checkUser = await this.checkExistingUser(userEntity);

    if (checkUser) {
      await this.authDataService.remove(authData);
      throw new UserAlreadyExistsException();
    } else {
      return await this.userService.save(userEntity);
    }
  }

  validatePassword(user: UserAccountDto) {
    const result = this.passwordPolicy.validatePassword(user.password);
    if (result.errors.length > 0) {
      throw new BadRequestException({
        errors: result.errors,
        message: i18n.__('Password not secure'),
      });
    }
  }

  removeUserFromSessionUsers(req, uuid) {
    const users = req.session.users.filter(user => {
      if (user.uuid !== uuid) {
        return user;
      }
    });
    req.session.users = users;
  }

  async chooseUser(req, uuid: string) {
    if (!req.session.users) {
      req.session.users = [];
    }

    const userFromSessionUsers = req.session.users.find(user => {
      if (user.uuid === uuid) {
        return user;
      }
    });

    const reqUser = await this.userService.findOne({ uuid });
    if (!userFromSessionUsers || !reqUser) {
      throw new InvalidUserException();
    }

    req.session.selectedUser = uuid;
    return userFromSessionUsers;
  }

  logout(req, res) {
    req.session.users = [];
    req.logout(() => {
      delete req.session.selectedUser;

      if (req.query && req.query.redirect) {
        return res.redirect(req.query.redirect);
      }

      return res.json({ message: SUCCESS });
    });
  }

  logoutUuid(uuid: string, req, res) {
    if (
      req.session.users &&
      req.session.users.filter(user => user.uuid === uuid).length > 0
    ) {
      req.session.users.splice(
        req.session.users.indexOf(
          req.session.users.filter(user => user && user.uuid === uuid)[0],
        ),
        1,
      );

      // Check if uuid is passport session user
      if (req.session.user && req.session.user.uuid === uuid) {
        delete req.session.selectedUser;
      }
      const users = req.session?.users;

      req.logout(() => {
        req.session.users = users;
        req.session.save();
        if (req.query.redirect) {
          return res.redirect(req.query.redirect);
        }
        return res.json({ message: SUCCESS });
      });
    } else {
      throw new InvalidUserException();
    }
  }

  async passwordLess(payload, req) {
    const user = await this.passwordLessLogin(payload);
    this.logger.log(`${PASSWORDLESS}-${user.uuid}`, this.constructor.name);
    req[defaultOptions.property] = user;
    const users = req.session?.users;
    await this.requestLogIn(req);
    req.session.users = users;
    addSessionUser(req, {
      uuid: user.uuid,
      email: user.email,
      phone: user.phone,
    });
    return {
      user: user.email,
      path: payload.redirect,
    };
  }

  async removeLoginHOTP(user: User) {
    await this.getUserHOTP(user);
    if (this.loginOTP) {
      await this.authDataService.remove(this.loginOTP);
    }
  }

  async requestLogIn<
    TRequest extends {
      logIn: (user, callback: (error) => any) => any;
    } = any,
  >(request: TRequest): Promise<void> {
    const user = request[defaultOptions.property];
    await new Promise<void>((resolve, reject) =>
      request.logIn(user, err => (err ? reject(err) : resolve())),
    );
  }

  async validateLoginAttempts(user: User, allowedAttempts?: number) {
    const settings = await this.settings.find();
    allowedAttempts = allowedAttempts || settings.allowedFailedLoginAttempts;

    const attempts = await this.log.countDocuments({
      entity: USER,
      entityId: user.uuid,
      logType: SystemLogType.Authentication,
      logCode: SystemLogCode.Failure,
    });
    this.logger.log(
      `${LOGIN}-${user.uuid}`,
      `attempts-${attempts}`,
      this.constructor.name,
    );

    if (allowedAttempts && attempts >= allowedAttempts) {
      this.logger.log(
        `${LOGIN}-${user.uuid}`,
        `${i18n.__('User blocked due to failed login attempts')}`,
        this.constructor.name,
      );
      throw new ForbiddenException(
        i18n.__('User blocked due to failed login attempts'),
      );
    }

    return attempts;
  }

  async saveAuthLog(
    uuid: string,
    logCode: SystemLogCode,
    data: SystemLogData,
    reason: string,
    saveLog: boolean = true,
  ) {
    return await this.commandBus.execute(
      new AppendSystemLogCommand(
        {
          entity: USER,
          entityId: uuid,
          logType: SystemLogType.Authentication,
          logCode,
          data,
          reason,
        } as SystemLog,
        saveLog,
      ),
    );
  }
  async clearAuthFailLogs(uuid: string) {
    return await this.log.deleteMany({
      entity: USER,
      entityId: uuid,
      logType: SystemLogType.Authentication,
      logCode: SystemLogCode.Failure,
    });
  }

  async revokeToken(
    req: Request & { token?: BearerToken },
    clientId: string,
    uuid: string,
  ) {
    const bearerToken = req?.token;
    if (bearerToken?.client !== clientId) {
      throw new InvalidClientException();
    }
    if (bearerToken?.user !== uuid) {
      throw new InvalidUserException();
    }
    return await this.commandBus.execute(
      new RemoveBearerTokenCommand(bearerToken.accessToken),
    );
  }

  userModel(doc: Partial<User>) {
    return new this.userService.model(doc);
  }
}
