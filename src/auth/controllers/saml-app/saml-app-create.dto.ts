import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsArray,
  IsBoolean,
  IsOptional,
  IsString,
  IsUUID,
  ValidateNested,
} from 'class-validator';

import { i18n } from '../../../i18n/i18n.config';

export class SAMLAppClaimType {
  @IsString()
  @ApiProperty({ description: 'SAML claim unique ID.' })
  claimId: string;

  @IsString()
  @ApiProperty({ description: 'SAML claim display name.' })
  claimDisplayName: string;

  @IsString()
  @ApiProperty({ description: 'SAML claim name.' })
  name: string;

  @IsString()
  @ApiProperty({ description: 'Tag for the claim value.' })
  valueTag: string;

  @IsString()
  @ApiProperty({ description: 'Format of the claim name.' })
  nameFormat: string;

  @IsString()
  @ApiProperty({ description: 'Type of the claim value.' })
  valueXsiType: string;
}

export class CreateSAMLAppDTO {
  @IsOptional()
  @IsString()
  @ApiProperty({
    description: i18n.__('Name of the SAML application.'),
  })
  name?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    description: i18n.__('Description of the SAML application.'),
  })
  description?: string;

  @IsOptional()
  @IsUUID()
  @ApiProperty({
    description: i18n.__('Unique identifier (UUID) for the SAML application.'),
  })
  uuid?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    description: i18n.__('Certificate associated with the SAML application.'),
  })
  cert?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    description: i18n.__('Private key associated with the SAML application.'),
  })
  key?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    description: i18n.__('Relay state for the SAML application.'),
  })
  relayState?: string;

  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => SAMLAppClaimType)
  @ApiProperty({
    description: i18n.__(
      'List of claim types associated with the SAML application.',
    ),
    type: [SAMLAppClaimType],
  })
  claimTypes?: SAMLAppClaimType[];

  @IsOptional()
  @IsString()
  @ApiProperty({
    description: i18n.__('Scope of the SAML application.'),
  })
  scope?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    description: i18n.__('Name ID attribute used in the SAML application.'),
  })
  nameIDAttribute?: string;

  @IsOptional()
  @ApiProperty({
    description: i18n.__('Custom login URL for the SAML application.'),
  })
  customLoginUrl?: string;

  @IsOptional()
  @ApiProperty({
    description: i18n.__('Encrypt response then sign'),
  })
  @IsBoolean()
  encryptThenSign?: boolean;

  @IsOptional()
  @ApiProperty({
    description: i18n.__('Password for private key'),
  })
  @IsString()
  privateKeyPass?: string;

  @IsOptional()
  @ApiProperty({
    description: i18n.__('Disable SAML App'),
  })
  @IsBoolean()
  disable?: boolean;

  @IsString()
  @ApiProperty({
    description: i18n.__('SP XML Metadata'),
  })
  spMetadataXml?: string;
}
