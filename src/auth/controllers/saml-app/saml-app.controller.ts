import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ApiOperation } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { Roles } from '../../../common/decorators/roles.decorator';
import { ListQueryDto } from '../../../common/policies/list-query/list-query';
import { ADMINISTRATOR } from '../../../constants/app-strings';
import { i18n } from '../../../i18n/i18n.config';
import { AddSAMLAppCommand } from '../../commands/add-saml-app/add-saml-app.command';
import { ModifySAMLAppCommand } from '../../commands/modify-saml-app/modify-saml-app.command';
import { RemoveSAMLAppCommand } from '../../commands/remove-saml-app/remove-saml-app.command';
import { SAMLPostLoginCommand } from '../../commands/saml-post-login/saml-post-login.command';
import { SAMLPostLogoutCommand } from '../../commands/saml-post-logout/saml-post-logout.command';
import { SAMLAppService } from '../../entities/saml-app/saml-app.service';
import { BearerTokenGuard } from '../../guards/bearer-token.guard';
import { RoleGuard } from '../../guards/role.guard';
import { FetchSamlMetadataQuery } from '../../queries/fetch-saml-metadata/fetch-saml-metadata.query';
import { ListSAMLAppQuery } from '../../queries/list-saml-app/list-saml-app.query';
import { SamlLoginRedirectQuery } from '../../queries/saml-login-redirect/saml-login-redirect.query';
import { SamlLogoutRedirectQuery } from '../../queries/saml-logout-redirect/saml-login-redirect.query';
import { CreateSAMLAppDTO } from './saml-app-create.dto';

@Controller('saml_app')
export class SamlAppController {
  constructor(
    private readonly queryBus: QueryBus,
    private readonly commandBus: CommandBus,
    private readonly samlApp: SAMLAppService,
  ) {}

  @Get('metadata/:samlApp')
  metadata(@Param('samlApp') samlApp: string, @Res() res: Response) {
    return this.queryBus.execute(new FetchSamlMetadataQuery(samlApp, res));
  }

  @Post('login/:samlApp')
  loginPost(
    @Param('samlApp') samlApp: string,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    return this.commandBus.execute(new SAMLPostLoginCommand(samlApp, req, res));
  }

  @Get('login/:samlApp')
  loginHttpRedirect(
    @Param('samlApp') samlApp: string,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    return this.queryBus.execute(new SamlLoginRedirectQuery(samlApp, req, res));
  }

  @Post('logout/:samlApp')
  logoutPost(
    @Param('samlApp') samlApp: string,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    return this.commandBus.execute(
      new SAMLPostLogoutCommand(samlApp, req, res),
    );
  }

  @Get('logout/:samlApp')
  logoutHttpRedirect(
    @Param('samlApp') samlApp: string,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    return this.queryBus.execute(
      new SamlLogoutRedirectQuery(samlApp, req, res),
    );
  }

  @Get('v1/get/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async findOne(@Param('uuid') uuid: string, @Req() req) {
    const samlApp = await this.samlApp.findOne({ uuid });
    samlApp.privateKeyPass = undefined;
    return samlApp;
  }

  @Get('v1/list')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async list(@Query() query: ListQueryDto) {
    const { offset, limit, search, sort } = query;
    return await this.queryBus.execute(
      new ListSAMLAppQuery(offset, limit, search, sort),
    );
  }

  @Post('v1/create')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  @ApiOperation({
    summary: i18n.__('Create SAML app'),
    description: 'create account with SAML app',
  })
  async create(@Body() body: CreateSAMLAppDTO, @Req() req) {
    const createdBy = req.user.user;
    return await this.commandBus.execute(
      new AddSAMLAppCommand(body, createdBy),
    );
  }

  @Post('v1/update/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  @ApiOperation({
    summary: i18n.__('update SAML app'),
    description: 'update SAML app details',
  })
  async update(@Body() payload: CreateSAMLAppDTO, @Param('uuid') uuid: string) {
    return await this.commandBus.execute(
      new ModifySAMLAppCommand(payload, uuid),
    );
  }

  @Post('v1/delete/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async deleteByUUID(@Param('uuid') uuid, @Req() req) {
    const userUuid = req.user.user;
    return await this.commandBus.execute<RemoveSAMLAppCommand>(
      new RemoveSAMLAppCommand(userUuid, uuid),
    );
  }
}
