import { CqrsModule } from '@nestjs/cqrs';
import { Test, TestingModule } from '@nestjs/testing';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { SamlAppController } from './saml-app.controller';
import { UserService } from '../../../user-management/entities/user/user.service';
import { SAMLAppService } from '../../entities/saml-app/saml-app.service';

describe('Saml App Controller', () => {
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [CqrsModule],
      controllers: [SamlAppController],
      providers: [
        { provide: SAMLAppService, useValue: {} },
        { provide: UserService, useValue: {} },
        { provide: ServerSettingsService, useValue: {} },
      ],
    }).compile();
  });
  it('should be defined', () => {
    const controller: SamlAppController =
      module.get<SamlAppController>(SamlAppController);
    expect(controller).toBeDefined();
  });
});
