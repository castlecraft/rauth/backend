import { Test, TestingModule } from '@nestjs/testing';
import { CqrsModule } from '@nestjs/cqrs';
import { KerberosController } from './kerberos.controller';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';
import { UserService } from '../../../user-management/entities/user/user.service';
import { BearerTokenService } from '../../entities/bearer-token/bearer-token.service';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';

describe('Kerberos Controller', () => {
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [CqrsModule],
      controllers: [KerberosController],
      providers: [
        {
          provide: KerberosRealmService,
          useValue: {},
        },
        {
          provide: UserService,
          useValue: {},
        },
        {
          provide: BearerTokenService,
          useValue: {},
        },
        {
          provide: ServerSettingsService,
          useValue: {},
        },
      ],
    }).compile();
  });
  it('should be defined', () => {
    const controller: KerberosController =
      module.get<KerberosController>(KerberosController);
    expect(controller).toBeDefined();
  });
});
