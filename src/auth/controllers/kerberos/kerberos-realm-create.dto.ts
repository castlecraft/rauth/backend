import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsOptional, IsUUID } from 'class-validator';
import { i18n } from '../../../i18n/i18n.config';

export class CreateKerberosRealmDto {
  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('Identifies a realm uniquely'),
    required: true,
    example: 'Example Realm',
  })
  name: string;

  @IsOptional()
  @ApiProperty({
    description: i18n.__('Kerberos Realm description'),
    required: true,
    example: 'Example Kerberos Realm',
  })
  description: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('Kerberos domain name'),
    required: true,
    example: 'EXAMPLE.COM',
  })
  domain?: string;

  @IsUUID()
  @ApiProperty({
    description: 'ldap client',
    required: true,
    example: 'client',
  })
  ldapClient: string;

  @IsOptional()
  @ApiProperty({
    description: i18n.__('Kerberos service principal name'),
    required: true,
    type: 'string',
  })
  servicePrincipalName: string;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({
    description: i18n.__('Force the generation of an OTP.'),
  })
  forceOTP?: boolean;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({
    description: i18n.__('Disable Kerberos Realm'),
  })
  disabled?: boolean;
}
