import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

export const BearerToken = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4, unique: true, sparse: true },
    creation: Date,
    modified: Date,
    createdBy: String,
    modifiedBy: String,
    accessToken: { type: String, unique: true, sparse: true },
    refreshToken: { type: String, unique: true, sparse: true },
    redirectUris: [String],
    scope: [String],
    expiresIn: Number,
    user: String,
    client: String,
    deleteAfter: Date,
  },
  { collection: 'bearer_token', versionKey: false },
);

export const BEARER_TOKEN = 'BearerToken';

export const BearerTokenModel = mongoose.model(BEARER_TOKEN, BearerToken);
