import { Document } from 'mongoose';

export interface KerberosRealm extends Document {
  name?: string;
  description?: string;
  uuid?: string;
  domain?: string;
  ldapClient?: string;
  servicePrincipalName?: string;
  forceOTP?: boolean;
  createdBy?: string;
  modifiedBy?: string;
  creation?: Date;
  modified?: Date;
  disabled?: boolean;
}
