import { Test, TestingModule } from '@nestjs/testing';
import { KerberosRealmService } from './kerberos-realm.service';
import { KERBEROS_REALM } from './kerberos-realm.schema';

describe('KerberosRealmService', () => {
  let service: KerberosRealmService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        KerberosRealmService,
        {
          provide: KERBEROS_REALM,
          useValue: {}, // provide mock values
        },
      ],
    }).compile();
    service = module.get<KerberosRealmService>(KerberosRealmService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
