import { Injectable, Inject } from '@nestjs/common';
import { KERBEROS_REALM } from './kerberos-realm.schema';
import { Model } from 'mongoose';
import { KerberosRealm } from './kerberos-realm.interface';

@Injectable()
export class KerberosRealmService {
  constructor(
    @Inject(KERBEROS_REALM)
    private readonly model: Model<KerberosRealm>,
  ) {}

  async save(params) {
    const model = new this.model(params);
    return await model.save();
  }

  async findOne(params) {
    return await this.model.findOne(params);
  }

  public async clear(): Promise<any> {
    return await this.model.deleteMany({});
  }

  public async find(params?) {
    return await this.model.find(params);
  }

  async remove(model: KerberosRealm) {
    return await model.deleteOne();
  }

  async list(
    search: string,
    query: any,
    sortQuery?: any,
    offset: number = 0,
    limit: number = 20,
  ) {
    if (search) {
      // Search through multiple keys
      // https://stackoverflow.com/a/41390870
      const nameExp = new RegExp(search, 'i');
      query.$or = ['name', 'uuid'].map(field => {
        const out = {};
        out[field] = nameExp;
        return out;
      });
    }

    const data = this.model
      .find(query, { adminPassword: 0 })
      .skip(Number(offset))
      .limit(Number(limit))
      .sort(sortQuery);

    return {
      docs: await data.exec(),
      length: await this.model.countDocuments(query),
      offset: Number(offset),
    };
  }
}
