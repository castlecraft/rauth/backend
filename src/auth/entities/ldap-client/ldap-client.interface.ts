import { Document } from 'mongoose';

export interface LDAPClient extends Document {
  name?: string;
  description?: string;
  uuid?: string;
  url?: string;
  adminDn?: string;
  adminPassword?: string;
  userSearchBase?: string;
  usernameAttribute?: string;
  emailAttribute?: string;
  phoneAttribute?: string;
  fullNameAttribute?: string;
  attributes?: LdapAttribute[];
  scope?: string;
  clientId: string;
  createdBy?: string;
  modifiedBy?: string;
  creation?: Date;
  modified?: Date;
  disabled?: boolean;
  timeoutMs?: number;
  allowedFailedLoginAttempts?: number;
  skipMobileVerification?: boolean;
  disableUserCreation?: boolean;
  isSecure?: boolean;
  key?: string;
  keyPassphrase?: string;
  cert?: string;
  ca?: string[];
}

export interface LdapAttribute {
  claim?: string;
  mapTo?: string;
}
