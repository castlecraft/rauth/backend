import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';
import { FIVE_THOUSAND_MS } from '../../../constants/app-strings';

export const schema = new mongoose.Schema(
  {
    name: String,
    description: String,
    uuid: { type: String, default: uuidv4, unique: true, sparse: true },
    url: String,
    adminDn: String,
    adminPassword: String,
    userSearchBase: String,
    usernameAttribute: String,
    emailAttribute: String,
    phoneAttribute: String,
    fullNameAttribute: String,
    attributes: { type: [mongoose.Schema.Types.Mixed] },
    scope: String,
    clientId: String,
    createdBy: String,
    modifiedBy: String,
    creation: { type: Date, default: () => new Date() },
    modified: Date,
    disabled: { type: Boolean, default: false },
    timeoutMs: { type: Number, default: FIVE_THOUSAND_MS },
    allowedFailedLoginAttempts: Number,
    skipMobileVerification: { type: Boolean, default: false },
    disableUserCreation: { type: Boolean, default: false },
    isSecure: { type: Boolean, default: false },
    key: String,
    keyPassphrase: String,
    cert: String,
    ca: [String],
  },
  { collection: 'ldap_client', versionKey: false },
);

export const LDAPClient = schema;

export const LDAP_CLIENT = 'LDAPClient';

export const LDAPServerModel = mongoose.model(LDAP_CLIENT, LDAPClient);
