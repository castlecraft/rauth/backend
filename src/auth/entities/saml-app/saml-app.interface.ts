import { Document } from 'mongoose';

export enum NameIDFormat {
  EmailAddress = 'emailAddress',
  Unspecified = 'unspecified',
}

export interface SAMLAppClaimType {
  claimId?: string;
  claimDisplayName?: string;
  name?: string;
  valueTag?: string;
  nameFormat?: string;
  valueXsiType?: string;
}

export interface SAMLApp extends Document {
  name?: string;
  description?: string;
  uuid?: string;
  cert?: string;
  key?: string;
  relayState?: string;
  claimTypes?: SAMLAppClaimType[];
  scope?: string;
  nameIDAttribute?: string;
  customLoginUrl?: string;
  createdBy?: string;
  modifiedBy?: string;
  creation?: Date;
  modified?: Date;
  encryptThenSign?: boolean;
  privateKeyPass?: string;
  disable?: boolean;
  spMetadataXml?: string;
}
