import { Test, TestingModule } from '@nestjs/testing';
import { SAMLAppService } from './saml-app.service';
import { SAML_APP } from './saml-app.schema';

describe('SAMLAppService', () => {
  let service: SAMLAppService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SAMLAppService,
        {
          provide: SAML_APP,
          useValue: {}, // provide mock values
        },
      ],
    }).compile();
    service = module.get<SAMLAppService>(SAMLAppService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
