import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { SAMLApp } from './saml-app.interface';
import { SAML_APP } from './saml-app.schema';

@Injectable()
export class SAMLAppService {
  constructor(
    @Inject(SAML_APP)
    private readonly model: Model<SAMLApp>,
  ) {}

  async save(params) {
    const model = new this.model(params);
    return await model.save();
  }

  async findOne(params) {
    return await this.model.findOne(params);
  }

  public async clear(): Promise<any> {
    return await this.model.deleteMany({});
  }

  public async find(params?) {
    return await this.model.find(params);
  }

  async remove(model: SAMLApp) {
    return await model.deleteOne();
  }

  async list(
    search: string,
    query: any,
    sortQuery?: any,
    offset: number = 0,
    limit: number = 20,
  ) {
    if (search) {
      // Search through multiple keys
      // https://stackoverflow.com/a/41390870
      const nameExp = new RegExp(search, 'i');
      query.$or = Object.keys(this.model.schema.obj).map(field => {
        const out = {};
        out[field] = nameExp;
        return out;
      });
    }

    const data = this.model
      .find(query, { adminPassword: 0 })
      .skip(Number(offset))
      .limit(Number(limit))
      .sort(sortQuery);

    return {
      docs: await data.exec(),
      length: await this.model.countDocuments(query),
      offset: Number(offset),
    };
  }
}
