import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

export const SAML_APP_COLLECTION_NAME = 'saml_app';

export const SAMLApp = new mongoose.Schema(
  {
    name: String,
    description: String,
    uuid: { type: String, default: uuidv4, unique: true, sparse: true },
    cert: String,
    key: String,
    customLoginUrl: String,
    relayState: String,
    claimTypes: [mongoose.Schema.Types.Mixed],
    scope: String,
    nameIDAttribute: String,
    createdBy: String,
    modifiedBy: String,
    creation: { type: Date, default: nowDate },
    modified: Date,
    encryptThenSign: { type: Boolean, default: false },
    privateKeyPass: String,
    disable: { type: Boolean, default: false },
    spMetadataXml: String,
  },
  { collection: SAML_APP_COLLECTION_NAME, versionKey: false },
);

export const SAML_APP = 'SAMLApp';

export const SAMLAppModel = mongoose.model(SAML_APP, SAMLApp);

function nowDate() {
  return new Date();
}
