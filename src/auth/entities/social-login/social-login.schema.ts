import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

export const schema = new mongoose.Schema(
  {
    name: String,
    description: String,
    disableUserCreation: { type: Boolean, default: false },
    uuid: { type: String, default: uuidv4, unique: true, sparse: true },
    clientId: String,
    clientSecret: String,
    authorizationURL: String,
    tokenURL: String,
    introspectionURL: String,
    baseURL: String,
    profileURL: String,
    revocationURL: String,
    emailAttribute: String,
    phoneAttribute: String,
    createdBy: String,
    modifiedBy: String,
    creation: { type: Date, default: nowDate },
    modified: Date,
    scope: [String],
    claimsScope: String,
    clientSecretToTokenEndpoint: { type: Boolean, default: false },
    disabled: { type: Boolean, default: false },
    claimsMap: { type: [mongoose.Schema.Types.Mixed], default: [] },
    usernameClaim: String,
    emailClaim: String,
    phoneClaim: String,
    fullNameClaim: String,
  },
  { collection: 'social_login', versionKey: false },
);

export const SocialLogin = schema;

export const SOCIAL_LOGIN = 'SocialLogin';

export const SocialLoginModel = mongoose.model(SOCIAL_LOGIN, SocialLogin);

function nowDate() {
  return new Date();
}
