import { Document } from 'mongoose';

export interface SocialLoginClaim {
  claim?: string;
  mapTo?: string;
}

export interface SocialLogin extends Document {
  name?: string;
  description?: string;
  disableUserCreation?: boolean;
  uuid?: string;
  clientId?: string;
  clientSecret?: string;
  authorizationURL?: string;
  tokenURL?: string;
  introspectionURL?: string;
  baseURL?: string;
  profileURL?: string;
  revocationURL?: string;
  emailAttribute?: string;
  phoneAttribute?: string;
  createdBy?: string;
  modifiedBy?: string;
  creation?: Date;
  modified?: Date;
  scope?: string[];
  claimsScope?: string;
  clientSecretToTokenEndpoint?: boolean;
  disabled?: boolean;
  claimsMap?: SocialLoginClaim[];
  usernameClaim?: string;
  emailClaim?: string;
  phoneClaim?: string;
  fullNameClaim?: string;
}
