import { Module } from '@nestjs/common';
import { AuthorizationCodeService } from './authorization-code/authorization-code.service';
import { BearerTokenService } from './bearer-token/bearer-token.service';
import { AuthModuleEntities } from './entities';
import { KerberosRealmService } from './kerberos-realm/kerberos-realm.service';
import { LDAPClientService } from './ldap-client/ldap-client.service';
import { OIDCKeyService } from './oidc-key/oidc-key.service';
import { SAMLAppService } from './saml-app/saml-app.service';
import { SessionService } from './session/session.service';
import { SocialLoginService } from './social-login/social-login.service';
import { UserClaimService } from './user-claim/user-claim.service';

@Module({
  providers: [
    ...AuthModuleEntities,
    AuthorizationCodeService,
    BearerTokenService,
    KerberosRealmService,
    LDAPClientService,
    OIDCKeyService,
    SAMLAppService,
    SessionService,
    SocialLoginService,
    UserClaimService,
  ],
  exports: [
    ...AuthModuleEntities,
    AuthorizationCodeService,
    BearerTokenService,
    KerberosRealmService,
    LDAPClientService,
    OIDCKeyService,
    SAMLAppService,
    SessionService,
    SocialLoginService,
    UserClaimService,
  ],
})
export class AuthEntitiesModule {}
