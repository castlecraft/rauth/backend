import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

export const UserClaim = new mongoose.Schema(
  {
    claimId: { type: String, default: uuidv4, unique: true, sparse: true },
    uuid: { type: String, sparse: true }, // uuid of user
    scope: String,
    name: String,
    value: mongoose.Schema.Types.Mixed,
  },
  { collection: 'user_claim', versionKey: false },
);

export const USER_CLAIM = 'UserClaim';

export const UserClaimModel = mongoose.model(USER_CLAIM, UserClaim);
