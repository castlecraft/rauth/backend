import { Injectable } from '@nestjs/common';
import * as crypto from 'crypto';
import { PasswordDecryptionException } from '../../../common/filters/exceptions';
import {
  ConfigService,
  PASSWORD_ENCRYPTION_KEY,
} from '../../../config/config.service';

@Injectable()
export class PasswordCryptoService {
  private readonly algorithm = 'aes-256-ctr';
  private readonly ENCRYPTION_KEY = Buffer.from(
    this.configService.get(PASSWORD_ENCRYPTION_KEY),
    'utf-8',
  );
  private readonly IV_LENGTH = 16;

  constructor(private readonly configService: ConfigService) {}

  encrypt(text: string): string {
    const iv = crypto.randomBytes(this.IV_LENGTH); // Generate a random IV
    const cipher = crypto.createCipheriv(
      this.algorithm,
      this.ENCRYPTION_KEY,
      iv,
    );
    let encrypted = cipher.update(text, 'utf8', 'hex');
    encrypted += cipher.final('hex');
    return iv.toString('hex') + ':' + encrypted; // Return IV and encrypted text
  }

  decrypt(text: string): string {
    try {
      // Extract the IV
      const textParts = text.split(':');
      const iv = Buffer.from(textParts.shift() as string, 'hex');

      // Get the encrypted text
      const encryptedText = Buffer.from(textParts.join(':'), 'hex');
      const decipher = crypto.createDecipheriv(
        this.algorithm,
        this.ENCRYPTION_KEY,
        iv,
      );

      // Update with undefined for input encoding
      let decrypted = decipher.update(encryptedText, undefined, 'utf8');
      decrypted += decipher.final('utf8');

      // Return the decrypted text
      return decrypted;
    } catch (error) {
      throw new PasswordDecryptionException(error);
    }
  }
}
