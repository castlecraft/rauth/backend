import { ApiProperty } from '@nestjs/swagger';
import { IsUUID, IsOptional } from 'class-validator';
import { i18n } from '../../../i18n/i18n.config';

export class RegisterDeviceDto {
  @IsUUID()
  @IsOptional()
  @ApiProperty({
    description: i18n.__('UUID of register device'),
    required: true,
    type: 'string',
  })
  userUuid: string;
}
