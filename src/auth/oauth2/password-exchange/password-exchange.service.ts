import { HttpStatus, Injectable } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { randomBytes } from 'crypto';
import { TokenError } from 'oauth2orize';
import { v4 as uuidv4 } from 'uuid';

import { Client } from '../../../client-management/entities/client/client.interface';
import { ClientService } from '../../../client-management/entities/client/client.service';
import { MfaTokenError } from '../../../common/filters/exceptions';
import { OAuth2ErrorMessage } from '../../../common/filters/oauth2-error.messages';
import { CryptographerService } from '../../../common/services/cryptographer/cryptographer.service';
import { SCOPE_OPENID } from '../../../constants/app-strings';
import {
  CLIENT_DISABLED,
  INVALID_CLIENT,
  INVALID_MFA_TOKEN,
  INVALID_SCOPE,
  MFA_REQUIRED,
} from '../../../constants/messages';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import {
  AccessType,
  SystemLoginType,
} from '../../../system-settings/entities/system-log/system-log.interface';
import {
  AuthData,
  AuthDataType,
} from '../../../user-management/entities/auth-data/auth-data.interface';
import { AuthDataService } from '../../../user-management/entities/auth-data/auth-data.service';
import { User } from '../../../user-management/entities/user/user.interface';
import { USER } from '../../../user-management/entities/user/user.schema';
import { UserService } from '../../../user-management/entities/user/user.service';
import { GenerateBearerTokenCommand } from '../../commands/generate-bearer-token/generate-bearer-token.command';
import { SendLoginOTPCommand } from '../../commands/send-login-otp/send-login-otp.command';
import { VerifyLDAPUserCommand } from '../../commands/verify-ldap-user/verify-ldap-user.command';
import { AuthService } from '../../controllers/auth/auth.service';
import { IDTokenGrantService } from '../id-token-grant/id-token-grant.service';
import { OAuth2TokenGeneratorService } from '../oauth2-token-generator/oauth2-token-generator.service';

export const MFA_TOKEN_EXPIRY_MIN = 5;

@Injectable()
export class PasswordExchangeService {
  constructor(
    private readonly authDataService: AuthDataService,
    private readonly clientService: ClientService,
    private readonly userService: UserService,
    private readonly cryptographerService: CryptographerService,
    private readonly tokenGeneratorService: OAuth2TokenGeneratorService,
    private readonly idTokenGeneratorService: IDTokenGrantService,
    private readonly settingsService: ServerSettingsService,
    private readonly authService: AuthService,
    private readonly commandBus: CommandBus,
  ) {}

  async exchangePassword(client, username, password, scope, body, done) {
    if (!scope) {
      return done(
        new TokenError(INVALID_SCOPE, OAuth2ErrorMessage.InvalidScope),
      );
    }

    // Validate the client
    try {
      const localClient = await this.clientService.findOne({
        clientId: client.clientId,
      });

      if (!localClient) {
        return done(null, false);
      }

      if (localClient.clientSecret !== client.clientSecret) {
        return done(null, false);
      }

      if (localClient.disabled) {
        return done(
          new TokenError(
            CLIENT_DISABLED,
            OAuth2ErrorMessage.UnauthorizedClient,
          ),
        );
      }

      const accessType = this.getValidLoginType(body);
      if (accessType === SystemLoginType.LDAP && !body.access_id) {
        return done(
          new TokenError(INVALID_CLIENT, OAuth2ErrorMessage.AccessDenied),
        );
      }
      // Validate the user
      const ldapUser =
        accessType === SystemLoginType.LDAP
          ? await this.commandBus.execute(
              new VerifyLDAPUserCommand(
                username,
                password,
                body.access_id,
                false,
                false,
              ),
            )
          : undefined;
      const user =
        accessType === SystemLoginType.Database
          ? await this.userService.findUserByIdentifier(username)
          : ldapUser?.user;

      if (!user) {
        return done(null, false);
      }

      if (accessType === SystemLoginType.Database) {
        const authData = await this.authDataService.findOne({
          uuid: user.password,
        });
        try {
          const validPassword = await this.cryptographerService.checkPassword(
            authData?.password,
            password,
          );
          if (!validPassword) {
            return done(null, false);
          }
        } catch {
          return done(null, false);
        }
      }

      // Validate Scopes
      const validScope = await this.tokenGeneratorService.getValidScopes(
        localClient,
        scope,
      );

      if (localClient.is2FAMandated || user.enable2fa || user.enableOTP) {
        const mfaToken = await this.generateMfaToken(
          localClient,
          user,
          validScope,
          accessType,
        );
        await this.commandBus.execute(new SendLoginOTPCommand(user));
        return done(
          new MfaTokenError(
            mfaToken,
            MFA_REQUIRED,
            OAuth2ErrorMessage.MfaRequired,
            '',
            HttpStatus.UNAUTHORIZED,
          ),
        );
      }

      // Everything validated, return the token
      const { bearerToken, extraParams } = await this.commandBus.execute(
        new GenerateBearerTokenCommand(
          localClient,
          undefined,
          user,
          validScope,
        ),
      );

      if (scope?.includes(SCOPE_OPENID)) {
        extraParams.id_token =
          await this.idTokenGeneratorService.generateIdToken(
            localClient.clientId,
            user,
            localClient.allowedScopes,
            bearerToken.accessToken,
          );
      }

      return done(
        null,
        bearerToken.accessToken,
        bearerToken.refreshToken,
        extraParams,
      );
    } catch (error) {
      return done(new TokenError(error.toString()));
    }
  }

  async authenticateMfaToken(
    mfaToken: string,
    authenticated: (err: TokenError, token?: AuthData | boolean) => void,
  ) {
    const token = await this.authDataService.findOne({
      authDataType: AuthDataType.MfaToken,
      password: mfaToken,
      entity: USER,
    });

    if (!token) {
      return authenticated(
        new TokenError(
          INVALID_MFA_TOKEN,
          OAuth2ErrorMessage.AccessDenied,
          HttpStatus.UNAUTHORIZED,
        ),
      );
    }
    if (token.expiry < new Date()) {
      return authenticated(
        new TokenError(
          INVALID_MFA_TOKEN,
          OAuth2ErrorMessage.AccessDenied,
          HttpStatus.UNAUTHORIZED,
        ),
      );
    }

    return authenticated(undefined, token);
  }

  async exchangeMfaToken(
    client: Client,
    mfaToken: AuthData,
    code: string,
    issued: (...args) => void,
  ) {
    try {
      const localClient = await this.clientService.findOne({
        clientId: client.clientId,
      });

      if (!localClient) {
        return issued(null, false);
      }

      if (localClient.clientSecret !== client.clientSecret) {
        return issued(null, false);
      }

      if (localClient.disabled) {
        return issued(
          new TokenError(
            CLIENT_DISABLED,
            OAuth2ErrorMessage.UnauthorizedClient,
          ),
        );
      }
      // Validate the user
      const user = await this.userService.findUserByIdentifier(
        mfaToken.entityUuid,
      );
      if (!user) {
        return issued(null, false);
      }
      await this.authService.validate2FaCode(
        user,
        code,
        mfaToken.metaData.loginType,
        true,
      );
      await this.authService.removeLoginHOTP(user);

      // Clear MFA Token after use
      await this.authDataService.remove(mfaToken);

      // Everything validated, return the token
      const { bearerToken, extraParams } = await this.commandBus.execute(
        new GenerateBearerTokenCommand(
          localClient,
          undefined,
          user,
          localClient.allowedScopes,
        ),
      );

      if ((mfaToken.metaData?.scope || []).includes(SCOPE_OPENID)) {
        extraParams.id_token =
          await this.idTokenGeneratorService.generateIdToken(
            localClient.clientId,
            user,
            localClient.allowedScopes,
            bearerToken.accessToken,
          );
      }
      return issued(
        null,
        bearerToken.accessToken,
        bearerToken.refreshToken,
        extraParams,
      );
    } catch (error) {
      return issued(
        new TokenError(error.toString(), OAuth2ErrorMessage.ServerError),
      );
    }
  }

  async generateMfaToken(
    client: Client,
    user: User,
    scope: string[],
    loginType: SystemLoginType = SystemLoginType.Database,
  ) {
    const settings = await this.settingsService.find();
    const expiry = new Date();
    expiry.setDate(
      expiry.getDate() + settings.otpExpiry || MFA_TOKEN_EXPIRY_MIN,
    );
    const authData = {
      uuid: uuidv4(),
      password: randomBytes(16).toString('hex'),
      metaData: {
        client: client.clientId,
        scope: scope || [],
        loginType,
      },
      entity: USER,
      entityUuid: user.uuid,
      expiry,
      authDataType: AuthDataType.MfaToken,
    };
    await this.authDataService.save(authData);
    return authData.password;
  }

  getValidLoginType(body: { access_type?: string }) {
    switch (body.access_type as AccessType) {
      case AccessType.LDAP:
        return SystemLoginType.LDAP;
      default:
        return SystemLoginType.Database;
    }
  }
}
