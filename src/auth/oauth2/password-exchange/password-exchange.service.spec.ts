import { CommandBus } from '@nestjs/cqrs';
import { Test, TestingModule } from '@nestjs/testing';
import { BearerTokenService } from '../../../auth/entities/bearer-token/bearer-token.service';
import { ClientService } from '../../../client-management/entities/client/client.service';
import { CryptographerService } from '../../../common/services/cryptographer/cryptographer.service';
import { ConfigService } from '../../../config/config.service';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { AuthDataService } from '../../../user-management/entities/auth-data/auth-data.service';
import { UserService } from '../../../user-management/entities/user/user.service';
import { AuthService } from '../../controllers/auth/auth.service';
import { IDTokenGrantService } from '../id-token-grant/id-token-grant.service';
import { OAuth2TokenGeneratorService } from '../oauth2-token-generator/oauth2-token-generator.service';
import { PasswordExchangeService } from './password-exchange.service';

describe('PasswordExchangeService', () => {
  let service: PasswordExchangeService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PasswordExchangeService,
        { provide: AuthDataService, useValue: {} },
        { provide: CryptographerService, useValue: {} },
        { provide: BearerTokenService, useValue: {} },
        { provide: UserService, useValue: {} },
        { provide: ClientService, useValue: {} },
        { provide: OAuth2TokenGeneratorService, useValue: {} },
        {
          provide: ConfigService,
          useValue: {
            get(env) {
              switch (env) {
                case 'TOKEN_VALIDITY':
                  return 3600;
              }
            },
          },
        },
        { provide: CommandBus, useValue: {} },
        { provide: AuthService, useValue: {} },
        { provide: IDTokenGrantService, useValue: {} },
        { provide: ServerSettingsService, useValue: {} },
      ],
    }).compile();
    service = module.get<PasswordExchangeService>(PasswordExchangeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
