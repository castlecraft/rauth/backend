import { Injectable } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { TokenError } from 'oauth2orize';

import { ClientService } from '../../../client-management/entities/client/client.service';
import { OAuth2ErrorMessage } from '../../../common/filters/oauth2-error.messages';
import { CLIENT_DISABLED } from '../../../constants/messages';
import { UserService } from '../../../user-management/entities/user/user.service';
import { GenerateBearerTokenCommand } from '../../commands/generate-bearer-token/generate-bearer-token.command';
import { RemoveBearerTokenCommand } from '../../commands/remove-bearer-token/remove-bearer-token.command';
import { BearerTokenService } from '../../entities/bearer-token/bearer-token.service';
import { OAuth2TokenGeneratorService } from '../oauth2-token-generator/oauth2-token-generator.service';

@Injectable()
export class RefreshTokenExchangeService {
  constructor(
    private readonly clientService: ClientService,
    private readonly tokenGeneratorService: OAuth2TokenGeneratorService,
    private readonly bearerTokenService: BearerTokenService,
    private readonly userService: UserService,
    private readonly commandBus: CommandBus,
  ) {}

  async exchangeRefreshToken(client, refreshToken, done) {
    try {
      // Validate Refresh Token
      const localRefreshToken = await this.bearerTokenService.findOne({
        refreshToken,
      });

      if (!localRefreshToken) {
        return done(null, false);
      }

      // Validate Client
      const localClient =
        (await this.clientService.findOne({
          clientId: client?.clientId,
        })) ||
        (await this.clientService.findOne({
          uuid: localRefreshToken.client,
        }));

      if (!localClient) {
        return done(null, false);
      }

      if (localClient.revokeOnRefresh) {
        try {
          await this.commandBus.execute(
            new RemoveBearerTokenCommand(localRefreshToken.accessToken),
          );
        } catch (error) {}
      }

      if (localClient.disabled) {
        return done(
          new TokenError(
            CLIENT_DISABLED,
            OAuth2ErrorMessage.UnauthorizedClient,
          ),
        );
      }

      // Validate Scopes
      const scope = await this.tokenGeneratorService.getValidScopes(
        localClient,
        localRefreshToken.scope,
      );

      const user = await this.userService.findOne({
        uuid: localRefreshToken.user,
      });

      // Everything validated, return the token
      const { bearerToken, extraParams } = await this.commandBus.execute(
        new GenerateBearerTokenCommand(client, undefined, user, scope, true),
      );
      return done(
        null,
        bearerToken.accessToken,
        bearerToken.refreshToken,
        extraParams,
      );
    } catch (error) {
      return done(new TokenError(error));
    }
  }
}
