import { HttpStatus, Injectable } from '@nestjs/common';
import { AuthorizationError } from 'oauth2orize';

import { AuthorizationCode } from '../../../auth/entities/authorization-code/authorization-code.interface';
import { AuthorizationCodeService } from '../../../auth/entities/authorization-code/authorization-code.service';
import { PKCEMethods } from '../../../client-management/entities/client/client.interface';
import { ClientService } from '../../../client-management/entities/client/client.service';
import { OAuth2ErrorMessage } from '../../../common/filters/oauth2-error.messages';
import { CryptographerService } from '../../../common/services/cryptographer/cryptographer.service';
import { THIRTY_NUMBER } from '../../../constants/app-strings';
import {
  CLIENT_DISABLED,
  CLIENT_REQUIRES_2FA_FOR_USER,
  INVALID_CLIENT,
  INVALID_CODE_CHALLENGE,
  INVALID_CODE_CHALLENGE_METHOD,
  INVALID_USER,
} from '../../../constants/messages';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { UserService } from '../../../user-management/entities/user/user.service';
import { OAuth2TokenGeneratorService } from '../oauth2-token-generator/oauth2-token-generator.service';

@Injectable()
export class CodeGrantService {
  constructor(
    private readonly userService: UserService,
    private readonly clientService: ClientService,
    private readonly tokenGeneratorService: OAuth2TokenGeneratorService,
    private readonly authorizationCodeService: AuthorizationCodeService,
    private readonly cryptographerService: CryptographerService,
    private readonly settings: ServerSettingsService,
  ) {}

  async grantCode(client, redirectUri, user, ares, areq, done) {
    try {
      const code = this.cryptographerService.getUid(16);
      const localClient = await this.clientService.findOne({
        clientId: areq.clientID,
      });

      if (!localClient) {
        return done(
          new AuthorizationError(
            INVALID_CLIENT,
            OAuth2ErrorMessage.UnauthorizedClient,
          ),
          null,
        );
      }

      if (localClient.disabled) {
        return done(
          new AuthorizationError(
            CLIENT_DISABLED,
            OAuth2ErrorMessage.UnauthorizedClient,
          ),
          null,
        );
      }

      const localUser = await this.userService.findOne({
        uuid: user.uuid,
      });

      if (!localUser) {
        return done(
          new AuthorizationError(INVALID_USER, OAuth2ErrorMessage.AccessDenied),
          null,
        );
      }

      const is2FAMandated = localUser.enable2fa || localUser.enableOTP;
      if (localClient.is2FAMandated && !is2FAMandated) {
        return done(
          new AuthorizationError(
            CLIENT_REQUIRES_2FA_FOR_USER,
            OAuth2ErrorMessage.AccessDenied,
          ),
          null,
        );
      }

      const scope = await this.tokenGeneratorService.getValidScopes(
        localClient,
        areq.scope,
      );
      const deleteAfter = new Date();
      const settings = await this.settings.find();
      deleteAfter.setMinutes(
        deleteAfter.getMinutes() + localClient.authCodeExpiresInMinutes ||
          settings.authCodeExpiresInMinutes ||
          THIRTY_NUMBER,
      );

      // Enforce PKCE if configured
      if (client.enforcePKCE === true && !areq.codeChallenge) {
        return done(
          new AuthorizationError(
            INVALID_CODE_CHALLENGE,
            OAuth2ErrorMessage.InvalidRequest,
          ),
          null,
        );
      }

      const codePayload = {
        code,
        client: localClient.clientId,
        redirectUri,
        user: localUser.uuid,
        scope,
        deleteAfter,
      } as AuthorizationCode;

      if (
        areq.codeChallenge &&
        !areq.codeChallengeMethod &&
        !client.pkceMethods?.includes(PKCEMethods.PLAIN)
      ) {
        return done(
          new AuthorizationError(
            INVALID_CODE_CHALLENGE_METHOD,
            OAuth2ErrorMessage.InvalidRequest,
          ),
          null,
        );
      }
      if (
        areq.codeChallengeMethod &&
        areq.codeChallengeMethod.toLowerCase() === 'plain' &&
        !client.pkceMethods?.includes('plain')
      ) {
        return done(
          new AuthorizationError(
            INVALID_CODE_CHALLENGE_METHOD,
            OAuth2ErrorMessage.InvalidRequest,
          ),
          null,
        );
      }

      if (areq.codeChallengeMethod) {
        codePayload.codeChallengeMethod =
          areq.codeChallengeMethod.toLowerCase();
      }

      if (
        codePayload.codeChallengeMethod &&
        !['plain', 's256'].includes(codePayload.codeChallengeMethod)
      ) {
        return done(
          new AuthorizationError(
            INVALID_CODE_CHALLENGE_METHOD,
            OAuth2ErrorMessage.InvalidRequest,
          ),
          null,
        );
      }

      if (areq.codeChallenge) {
        codePayload.codeChallenge = areq.codeChallenge;
      }

      if (areq && areq.nonce) {
        codePayload.nonce = areq.nonce;
      }

      await this.authorizationCodeService.save(codePayload);
      return done(null, code);
    } catch (error) {
      return done(
        new AuthorizationError(
          error.message || error,
          error.code || OAuth2ErrorMessage.ServerError,
          undefined,
          error.status || HttpStatus.INTERNAL_SERVER_ERROR,
        ),
        null,
      );
    }
  }
}
