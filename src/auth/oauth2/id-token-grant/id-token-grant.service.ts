import { Injectable } from '@nestjs/common';
import crypto from 'crypto';
import AuthorizationError from 'oauth2orize-openid/lib/errors/authorizationerror';

import { OIDCKeyService } from '../../../auth/entities/oidc-key/oidc-key.service';
import { Client } from '../../../client-management/entities/client/client.interface';
import { OAuth2ErrorMessage } from '../../../common/filters/oauth2-error.messages';
import { ConfigService, TOKEN_VALIDITY } from '../../../config/config.service';
import {
  ROLES,
  SCOPE_EMAIL,
  SCOPE_PHONE,
  SCOPE_PROFILE,
} from '../../../constants/app-strings';
import { CLIENT_DISABLED } from '../../../constants/messages';
import { ServerSettings } from '../../../system-settings/entities/server-settings/server-settings.interface';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { User } from '../../../user-management/entities/user/user.interface';
import { UserService } from '../../../user-management/entities/user/user.service';
import { UserClaimService } from '../../entities/user-claim/user-claim.service';
import { IDTokenClaims } from '../../middlewares/interfaces';

@Injectable()
export class IDTokenGrantService {
  settings: ServerSettings;

  constructor(
    private readonly oidcKeyService: OIDCKeyService,
    private readonly settingsService: ServerSettingsService,
    private readonly configService: ConfigService,
    private readonly userClaimService: UserClaimService,
    private readonly userService: UserService,
  ) {}

  async grantIDToken(
    client: Client,
    user: User,
    req,
    done: (error?, idToken?) => void,
    accessToken?: string,
  ) {
    try {
      if (client.disabled) {
        return done(
          new AuthorizationError(
            CLIENT_DISABLED,
            OAuth2ErrorMessage.UnauthorizedClient,
          ),
        );
      }

      const signedToken = await this.generateIdToken(
        client.clientId,
        user,
        req.scope,
        accessToken,
        req.nonce,
      );
      return done(null, signedToken);
    } catch (error) {
      return done(new AuthorizationError(), null);
    }
  }

  async generateIdToken(
    clientId: string,
    user: User,
    scope: string[],
    accessToken?: string,
    nonce?: string,
  ) {
    if (!this.settings)
      this.settings = await this.settingsService.findWithoutError();

    const issuedAt = Date.parse(new Date().toString()) / 1000;
    const localUser = await this.userService.findOne({ uuid: user.uuid });
    const claims: IDTokenClaims = {
      iss: this.settings?.issuerUrl,
      aud: clientId,
      iat: Math.trunc(issuedAt),
      exp:
        Math.trunc(issuedAt) + Number(this.configService.get(TOKEN_VALIDITY)),
      sub: localUser.uuid,
      nonce,
    };

    if (scope.includes(ROLES)) {
      claims.roles = localUser.roles;
    }
    if (scope.includes(SCOPE_EMAIL)) {
      claims.email = localUser.email;
      claims.email_verified = localUser.isEmailVerified;
    }
    if (scope.includes(SCOPE_PROFILE)) {
      claims.name = localUser.name;
    }
    if (scope.includes(SCOPE_PHONE) && localUser.phone) {
      claims.phone_number = localUser.phone;
      claims.phone_number_verified = !localUser.unverifiedPhone;
    }

    const userClaims = await this.userClaimService.find({
      uuid: localUser.uuid,
      scope: { $in: scope },
    });

    if (userClaims && userClaims.length > 0) {
      userClaims.forEach(claim => {
        claims[claim.name] = claim.value;
      });
    }

    if (accessToken) {
      // Thanks https://github.com/mozilla/fxa-oauth-server/pull/598/files
      const atHash = this.generateTokenHash(accessToken);
      claims.at_hash = atHash;
    }

    return await this.oidcKeyService.signWithLatestKey(JSON.stringify(claims));
  }

  generateTokenHash(accessTokenBuf) {
    const hash = this.encryptHash(accessTokenBuf.toString('ascii'));
    return this.base64URLEncode(hash.slice(0, hash.length / 2));
  }

  base64URLEncode(buf) {
    return buf
      .toString('base64')
      .replace(/\+/g, '-')
      .replace(/\//g, '_')
      .replace(/=/g, '');
  }

  encryptHash(hash) {
    const value = Buffer.from(hash, 'ascii');
    const sha = crypto.createHash('sha256');
    sha.update(value);
    return sha.digest();
  }
}
