import { Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { OAuth2Error } from 'oauth2orize';

import { BearerTokenAddedEvent } from '../../../auth/events/bearer-token-added/bearer-token-added.event';
import { Client } from '../../../client-management/entities/client/client.interface';
import { ClientService } from '../../../client-management/entities/client/client.service';
import { OAuth2ErrorMessage } from '../../../common/filters/oauth2-error.messages';
import { CryptographerService } from '../../../common/services/cryptographer/cryptographer.service';
import {
  ConfigService,
  TOKEN_LENGTH,
  TOKEN_VALIDITY,
} from '../../../config/config.service';
import {
  HOUR_IN_SECONDS,
  THIRTY_DAYS_IN_HOURS,
} from '../../../constants/app-strings';
import { INVALID_CLIENT, INVALID_SCOPE } from '../../../constants/messages';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { User } from '../../../user-management/entities/user/user.interface';
import { AuthorizationCode } from '../../entities/authorization-code/authorization-code.interface';
import { BearerToken } from '../../entities/bearer-token/bearer-token.interface';
import { BearerTokenService } from '../../entities/bearer-token/bearer-token.service';

@Injectable()
export class OAuth2TokenGeneratorService extends AggregateRoot {
  constructor(
    private readonly cryptographerService: CryptographerService,
    private readonly bearerTokenService: BearerTokenService,
    private readonly clientService: ClientService,
    private readonly configService: ConfigService,
    private readonly settings: ServerSettingsService,
  ) {
    super();
  }

  /**
   * Generates Bearer token as per the parameters passed,
   * @param client Client to be linked on Bearer Token
   * @param user User to be linked on Bearer Token
   * @param scope scopes to be stored on Bearer Token
   * @param refresh if set, adds Refresh Token on Bearer Token
   */
  async getBearerToken(
    client: Client,
    code: AuthorizationCode,
    user: User,
    scope: string[],
    refresh: boolean = true,
  ): Promise<{ bearerToken: BearerToken; extraParams: any }> {
    const bearerToken = {} as BearerToken;
    const tokenLength = Number(this.configService.get(TOKEN_LENGTH));
    bearerToken.accessToken = this.cryptographerService.getUid(tokenLength);
    // Saves client by clientId NOT uuid
    bearerToken.client = client?.clientId || code?.client;
    const localClient = await this.clientService.findOne({
      clientId: bearerToken.client,
    });

    if (!localClient) {
      throw new OAuth2Error(
        INVALID_CLIENT,
        OAuth2ErrorMessage.UnauthorizedClient,
      );
    }

    bearerToken.redirectUris = localClient.redirectUris;

    bearerToken.creation = new Date();
    bearerToken.modified = bearerToken.creation;

    if (user) {
      bearerToken.user = user.uuid;
    }

    const extraParams: any = {
      // list of scopes to space separated string
      scope: scope.join(' '),
      expires_in:
        localClient.accessTokenValidity ||
        Number(this.configService.get(TOKEN_VALIDITY)),
    };

    bearerToken.scope = scope;
    bearerToken.expiresIn = extraParams.expires_in;
    bearerToken.deleteAfter = this.getDeleteAfterDate(bearerToken.expiresIn);

    if (refresh) {
      bearerToken.refreshToken = this.cryptographerService.getUid(tokenLength);
      const settings = await this.settings.find();
      const tokenExpiry =
        localClient.refreshTokenExpiresInHours ||
        settings.refreshTokenExpiresInHours ||
        THIRTY_DAYS_IN_HOURS;
      bearerToken.deleteAfter = this.getDeleteAfterDate(
        tokenExpiry * HOUR_IN_SECONDS,
      );
    }

    if (localClient.restrictTokenUsage) {
      await this.bearerTokenService.deleteMany({
        user: user.uuid,
        client: localClient.clientId,
      });
    }

    this.apply(new BearerTokenAddedEvent(bearerToken));

    return { bearerToken, extraParams };
  }

  async getValidScopes(client: Client, scope: string[]): Promise<string[]> {
    const allowedScopes = (client && client.allowedScopes) || [];
    if (
      !scope ||
      (scope && !scope.every(checkScope => allowedScopes.includes(checkScope)))
    ) {
      throw new OAuth2Error(INVALID_SCOPE, OAuth2ErrorMessage.InvalidScope);
    }

    return scope;
  }

  getDeleteAfterDate(seconds: number) {
    const deleteAfter = new Date();
    deleteAfter.setSeconds(deleteAfter.getSeconds() + seconds);
    return deleteAfter;
  }
}
