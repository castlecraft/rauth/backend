import { Injectable } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import crypto from 'crypto';
import { TokenError } from 'oauth2orize';

import { AuthorizationCode } from '../../../auth/entities/authorization-code/authorization-code.interface';
import { AuthorizationCodeService } from '../../../auth/entities/authorization-code/authorization-code.service';
import { OAuth2ErrorMessage } from '../../../common/filters/oauth2-error.messages';
import { TEN_NUMBER } from '../../../constants/app-strings';
import {
  INVALID_AUTH_CODE,
  INVALID_CLIENT,
  INVALID_CODE_CHALLENGE,
} from '../../../constants/messages';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { UserService } from '../../../user-management/entities/user/user.service';
import { GenerateBearerTokenCommand } from '../../commands/generate-bearer-token/generate-bearer-token.command';
import { BearerTokenService } from '../../entities/bearer-token/bearer-token.service';
import { IDTokenGrantService } from '../id-token-grant/id-token-grant.service';

@Injectable()
export class CodeExchangeService {
  constructor(
    private readonly authorizationCodeService: AuthorizationCodeService,
    private readonly idTokenGrantService: IDTokenGrantService,
    private readonly userService: UserService,
    private readonly settings: ServerSettingsService,
    private readonly token: BearerTokenService,
    private readonly commandBus: CommandBus,
  ) {}

  async exchangeCode(client, code, redirectUri, body, issued) {
    try {
      if (client.disabled) {
        return issued(
          new TokenError(INVALID_CLIENT, OAuth2ErrorMessage.UnauthorizedClient),
        );
      }

      const localCode: AuthorizationCode =
        await this.authorizationCodeService.findOne({ code });

      if (!localCode) {
        issued(
          new TokenError(INVALID_AUTH_CODE, OAuth2ErrorMessage.InvalidRequest),
        );
        return;
      }
      // Check code expiry
      const settings = await this.settings.find();
      const expiry = localCode.creation;
      const authCodeExpiresInMinutes =
        client?.authCodeExpiresInMinutes ||
        settings?.authCodeExpiresInMinutes ||
        TEN_NUMBER;
      expiry.setMinutes(expiry.getMinutes() + authCodeExpiresInMinutes);

      if (new Date() > expiry) {
        this.authorizationCodeService.delete({ code: localCode.code });
        issued(
          new TokenError(INVALID_AUTH_CODE, OAuth2ErrorMessage.InvalidRequest),
        );
        return;
      }

      // Generate Bearer Token
      const user = await this.userService.findOne({ uuid: localCode.user });
      const scope: string[] = localCode.scope;
      const { bearerToken, extraParams } = await this.commandBus.execute(
        new GenerateBearerTokenCommand(undefined, localCode, user, scope),
      );

      // For PKCE
      if (localCode.codeChallenge && localCode.codeChallengeMethod) {
        switch (localCode.codeChallengeMethod) {
          case 's256':
            if (!body.code_verifier) {
              await this.authorizationCodeService.delete({
                code: localCode.code,
              });
              await this.token.remove(bearerToken);
              issued(
                new TokenError(
                  INVALID_CODE_CHALLENGE,
                  OAuth2ErrorMessage.InvalidRequest,
                ),
              );
              return;
            }
            // decode
            const codeVerifier = crypto
              .createHash('sha256')
              .update(body.code_verifier)
              .digest();

            const codeChallenge = codeVerifier
              .toString('base64')
              .replace(/\+/g, '-')
              .replace(/\//g, '_')
              .replace(/=/g, '');

            if (codeChallenge !== localCode.codeChallenge) {
              await this.authorizationCodeService.delete({
                code: localCode.code,
              });
              await this.token.remove(bearerToken);
              issued(
                new TokenError(
                  INVALID_CODE_CHALLENGE,
                  OAuth2ErrorMessage.InvalidRequest,
                ),
              );
              return;
            }
            break;
          case 'plain':
            // direct compare
            if (body.code_verifier !== localCode.codeChallenge) {
              await this.authorizationCodeService.delete({
                code: localCode.code,
              });
              await this.token.remove(bearerToken);
              issued(
                new TokenError(
                  INVALID_CODE_CHALLENGE,
                  OAuth2ErrorMessage.InvalidRequest,
                ),
              );
              return;
            }
            break;
        }
      }

      /**
       * OIDC scope: openid >
       * add name, family_name, given_name, middle_name, nickname,
       * preferred_username, profile, picture, website, gender,
       * birthdate, zoneinfo, locale, updated_at
       */
      if (scope.includes('openid')) {
        extraParams.id_token = await this.idTokenGrantService.generateIdToken(
          localCode.client,
          user,
          localCode.scope,
          bearerToken.accessToken,
          localCode.nonce,
        );
      }

      await this.authorizationCodeService.delete({ code: localCode.code });
      issued(
        null,
        bearerToken.accessToken,
        bearerToken.refreshToken,
        extraParams,
      );
    } catch (error) {
      issued(error);
    }
  }
}
