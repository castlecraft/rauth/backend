import { Injectable } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { AuthorizationError } from 'oauth2orize';

import { ClientService } from '../../../client-management/entities/client/client.service';
import { OAuth2ErrorMessage } from '../../../common/filters/oauth2-error.messages';
import {
  CLIENT_DISABLED,
  CLIENT_REQUIRES_2FA_FOR_USER,
  INVALID_CLIENT,
  INVALID_USER,
} from '../../../constants/messages';
import { UserService } from '../../../user-management/entities/user/user.service';
import { GenerateBearerTokenCommand } from '../../commands/generate-bearer-token/generate-bearer-token.command';
import { OAuth2TokenGeneratorService } from '../oauth2-token-generator/oauth2-token-generator.service';

@Injectable()
export class TokenGrantService {
  accessToken: string;

  constructor(
    private readonly userService: UserService,
    private readonly clientService: ClientService,
    private readonly tokenGeneratorService: OAuth2TokenGeneratorService,
    private readonly commandBus: CommandBus,
  ) {}

  async grantToken(code, user, ares, areq, done) {
    try {
      const localUser = await this.userService.findOne({
        uuid: user.uuid,
      });

      if (!localUser) {
        return done(
          new AuthorizationError(INVALID_USER, OAuth2ErrorMessage.AccessDenied),
        );
      }

      const localClient = await this.clientService.findOne({
        clientId: areq.clientID,
      });

      if (!localClient) {
        return done(
          new AuthorizationError(
            INVALID_CLIENT,
            OAuth2ErrorMessage.UnauthorizedClient,
          ),
        );
      }

      if (localClient.disabled) {
        return done(
          new AuthorizationError(
            CLIENT_DISABLED,
            OAuth2ErrorMessage.UnauthorizedClient,
          ),
        );
      }

      const is2FAMandated = localUser.enable2fa || localUser.enableOTP;
      if (localClient.is2FAMandated && !is2FAMandated) {
        return done(
          new AuthorizationError(
            CLIENT_REQUIRES_2FA_FOR_USER,
            OAuth2ErrorMessage.AccessDenied,
          ),
          null,
        );
      }

      const scope = await this.tokenGeneratorService.getValidScopes(
        localClient,
        areq.scope,
      );
      const { bearerToken, extraParams } = await this.commandBus.execute(
        new GenerateBearerTokenCommand(
          localClient,
          code,
          localUser,
          scope,
          false,
        ),
      );
      this.accessToken = bearerToken.accessToken;
      return done(null, bearerToken.accessToken, extraParams);
    } catch (error) {
      return done(new AuthorizationError(error));
    }
  }

  getAccessToken() {
    return this.accessToken;
  }
}
