import { Injectable } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { TokenError } from 'oauth2orize';

import { ClientService } from '../../../client-management/entities/client/client.service';
import { OAuth2ErrorMessage } from '../../../common/filters/oauth2-error.messages';
import { CLIENT_DISABLED, INVALID_SCOPE } from '../../../constants/messages';
import { GenerateBearerTokenCommand } from '../../commands/generate-bearer-token/generate-bearer-token.command';
import { OAuth2TokenGeneratorService } from '../oauth2-token-generator/oauth2-token-generator.service';

@Injectable()
export class ClientCredentialExchangeService {
  constructor(
    private readonly clientService: ClientService,
    private readonly tokenGeneratorService: OAuth2TokenGeneratorService,
    private readonly commandBus: CommandBus,
  ) {}

  async exchangeClientCredentials(client, scope, done) {
    if (!scope) {
      return done(
        new TokenError(INVALID_SCOPE, OAuth2ErrorMessage.InvalidScope),
      );
    }

    // Validate the client
    try {
      const localClient = await this.clientService.findOne({
        clientId: client.clientId,
      });

      if (!localClient) {
        return done(null, false);
      }

      if (localClient.disabled) {
        return done(
          new TokenError(
            CLIENT_DISABLED,
            OAuth2ErrorMessage.UnauthorizedClient,
          ),
        );
      }

      if (localClient.clientSecret !== client.clientSecret) {
        return done(null, false);
      }

      // Validate Scope
      const validScope = await this.tokenGeneratorService.getValidScopes(
        localClient,
        scope,
      );
      // Everything validated, return the token
      // Pass in a null for user id since there is no user with this grant type
      const { bearerToken, extraParams } = await this.commandBus.execute(
        new GenerateBearerTokenCommand(client, null, null, validScope, true),
      );

      return done(
        null,
        bearerToken.accessToken,
        bearerToken.refreshToken,
        extraParams,
      );
    } catch (error) {
      return done(TokenError(error));
    }
  }
}
