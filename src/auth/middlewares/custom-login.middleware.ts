import { Injectable, NestMiddleware } from '@nestjs/common';
import { stringify } from 'querystring';
import { ServerSettingsService } from '../../system-settings/entities/server-settings/server-settings.service';
import { TRUE } from '../../constants/app-strings';
import { ClientService } from '../../client-management/entities/client/client.service';

@Injectable()
export class CustomLoginMiddleware implements NestMiddleware {
  constructor(
    private readonly settings: ServerSettingsService,
    private readonly client: ClientService,
  ) {}

  async use(req: any, res: any, next: () => void) {
    const settings = await this.settings.find();
    if (!settings.enableCustomLogin) {
      return next();
    }

    if (req.query.custom_login === TRUE) {
      const clientId = req.query.client_id;
      const client = await this.client.findOne({ clientId });
      if (client?.customLoginRoute) {
        delete req.query.custom_login;
        const query = stringify(req.query);
        return res.redirect(client.customLoginRoute + '?' + query);
      }
    }

    return next();
  }
}
