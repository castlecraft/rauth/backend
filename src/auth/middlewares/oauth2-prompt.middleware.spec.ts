import { Test, TestingModule } from '@nestjs/testing';
import { ServerSettingsService } from '../../system-settings/entities/server-settings/server-settings.service';
import { UserService } from '../../user-management/entities/user/user.service';
import { OAuth2PromptMiddleware } from './oauth2-prompt.middleware';

describe('OAuth2PromptMiddleware', () => {
  let middleware: OAuth2PromptMiddleware;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        OAuth2PromptMiddleware,
        {
          provide: ServerSettingsService,
          useFactory: () => jest.fn(),
        },
        {
          provide: UserService,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    middleware = module.get<OAuth2PromptMiddleware>(OAuth2PromptMiddleware);
  });

  it('should be defined', () => {
    expect(middleware).toBeDefined();
  });
});
