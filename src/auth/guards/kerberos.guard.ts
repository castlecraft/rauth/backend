import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { REDIRECT_PATH } from '../../constants/app-strings';
import { User } from '../../user-management/entities/user/user.interface';
import { callback } from '../passport/strategies/local.strategy';
import {
  addSessionUser,
  createPassportContext,
  defaultOptions,
  RequestUser,
} from './guard.utils';

@Injectable()
export class KerberosGuard implements CanActivate {
  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const httpContext = context.switchToHttp();
    const [request, response] = [
      httpContext.getRequest(),
      httpContext.getResponse(),
    ];

    const passportFn = createPassportContext(request, response);
    const userData: {
      user?: RequestUser;
      isOtpValidationComplete?: boolean;
      path?: string;
    } = await passportFn('krb5-negotiate', {
      session: true,
      callback,
      keepSessionInfo: true,
    });
    request[defaultOptions.property] = {
      ...(userData.user as User).toObject(),
      isOtpValidationComplete: userData.isOtpValidationComplete,
    };
    if (userData.path) {
      request[REDIRECT_PATH] = userData.path;
    }
    const reqUser = userData.user;
    const users = request.session?.users;
    if (userData.isOtpValidationComplete) {
      await this.logIn(request);
      request.session.users = users;
      addSessionUser(request, {
        uuid: reqUser.uuid,
        email: reqUser.email,
        phone: reqUser.phone,
      });
    }
    return true;
  }

  public async logIn<
    TRequest extends {
      logIn: (user, callback: (error) => any) => any;
    } = any,
  >(request: TRequest): Promise<void> {
    const user = request[defaultOptions.property];
    await new Promise<void>((resolve, reject) =>
      request.logIn(user, err => (err ? reject(err) : resolve())),
    );
  }
}
