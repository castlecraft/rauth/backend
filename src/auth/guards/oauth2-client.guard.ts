import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ACCOUNTS_ROUTE } from '../../constants/app-strings';
import { ServerSettingsService } from '../../system-settings/entities/server-settings/server-settings.service';
import { callback } from '../passport/strategies/oauth2-client.strategy';
import {
  addSessionUser,
  createPassportContext,
  defaultOptions,
  RequestUser,
} from './guard.utils';

@Injectable()
export class OAuth2ClientGuard implements CanActivate {
  constructor(private readonly settings: ServerSettingsService) {}

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const settings = await this.settings.find();
    const failureRedirect = settings.issuerUrl + ACCOUNTS_ROUTE;
    const httpContext = context.switchToHttp();
    const [request, response] = [
      httpContext.getRequest(),
      httpContext.getResponse(),
    ];

    const passportFn = createPassportContext(request, response);
    const user = await passportFn('oauth2-client', {
      session: true,
      callback,
      failureRedirect,
    });
    request[defaultOptions.property] = user;

    const reqUser = user as RequestUser;
    const users = request.session?.users;
    const state = request.session?.state;
    await this.logIn(request);
    request.session.users = users;
    request.session.state = state;
    addSessionUser(request, {
      uuid: reqUser.uuid,
      email: reqUser.email,
      phone: reqUser.phone,
    });

    return true;
  }

  public async logIn<
    TRequest extends {
      logIn: (user, callback: (error) => any) => any;
    } = any,
  >(request: TRequest): Promise<void> {
    const user = request[defaultOptions.property];
    await new Promise<void>((resolve, reject) =>
      request.logIn(user, err => (err ? reject(err) : resolve())),
    );
  }
}
