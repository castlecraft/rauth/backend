import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { BearerToken } from '../entities/bearer-token/bearer-token.interface';
import { callback } from '../passport/strategies/local.strategy';
import { createPassportContext, defaultOptions } from './guard.utils';

export const TOKEN = 'token';

@Injectable()
export class BearerTokenGuard implements CanActivate {
  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const httpContext = context.switchToHttp();
    const [request, response] = [
      httpContext.getRequest(),
      httpContext.getResponse(),
    ];
    const passportFn = createPassportContext(request, response);
    const user: { user?: string; token?: BearerToken } = await passportFn(
      'bearer',
      { session: false, callback },
    );
    request[defaultOptions.property] = user;

    const token = user?.token;
    if (token) {
      request[TOKEN] = token;
    }
    return true;
  }

  getAccessToken(request) {
    if (!request.headers.authorization) {
      if (!request.query.access_token) return null;
    }
    return (
      request.query.access_token ||
      request.headers.authorization.split(' ')[1] ||
      null
    );
  }
}
