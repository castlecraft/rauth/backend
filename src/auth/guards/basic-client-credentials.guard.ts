import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Request } from 'express';
import { Client } from '../../client-management/entities/client/client.interface';
import { ClientService } from '../../client-management/entities/client/client.service';

@Injectable()
export class BasicClientCredentialsGuard implements CanActivate {
  constructor(private readonly clientService: ClientService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request: Request & {
      client: Client;
    } = context.switchToHttp().getRequest();
    try {
      const authHeader = request.headers.authorization.split(' ')[1];
      const [clientId, clientSecret] = Buffer.from(authHeader, 'base64')
        .toString()
        .split(':');
      const client = await this.clientService.findOne({ clientId });

      if (client && client.clientSecret === clientSecret && !client.disabled) {
        request.client = client;
        return true;
      }
    } catch (error) {
      return false;
    }
    return false;
  }
}
