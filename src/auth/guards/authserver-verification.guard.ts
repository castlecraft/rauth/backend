import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Client } from '../../client-management/entities/client/client.interface';
import { Request } from 'express';
import { ClientService } from '../../client-management/entities/client/client.service';
import { InvalidClientException } from '../../common/filters/exceptions';
import { i18n } from '../../i18n/i18n.config';

@Injectable()
export class AuthServerVerificationGuard implements CanActivate {
  constructor(private readonly clientService: ClientService) {}
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request: Request & {
      client: Client;
    } = context.switchToHttp().getRequest();
    let basicAuthHeader: string;
    try {
      basicAuthHeader = request.headers.authorization.split(' ')[1];
      const [clientId, clientSecret] = Buffer.from(basicAuthHeader, 'base64')
        .toString()
        .split(':');
      const client = await this.clientService.findOne({ clientId });
      if (client && client.clientSecret === clientSecret && client.isTrusted) {
        request.client = client;
        return true;
      } else {
        throw new InvalidClientException();
      }
    } catch (error) {
      throw new UnauthorizedException(i18n.__('Invalid Credentials'));
    }
  }
}
