import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveKerberosRealmCommand } from './remove-kerberos-realm.command';
import { KerberosAuthAggregateService } from '../../aggregates/kerberos-auth-aggregate/kerberos-auth-aggregate.service';

@CommandHandler(RemoveKerberosRealmCommand)
export class RemoveKerberosRealmHandler
  implements ICommandHandler<RemoveKerberosRealmCommand>
{
  constructor(
    private readonly manager: KerberosAuthAggregateService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: RemoveKerberosRealmCommand) {
    const { userUuid, kerberosRealmUuid } = command;

    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.removeKerberosRealm(kerberosRealmUuid, userUuid);
    aggregate.commit();
  }
}
