import { ICommand } from '@nestjs/cqrs';

export class RemoveKerberosRealmCommand implements ICommand {
  constructor(
    public readonly userUuid: string,
    public readonly kerberosRealmUuid: string,
  ) {}
}
