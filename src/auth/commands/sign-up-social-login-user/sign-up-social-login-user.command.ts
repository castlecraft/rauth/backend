import { ICommand } from '@nestjs/cqrs';
import { SocialLogin } from '../../entities/social-login/social-login.interface';
import { IDTokenClaims } from '../../middlewares/interfaces';

export class SignUpSocialLoginUserCommand implements ICommand {
  constructor(
    public readonly profile: IDTokenClaims,
    public readonly socialLogin: SocialLogin,
  ) {}
}
