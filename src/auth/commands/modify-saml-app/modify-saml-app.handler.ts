import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { ModifySAMLAppCommand } from './modify-saml-app.command';
import { SamlAppAggregateService } from '../../aggregates/saml-app/saml-app-aggregate.service';

@CommandHandler(ModifySAMLAppCommand)
export class ModifySAMLAppHandler
  implements ICommandHandler<ModifySAMLAppCommand>
{
  constructor(
    private readonly manager: SamlAppAggregateService,
    private readonly publisher: EventPublisher,
  ) {}
  async execute(command: ModifySAMLAppCommand) {
    const { payload, uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const samlApp = await aggregate.modifySAMLApp(payload, uuid);
    aggregate.commit();
    return samlApp;
  }
}
