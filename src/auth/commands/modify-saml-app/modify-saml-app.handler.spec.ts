import { Test } from '@nestjs/testing';
import { CommandBus, CqrsModule, EventPublisher } from '@nestjs/cqrs';
import { SAMLApp } from '../../entities/saml-app/saml-app.interface';
import { ModifySAMLAppHandler } from './modify-saml-app.handler';
import { ModifySAMLAppCommand } from './modify-saml-app.command';
import { CreateSAMLAppDTO } from '../../controllers/saml-app/saml-app-create.dto';
import { SamlAppAggregateService } from '../../aggregates/saml-app/saml-app-aggregate.service';

describe('Command: ModifySAMLAppHandler', () => {
  let commandBus$: CommandBus;
  let manager: SamlAppAggregateService;
  let commandHandler: ModifySAMLAppHandler;
  let publisher: EventPublisher;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        ModifySAMLAppHandler,
        {
          provide: CommandBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: SamlAppAggregateService,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    commandBus$ = module.get<CommandBus>(CommandBus);
    manager = module.get<SamlAppAggregateService>(SamlAppAggregateService);
    commandHandler = module.get<ModifySAMLAppHandler>(ModifySAMLAppHandler);
    publisher = module.get<EventPublisher>(EventPublisher);
  });

  it('should be defined', () => {
    expect(commandBus$).toBeDefined();
    expect(manager).toBeDefined();
    expect(commandHandler).toBeDefined();
  });

  it('should remove role using the SamlLoginAggregateService', async () => {
    const modifySAMLApp = jest.fn(() =>
      Promise.resolve({} as SAMLApp & { _id: any }),
    );
    manager.modifySAMLApp = modifySAMLApp;
    commandBus$.execute = jest.fn();
    publisher.mergeObjectContext = jest.fn().mockImplementation((...args) => ({
      commit: () => {},
      modifySAMLApp,
    }));
    await commandHandler.execute(
      new ModifySAMLAppCommand({} as CreateSAMLAppDTO, '420'),
    );
    expect(manager.modifySAMLApp).toHaveBeenCalledTimes(1);
  });
});
