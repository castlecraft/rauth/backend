import { ICommand } from '@nestjs/cqrs';
import { CreateSAMLAppDTO } from '../../controllers/saml-app/saml-app-create.dto';

export class ModifySAMLAppCommand implements ICommand {
  constructor(
    public readonly payload: CreateSAMLAppDTO,
    public readonly uuid: string,
  ) {}
}
