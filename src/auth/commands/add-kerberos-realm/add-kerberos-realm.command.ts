import { ICommand } from '@nestjs/cqrs';
import { CreateKerberosRealmDto } from '../../controllers/kerberos/kerberos-realm-create.dto';

export class AddKerberosRealmCommand implements ICommand {
  constructor(
    public readonly payload: CreateKerberosRealmDto,
    public readonly createdBy: string,
  ) {}
}
