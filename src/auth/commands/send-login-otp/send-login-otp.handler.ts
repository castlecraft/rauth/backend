import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AuthData } from '../../../user-management/entities/auth-data/auth-data.interface';
import { OTPAggregateService } from '../../aggregates/otp-aggregate/otp-aggregate.service';
import { SendLoginOTPCommand } from './send-login-otp.command';

@CommandHandler(SendLoginOTPCommand)
export class SendLoginOTPHandler
  implements ICommandHandler<SendLoginOTPCommand>
{
  constructor(
    private publisher: EventPublisher,
    private readonly otpAggregate: OTPAggregateService,
  ) {}
  async execute(command: SendLoginOTPCommand) {
    const { user } = command;
    const aggregate = this.publisher.mergeObjectContext(this.otpAggregate);
    const otp = await aggregate.sendLoginOTP(user);
    aggregate.commit();
    return {
      resendOTPInfo: this.getResendOTPInfo(otp),
    };
  }

  getResendOTPInfo(otp: AuthData) {
    if (
      Number(otp?.metaData?.resendOTPAfterSec ?? -1) >= 0 &&
      otp?.metaData?.resendOTPDelayInSec
    ) {
      return {
        resendOTPAfterSec: otp.metaData?.resendOTPAfterSec,
        resendOTPDelayInSec: otp.metaData?.resendOTPDelayInSec,
      };
    }
  }
}
