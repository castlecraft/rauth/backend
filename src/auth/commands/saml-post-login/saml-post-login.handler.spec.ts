import { CommandBus, CqrsModule, EventPublisher } from '@nestjs/cqrs';
import { Test } from '@nestjs/testing';
import { Request, Response } from 'express';

import { SamlAppAggregateService } from '../../aggregates/saml-app/saml-app-aggregate.service';
import { SAMLPostLoginHandler } from './saml-post-login.handler';
import { SAMLPostLoginCommand } from './saml-post-login.command';

describe('Command: SAMLPostLoginHandler', () => {
  let commandBus$: CommandBus;
  let manager: SamlAppAggregateService;
  let commandHandler: SAMLPostLoginHandler;
  let publisher: EventPublisher;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        SAMLPostLoginHandler,
        {
          provide: CommandBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: SamlAppAggregateService,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    commandBus$ = module.get<CommandBus>(CommandBus);
    manager = module.get<SamlAppAggregateService>(SamlAppAggregateService);
    commandHandler = module.get<SAMLPostLoginHandler>(SAMLPostLoginHandler);
    publisher = module.get<EventPublisher>(EventPublisher);
  });

  it('should be defined', () => {
    expect(commandBus$).toBeDefined();
    expect(manager).toBeDefined();
    expect(commandHandler).toBeDefined();
  });

  it('should execute auth', async () => {
    const auth = jest.fn(() => Promise.resolve());
    manager.auth = auth;
    commandBus$.execute = jest.fn();
    publisher.mergeObjectContext = jest.fn().mockImplementation((...args) => ({
      commit: () => {},
      auth,
    }));
    await commandHandler.execute(
      new SAMLPostLoginCommand('uuid', {} as Request, {} as Response),
    );
    expect(manager.auth).toHaveBeenCalledTimes(1);
  });
});
