import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import * as samlify from 'samlify';
import { SamlAppAggregateService } from '../../aggregates/saml-app/saml-app-aggregate.service';
import { SAMLPostLoginCommand } from './saml-post-login.command';

@CommandHandler(SAMLPostLoginCommand)
export class SAMLPostLoginHandler
  implements ICommandHandler<SAMLPostLoginCommand>
{
  constructor(
    private readonly manager: SamlAppAggregateService,
    private readonly publisher: EventPublisher,
  ) {}
  async execute(command: SAMLPostLoginCommand) {
    const { samlApp, req, res } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const loggedIn = await aggregate.auth(
      samlApp,
      req,
      res,
      samlify.Constants.wording.binding.post,
    );
    aggregate.commit();
    return loggedIn;
  }
}
