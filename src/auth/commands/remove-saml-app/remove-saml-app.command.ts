import { ICommand } from '@nestjs/cqrs';

export class RemoveSAMLAppCommand implements ICommand {
  constructor(
    public readonly userUuid: string,
    public readonly samlAppUuid: string,
  ) {}
}
