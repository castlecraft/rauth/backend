import { Test } from '@nestjs/testing';
import { CommandBus, CqrsModule, EventPublisher } from '@nestjs/cqrs';
import { RemoveSAMLAppHandler } from './remove-saml-app.handler';
import { RemoveSAMLAppCommand } from './remove-saml-app.command';
import { SamlAppAggregateService } from '../../aggregates/saml-app/saml-app-aggregate.service';
import { SAMLApp } from '../../entities/saml-app/saml-app.interface';

describe('Command: RemoveSAMLAppHandler', () => {
  let commandBus$: CommandBus;
  let manager: SamlAppAggregateService;
  let commandHandler: RemoveSAMLAppHandler;
  let publisher: EventPublisher;

  const mockSamlApp = {
    createdBy: '62843fea-1e30-4b1f-af91-3ff582ff9948',
    creation: new Date(),
    uuid: 'f97cef1e-4b15-4194-b40d-7ea0c0d8fa24',
  } as SAMLApp;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        RemoveSAMLAppHandler,
        {
          provide: CommandBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: SamlAppAggregateService,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    commandBus$ = module.get<CommandBus>(CommandBus);
    manager = module.get<SamlAppAggregateService>(SamlAppAggregateService);
    commandHandler = module.get<RemoveSAMLAppHandler>(RemoveSAMLAppHandler);
    publisher = module.get<EventPublisher>(EventPublisher);
  });

  it('should be defined', () => {
    expect(commandBus$).toBeDefined();
    expect(manager).toBeDefined();
    expect(commandHandler).toBeDefined();
  });

  it('should remove SAMLApp using the SamlLoginAggregateService', async () => {
    manager.removeSAMLApp = jest.fn(() => Promise.resolve());
    publisher.mergeObjectContext = jest
      .fn()
      .mockImplementation((...args) => ({ commit: () => {} }));
    await commandHandler.execute(
      new RemoveSAMLAppCommand(mockSamlApp.createdBy, mockSamlApp.uuid),
    );
    expect(manager.removeSAMLApp).toHaveBeenCalledTimes(1);
  });
});
