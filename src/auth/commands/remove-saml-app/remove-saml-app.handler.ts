import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveSAMLAppCommand } from './remove-saml-app.command';
import { SamlAppAggregateService } from '../../aggregates/saml-app/saml-app-aggregate.service';

@CommandHandler(RemoveSAMLAppCommand)
export class RemoveSAMLAppHandler
  implements ICommandHandler<RemoveSAMLAppCommand>
{
  constructor(
    private readonly manager: SamlAppAggregateService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: RemoveSAMLAppCommand) {
    const { userUuid, samlAppUuid } = command;

    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.removeSAMLApp(samlAppUuid, userUuid);
    aggregate.commit();
  }
}
