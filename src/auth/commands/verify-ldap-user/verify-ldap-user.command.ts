import { ICommand } from '@nestjs/cqrs';

export class VerifyLDAPUserCommand implements ICommand {
  constructor(
    public readonly username: string,
    public readonly password: string,
    public readonly ldapClient: string,
    public readonly shouldSendOtp: boolean = true,
    public readonly isUserSanitized: boolean = true,
  ) {}
}
