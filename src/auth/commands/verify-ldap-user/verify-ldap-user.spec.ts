import { CommandBus, CqrsModule, EventPublisher } from '@nestjs/cqrs';
import { Test } from '@nestjs/testing';
import { AuthData } from '../../../user-management/entities/auth-data/auth-data.interface';
import { User } from '../../../user-management/entities/user/user.interface';
import { LDAPClientAggregateService } from '../../aggregates/ldap-aggregate/ldap-client-aggregate.service';
import { VerifyLDAPUserCommand } from './verify-ldap-user.command';
import { VerifyLDAPUserHandler } from './verify-ldap-user.handler';

describe('Command: VerifyLDAPUserHandler', () => {
  let commandBus$: CommandBus;
  let manager: LDAPClientAggregateService;
  let commandHandler: VerifyLDAPUserHandler;
  let publisher: EventPublisher;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        VerifyLDAPUserHandler,
        {
          provide: CommandBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: LDAPClientAggregateService,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    commandBus$ = module.get<CommandBus>(CommandBus);
    manager = module.get<LDAPClientAggregateService>(
      LDAPClientAggregateService,
    );
    commandHandler = module.get<VerifyLDAPUserHandler>(VerifyLDAPUserHandler);
    publisher = module.get<EventPublisher>(EventPublisher);
  });

  it('should be defined', () => {
    expect(commandBus$).toBeDefined();
    expect(manager).toBeDefined();
    expect(commandHandler).toBeDefined();
  });

  it('should verify phone/otp using the LDAPClientAggregateService', async () => {
    const getUserOTP = jest.fn((...args) => Promise.resolve({} as AuthData));
    manager.getUserOTP = getUserOTP;
    const verifyViaLDAP = jest.fn((...args) => Promise.resolve({} as User));
    manager.verifyViaLDAP = verifyViaLDAP;
    manager.sanitizeUser = jest.fn((...args) => ({}) as User);
    commandBus$.execute = jest.fn();
    publisher.mergeObjectContext = jest.fn().mockImplementation((...args) => ({
      commit: () => {},
      verifyViaLDAP,
      getUserOTP,
    }));
    await commandHandler.execute(
      new VerifyLDAPUserCommand('client', 'username', 'password'),
    );
    expect(manager.verifyViaLDAP).toHaveBeenCalledTimes(1);
  });
});
