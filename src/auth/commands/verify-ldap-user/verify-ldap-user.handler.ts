import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AuthData } from '../../../user-management/entities/auth-data/auth-data.interface';
import { User } from '../../../user-management/entities/user/user.interface';
import { LDAPClientAggregateService } from '../../aggregates/ldap-aggregate/ldap-client-aggregate.service';
import { VerifyLDAPUserCommand } from './verify-ldap-user.command';

@CommandHandler(VerifyLDAPUserCommand)
export class VerifyLDAPUserHandler
  implements ICommandHandler<VerifyLDAPUserCommand>
{
  constructor(
    private readonly manager: LDAPClientAggregateService,
    private readonly publisher: EventPublisher,
  ) {}
  async execute(command: VerifyLDAPUserCommand) {
    const { ldapClient, username, password, shouldSendOtp, isUserSanitized } =
      command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const user = await aggregate.verifyViaLDAP(
      ldapClient,
      username,
      password,
      shouldSendOtp,
    );
    aggregate.commit();

    const verifiedLDAPUser = this.getUserObject(user, isUserSanitized);

    if (!password) {
      return verifiedLDAPUser;
    }

    const otp = await aggregate.getUserOTP(user);

    if (otp) {
      return {
        ...verifiedLDAPUser,
        resendOTPInfo: this.getResendOTPInfo(otp),
      };
    }

    return verifiedLDAPUser;
  }

  getUserObject(user: User, isUserSanitized: boolean) {
    if (!isUserSanitized) {
      return { user: { ...user?.toObject(), _id: undefined } };
    }
    try {
      return {
        user: this.manager.sanitizeUser({
          ...user.toObject(),
          _id: undefined,
        } as User),
      };
    } catch (error) {
      return {
        user: this.manager.sanitizeUser({ ...user, _id: undefined } as User),
      };
    }
  }

  getResendOTPInfo(otp: AuthData) {
    if (
      Number(otp?.metaData?.resendOTPAfterSec ?? -1) >= 0 &&
      otp?.metaData?.resendOTPDelayInSec
    ) {
      return {
        resendOTPAfterSec: otp.metaData?.resendOTPAfterSec,
        resendOTPDelayInSec: otp.metaData?.resendOTPDelayInSec,
      };
    }
  }
}
