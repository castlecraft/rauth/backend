import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import * as samlify from 'samlify';
import { SamlAppAggregateService } from '../../aggregates/saml-app/saml-app-aggregate.service';
import { SAMLPostLogoutCommand } from './saml-post-logout.command';

@CommandHandler(SAMLPostLogoutCommand)
export class SAMLPostLogoutHandler
  implements ICommandHandler<SAMLPostLogoutCommand>
{
  constructor(
    private readonly manager: SamlAppAggregateService,
    private readonly publisher: EventPublisher,
  ) {}
  async execute(command: SAMLPostLogoutCommand) {
    const { samlApp, req, res } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const loggedIn = await aggregate.logout(
      samlApp,
      req,
      res,
      samlify.Constants.wording.binding.post,
    );
    aggregate.commit();
    return loggedIn;
  }
}
