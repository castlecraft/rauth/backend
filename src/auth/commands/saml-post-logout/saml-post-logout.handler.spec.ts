import { CommandBus, CqrsModule, EventPublisher } from '@nestjs/cqrs';
import { Test } from '@nestjs/testing';
import { Request, Response } from 'express';

import { SamlAppAggregateService } from '../../aggregates/saml-app/saml-app-aggregate.service';
import { SAMLPostLogoutHandler } from './saml-post-logout.handler';
import { SAMLPostLogoutCommand } from './saml-post-logout.command';

describe('Command: SAMLPostLoginHandler', () => {
  let commandBus$: CommandBus;
  let manager: SamlAppAggregateService;
  let commandHandler: SAMLPostLogoutHandler;
  let publisher: EventPublisher;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        SAMLPostLogoutHandler,
        {
          provide: CommandBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: SamlAppAggregateService,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    commandBus$ = module.get<CommandBus>(CommandBus);
    manager = module.get<SamlAppAggregateService>(SamlAppAggregateService);
    commandHandler = module.get<SAMLPostLogoutHandler>(SAMLPostLogoutHandler);
    publisher = module.get<EventPublisher>(EventPublisher);
  });

  it('should be defined', () => {
    expect(commandBus$).toBeDefined();
    expect(manager).toBeDefined();
    expect(commandHandler).toBeDefined();
  });

  it('should execute logout', async () => {
    const logout = jest.fn(() => Promise.resolve());
    manager.auth = logout;
    commandBus$.execute = jest.fn();
    publisher.mergeObjectContext = jest.fn().mockImplementation((...args) => ({
      commit: () => {},
      logout,
    }));
    await commandHandler.execute(
      new SAMLPostLogoutCommand('uuid', {} as Request, {} as Response),
    );
    expect(manager.auth).toHaveBeenCalledTimes(1);
  });
});
