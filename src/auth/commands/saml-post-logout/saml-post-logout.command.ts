import { ICommand } from '@nestjs/cqrs';
import { Request, Response } from 'express';

export class SAMLPostLogoutCommand implements ICommand {
  constructor(
    public readonly samlApp: string,
    public readonly req: Request,
    public readonly res: Response,
  ) {}
}
