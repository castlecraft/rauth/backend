import { ICommand } from '@nestjs/cqrs';
import { CreateLDAPClientDto } from '../../controllers/ldap-client/ldap-client-create.dto';

export class AddLDAPClientCommand implements ICommand {
  constructor(
    public readonly payload: CreateLDAPClientDto,
    public readonly createdBy: string,
  ) {}
}
