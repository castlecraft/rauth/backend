import { ICommand } from '@nestjs/cqrs';
import { User } from '../../../user-management/entities/user/user.interface';
import { SocialLogin } from '../../entities/social-login/social-login.interface';
import { IDTokenClaims } from '../../middlewares/interfaces';

export class UpdateUserClaimsCommand implements ICommand {
  constructor(
    public readonly profile: IDTokenClaims,
    public readonly user: User,
    public readonly socialLogin: SocialLogin,
  ) {}
}
