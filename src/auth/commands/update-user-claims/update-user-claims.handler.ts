import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateUserClaimsCommand } from './update-user-claims.command';
import { SocialLoginManagementService } from '../../aggregates/social-login-management/social-login-management.service';

@CommandHandler(UpdateUserClaimsCommand)
export class UpdateUserClaimsHandler
  implements ICommandHandler<UpdateUserClaimsCommand>
{
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: SocialLoginManagementService,
  ) {}
  async execute(command: UpdateUserClaimsCommand) {
    const { profile, user, socialLogin } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.updateUserClaims(profile, user, socialLogin);
    aggregate.commit();
  }
}
