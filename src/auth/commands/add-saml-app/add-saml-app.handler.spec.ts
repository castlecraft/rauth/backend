import { Test } from '@nestjs/testing';
import { CommandBus, CqrsModule, EventPublisher } from '@nestjs/cqrs';
import { SAMLApp } from '../../entities/saml-app/saml-app.interface';
import { AddSAMLAppHandler } from './add-saml-app.handler';
import { AddSAMLAppCommand } from './add-saml-app.command';
import { CreateSAMLAppDTO } from '../../controllers/saml-app/saml-app-create.dto';
import { SamlAppAggregateService } from '../../aggregates/saml-app/saml-app-aggregate.service';

describe('Command: AddSAMLAppHandler', () => {
  let commandBus$: CommandBus;
  let manager: SamlAppAggregateService;
  let commandHandler: AddSAMLAppHandler;
  let publisher: EventPublisher;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        AddSAMLAppHandler,
        {
          provide: CommandBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: SamlAppAggregateService,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    commandBus$ = module.get<CommandBus>(CommandBus);
    manager = module.get<SamlAppAggregateService>(SamlAppAggregateService);
    commandHandler = module.get<AddSAMLAppHandler>(AddSAMLAppHandler);
    publisher = module.get<EventPublisher>(EventPublisher);
  });

  it('should be defined', () => {
    expect(commandBus$).toBeDefined();
    expect(manager).toBeDefined();
    expect(commandHandler).toBeDefined();
  });

  it('should remove role using the SamlLoginAggregateService', async () => {
    const addSAMLApp = jest.fn(() => Promise.resolve({} as SAMLApp));
    manager.addSAMLApp = addSAMLApp;
    commandBus$.execute = jest.fn();
    publisher.mergeObjectContext = jest.fn().mockImplementation((...args) => ({
      commit: () => {},
      addSAMLApp,
    }));
    await commandHandler.execute(
      new AddSAMLAppCommand({} as CreateSAMLAppDTO, '420'),
    );
    expect(manager.addSAMLApp).toHaveBeenCalledTimes(1);
  });
});
