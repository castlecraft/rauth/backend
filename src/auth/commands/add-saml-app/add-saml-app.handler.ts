// export class SamlAppHandler {}
import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { AddSAMLAppCommand } from './add-saml-app.command';
import { SamlAppAggregateService } from '../../aggregates/saml-app/saml-app-aggregate.service';

@CommandHandler(AddSAMLAppCommand)
export class AddSAMLAppHandler implements ICommandHandler<AddSAMLAppCommand> {
  constructor(
    private readonly manager: SamlAppAggregateService,
    private readonly publisher: EventPublisher,
  ) {}
  async execute(command: AddSAMLAppCommand) {
    const { payload, createdBy } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const samlApp = await aggregate.addSAMLApp(payload, createdBy);
    aggregate.commit();
    return samlApp;
  }
}
