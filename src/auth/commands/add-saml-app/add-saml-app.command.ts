import { ICommand } from '@nestjs/cqrs';
import { CreateSAMLAppDTO } from '../../controllers/saml-app/saml-app-create.dto';

export class AddSAMLAppCommand implements ICommand {
  constructor(
    public readonly payload: CreateSAMLAppDTO,
    public readonly createdBy: string,
  ) {}
}
