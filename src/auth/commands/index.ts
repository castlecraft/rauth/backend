import { AddKerberosRealmHandler } from './add-kerberos-realm/add-kerberos-realm.handler';
import { AddLDAPClientHandler } from './add-ldap-client/add-ldap-client.handler';
import { AddSocialLoginHandler } from './add-social-login/add-social-login.handler';
import { AddUnverifiedEmailHandler } from './add-unverified-email/add-unverified-phone.handler';
import { AddUnverifiedMobileHandler } from './add-unverified-phone/add-unverified-phone.handler';
import { GenerateBearerTokenHandler } from './generate-bearer-token/generate-bearer-token.handler';
import { ModifyKerberosRealmHandler } from './modify-kerberos-realm/modify-kerberos-realm.handler';
import { ModifyLDAPClientHandler } from './modify-ldap-client/modify-ldap-client.handler';
import { ModifySocialLoginHandler } from './modify-social-login/modify-social-login.handler';
import { RegisterWebAuthnKeyHandler } from './register-webauthn-key/register-webauthn-key.handler';
import { RemoveBearerTokenHandler } from './remove-bearer-token/remove-bearer-token.handler';
import { RemoveKerberosRealmHandler } from './remove-kerberos-realm/remove-kerberos-realm.handler';
import { RemoveLDAPClientHandler } from './remove-ldap-client/remove-ldap-client.handler';
import { RemoveSocialLoginHandler } from './remove-social-login/remove-social-login.handler';
import { RemoveUserAuthenticatorHandler } from './remove-user-authenticator/remove-user-authenticator.handler';
import { RenameUserAuthenticatorHandler } from './rename-user-authenticator/rename-user-authenticator.handler';
import { RequestWebAuthnKeyRegistrationHandler } from './request-webauthn-key-registration/request-webauthn-key-registration.handler';
import { AddSAMLAppHandler } from './add-saml-app/add-saml-app.handler';
import { SAMLPostLoginHandler } from './saml-post-login/saml-post-login.handler';
import { SAMLPostLogoutHandler } from './saml-post-logout/saml-post-logout.handler';
import { SendLoginOTPHandler } from './send-login-otp/send-login-otp.handler';
import { SignUpSocialLoginUserHandler } from './sign-up-social-login-user/sign-up-social-login-user.handler';
import { UpdateUserClaimsHandler } from './update-user-claims/update-user-claims.handler';
import { VerifyEmailHandler } from './verify-email/verify-email.handler';
import { VerifyLDAPUserHandler } from './verify-ldap-user/verify-ldap-user.handler';
import { VerifyPhoneHandler } from './verify-phone/verify-phone.handler';
import { WebAuthnLoginHandler } from './webauthn-login/webauthn-login.handler';
import { WebAuthnRequestLoginHandler } from './webauthn-request-login/webauthn-request-login.handler';
import { ModifySAMLAppHandler } from './modify-saml-app/modify-saml-app.handler';
import { RemoveSAMLAppHandler } from './remove-saml-app/remove-saml-app.handler';

export const AuthCommandHandlers = [
  AddKerberosRealmHandler,
  AddLDAPClientHandler,
  AddSocialLoginHandler,
  AddUnverifiedEmailHandler,
  AddUnverifiedMobileHandler,
  GenerateBearerTokenHandler,
  ModifyKerberosRealmHandler,
  ModifyLDAPClientHandler,
  ModifySocialLoginHandler,
  RegisterWebAuthnKeyHandler,
  RemoveBearerTokenHandler,
  RemoveKerberosRealmHandler,
  RemoveLDAPClientHandler,
  RemoveSocialLoginHandler,
  RemoveUserAuthenticatorHandler,
  RenameUserAuthenticatorHandler,
  RequestWebAuthnKeyRegistrationHandler,
  SAMLPostLoginHandler,
  SAMLPostLogoutHandler,
  AddSAMLAppHandler,
  ModifySAMLAppHandler,
  RemoveSAMLAppHandler,
  SendLoginOTPHandler,
  SignUpSocialLoginUserHandler,
  UpdateUserClaimsHandler,
  VerifyEmailHandler,
  VerifyLDAPUserHandler,
  VerifyPhoneHandler,
  WebAuthnLoginHandler,
  WebAuthnRequestLoginHandler,
];
