import { Test } from '@nestjs/testing';
import { CommandBus, CqrsModule, EventPublisher } from '@nestjs/cqrs';
import { LDAPClientAggregateService } from '../../aggregates/ldap-aggregate/ldap-client-aggregate.service';
import { ModifyLDAPClientHandler } from './modify-ldap-client.handler';
import { ModifyLDAPClientCommand } from './modify-ldap-client.command';
import { CreateLDAPClientDto } from '../../controllers/ldap-client/ldap-client-create.dto';
import { LDAPClient } from '../../entities/ldap-client/ldap-client.interface';

describe('Command: ModifyLDAPClientHandler', () => {
  let commandBus$: CommandBus;
  let manager: LDAPClientAggregateService;
  let commandHandler: ModifyLDAPClientHandler;
  let publisher: EventPublisher;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        ModifyLDAPClientHandler,
        {
          provide: CommandBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: LDAPClientAggregateService,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    commandBus$ = module.get<CommandBus>(CommandBus);
    manager = module.get<LDAPClientAggregateService>(
      LDAPClientAggregateService,
    );
    commandHandler = module.get<ModifyLDAPClientHandler>(
      ModifyLDAPClientHandler,
    );
    publisher = module.get<EventPublisher>(EventPublisher);
  });

  it('should be defined', () => {
    expect(commandBus$).toBeDefined();
    expect(manager).toBeDefined();
    expect(commandHandler).toBeDefined();
  });

  it('should remove client using the LDAPClientAggregateService', async () => {
    const modifyLDAPClient = jest.fn(() =>
      Promise.resolve({} as LDAPClient & { _id: any }),
    );
    manager.modifyLDAPClient = modifyLDAPClient;
    commandBus$.execute = jest.fn();
    publisher.mergeObjectContext = jest.fn().mockImplementation((...args) => ({
      commit: () => {},
      modifyLDAPClient,
    }));
    await commandHandler.execute(
      new ModifyLDAPClientCommand(
        {} as CreateLDAPClientDto,
        '86af617a-53fd-4adb-99c8-87f2f0d246d9',
        '648ae71b-c50e-4609-99ef-56e57863f7a9',
      ),
    );
    expect(manager.modifyLDAPClient).toHaveBeenCalledTimes(1);
  });
});
