import { ICommand } from '@nestjs/cqrs';
import { CreateLDAPClientDto } from '../../controllers/ldap-client/ldap-client-create.dto';

export class ModifyLDAPClientCommand implements ICommand {
  constructor(
    public readonly payload: CreateLDAPClientDto,
    public readonly uuid: string,
    public readonly modifiedBy: string,
  ) {}
}
