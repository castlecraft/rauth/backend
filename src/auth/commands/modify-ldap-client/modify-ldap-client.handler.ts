import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { ModifyLDAPClientCommand } from './modify-ldap-client.command';
import { LDAPClientAggregateService } from '../../aggregates/ldap-aggregate/ldap-client-aggregate.service';

@CommandHandler(ModifyLDAPClientCommand)
export class ModifyLDAPClientHandler
  implements ICommandHandler<ModifyLDAPClientCommand>
{
  constructor(
    private readonly manager: LDAPClientAggregateService,
    private readonly publisher: EventPublisher,
  ) {}
  async execute(command: ModifyLDAPClientCommand) {
    const { payload, uuid, modifiedBy } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const ldapClient = await aggregate.modifyLDAPClient(
      payload,
      uuid,
      modifiedBy,
    );
    aggregate.commit();
    return ldapClient;
  }
}
