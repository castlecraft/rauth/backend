import { ICommand } from '@nestjs/cqrs';
import { Client } from '../../../client-management/entities/client/client.interface';
import { User } from '../../../user-management/entities/user/user.interface';
import { AuthorizationCode } from '../../entities/authorization-code/authorization-code.interface';

export class GenerateBearerTokenCommand implements ICommand {
  constructor(
    public readonly client: Client,
    public readonly code: AuthorizationCode,
    public readonly user: User,
    public readonly scope: string[],
    public readonly refresh: boolean = true,
  ) {}
}
