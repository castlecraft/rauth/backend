import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Request } from 'express';
import { BearerTokenService } from '../../../auth/entities/bearer-token/bearer-token.service';
import { ConfigService, TOKEN_VALIDITY } from '../../../config/config.service';
import { PassportHttpBearerStrategy } from '../base/http-bearer.strategy';
import { PassportStrategy } from './passport.strategy';

@Injectable()
export class HttpBearerStrategy extends PassportStrategy(
  PassportHttpBearerStrategy,
) {
  constructor(
    private readonly bearerTokenService: BearerTokenService,
    private readonly configService: ConfigService,
  ) {
    super({ passReqToCallback: true });
  }
  async validate(
    req: Request,
    token: string,
    done: (err?, user?, info?) => any,
  ) {
    try {
      const localToken = await this.bearerTokenService.findOne({
        accessToken: token,
      });
      if (!localToken) done(new UnauthorizedException());
      const validity =
        localToken.expiresIn || Number(this.configService.get(TOKEN_VALIDITY));
      const expires = new Date(
        new Date(localToken.creation).getTime() + validity * 1000,
      );
      const now = new Date();
      if (now > expires) {
        done(new UnauthorizedException());
      } else return done(null, { user: localToken.user, token: localToken });
    } catch (error) {
      return done(error);
    }
  }
}
