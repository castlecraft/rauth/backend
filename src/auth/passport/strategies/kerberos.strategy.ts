import { Injectable } from '@nestjs/common';
import { EventPublisher } from '@nestjs/cqrs';
import { Request } from 'express';
import { PassportKerberosNegotiationStrategy } from '../base/kerberos.strategy';
import { PassportStrategy } from './passport.strategy';
import { KerberosAuthAggregateService } from '../../aggregates/kerberos-auth-aggregate/kerberos-auth-aggregate.service';

@Injectable()
export class KerberosNegotiationStrategy extends PassportStrategy(
  PassportKerberosNegotiationStrategy,
) {
  constructor(
    private readonly kerberos: KerberosAuthAggregateService,
    private readonly publisher: EventPublisher,
  ) {
    super({ passReqToCallback: true });
  }

  async validate(req: Request, auth: string, verified: (...args) => unknown) {
    try {
      const aggregate = this.publisher.mergeObjectContext(this.kerberos);
      const user = await aggregate.loginViaKerberos(req, auth);
      aggregate.commit();
      return verified(null, user);
    } catch (error) {
      return verified(error);
    }
  }
}
