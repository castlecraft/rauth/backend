import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Request } from 'express';
import { PassportStrategy } from './passport.strategy';
import { EventPublisher } from '@nestjs/cqrs';
import { PassportLDAPClientAuthStrategy } from '../base/ldap-client.strategy';
import { LDAPClientAggregateService } from '../../aggregates/ldap-aggregate/ldap-client-aggregate.service';

@Injectable()
export class LDAPClientAuthStrategy extends PassportStrategy(
  PassportLDAPClientAuthStrategy,
) {
  constructor(
    private readonly ldapAuth: LDAPClientAggregateService,
    private readonly publisher: EventPublisher,
  ) {
    super({ passReqToCallback: true });
  }

  async validate(
    req: Request,
    username: string,
    userPassword: string,
    code: string,
    done: (err?, user?, info?) => any,
  ) {
    try {
      const ldapClient = req.params.ldapClient as string;
      const aggregate = this.publisher.mergeObjectContext(this.ldapAuth);
      const user = await aggregate.loginViaLDAP(
        ldapClient,
        username,
        userPassword,
        code,
      );
      aggregate.commit();
      return done(null, user);
    } catch (error) {
      return done(new UnauthorizedException(error), null);
    }
  }
}
