import passport from 'passport';
import { Request } from 'express';

// reference from:
// https://github.com/vesse/passport-ldapauth

export interface CredentialsLookupResult {
  username?: string;
  password?: string;
  user?: string;
  pass?: string;
  name?: string;
}

export interface LDAPAuthOptions {
  badRequestMessage?: string;
  usernameField?: string;
  passwordField?: string;
  missingCredentialsStatus?: number;
  passReqToCallback?: boolean;
  credentialsLookup?: (req: Request) => CredentialsLookupResult;
  handleErrorAsFailures?: boolean;
  failureErrorCallback?: (err: Error) => void;
}

export function lookup(obj: unknown, field: string): string | null {
  const chain = field.split(']').join('').split('[');
  for (let i = 0, len = chain.length; i < len; i++) {
    const prop = obj[chain[i]];
    if (typeof prop === 'undefined') {
      return null;
    }
    if (typeof prop !== 'object') {
      return prop;
    }
    obj = prop;
  }
  return null;
}

export class PassportLDAPClientAuthStrategy extends passport.Strategy {
  private passReqToCallback: boolean;
  private verify: (...args) => unknown;
  name = 'ldapauth';

  constructor(
    readonly options: LDAPAuthOptions,
    verify: (...args) => unknown,
  ) {
    super();
    if (!verify) {
      throw new TypeError('LocalStrategy requires a verify callback');
    }
    if (!options) {
      throw new Error('LDAP authentication strategy requires options');
    }
    passport.Strategy.call(this);
    this.verify = verify;
    this.passReqToCallback = options?.passReqToCallback;
  }

  authenticate(
    req: Request & { session: any },
    options?: LDAPAuthOptions,
  ): void {
    const username: string = req.body.username;
    const password: string = req.body.password;
    const code: string = req.body.code;

    if (!username || !password) {
      return this.fail(
        { message: options.badRequestMessage || 'Missing credentials' },
        this.options.missingCredentialsStatus || 400,
      );
    }
    const self = this;

    const verified = (err, user, info) => {
      if (err) {
        return self.error(err);
      }
      if (!user) {
        return self.fail(info);
      }
      self.success(user, info);
    };

    try {
      if (this.passReqToCallback) {
        this.verify(req, username, password, code, verified);
      } else {
        this.verify(username, password, code, verified);
      }
    } catch (ex) {
      return this.error(ex);
    }
  }
}
