import { SetMetadata } from '@nestjs/common';

export const SCOPE = 'scope';

export const Scope = (...scope: string[]) => SetMetadata(SCOPE, scope);
