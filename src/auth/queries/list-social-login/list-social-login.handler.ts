import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { ListSocialLoginQuery } from './list-social-login.query';
import { SocialLoginService } from '../../entities/social-login/social-login.service';

@QueryHandler(ListSocialLoginQuery)
export class ListSocialLoginHandler implements IQueryHandler {
  constructor(private readonly socialLogin: SocialLoginService) {}

  async execute(query: ListSocialLoginQuery) {
    const { offset, limit, search, sort } = query;
    const where = {};
    const sortQuery: { name?: string } = {};
    if (sort) {
      sortQuery.name = sort;
    }
    return await this.socialLogin.list(search, where, sortQuery, offset, limit);
  }
}
