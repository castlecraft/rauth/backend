import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { ListSAMLAppQuery } from './list-saml-app.query';
import { SAMLAppService } from '../../entities/saml-app/saml-app.service';

@QueryHandler(ListSAMLAppQuery)
export class ListSAMLAppHandler implements IQueryHandler {
  constructor(private readonly samlApp: SAMLAppService) {}

  async execute(query: ListSAMLAppQuery) {
    const { offset, limit, search, sort } = query;
    const where = {};
    const sortQuery: { name?: string } = {};
    if (sort) {
      sortQuery.name = sort;
    }
    return await this.samlApp.list(search, where, sortQuery, offset, limit);
  }
}
