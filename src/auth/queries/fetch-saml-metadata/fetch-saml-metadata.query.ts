import { IQuery } from '@nestjs/cqrs';
import { Response } from 'express';

export class FetchSamlMetadataQuery implements IQuery {
  constructor(
    public readonly samlApp: string,
    public readonly res: Response,
  ) {}
}
