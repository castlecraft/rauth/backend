import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { SamlAppAggregateService } from '../../aggregates/saml-app/saml-app-aggregate.service';
import { FetchSamlMetadataQuery } from './fetch-saml-metadata.query';

@QueryHandler(FetchSamlMetadataQuery)
export class FetchSamlMetadataHandler implements IQueryHandler {
  constructor(private readonly samlLogin: SamlAppAggregateService) {}

  async execute(query: FetchSamlMetadataQuery) {
    const { samlApp, res } = query;
    return await this.samlLogin.getIdpMetadata(samlApp, res);
  }
}
