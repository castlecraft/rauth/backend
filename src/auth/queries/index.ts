import { FetchSamlMetadataHandler } from './fetch-saml-metadata/fetch-saml-metadata.handler';
import { FindUserAuthenticatorsHandler } from './find-user-authenticators/find-user-authenticators.handler';
import { ListSAMLAppHandler } from './list-saml-app/list-saml-app.handler';
import { ListSocialLoginHandler } from './list-social-login/list-social-login.handler';
import { SamlLoginRedirectHandler } from './saml-login-redirect/saml-login-redirect.handler';
import { SamlLogoutRedirectHandler } from './saml-logout-redirect/saml-login-redirect.handler';

export const AuthQueryHandlers = [
  FetchSamlMetadataHandler,
  FindUserAuthenticatorsHandler,
  ListSocialLoginHandler,
  ListSAMLAppHandler,
  SamlLoginRedirectHandler,
  SamlLogoutRedirectHandler,
];
