import { IQuery } from '@nestjs/cqrs';
import { Request, Response } from 'express';

export class SamlLogoutRedirectQuery implements IQuery {
  constructor(
    public readonly samlApp: string,
    public readonly req: Request,
    public readonly res: Response,
  ) {}
}
