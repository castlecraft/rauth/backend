import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import * as samlify from 'samlify';

import { SamlAppAggregateService } from '../../aggregates/saml-app/saml-app-aggregate.service';
import { SamlLogoutRedirectQuery } from './saml-login-redirect.query';

@QueryHandler(SamlLogoutRedirectQuery)
export class SamlLogoutRedirectHandler implements IQueryHandler {
  constructor(private readonly samlLogin: SamlAppAggregateService) {}

  async execute(query: SamlLogoutRedirectQuery) {
    const { samlApp, req, res } = query;
    return await this.samlLogin.logout(
      samlApp,
      req,
      res,
      samlify.Constants.wording.binding.redirect,
    );
  }
}
