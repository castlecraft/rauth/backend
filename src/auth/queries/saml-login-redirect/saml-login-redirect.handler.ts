import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import * as samlify from 'samlify';

import { SamlAppAggregateService } from '../../aggregates/saml-app/saml-app-aggregate.service';
import { SamlLoginRedirectQuery } from './saml-login-redirect.query';

@QueryHandler(SamlLoginRedirectQuery)
export class SamlLoginRedirectHandler implements IQueryHandler {
  constructor(private readonly samlLogin: SamlAppAggregateService) {}

  async execute(query: SamlLoginRedirectQuery) {
    const { samlApp, req, res } = query;
    return await this.samlLogin.auth(
      samlApp,
      req,
      res,
      samlify.Constants.wording.binding.redirect,
    );
  }
}
