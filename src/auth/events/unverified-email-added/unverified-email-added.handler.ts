import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { UnverifiedEmailAddedEvent } from './unverified-email-added.event';
import { AuthDataService } from '../../../user-management/entities/auth-data/auth-data.service';
import { forkJoin, from } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { EmailRequestService } from '../../../user-management/aggregates/email-request/email-request.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';

@EventsHandler(UnverifiedEmailAddedEvent)
export class UnverifiedEmailAddedHandler
  implements IEventHandler<UnverifiedEmailAddedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly authData: AuthDataService,
    private readonly email: EmailRequestService,
    private readonly config: ConfigService,
  ) {}
  handle(event: UnverifiedEmailAddedEvent) {
    const { unverifiedEmail, verificationCode } = event;

    forkJoin({
      email: from(this.authData.save(unverifiedEmail)),
      code: from(this.authData.save(verificationCode)),
    })
      .pipe(
        switchMap(({ email, code }) => {
          return this.email.verifyEmail(email, code);
        }),
      )
      .subscribe({
        next: success => {
          this.logger.log(
            UnverifiedEmailAddedEvent.name,
            this.constructor.name,
          );
        },
        error: error => {
          this.logger.error(error, error, this.constructor.name);
        },
      });
  }
}
