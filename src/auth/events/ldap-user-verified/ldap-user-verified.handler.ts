import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { LDAPUserVerifiedEvent } from './ldap-user-verified.event';

@EventsHandler(LDAPUserVerifiedEvent)
export class LDAPUserVerifiedHandler
  implements IEventHandler<LDAPUserVerifiedEvent>
{
  handle(event: LDAPUserVerifiedEvent) {}
}
