import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { UnverifiedPhoneAddedEvent } from './unverified-phone-added.event';
import { AuthDataService } from '../../../user-management/entities/auth-data/auth-data.service';
import { from } from 'rxjs';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(UnverifiedPhoneAddedEvent)
export class UnverifiedPhoneAddedHandler
  implements IEventHandler<UnverifiedPhoneAddedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly authData: AuthDataService,
    private readonly config: ConfigService,
  ) {}
  handle(event: UnverifiedPhoneAddedEvent) {
    const { phoneOTP } = event;

    from(this.authData.save(phoneOTP)).subscribe({
      next: success => {
        this.logger.log(UnverifiedPhoneAddedEvent.name, this.constructor.name);
      },
      error: error => {
        this.logger.error(error, error, this.constructor.name);
      },
    });
  }
}
