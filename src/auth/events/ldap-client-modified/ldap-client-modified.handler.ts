import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { LDAPClientModifiedEvent } from './ldap-client-modified.event';
import { LDAPClientService } from '../../entities/ldap-client/ldap-client.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(LDAPClientModifiedEvent)
export class LDAPClientModifiedHandler
  implements IEventHandler<LDAPClientModifiedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly ldapClient: LDAPClientService,
    private readonly config: ConfigService,
  ) {}
  handle(event: LDAPClientModifiedEvent) {
    this.ldapClient
      .save(event.ldapClient)
      .then(modified => {
        this.logger.log(LDAPClientModifiedEvent.name, this.constructor.name);
      })
      .catch(error => {
        this.logger.error(error, error, this.constructor.name);
      });
  }
}
