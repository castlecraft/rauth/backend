import { IEvent } from '@nestjs/cqrs';
import { LDAPClient } from '../../entities/ldap-client/ldap-client.interface';

export class LDAPClientModifiedEvent implements IEvent {
  constructor(public readonly ldapClient: LDAPClient) {}
}
