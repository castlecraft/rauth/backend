import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { LDAPClientModifiedHandler } from './ldap-client-modified.handler';
import { LDAPClientService } from '../../entities/ldap-client/ldap-client.service';
import { LDAPClient } from '../../entities/ldap-client/ldap-client.interface';
import { LDAPClientModifiedEvent } from './ldap-client-modified.event';
import { ConfigService } from '../../../config/config.service';

describe('Event: LDAPClientModifiedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: LDAPClientModifiedHandler;

  let ldapClient: LDAPClientService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        LDAPClientModifiedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: LDAPClientService,
          useValue: () => jest.fn(),
        },
        {
          provide: ConfigService,
          useValue: {
            get: (key: string) => key,
          },
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<LDAPClientModifiedHandler>(
      LDAPClientModifiedHandler,
    );
    ldapClient = module.get<LDAPClientService>(LDAPClientService);
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should add Client using LDAPClientService', async () => {
    ldapClient.save = jest.fn((...args) =>
      Promise.resolve({} as LDAPClient & { _id: any }),
    );
    eventBus$.publish = jest.fn(() => {});
    await eventHandler.handle(new LDAPClientModifiedEvent({} as LDAPClient));
    expect(ldapClient.save).toHaveBeenCalledTimes(1);
  });
});
