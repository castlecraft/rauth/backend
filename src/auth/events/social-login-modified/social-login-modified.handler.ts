import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { SocialLoginModifiedEvent } from './social-login-modified.event';
import { SocialLoginService } from '../../entities/social-login/social-login.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';

@EventsHandler(SocialLoginModifiedEvent)
export class SocialLoginModifiedHandler
  implements IEventHandler<SocialLoginModifiedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly socialLogin: SocialLoginService,
    private readonly config: ConfigService,
  ) {}
  handle(event: SocialLoginModifiedEvent) {
    this.socialLogin
      .save(event.socialLogin)
      .then(modified => {
        this.logger.log(SocialLoginModifiedEvent.name, this.constructor.name);
      })
      .catch(error => {
        this.logger.error(error, error, this.constructor.name);
      });
  }
}
