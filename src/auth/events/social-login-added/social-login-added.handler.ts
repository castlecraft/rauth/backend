import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { SocialLoginAddedEvent } from './social-login-added.event';
import { SocialLoginService } from '../../entities/social-login/social-login.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(SocialLoginAddedEvent)
export class SocialLoginAddedHandler
  implements IEventHandler<SocialLoginAddedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly socialLogin: SocialLoginService,
    private readonly config: ConfigService,
  ) {}
  handle(event: SocialLoginAddedEvent) {
    this.socialLogin
      .save(event.socialLogin)
      .then(added => {
        this.logger.log(SocialLoginAddedEvent.name, this.constructor.name);
      })
      .catch(error => {
        this.logger.error(error, error, this.constructor.name);
      });
  }
}
