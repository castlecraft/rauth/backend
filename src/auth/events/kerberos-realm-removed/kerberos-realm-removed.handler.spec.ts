import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { KerberosRealmRemovedHandler } from './kerberos-realm-removed.handler';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';
import { KerberosRealm } from '../../entities/kerberos-realm/kerberos-realm.interface';
import { KerberosRealmRemovedEvent } from './kerberos-realm-removed.event';
import { ConfigService } from '../../../config/config.service';

describe('Event: KerberosRealmRemovedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: KerberosRealmRemovedHandler;
  let realm: KerberosRealmService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        KerberosRealmRemovedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: KerberosRealmService,
          useFactory: () => jest.fn(),
        },
        {
          provide: ConfigService,
          useValue: {
            get: (key: string) => key,
          },
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<KerberosRealmRemovedHandler>(
      KerberosRealmRemovedHandler,
    );
    realm = module.get<KerberosRealmService>(KerberosRealmService);
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should remove realm using KerberosRealmService', async () => {
    const mock = {} as KerberosRealm;
    realm.remove = jest.fn(() => Promise.resolve(mock));
    eventBus$.publish = jest.fn(() => {});
    await eventHandler.handle(
      new KerberosRealmRemovedEvent(
        '9ac0d438-c9c4-4166-a896-3b0902f8bd99',
        mock,
      ),
    );
    expect(realm.remove).toHaveBeenCalledTimes(1);
  });
});
