import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { from } from 'rxjs';
import { KerberosRealmRemovedEvent } from './kerberos-realm-removed.event';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';

@EventsHandler(KerberosRealmRemovedEvent)
export class KerberosRealmRemovedHandler
  implements IEventHandler<KerberosRealmRemovedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly realm: KerberosRealmService,
    private readonly config: ConfigService,
  ) {}
  handle(event: KerberosRealmRemovedEvent) {
    const { realm } = event;
    from(this.realm.remove(realm)).subscribe({
      next: success => {
        this.logger.log(KerberosRealmRemovedEvent.name, this.constructor.name);
      },
      error: error => {
        this.logger.error(error, error, this.constructor.name);
      },
    });
  }
}
