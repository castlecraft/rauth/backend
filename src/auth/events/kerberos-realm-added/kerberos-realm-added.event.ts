import { IEvent } from '@nestjs/cqrs';
import { KerberosRealm } from '../../entities/kerberos-realm/kerberos-realm.interface';

export class KerberosRealmAddedEvent implements IEvent {
  constructor(public readonly realm: KerberosRealm) {}
}
