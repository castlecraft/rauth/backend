import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { KerberosRealmAddedEvent } from './kerberos-realm-added.event';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(KerberosRealmAddedEvent)
export class KerberosRealmAddedHandler
  implements IEventHandler<KerberosRealmAddedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly realm: KerberosRealmService,
    private readonly config: ConfigService,
  ) {}
  handle(event: KerberosRealmAddedEvent) {
    this.realm
      .save(event.realm)
      .then(added => {
        this.logger.log(KerberosRealmAddedEvent.name, this.constructor.name);
      })
      .catch(error => {
        this.logger.error(error, error, this.constructor.name);
      });
  }
}
