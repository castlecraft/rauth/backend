import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { KerberosRealmAddedHandler } from './kerberos-realm-added.handler';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';
import { KerberosRealmAddedEvent } from './kerberos-realm-added.event';
import { LDAPClient } from '../../entities/ldap-client/ldap-client.interface';
import { ConfigService } from '../../../config/config.service';

describe('Event: KerberosRealmAddedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: KerberosRealmAddedHandler;

  let ldapClient: KerberosRealmService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        KerberosRealmAddedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: KerberosRealmService,
          useValue: () => jest.fn(),
        },
        {
          provide: ConfigService,
          useValue: {
            get: (key: string) => key,
          },
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<KerberosRealmAddedHandler>(
      KerberosRealmAddedHandler,
    );
    ldapClient = module.get<KerberosRealmService>(KerberosRealmService);
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should add realm using KerberosRealmService', async () => {
    ldapClient.save = jest.fn((...args) =>
      Promise.resolve({} as LDAPClient & { _id: any }),
    );
    eventBus$.publish = jest.fn(() => {});
    await eventHandler.handle(new KerberosRealmAddedEvent({} as LDAPClient));
    expect(ldapClient.save).toHaveBeenCalledTimes(1);
  });
});
