import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { SAMLAppRemovedHandler } from './saml-app-removed.handler';
import { SAMLAppService } from '../../entities/saml-app/saml-app.service';
import { SAMLAppRemovedEvent } from './saml-app-removed.event';
import { SAMLApp } from '../../entities/saml-app/saml-app.interface';
import { ConfigService } from '../../../config/config.service';

describe('Event: SAMLAppRemovedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: SAMLAppRemovedHandler;
  let samlApp: SAMLAppService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        SAMLAppRemovedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: SAMLAppService,
          useFactory: () => jest.fn(),
        },
        {
          provide: ConfigService,
          useValue: {
            get: (key: string) => key,
          },
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<SAMLAppRemovedHandler>(SAMLAppRemovedHandler);
    samlApp = module.get<SAMLAppService>(SAMLAppService);
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should remove SAMLApp using SAMLAppService', async () => {
    const mockSamlApp = {} as SAMLApp;
    samlApp.remove = jest.fn(() => Promise.resolve(mockSamlApp));
    eventBus$.publish = jest.fn(() => {});
    await eventHandler.handle(
      new SAMLAppRemovedEvent(
        '9ac0d438-c9c4-4166-a896-3b0902f8bd99',
        mockSamlApp,
      ),
    );
    expect(samlApp.remove).toHaveBeenCalledTimes(1);
  });
});
