import { IEvent } from '@nestjs/cqrs';
import { SAMLApp } from '../../entities/saml-app/saml-app.interface';

export class SAMLAppRemovedEvent implements IEvent {
  constructor(
    public readonly userUuid: string,
    public readonly samlApp: SAMLApp,
  ) {}
}
