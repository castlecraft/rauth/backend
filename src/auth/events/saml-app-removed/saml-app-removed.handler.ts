import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { from } from 'rxjs';
import { SAMLAppRemovedEvent } from './saml-app-removed.event';
import { SAMLAppService } from '../../entities/saml-app/saml-app.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';

@EventsHandler(SAMLAppRemovedEvent)
export class SAMLAppRemovedHandler
  implements IEventHandler<SAMLAppRemovedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly samlApp: SAMLAppService,
    private readonly config: ConfigService,
  ) {}
  handle(event: SAMLAppRemovedEvent) {
    const { samlApp } = event;
    from(this.samlApp.remove(samlApp)).subscribe({
      next: success => {
        this.logger.log(SAMLAppRemovedEvent.name, this.constructor.name);
      },
      error: error => {
        this.logger.error(error, error, this.constructor.name);
      },
    });
  }
}
