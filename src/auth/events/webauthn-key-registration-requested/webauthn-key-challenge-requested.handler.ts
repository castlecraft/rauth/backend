import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { from } from 'rxjs';
import { WebAuthnKeyChallengeRequestedEvent } from './webauthn-key-challenge-requested.event';
import { AuthDataService } from '../../../user-management/entities/auth-data/auth-data.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';

@EventsHandler(WebAuthnKeyChallengeRequestedEvent)
export class WebAuthnKeyChallengeRequestedHandler
  implements IEventHandler<WebAuthnKeyChallengeRequestedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly authData: AuthDataService,
    private readonly config: ConfigService,
  ) {}
  handle(event: WebAuthnKeyChallengeRequestedEvent) {
    const { authData } = event;
    from(this.authData.save(authData)).subscribe({
      next: success => {
        this.logger.log(
          WebAuthnKeyChallengeRequestedEvent.name,
          this.constructor.name,
        );
      },
      error: error => {
        this.logger.error(error, error, this.constructor.name);
      },
    });
  }
}
