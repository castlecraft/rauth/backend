import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { from } from 'rxjs';
import { WebAuthnKeyRegisteredEvent } from './webauthn-key-registered.event';
import { UserAuthenticatorService } from '../../../user-management/entities/user-authenticator/user-authenticator.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(WebAuthnKeyRegisteredEvent)
export class WebAuthnKeyRegisteredHandler
  implements IEventHandler<WebAuthnKeyRegisteredEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly authenticator: UserAuthenticatorService,
    private readonly config: ConfigService,
  ) {}
  handle(event: WebAuthnKeyRegisteredEvent) {
    const { authenticator } = event;
    from(this.authenticator.save(authenticator)).subscribe({
      next: success => {
        this.logger.log(WebAuthnKeyRegisteredEvent.name, this.constructor.name);
      },
      error: error => {
        this.logger.error(error, error, this.constructor.name);
      },
    });
  }
}
