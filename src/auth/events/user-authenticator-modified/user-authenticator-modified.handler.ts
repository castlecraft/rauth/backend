import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { UserAuthenticatorModifiedEvent } from './user-authenticator-modified.event';
import { from } from 'rxjs';
import { UserAuthenticatorService } from '../../../user-management/entities/user-authenticator/user-authenticator.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';

@EventsHandler(UserAuthenticatorModifiedEvent)
export class UserAuthenticatorModifiedHandler
  implements IEventHandler<UserAuthenticatorModifiedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly authenticator: UserAuthenticatorService,
    private readonly config: ConfigService,
  ) {}
  handle(event: UserAuthenticatorModifiedEvent) {
    const { authKey } = event;
    from(this.authenticator.save(authKey)).subscribe({
      next: success => {
        this.logger.log(
          UserAuthenticatorModifiedEvent.name,
          this.constructor.name,
        );
      },
      error: error => {
        this.logger.error(error, error, this.constructor.name);
      },
    });
  }
}
