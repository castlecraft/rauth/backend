import { IEvent } from '@nestjs/cqrs';
import { SAMLApp } from '../../entities/saml-app/saml-app.interface';

export class SAMLAppAddedEvent implements IEvent {
  constructor(public readonly samlApp: SAMLApp) {}
}
