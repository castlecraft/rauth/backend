import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { SAMLAppAddedEvent } from './saml-app-added.event';
import { SAMLAppService } from '../../entities/saml-app/saml-app.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';

@EventsHandler(SAMLAppAddedEvent)
export class SAMLAppAddedHandler implements IEventHandler<SAMLAppAddedEvent> {
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly samlApp: SAMLAppService,
    private readonly config: ConfigService,
  ) {}
  handle(event: SAMLAppAddedEvent) {
    this.samlApp
      .save(event.samlApp)
      .then(added => {
        this.logger.log(SAMLAppAddedEvent.name, this.constructor.name);
      })
      .catch(error => {
        this.logger.error(error, error, this.constructor.name);
      });
  }
}
