import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { SAMLAppAddedHandler } from './saml-app-added.handler';
import { SAMLAppAddedEvent } from './saml-app-added.event';
import { SAMLApp } from '../../entities/saml-app/saml-app.interface';
import { SAMLAppService } from '../../entities/saml-app/saml-app.service';
import { ConfigService } from '../../../config/config.service';

describe('Event: SAMLAppAddedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: SAMLAppAddedHandler;
  let samlApp: SAMLAppService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        SAMLAppAddedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: SAMLAppService,
          useValue: () => jest.fn(),
        },
        {
          provide: ConfigService,
          useValue: {
            get: (key: string) => key,
          },
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<SAMLAppAddedHandler>(SAMLAppAddedHandler);
    samlApp = module.get<SAMLAppService>(SAMLAppService);
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should add Client using SAMLAppService', async () => {
    samlApp.save = jest.fn((...args) =>
      Promise.resolve({} as SAMLApp & { _id: any }),
    );
    eventBus$.publish = jest.fn(() => {});
    await eventHandler.handle(new SAMLAppAddedEvent({} as SAMLApp));
    expect(samlApp.save).toHaveBeenCalledTimes(1);
  });
});
