import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { KerberosRealmModifiedHandler } from './kerberos-realm-modified.handler';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';
import { KerberosRealm } from '../../entities/kerberos-realm/kerberos-realm.interface';
import { KerberosRealmModifiedEvent } from './kerberos-realm-modified.event';
import { ConfigService } from '../../../config/config.service';

describe('Event: KerberosRealmModifiedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: KerberosRealmModifiedHandler;

  let ldapClient: KerberosRealmService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        KerberosRealmModifiedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: KerberosRealmService,
          useValue: () => jest.fn(),
        },
        {
          provide: ConfigService,
          useValue: {
            get: (key: string) => key,
          },
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<KerberosRealmModifiedHandler>(
      KerberosRealmModifiedHandler,
    );
    ldapClient = module.get<KerberosRealmService>(KerberosRealmService);
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should modify realm using KerberosRealmService', async () => {
    ldapClient.save = jest.fn((...args) =>
      Promise.resolve({} as KerberosRealm & { _id: any }),
    );
    eventBus$.publish = jest.fn(() => {});
    await eventHandler.handle(
      new KerberosRealmModifiedEvent({} as KerberosRealm),
    );
    expect(ldapClient.save).toHaveBeenCalledTimes(1);
  });
});
