import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { KerberosRealmModifiedEvent } from './kerberos-realm-modified.event';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';

@EventsHandler(KerberosRealmModifiedEvent)
export class KerberosRealmModifiedHandler
  implements IEventHandler<KerberosRealmModifiedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly realm: KerberosRealmService,
    private readonly config: ConfigService,
  ) {}
  handle(event: KerberosRealmModifiedEvent) {
    this.realm
      .save(event.realm)
      .then(modified => {
        this.logger.log(KerberosRealmModifiedEvent.name, this.constructor.name);
      })
      .catch(error => {
        this.logger.error(error, error, this.constructor.name);
      });
  }
}
