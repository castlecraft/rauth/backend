import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { SocialLoginRemovedEvent } from './social-login-removed.event';
import { from } from 'rxjs';
import { SocialLoginService } from '../../entities/social-login/social-login.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';

@EventsHandler(SocialLoginRemovedEvent)
export class SocialLoginRemovedHandler
  implements IEventHandler<SocialLoginRemovedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly socialLogin: SocialLoginService,
    private readonly config: ConfigService,
  ) {}
  handle(event: SocialLoginRemovedEvent) {
    const { socialLogin } = event;
    from(this.socialLogin.remove(socialLogin)).subscribe({
      next: success => {
        this.logger.log(SocialLoginRemovedEvent.name, this.constructor.name);
      },
      error: error => {
        this.logger.error(error, error, this.constructor.name);
      },
    });
  }
}
