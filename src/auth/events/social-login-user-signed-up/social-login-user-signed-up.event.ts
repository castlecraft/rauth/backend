import { IEvent } from '@nestjs/cqrs';

export class SocialLoginUserSignedUpEvent implements IEvent {
  constructor(
    public readonly email: string,
    public readonly phone: string,
    public readonly socialLogin: string,
  ) {}
}
