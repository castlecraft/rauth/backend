import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { LDAPClientAddedEvent } from './ldap-client-added.event';
import { LDAPClientService } from '../../entities/ldap-client/ldap-client.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(LDAPClientAddedEvent)
export class LDAPClientAddedHandler
  implements IEventHandler<LDAPClientAddedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly ldapClient: LDAPClientService,
    private readonly config: ConfigService,
  ) {}
  handle(event: LDAPClientAddedEvent) {
    this.ldapClient
      .save(event.ldapClient)
      .then(added => {
        this.logger.log(LDAPClientAddedEvent.name, this.constructor.name);
      })
      .catch(error => {
        this.logger.error(error, error, this.constructor.name);
      });
  }
}
