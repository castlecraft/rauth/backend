import { IEvent } from '@nestjs/cqrs';
import { LDAPClient } from '../../entities/ldap-client/ldap-client.interface';

export class LDAPClientAddedEvent implements IEvent {
  constructor(public readonly ldapClient: LDAPClient) {}
}
