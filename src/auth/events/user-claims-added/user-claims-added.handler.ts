import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { UserClaimsAddedEvent } from './user-claims-added.event';
import { UserClaimService } from '../../entities/user-claim/user-claim.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { USER_CLAIMS_ADDED } from '../../../constants/app-strings';
import { ConfigService } from '../../../config/config.service';

@EventsHandler(UserClaimsAddedEvent)
export class UserClaimsAddedHandler
  implements IEventHandler<UserClaimsAddedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly userClaim: UserClaimService,
    private readonly config: ConfigService,
  ) {}

  handle(event: UserClaimsAddedEvent) {
    event.claims?.forEach(claim => {
      this.userClaim
        .findOneAndUpdate(
          { scope: claim.scope, name: claim.name, uuid: claim.uuid },
          claim,
          { upsert: true },
        )
        .then(added => {
          this.logger.debug(USER_CLAIMS_ADDED, added, this.constructor.name);
        })
        .catch(err => {
          this.logger.error(err, this.constructor.name);
        });
    });
  }
}
