import { IEvent } from '@nestjs/cqrs';
import { UserClaim } from '../../entities/user-claim/user-claim.interface';

export class UserClaimsAddedEvent implements IEvent {
  constructor(public readonly claims: UserClaim[]) {}
}
