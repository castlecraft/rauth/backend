import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { Test } from '@nestjs/testing';
import { ConfigService } from '../../../config/config.service';
import { UserClaim } from '../../entities/user-claim/user-claim.interface';
import { UserClaimService } from '../../entities/user-claim/user-claim.service';
import { UserClaimsAddedEvent } from './user-claims-added.event';
import { UserClaimsAddedHandler } from './user-claims-added.handler';

describe('Event: LDAPClientAddedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: UserClaimsAddedHandler;

  let userClaim: UserClaimService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        UserClaimsAddedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: UserClaimService,
          useFactory: () => jest.fn(),
        },
        {
          provide: ConfigService,
          useValue: {
            get: (key: string) => key,
          },
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<UserClaimsAddedHandler>(UserClaimsAddedHandler);
    userClaim = module.get<UserClaimService>(UserClaimService);
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should add Client using UserClaimService', async () => {
    userClaim.findOneAndUpdate = jest.fn((...args) =>
      Promise.resolve({} as { _id: any } & UserClaim),
    );
    eventBus$.publish = jest.fn(() => {});
    await eventHandler.handle(
      new UserClaimsAddedEvent([
        {
          uuid: '68ea855c-64ec-4d68-ac7a-ec021d3932f7',
          claimId: '7c481f1d-86d3-40e7-a352-e587dabae296',
          name: 'given_name',
          value: 'no-value',
        } as UserClaim,
      ]),
    );
    expect(userClaim.findOneAndUpdate).toHaveBeenCalledTimes(1);
  });
});
