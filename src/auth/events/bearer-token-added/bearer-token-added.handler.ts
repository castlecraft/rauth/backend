import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { BearerTokenAddedEvent } from './bearer-token-added.event';
import { BearerTokenService } from '../../entities/bearer-token/bearer-token.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';

@EventsHandler(BearerTokenAddedEvent)
export class BearerTokenAddedHandler
  implements IEventHandler<BearerTokenAddedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly token: BearerTokenService,
    private readonly config: ConfigService,
  ) {}
  handle(event: BearerTokenAddedEvent) {
    const { token } = event;
    this.token.save(token).catch(error => {
      this.logger.error(error, error, this.constructor.name);
    });
  }
}
