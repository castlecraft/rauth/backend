import { IEvent } from '@nestjs/cqrs';
import { SAMLApp } from '../../entities/saml-app/saml-app.interface';

export class SAMLAppModifiedEvent implements IEvent {
  constructor(public readonly samlApp: SAMLApp) {}
}
