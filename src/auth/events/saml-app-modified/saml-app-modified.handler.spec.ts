import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { SAMLApp } from '../../entities/saml-app/saml-app.interface';
import { SAMLAppModifiedEvent } from './saml-app-modified.event';
import { SAMLAppService } from '../../entities/saml-app/saml-app.service';
import { SAMLAppModifiedHandler } from './saml-app-modified.handler';
import { ConfigService } from '../../../config/config.service';

describe('Event: SAMLAppModifiedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: SAMLAppModifiedHandler;
  let samlApp: SAMLAppService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        SAMLAppModifiedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: SAMLAppService,
          useValue: () => jest.fn(),
        },
        {
          provide: ConfigService,
          useValue: {
            get: (key: string) => key,
          },
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<SAMLAppModifiedHandler>(SAMLAppModifiedHandler);
    samlApp = module.get<SAMLAppService>(SAMLAppService);
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should add Client using SAMLAppService', async () => {
    samlApp.save = jest.fn((...args) =>
      Promise.resolve({} as SAMLApp & { _id: any }),
    );
    eventBus$.publish = jest.fn(() => {});
    await eventHandler.handle(new SAMLAppModifiedEvent({} as SAMLApp));
    expect(samlApp.save).toHaveBeenCalledTimes(1);
  });
});
