import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { SAMLAppModifiedEvent } from './saml-app-modified.event';
import { SAMLAppService } from '../../entities/saml-app/saml-app.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';

@EventsHandler(SAMLAppModifiedEvent)
export class SAMLAppModifiedHandler
  implements IEventHandler<SAMLAppModifiedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly samlApp: SAMLAppService,
    private readonly config: ConfigService,
  ) {}
  handle(event: SAMLAppModifiedEvent) {
    this.samlApp
      .save(event.samlApp)
      .then(modified => {
        this.logger.log(SAMLAppModifiedEvent.name, this.constructor.name);
      })
      .catch(error => {
        this.logger.error(error, error, this.constructor.name);
      });
  }
}
