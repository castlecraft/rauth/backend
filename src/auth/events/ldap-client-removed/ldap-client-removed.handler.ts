import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { from } from 'rxjs';
import { LDAPClientRemovedEvent } from './ldap-client-removed.event';
import { LDAPClientService } from '../../entities/ldap-client/ldap-client.service';
import { SUCCESS } from '../../../constants/app-strings';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';

@EventsHandler(LDAPClientRemovedEvent)
export class LDAPClientRemovedHandler
  implements IEventHandler<LDAPClientRemovedEvent>
{
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly ldapClient: LDAPClientService,
    private readonly config: ConfigService,
  ) {}
  handle(event: LDAPClientRemovedEvent) {
    const { ldapClient } = event;
    from(this.ldapClient.remove(ldapClient)).subscribe({
      next: success => {
        this.logger.log(SUCCESS, this.constructor.name);
      },
      error: error => {
        this.logger.error(error, error, this.constructor.name);
      },
    });
  }
}
