import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { LDAPClientRemovedHandler } from './ldap-client-removed.handler';
import { LDAPClientService } from '../../entities/ldap-client/ldap-client.service';
import { LDAPClient } from '../../entities/ldap-client/ldap-client.interface';
import { LDAPClientRemovedEvent } from './ldap-client-removed.event';
import { ConfigService } from '../../../config/config.service';

describe('Event: LDAPClientRemovedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: LDAPClientRemovedHandler;
  let ldapClient: LDAPClientService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        LDAPClientRemovedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: LDAPClientService,
          useFactory: () => jest.fn(),
        },
        {
          provide: ConfigService,
          useValue: {
            get: (key: string) => key,
          },
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<LDAPClientRemovedHandler>(
      LDAPClientRemovedHandler,
    );
    ldapClient = module.get<LDAPClientService>(LDAPClientService);
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should remove LDAPClient using LDAPClientService', async () => {
    const mockLDAPClient = {} as LDAPClient;
    ldapClient.remove = jest.fn(() => Promise.resolve(mockLDAPClient));
    eventBus$.publish = jest.fn(() => {});
    await eventHandler.handle(
      new LDAPClientRemovedEvent(
        '9ac0d438-c9c4-4166-a896-3b0902f8bd99',
        mockLDAPClient,
      ),
    );
    expect(ldapClient.remove).toHaveBeenCalledTimes(1);
  });
});
