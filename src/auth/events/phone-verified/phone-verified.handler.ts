import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { from, forkJoin } from 'rxjs';
import { PhoneVerifiedEvent } from './phone-verified.event';
import { AuthDataService } from '../../../user-management/entities/auth-data/auth-data.service';
import { UserService } from '../../../user-management/entities/user/user.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@EventsHandler(PhoneVerifiedEvent)
export class PhoneVerifiedHandler implements IEventHandler<PhoneVerifiedEvent> {
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly user: UserService,
    private readonly authData: AuthDataService,
    private readonly config: ConfigService,
  ) {}
  handle(event: PhoneVerifiedEvent) {
    const { user, phoneOTP } = event;

    forkJoin({
      user: from(this.user.save(user)),
      authData: from(this.authData.remove(phoneOTP)),
    }).subscribe({
      next: success => {
        this.logger.log(PhoneVerifiedEvent.name, this.constructor.name);
      },
      error: error => {
        this.logger.error(error, error, this.constructor.name);
      },
    });
  }
}
