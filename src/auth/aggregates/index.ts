import { BearerTokenManagerService } from './bearer-token-manager/bearer-token-manager.service';
import { KerberosAuthAggregateService } from './kerberos-auth-aggregate/kerberos-auth-aggregate.service';
import { LDAPClientAggregateService } from './ldap-aggregate/ldap-client-aggregate.service';
import { OTPAggregateService } from './otp-aggregate/otp-aggregate.service';
import { SamlAppAggregateService } from './saml-app/saml-app-aggregate.service';
import { SocialLoginCallbackService } from './social-login-callback/social-login-callback.service';
import { SocialLoginManagementService } from './social-login-management/social-login-management.service';
import { WebAuthnAggregateService } from './webauthn-aggregate/webauthn-aggregate.service';

export const AuthAggregates = [
  BearerTokenManagerService,
  KerberosAuthAggregateService,
  LDAPClientAggregateService,
  OTPAggregateService,
  SamlAppAggregateService,
  SocialLoginCallbackService,
  SocialLoginManagementService,
  WebAuthnAggregateService,
];
