import { BadRequestException, Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { Request } from 'express';
import { authenticator, hotp } from 'otplib';
import { v4 as uuidv4 } from 'uuid';

import {
  EventsNotConnectedException,
  PhoneAlreadyRegisteredException,
  PhoneRegistrationNotAllowedException,
  InvalidOTPException,
  InvalidUserException,
} from '../../../common/filters/exceptions';
import {
  ConfigService,
  EVENTS_HOST,
  EVENTS_PASSWORD,
  EVENTS_PORT,
  EVENTS_USER,
} from '../../../config/config.service';
import { i18n } from '../../../i18n/i18n.config';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { EmailRequestService } from '../../../user-management/aggregates/email-request/email-request.service';
import {
  AuthData,
  AuthDataType,
} from '../../../user-management/entities/auth-data/auth-data.interface';
import { AuthDataService } from '../../../user-management/entities/auth-data/auth-data.service';
import {
  User,
  UserStatus,
} from '../../../user-management/entities/user/user.interface';
import { USER } from '../../../user-management/entities/user/user.schema';
import { UserService } from '../../../user-management/entities/user/user.service';
import { UserAccountAddedEvent } from '../../../user-management/events/user-account-added/user-account-added.event';
import { SignupViaPhoneDto } from '../../../user-management/policies';
import { PhoneVerifiedEvent } from '../../events/phone-verified/phone-verified.event';
import { UnverifiedPhoneAddedEvent } from '../../events/unverified-phone-added/unverified-phone-added.event';
import { UserLogInHOTPGeneratedEvent } from '../../events/user-login-hotp-generated/user-login-hotp-generated.event';
import { addSessionUser } from '../../guards/guard.utils';
import {
  DB,
  PHONE,
  RAUTH_SOURCE_ID,
  RAUTH_SOURCE_TYPE,
  SCOPE_SELF_SERVICE,
  SUCCESS,
} from '../../../constants/app-strings';
import { UserClaim } from '../../entities/user-claim/user-claim.interface';
import { UserClaimsAddedEvent } from '../../events/user-claims-added/user-claims-added.event';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@Injectable()
export class OTPAggregateService extends AggregateRoot {
  private otp: AuthData;
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly authData: AuthDataService,
    private readonly serverSettings: ServerSettingsService,
    private readonly user: UserService,
    private readonly config: ConfigService,
    private readonly email: EmailRequestService,
  ) {
    super();
  }

  async sendLoginOTP(user: User) {
    const hotp = await this.generateHOTP(user);

    if (
      this.otp.entityUuid === user.uuid &&
      this.otp.metaData.resendOTPAfterSec === 0
    ) {
      // Send Email
      this.sendEmail(user, hotp);

      // Broadcast Event for SMS Services
      this.apply(new UserLogInHOTPGeneratedEvent(user, hotp));
    }

    return this.otp;
  }

  async checkLocalOTP(user: User) {
    this.otp = await this.authData.findOne({
      entity: USER,
      entityUuid: user.uuid,
      authDataType: AuthDataType.LoginOTP,
    });

    if (this.otp) {
      const currentTime = Math.floor(Date.now() / 1000);
      const resendOtpAfterSec = Math.floor(
        new Date(this.otp.metaData.lastSent).getTime() / 1000 +
          Number(this.otp.metaData.resendOTPDelayInSec),
      );

      if (resendOtpAfterSec && currentTime < resendOtpAfterSec) {
        this.otp.metaData.resendOTPAfterSec = resendOtpAfterSec - currentTime;
        await this.authData.save(this.otp);
        return;
      } else {
        this.otp.metaData.resendOTPAfterSec = 0;
        this.otp.metaData.lastSent = new Date().toISOString();
        await this.authData.save(this.otp);
      }

      const settings = await this.serverSettings.find();
      // check expired?
      if (settings.regenerateOTP || this.otp.expiry <= new Date()) {
        // Generate new
        await this.generateLoginOTP(user);
      }
    }

    if (!this.otp) {
      this.otp = {} as AuthData;

      await this.generateLoginOTP(user);
    }
  }

  async generateLoginOTP(user: User) {
    if (!this.otp) {
      this.otp = {} as AuthData;
    }

    const secret = authenticator.generateSecret();
    const settings = await this.serverSettings.find();
    this.otp.metaData = {
      counter: Math.floor(Math.random() * 100),
      secret,
      resendOTPAfterSec: 0,
      lastSent: new Date().toISOString(),
      resendOTPDelayInSec: settings.resendOTPDelayInSec,
    };
    this.otp.entity = USER;
    this.otp.entityUuid = user.uuid;
    this.otp.authDataType = AuthDataType.LoginOTP;

    const expiry = new Date();
    expiry.setMinutes(expiry.getMinutes() + settings.otpExpiry);
    this.otp.expiry = expiry;

    return await this.authData.save(this.otp);
  }

  async generateHOTP(user: User): Promise<string> {
    await this.checkLocalOTP(user);
    return hotp.generate(
      this.otp.metaData.secret as string,
      Number(this.otp.metaData.counter),
    );
  }

  sendEmail(user: User, hotp: string) {
    return this.email.sendOTP(user, hotp, this.otp).subscribe({
      next: success => {
        this.logger.log(SUCCESS, this.constructor.name);
      },
      error: error => {
        this.logger.error(error, error, this.constructor.name);
      },
    });
  }

  async addUnverifiedPhone(userUuid: string, unverifiedPhone: string) {
    this.verifyConnectedEvents();
    await this.checkPhoneAlreadyRegistered(unverifiedPhone);

    const user = await this.user.findOne({ uuid: userUuid });

    if (!user) {
      throw new InvalidUserException();
    }

    // Generate OTP and broadcast Event
    const phoneOTP = await this.getPhoneVerificationCode(user, unverifiedPhone);

    const generatedHOTP = hotp.generate(
      phoneOTP.metaData.secret as string,
      Number(phoneOTP.metaData.counter),
    );

    this.apply(new UnverifiedPhoneAddedEvent(user, phoneOTP, generatedHOTP));
  }

  async checkLocalPhoneVerificationCode(user: User) {
    return await this.authData.findOne({
      entity: USER,
      entityUuid: user.uuid,
      authDataType: AuthDataType.PhoneVerificationCode,
    });
  }

  verifyConnectedEvents() {
    let isEventsConnected = false;

    if (
      this.config.get(EVENTS_HOST) &&
      this.config.get(EVENTS_PORT) &&
      this.config.get(EVENTS_USER) &&
      this.config.get(EVENTS_PASSWORD)
    ) {
      isEventsConnected = true;
    }

    if (!isEventsConnected) {
      throw new EventsNotConnectedException();
    }
  }

  async checkPhoneAlreadyRegistered(unverifiedPhone: string) {
    const existingPhoneUser = await this.user.findOne({
      phone: unverifiedPhone,
    });
    if (existingPhoneUser) {
      throw new PhoneAlreadyRegisteredException();
    }
  }

  async getPhoneVerificationCode(user: User, unverifiedPhone: string) {
    const settings = await this.serverSettings.find();
    // Check server settings for enableUserPhone
    if (!settings?.enableUserPhone) {
      throw new PhoneRegistrationNotAllowedException();
    }

    const phoneOTP = await this.checkLocalPhoneVerificationCode(user);
    if (phoneOTP) {
      return phoneOTP;
    }

    const newPhoneOTP = {} as AuthData;
    const secret = authenticator.generateSecret();

    newPhoneOTP.metaData = {
      counter: Math.floor(Math.random() * 100),
      secret,
      phone: unverifiedPhone,
    };
    newPhoneOTP.entity = USER;
    newPhoneOTP.entityUuid = user.uuid;
    newPhoneOTP.authDataType = AuthDataType.PhoneVerificationCode;

    const expiry = new Date();
    expiry.setMinutes(expiry.getMinutes() + settings?.otpExpiry || 5);
    newPhoneOTP.expiry = expiry;

    return newPhoneOTP;
  }

  async verifyPhone(
    userUuid: string,
    otp: string,
    req?: Request & { session: { users?: unknown[] } & unknown },
  ) {
    // Check user
    const user = await this.user.findOne({ uuid: userUuid });
    if (!user) {
      throw new InvalidUserException();
    }

    // check local otp
    const phoneOTP = await this.checkLocalPhoneVerificationCode(user);
    if (!phoneOTP) {
      throw new InvalidOTPException();
    }
    if (phoneOTP && phoneOTP.expiry < new Date()) {
      throw new InvalidOTPException();
    }

    const phone = phoneOTP.metaData.phone as string;

    // validate otp with payload otp
    const validHOTP = hotp.generate(
      phoneOTP.metaData.secret as string,
      Number(phoneOTP.metaData.counter),
    );
    if (validHOTP !== otp) {
      throw new InvalidOTPException();
    }

    // check already registered phone
    await this.checkPhoneAlreadyRegistered(phone);

    // set phone
    user.phone = phone;
    user.unverifiedPhone = undefined;

    // set user enabled
    user.disabled = false;
    user.status = UserStatus.Verified;

    // Login as user
    if (req && req.logIn) {
      const users = req.session?.users;
      req.logIn(user, () => {
        req.session.users = users;
        addSessionUser(req, {
          uuid: user.uuid,
          email: user.email,
          phone: user.phone,
        });
      });
    }

    this.apply(new PhoneVerifiedEvent(user, phoneOTP));
  }

  async signupViaPhone(payload: SignupViaPhoneDto) {
    await this.validateSignupEnabled();
    await this.checkPhoneAlreadyRegistered(payload.unverifiedPhone);

    // Check if sending sms is available
    this.verifyConnectedEvents();
    const settings = await this.serverSettings.find();
    if (!settings?.enableUserPhone) {
      throw new PhoneRegistrationNotAllowedException();
    }

    const unverifiedUser =
      (await this.getUnverifiedPhoneIfAlreadyRegistered(
        payload.unverifiedPhone,
      )) || ({} as User);

    if (!unverifiedUser.uuid) {
      unverifiedUser.uuid = uuidv4();
    }

    unverifiedUser.name = payload.name;
    unverifiedUser.unverifiedPhone = payload.unverifiedPhone;
    unverifiedUser.enablePasswordLess = true;
    unverifiedUser.disabled = true;
    unverifiedUser.creation = new Date();
    unverifiedUser.status = UserStatus.Unverified;

    this.apply(new UserAccountAddedEvent(unverifiedUser));
    const claims = [
      {
        scope: SCOPE_SELF_SERVICE,
        name: RAUTH_SOURCE_TYPE,
        uuid: unverifiedUser.uuid,
        value: DB,
      } as UserClaim,
      {
        scope: SCOPE_SELF_SERVICE,
        name: RAUTH_SOURCE_ID,
        uuid: unverifiedUser.uuid,
        value: PHONE,
      } as UserClaim,
    ];
    if (claims.length > 0) {
      this.apply(new UserClaimsAddedEvent(claims));
    }

    // Generate OTP and broadcast Event
    const phoneOTP = await this.getPhoneVerificationCode(
      unverifiedUser,
      unverifiedUser.unverifiedPhone,
    );

    const validHOTP = hotp.generate(
      phoneOTP.metaData.secret as string,
      Number(phoneOTP.metaData.counter),
    );

    this.apply(
      new UnverifiedPhoneAddedEvent(unverifiedUser, phoneOTP, validHOTP),
    );
    const result = {
      ...payload,
    };
    return result;
  }

  async getUnverifiedPhoneIfAlreadyRegistered(unverifiedPhone: string) {
    return await this.user.findOne({ unverifiedPhone });
  }

  async validateSignupEnabled() {
    const settings = await this.serverSettings.find();
    if (settings.disableSignup) {
      throw new BadRequestException({
        message: i18n.__('Signup Disabled'),
      });
    }
  }
}
