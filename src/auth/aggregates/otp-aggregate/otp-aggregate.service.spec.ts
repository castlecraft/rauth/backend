import { Test, TestingModule } from '@nestjs/testing';
import { OTPAggregateService } from './otp-aggregate.service';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { AuthDataService } from '../../../user-management/entities/auth-data/auth-data.service';
import { UserService } from '../../../user-management/entities/user/user.service';
import { ConfigService } from '../../../config/config.service';
import { EmailRequestService } from '../../../user-management/aggregates/email-request/email-request.service';

describe('OTPAggregateService', () => {
  let service: OTPAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        OTPAggregateService,
        {
          provide: ServerSettingsService,
          useValue: {},
        },
        {
          provide: EmailRequestService,
          useValue: {},
        },
        {
          provide: AuthDataService,
          useValue: {},
        },
        {
          provide: UserService,
          useValue: {},
        },
        {
          provide: ConfigService,
          useValue: {
            get: (key: string) => key,
          },
        },
      ],
    }).compile();

    service = module.get<OTPAggregateService>(OTPAggregateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
