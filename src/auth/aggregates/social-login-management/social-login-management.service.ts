import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { v4 as uuidv4 } from 'uuid';
import { User } from '../../../user-management/entities/user/user.interface';
import { UserAccountAddedEvent } from '../../../user-management/events/user-account-added/user-account-added.event';
import { CreateSocialLoginDto } from '../../controllers/social-login/social-login-create.dto';
import { SocialLogin } from '../../entities/social-login/social-login.interface';
import { SocialLoginService } from '../../entities/social-login/social-login.service';
import { SocialLoginUserSignedUpEvent } from '../../events';
import { SocialLoginAddedEvent } from '../../events/social-login-added/social-login-added.event';
import { SocialLoginModifiedEvent } from '../../events/social-login-modified/social-login-modified.event';
import { SocialLoginRemovedEvent } from '../../events/social-login-removed/social-login-removed.event';
import {
  EMAIL,
  PHONE_NUMBER,
  RAUTH_SOURCE_ID,
  RAUTH_SOURCE_TYPE,
  SCOPE_SELF_SERVICE,
  SOCIAL_LOGIN,
} from '../../../constants/app-strings';
import { UserClaim } from '../../entities/user-claim/user-claim.interface';
import { UserClaimsAddedEvent } from '../../events/user-claims-added/user-claims-added.event';
import { IDTokenClaims } from '../../middlewares/interfaces';
import { UserClaimService } from '../../entities/user-claim/user-claim.service';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { PasswordCryptoService } from '../../../auth/policies/password-crypto/password-crypto.service';
@Injectable()
export class SocialLoginManagementService extends AggregateRoot {
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly userClaim: UserClaimService,
    private readonly socialLoginService: SocialLoginService,
    private readonly config: ConfigService,
    private readonly passwordEncryptionService: PasswordCryptoService,
  ) {
    super();
  }

  async signUpSocialLoginUser(
    profile: IDTokenClaims,
    socialLogin: SocialLogin,
  ) {
    const uuid = uuidv4();
    this.logger.log(`${SOCIAL_LOGIN}- ${uuid}`, this.constructor.name);
    const user: User = {
      name: profile.name,
      email: profile[socialLogin.emailAttribute || EMAIL],
      isEmailVerified: profile.email_verified,
      uuid,
    } as User;
    if (profile[socialLogin.phoneAttribute || PHONE_NUMBER]) {
      user.phone = profile[
        socialLogin.phoneAttribute || PHONE_NUMBER
      ] as string;
    }
    this.apply(
      new SocialLoginUserSignedUpEvent(
        profile.name,
        profile.email,
        socialLogin.uuid,
      ),
    );
    let claims = [
      {
        scope: SCOPE_SELF_SERVICE,
        name: RAUTH_SOURCE_TYPE,
        uuid,
        value: SOCIAL_LOGIN,
      } as UserClaim,
      {
        scope: SCOPE_SELF_SERVICE,
        name: RAUTH_SOURCE_ID,
        uuid,
        value: socialLogin.uuid,
      } as UserClaim,
    ];
    claims = [
      ...claims,
      ...(await this.getClaimsToBeAdded(profile, user, socialLogin)),
    ];

    if (claims.length > 0) {
      this.apply(new UserClaimsAddedEvent(claims));
    }
    this.apply(new UserAccountAddedEvent(user));
    return user;
  }

  async removeSocialLogin(uuid: string, userUuid: string) {
    const socialLogin = await this.socialLoginService.findOne({ uuid });
    if (socialLogin) {
      this.apply(new SocialLoginRemovedEvent(userUuid, socialLogin));
    } else {
      throw new NotFoundException();
    }
  }

  async addSocialLogin(payload: CreateSocialLoginDto, createdBy: string) {
    payload.clientSecret = this.passwordEncryptionService.encrypt(
      payload.clientSecret,
    );
    const params = {
      ...payload,
      ...{
        createdBy,
        creation: new Date(),
      },
    } as SocialLogin;
    this.apply(new SocialLoginAddedEvent(params));
    return params;
  }

  async modifySocialLogin(payload: CreateSocialLoginDto, uuid: string) {
    const socialLogin = await this.socialLoginService.findOne({ uuid });
    if (payload.clientSecret) {
      payload.clientSecret = this.passwordEncryptionService.encrypt(
        payload.clientSecret,
      );
    }
    Object.assign(socialLogin, payload);
    socialLogin.modified = new Date();
    this.apply(new SocialLoginModifiedEvent(socialLogin));
    return socialLogin;
  }

  async updateUserClaims(
    profile: IDTokenClaims,
    user: User,
    socialLogin: SocialLogin,
  ) {
    const getDbClaim = await this.userClaim.find({ uuid: user.uuid });
    const claims = await this.getClaimsToBeAdded(profile, user, socialLogin);

    const updateClaims = [];
    for (const claim of claims) {
      const matchingAttribute = getDbClaim.find(
        claimMap => claimMap.name === claim.name,
      );
      if (matchingAttribute) {
        if (matchingAttribute.value !== claim.value) {
          updateClaims.push(claim);
        }
      } else {
        updateClaims.push(claim);
      }
    }
    this.apply(new UserClaimsAddedEvent(updateClaims));
  }

  async getClaimsToBeAdded(
    profile: IDTokenClaims,
    user: User,
    socialLogin: SocialLogin,
  ) {
    const claims = [];
    for (const claimMap of socialLogin.claimsMap) {
      if (profile[claimMap.claim]) {
        claims.push({
          scope: socialLogin.claimsScope,
          name: claimMap.mapTo,
          uuid: user.uuid,
          value: profile[claimMap.claim],
        } as UserClaim);
      }
    }
    return claims;
  }
}
