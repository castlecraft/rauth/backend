import {
  BadRequestException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { Request } from 'express';
import { KerberosServer, initializeServer } from 'kerberos';
import { v4 as uuidv4 } from 'uuid';

import { InvalidOTPException } from '../../../common/filters/exceptions';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';
import {
  KERBEROS,
  LOGIN_ROUTE,
  NEGOTIATE,
  OTP_ENABLED,
  SUCCESS,
} from '../../../constants/app-strings';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import {
  SystemLogCode,
  SystemLoginType,
} from '../../../system-settings/entities/system-log/system-log.interface';
import { User } from '../../../user-management/entities/user/user.interface';
import { UserAccountAddedEvent } from '../../../user-management/events/user-account-added/user-account-added.event';
import { AuthService } from '../../controllers/auth/auth.service';
import { CreateKerberosRealmDto } from '../../controllers/kerberos/kerberos-realm-create.dto';
import { KerberosRealm } from '../../entities/kerberos-realm/kerberos-realm.interface';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';
import { LDAPClient } from '../../entities/ldap-client/ldap-client.interface';
import { KerberosRealmAddedEvent } from '../../events/kerberos-realm-added/kerberos-realm-added.event';
import { KerberosRealmModifiedEvent } from '../../events/kerberos-realm-modified/kerberos-realm-modified.event';
import { KerberosRealmRemovedEvent } from '../../events/kerberos-realm-removed/kerberos-realm-removed.event';
import { UserClaimsAddedEvent } from '../../events/user-claims-added/user-claims-added.event';
import { KerberosStrategyOptions } from '../../passport/base/kerberos.strategy';
import { PasswordCryptoService } from '../../policies/password-crypto/password-crypto.service';
import {
  LDAPClientAggregateService,
  LDAPUser,
} from '../ldap-aggregate/ldap-client-aggregate.service';
import { i18n } from '../../../i18n/i18n.config';

@Injectable()
export class KerberosAuthAggregateService extends AggregateRoot {
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly authService: AuthService,
    private readonly kerberosRealm: KerberosRealmService,
    private readonly ldapClient: LDAPClientAggregateService,
    private readonly config: ConfigService,
    private readonly settings: ServerSettingsService,
    private readonly password: PasswordCryptoService,
  ) {
    super();
  }

  async loginViaKerberos(
    req: Request,
    auth: string,
    options?: KerberosStrategyOptions,
  ) {
    const realm = await this.kerberosRealm.findOne({
      uuid: req?.params?.kerberosRealm,
    });
    if (!realm) {
      throw new NotFoundException({
        KerberosRealm: req?.params?.kerberosRealm,
      });
    }
    if (realm.disabled) {
      throw new BadRequestException({ KerberosDisabled: realm.uuid });
    }
    if (realm.servicePrincipalName) {
      if (!options) {
        options = {};
      }
      options.servicePrincipalName = realm.servicePrincipalName;
    }
    const path = await this.getRedirectPath(
      req.query.redirect as unknown as string,
    );
    const context = await this.fetchKerberosServerContext(options);
    const principal = await this.fetchPrincipal(context, auth);
    const client = await this.ldapClient.findOne({ uuid: realm.ldapClient });
    const ldapUser = await this.getLDAPUser(client, principal);
    const user = await this.ldapClient.getLocalUser(ldapUser, client);
    if (!user) {
      return {
        user: this.createUser(ldapUser, client),
        isOtpValidationComplete: true,
        path,
      };
    }

    if (user.disabled) {
      throw new UnauthorizedException(i18n.__('User Disabled'));
    }

    if (realm.forceOTP || user.enable2fa || user.enableOTP) {
      this.logger.log(
        `${KERBEROS}-${user.uuid}`,
        `${OTP_ENABLED}`,
        this.constructor.name,
      );
      try {
        await this.authService.validate2FaCode(
          user,
          req.body.code,
          SystemLoginType.Kerberos,
          true,
        );
        await this.authService.removeLoginHOTP(user);
        return { user, isOtpValidationComplete: true, path };
      } catch (error) {
        if (req.body.code) {
          await this.authService.saveAuthLog(
            user.uuid,
            SystemLogCode.Failure,
            { loginType: SystemLoginType.Kerberos },
            error.message,
          );
          this.logger.error(error, error, this.constructor.name);
          throw new InvalidOTPException();
        }
      }
      const separator = path.includes('?') ? '&' : '?';
      return {
        user,
        isOtpValidationComplete: false,
        path: path + separator + 'user=' + user.uuid,
      };
    }
    await this.authService.saveAuthLog(
      user.uuid,
      SystemLogCode.Success,
      { loginType: SystemLoginType.Kerberos },
      SUCCESS,
      false,
    );
    return { user, isOtpValidationComplete: true, path };
  }

  fetchKerberosServerContext(options?: KerberosStrategyOptions) {
    return new Promise<KerberosServer>((resolve, reject) => {
      initializeServer(
        options?.servicePrincipalName || 'HTTP',
        (err, context) => {
          if (err) {
            return reject(new Error(err));
          }
          return resolve(context);
        },
      );
    });
  }

  fetchPrincipal(context: KerberosServer, auth: string) {
    return new Promise<string>((resolve, reject) => {
      context.step(auth.substring(`${NEGOTIATE} `.length), err => {
        if (err) {
          return reject(new Error(err));
        }
        return resolve(context.username);
      });
    });
  }

  async removeKerberosRealm(uuid: string, userUuid: string) {
    const realm = await this.kerberosRealm.findOne({ uuid });
    if (realm) {
      this.apply(new KerberosRealmRemovedEvent(userUuid, realm));
    } else {
      throw new NotFoundException({ uuid });
    }
  }

  async addKerberosRealm(payload: CreateKerberosRealmDto, createdBy: string) {
    const params = {
      ...payload,
      ...{
        uuid: uuidv4(),
        createdBy,
        creation: new Date(),
      },
    } as KerberosRealm;
    this.apply(new KerberosRealmAddedEvent(params));
    return params;
  }

  async modifyKerberosRealm(payload: CreateKerberosRealmDto, uuid: string) {
    const realm = await this.kerberosRealm.findOne({ uuid });
    if (!realm) {
      throw new NotFoundException({ uuid });
    }
    if (payload.ldapClient) {
      const ldapClient = await this.ldapClient.findOne({
        uuid: payload.ldapClient,
      });
      if (!ldapClient) {
        throw new NotFoundException({ LDAPClient: payload.ldapClient });
      }
    }
    Object.assign(realm, payload);
    realm.modified = new Date();
    this.apply(new KerberosRealmModifiedEvent(realm));
    return realm;
  }

  async getLDAPUser(client: LDAPClient, principal: string): Promise<LDAPUser> {
    const username = principal.replace(/@.*$/, '');
    const ldapUser = await this.ldapClient.authenticate({
      username,
      ldapOpts: { url: client.url },
      adminDn: client.adminDn,
      adminPassword: this.password.decrypt(client.adminPassword),
      userSearchBase: client.userSearchBase,
      usernameAttribute: client.usernameAttribute,
      verifyUserExists: true,
    });
    return ldapUser as LDAPUser;
  }

  createUser(ldapUser: LDAPUser, client: LDAPClient) {
    const user = this.authService.userModel({
      email: ldapUser[client.emailAttribute],
      phone: ldapUser[client.phoneAttribute],
      name: ldapUser[client.fullNameAttribute],
      isEmailVerified: true,
      uuid: uuidv4(),
    } as User);
    user.email = user.email?.trim().toLowerCase();

    this.apply(new UserAccountAddedEvent(user));
    const claims = this.ldapClient.getClaimsToBeAdded(user, client, ldapUser);
    if (claims.length > 0) {
      this.apply(new UserClaimsAddedEvent(claims));
    }
    return user;
  }

  async getRedirectPath(redirect: string) {
    try {
      const url = new URL(redirect);
      url.searchParams.delete('user');
      return url.toString();
    } catch (error) {
      const settings = await this.settings.find();
      const redirectTo = redirect ? '&redirect=' + redirect : '';
      return settings.issuerUrl + LOGIN_ROUTE + redirectTo;
    }
  }
}
