import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '../../../config/config.service';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { AuthDataService } from '../../../user-management/entities/auth-data/auth-data.service';
import { AuthService } from '../../controllers/auth/auth.service';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';
import { PasswordCryptoService } from '../../policies/password-crypto/password-crypto.service';
import { LDAPClientAggregateService } from '../ldap-aggregate/ldap-client-aggregate.service';
import { KerberosAuthAggregateService } from './kerberos-auth-aggregate.service';

describe('KerberosAuthAggregateService', () => {
  let service: KerberosAuthAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthDataService,
        KerberosAuthAggregateService,
        { provide: AuthService, useValue: {} },
        { provide: AuthDataService, useValue: {} },
        { provide: KerberosRealmService, useValue: {} },
        { provide: LDAPClientAggregateService, useValue: {} },
        {
          provide: ConfigService,
          useValue: {
            get: (key: string) => key,
          },
        },
        { provide: ServerSettingsService, useValue: {} },
        { provide: PasswordCryptoService, useValue: {} },
      ],
    }).compile();

    service = module.get<KerberosAuthAggregateService>(
      KerberosAuthAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
