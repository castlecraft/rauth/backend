import {
  BadRequestException,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import {
  generateAuthenticationOptions,
  generateRegistrationOptions,
  verifyAuthenticationResponse,
  verifyRegistrationResponse,
} from '@simplewebauthn/server';
import {
  decodeClientDataJSON,
  isoBase64URL,
  isoUint8Array,
} from '@simplewebauthn/server/helpers';
import {
  PublicKeyCredentialCreationOptionsJSON,
  PublicKeyCredentialRequestOptionsJSON,
} from '@simplewebauthn/typescript-types';
import { v4 as uuidv4 } from 'uuid';
import { InvalidUserException } from '../../../common/filters/exceptions';
import {
  ACCOUNTS_ROUTE,
  LOGIN,
  SERVICE,
  TEN_NUMBER,
} from '../../../constants/app-strings';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import {
  AuthData,
  AuthDataType,
} from '../../../user-management/entities/auth-data/auth-data.interface';
import { AuthDataService } from '../../../user-management/entities/auth-data/auth-data.service';
import {
  Fmt,
  UserAuthenticator,
} from '../../../user-management/entities/user-authenticator/user-authenticator.interface';
import { UserAuthenticatorService } from '../../../user-management/entities/user-authenticator/user-authenticator.service';
import { User } from '../../../user-management/entities/user/user.interface';
import { USER } from '../../../user-management/entities/user/user.schema';
import { UserService } from '../../../user-management/entities/user/user.service';
import { UserAccountModifiedEvent } from '../../../user-management/events/user-account-modified/user-account-modified.event';
import { AuthService } from '../../controllers/auth/auth.service';
import { UserAuthenticatorRemovedEvent } from '../../events/user-authenticator-removed/user-authenticator-removed.event';
import { UserLoggedInWithWebAuthnEvent } from '../../events/user-logged-in-with-webauthn-key/user-logged-in-with-webauthn-key.event';
import { WebAuthnKeyRegisteredEvent } from '../../events/webauthn-key-registered/webauthn-key-registered.event';
import {
  WebauthnChallengeType,
  WebAuthnKeyChallengeRequestedEvent,
} from '../../events/webauthn-key-registration-requested/webauthn-key-challenge-requested.event';
import { addSessionUser, defaultOptions } from '../../guards/guard.utils';
import { ConfigService } from '../../../config/config.service';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';

@Injectable()
export class WebAuthnAggregateService extends AggregateRoot {
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly user: UserService,
    private readonly authData: AuthDataService,
    private readonly authenticator: UserAuthenticatorService,
    private readonly settings: ServerSettingsService,
    private readonly auth: AuthService,
    private readonly config: ConfigService,
  ) {
    super();
  }

  async requestRegister(
    actorUuid: string,
    userUuid: string,
  ): Promise<PublicKeyCredentialCreationOptionsJSON> {
    const relyingParty = await this.getRelyingParty();

    await this.validateAuthorizedUser(actorUuid, userUuid);

    if (!userUuid) userUuid = actorUuid;

    const user = await this.user.findOne({ uuid: userUuid });
    if (!user) {
      throw new ForbiddenException();
    }

    const authenticators = await this.authenticator.find({
      userUuid: user.uuid,
    });

    const challengeResponse = await generateRegistrationOptions({
      rpName: relyingParty.name,
      rpID: relyingParty.id,
      userID: isoUint8Array.fromUTF8String(user.uuid),
      userName: user.email || user.phone,
      userDisplayName: user.name,
      // Don't prompt users for additional information about the authenticator
      // (Recommended for smoother UX)
      attestationType: 'none',
      authenticatorSelection: {
        // See https://www.w3.org/TR/webauthn-2/#enumdef-residentkeyrequirement
        residentKey: 'discouraged',
      },
      // Prevent users from re-registering existing authenticators
      excludeCredentials: authenticators.map(cred => {
        return {
          id: cred.credID,
          type: 'public-key',
        };
      }),
    });

    const authData = this.saveChallenge(user, challengeResponse.challenge);
    this.apply(
      new WebAuthnKeyChallengeRequestedEvent(
        authData,
        WebauthnChallengeType.Registration,
      ),
    );

    return challengeResponse;
  }

  async register(body, userUuid: string) {
    const user = await this.user.findOne({ uuid: userUuid });
    const challenge = this.getChallengeFromClientData(
      body.response.clientDataJSON,
    );

    const authData = await this.authData.findOne({
      entity: USER,
      entityUuid: user?.uuid,
      authDataType: AuthDataType.Challenge,
      password: challenge,
    });

    if (!authData) {
      throw new ForbiddenException();
    }

    const relyingParty = await this.getRelyingParty();

    try {
      const verification = await verifyRegistrationResponse({
        response: body,
        expectedChallenge: authData?.password,
        expectedOrigin: relyingParty.origin,
        expectedRPID: relyingParty.id,
        requireUserVerification: false,
      });

      const userUuid = authData.entityUuid;

      await this.authData.remove(authData);

      const authenticator = {} as UserAuthenticator;
      const deviceInfo = verification?.registrationInfo;
      authenticator.uuid = uuidv4();
      authenticator.credID = deviceInfo?.credentialID;
      authenticator.fmt = deviceInfo.fmt as unknown as Fmt;
      authenticator.publicKey = isoBase64URL.fromBuffer(
        deviceInfo?.credentialPublicKey,
      );
      authenticator.counter = deviceInfo.counter;
      authenticator.userUuid = userUuid;

      this.apply(new WebAuthnKeyRegisteredEvent(authenticator));

      return { registered: authenticator.uuid };
    } catch (error) {
      this.logger.error(error, error, this.constructor.name);
      throw new BadRequestException(error);
    }
  }

  async login(
    username: string,
  ): Promise<PublicKeyCredentialRequestOptionsJSON> {
    const user = await this.user.findUserByIdentifier(username);
    this.logger.log(`${LOGIN}- ${user.uuid}`, this.constructor.name);

    const authenticators = await this.authenticator.find({
      userUuid: user.uuid,
    });

    if (authenticators.length === 0) {
      throw new BadRequestException();
    }

    const relayingParty = await this.getRelyingParty();
    const options = await generateAuthenticationOptions({
      // Require users to use a previously-registered authenticator
      allowCredentials: authenticators.map(cred => ({
        id: cred.credID,
        type: 'public-key',
        // Optional
        transports: ['ble', 'internal', 'usb', 'nfc'],
      })),
      rpID: relayingParty.id,
      userVerification: 'preferred',
    });

    const authData = this.saveChallenge(user, options.challenge);
    this.apply(
      new WebAuthnKeyChallengeRequestedEvent(
        authData,
        WebauthnChallengeType.Login,
      ),
    );

    return options;
  }

  async loginChallenge(req) {
    const { body, query } = req;

    const challenge = this.getChallengeFromClientData(
      body.response.clientDataJSON,
    );
    if (!challenge) {
      throw new BadRequestException();
    }

    const authChallenge = await this.authData.findOne({
      authDataType: AuthDataType.Challenge,
      password: challenge,
      entity: USER,
    });
    if (!authChallenge) {
      throw new BadRequestException();
    }

    const user = await this.user.findOne({ uuid: authChallenge.entityUuid });
    if (!user || user.disabled || user.deleted) {
      throw new InvalidUserException();
    }

    const authenticator = await this.authenticator.findOne({
      userUuid: user.uuid,
      credID: body.rawId,
    });

    if (!authenticator) {
      throw new BadRequestException();
    }

    const relyingParty = await this.getRelyingParty();

    try {
      const verification = await verifyAuthenticationResponse({
        response: body,
        expectedChallenge: authChallenge.password,
        expectedOrigin: relyingParty.origin,
        expectedRPID: relyingParty.id,
        authenticator: {
          credentialPublicKey: Buffer.from(authenticator.publicKey, 'base64'),
          credentialID: authenticator.credID,
          counter: authenticator.counter,
        },
        requireUserVerification: false,
      });

      await this.authData.remove(authChallenge);
      const { verified, authenticationInfo } = verification;
      await this.authenticator.updateOne(
        { uuid: authenticator.uuid },
        { $set: { counter: authenticationInfo.newCounter } },
      );
      authenticator.counter = authenticationInfo.newCounter;

      this.apply(new UserLoggedInWithWebAuthnEvent(user, authenticator));
      if (verified) {
        req[defaultOptions.property] = user;
        const users = req.session?.users;
        await this.auth.requestLogIn(req);
        req.session.users = users;
        addSessionUser(req, {
          uuid: user.uuid,
          email: user.email,
          phone: user.phone,
        });
      }
      const settings = await this.settings.find();
      return {
        verified,
        redirect: query.redirect
          ? query.redirect
          : settings.issuerUrl + ACCOUNTS_ROUTE,
      };
    } catch (error) {
      this.logger.error(error, error, this.constructor.name);
      throw new BadRequestException(error);
    }
  }

  async find(actorUuid: string, userUuid: string) {
    await this.validateAuthorizedUser(actorUuid, userUuid);
    return await this.authenticator.find({ userUuid });
  }

  async validateAuthorizedUser(actorUuid: string, userUuid: string) {
    const isAdmin = await this.user.checkAdministrator(actorUuid);
    if (!isAdmin && userUuid !== actorUuid) {
      throw new ForbiddenException();
    }
  }

  async removeAuthenticator(uuid: string, actorUuid: string, userUuid: string) {
    if (!userUuid) userUuid = actorUuid;
    await this.validateAuthorizedUser(actorUuid, userUuid);

    const user = await this.user.findOne({ uuid: userUuid });
    if (!user) {
      throw new InvalidUserException();
    }

    const authKey = await this.authenticator.findOne({ uuid });
    this.apply(new UserAuthenticatorRemovedEvent(authKey));

    const authenticators = await this.authenticator.find({ userUuid });
    if (authenticators.length === 0 && !user.enable2fa) {
      user.enablePasswordLess = false;
      this.apply(new UserAccountModifiedEvent(user));
    }
  }

  async renameAuthenticator(uuid: string, name: string, actorUuid: string) {
    await this.validateAuthorizedUser(actorUuid, actorUuid);
    const authKey = await this.authenticator.findOne({ uuid });
    authKey.name = name;
    await this.authenticator.save(authKey);
    return { name: authKey.name, uuid: authKey.uuid };
  }

  saveChallenge(user: User, challenge: string) {
    const now = new Date();
    const expiry = now;
    expiry.setMinutes(now.getMinutes() + TEN_NUMBER);

    const authData = {} as AuthData;
    authData.uuid = uuidv4();
    authData.authDataType = AuthDataType.Challenge;
    authData.password = challenge;
    authData.entity = USER;
    authData.entityUuid = user.uuid;
    authData.expiry = expiry;

    return authData;
  }

  async getRelyingParty() {
    const settings = await this.settings.findWithoutError();
    const partyName = settings.organizationName || SERVICE;
    const issuerUrl = settings.issuerUrl;

    const relyingParty: { name: string; id?: string; origin?: string } = {
      name: partyName,
    };

    const rpUrl = new URL(issuerUrl);
    relyingParty.id = rpUrl.hostname;
    relyingParty.origin = `${rpUrl.protocol}//${rpUrl.host}`;

    return relyingParty;
  }

  getChallengeFromClientData(clientDataJSON: string) {
    const clientData = decodeClientDataJSON(clientDataJSON);
    return clientData.challenge;
  }
}
