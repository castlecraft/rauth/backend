export const SAML_LOGIN_RESPONSE_XML_TEMPLATE = `<?xml version="1.0" encoding="UTF-8"?>
<samlp:Response
    xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol"
    xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion"
    ID="{ID}"
    Version="2.0"
    InResponseTo="{InResponseTo}"
    IssueInstant="{IssueInstant}"
    Destination="{Destination}"
>
    <saml:Issuer
        Format="urn:oasis:names:tc:SAML:2.0:nameid-format:entity"
        xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion"
    >
        {Issuer}
    </saml:Issuer>
    <samlp:Status>
        <samlp:StatusCode Value="{StatusCode}" />
    </samlp:Status>
    <saml:Assertion
        ID="{AssertionID}"
        Version="2.0"
        IssueInstant="{IssueInstant}"
        xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion"
        xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    >
        <saml:Issuer Format="urn:oasis:names:tc:SAML:2.0:nameid-format:entity">{Issuer}</saml:Issuer>
    <saml:Subject>
        <saml:NameID Format="{NameIDFormat}">{NameID}</saml:NameID>
        <saml:SubjectConfirmation Method="urn:oasis:names:tc:SAML:2.0:cm:bearer">
            <saml:SubjectConfirmationData
                NotOnOrAfter="{SubjectConfirmationDataNotOnOrAfter}"
                Recipient="{SubjectRecipient}"
                InResponseTo="{InResponseTo}"/>
        </saml:SubjectConfirmation>
    </saml:Subject>
    <saml:Conditions
        NotBefore="{ConditionsNotBefore}"
        NotOnOrAfter="{ConditionsNotOnOrAfter}"
    >
        <saml:AudienceRestriction>
            <saml:Audience>{Audience}</saml:Audience>
        </saml:AudienceRestriction>
    </saml:Conditions>
    <saml:AuthnStatement
        AuthnInstant="{IssueInstant}"
        SessionIndex="{SessionIndex}"
    >
        <saml:AuthnContext>
            <saml:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:Password</saml:AuthnContextClassRef>
        </saml:AuthnContext>
    </saml:AuthnStatement>
    {AttributeStatement}
    </saml:Assertion>
</samlp:Response>`;
