export const SAML_METADATA_XML_TEMPLATE = `<EntityDescriptor
  entityID='{{ issuer   }}'
  xmlns='urn:oasis:names:tc:SAML:2.0:metadata'
>
  <IDPSSODescriptor
    protocolSupportEnumeration='urn:oasis:names:tc:SAML:2.0:protocol'
  >
    <KeyDescriptor use='signing'>
      <KeyInfo xmlns='http://www.w3.org/2000/09/xmldsig#'>
        <X509Data>
          <X509Certificate>
            {{pem}}
          </X509Certificate>
        </X509Data>
      </KeyInfo>
    </KeyDescriptor>
    <SingleLogoutService
      Binding='urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect'
      Location='{{redirectLogoutEndpoint}}'
    />
    <SingleLogoutService
      Binding='urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST'
      Location='{{postLogoutEndpoint}}'
    />
    <NameIDFormat
    >urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress</NameIDFormat>
    <NameIDFormat
    >urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified</NameIDFormat>
    <SingleSignOnService
      Binding='urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect'
      Location='{{redirectEndpoint}}'
    />
    <SingleSignOnService
      Binding='urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST'
      Location='{{postEndpoint}}'
    />
  </IDPSSODescriptor>
</EntityDescriptor>`;
