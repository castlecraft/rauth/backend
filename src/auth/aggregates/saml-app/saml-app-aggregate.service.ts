import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { isEmail } from 'class-validator';
import { randomBytes } from 'crypto';
import { Request, Response } from 'express';
import { handlebars } from 'hbs';
import * as samlify from 'samlify';
import { IdentityProvider } from 'samlify/types/src/entity-idp';
import { ServiceProvider } from 'samlify/types/src/entity-sp';
import { v4 as uuidv4 } from 'uuid';

import {
  APPLICATION_XML,
  CONTENT_TYPE_HEADER,
  EMAIL,
  LOGIN_ROUTE,
  SCOPE_OPENID,
  TWENTY_FOUR_NUMBER,
  USER_PROPERTIES,
} from '../../../constants/app-strings';
import {
  SAML_METADATA_ENDPOINT,
  SAML_LOGIN_ENDPOINT,
  SAML_LOGOUT_ENDPOINT,
} from '../../../constants/url-strings';
import { ServerSettings } from '../../../system-settings/entities/server-settings/server-settings.interface';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { SAMLApp } from '../../entities/saml-app/saml-app.interface';
import { SAMLAppService } from '../../entities/saml-app/saml-app.service';
import { UserClaimService } from '../../entities/user-claim/user-claim.service';
import { SAML_LOGIN_RESPONSE_XML_TEMPLATE } from './templates/saml-login-response';
import { SAML_METADATA_XML_TEMPLATE } from './templates/saml-metadata';
import { CreateSAMLAppDTO } from '../../controllers/saml-app/saml-app-create.dto';
import { SAMLAppAddedEvent } from '../../events/saml-app-added/saml-app-added.event';
import { SAMLAppModifiedEvent } from '../../events/saml-app-modified/saml-app-modified.event';
import { SAMLAppRemovedEvent } from '../../events/saml-app-removed/saml-app-removed.event';
import { PasswordCryptoService } from '../../../auth/policies/password-crypto/password-crypto.service';

@Injectable()
export class SamlAppAggregateService extends AggregateRoot {
  nameIDFormat = samlify.Constants.namespace.format.unspecified;
  nameID: string;
  profile: { [key: string]: unknown };
  sp: ServiceProvider;
  idp: IdentityProvider;
  inResponseTo: string;
  sessionIndex: string;

  constructor(
    private readonly settings: ServerSettingsService,
    private readonly samlApp: SAMLAppService,
    private readonly userClaims: UserClaimService,
    private readonly passwordCrypto: PasswordCryptoService,
  ) {
    super();
  }

  async auth(
    uuid: string,
    req: Request,
    res: Response,
    binding = samlify.Constants.wording.binding.redirect,
  ) {
    const samlApp = await this.getValidSamlApp(uuid);
    const settings = await this.settings.find();
    if (req.user) {
      this.idp = await this.getIdp(samlApp, settings);
      this.sp = this.getSp(samlApp);
      const result = await this.idp.parseLoginRequest(this.sp, binding, req);
      const relayState =
        req.body.RelayState || req.query.RelayState || samlApp.relayState || '';
      await this.setNameIDAndProfile(req, samlApp);
      this.sessionIndex = req.session.id;
      this.inResponseTo = result?.extract?.request?.id;
      const loginResponse = await this.idp.createLoginResponse(
        this.sp,
        result,
        samlify.Constants.wording.binding.post,
        this.profile,
        this.tagReplacement.bind(this),
        samlApp.encryptThenSign,
        relayState,
      );

      if (!loginResponse?.context) {
        throw new BadRequestException({ InvalidContext: uuid });
      }

      const nonce = randomBytes(TWENTY_FOUR_NUMBER).toString('base64');
      res.set('Content-Security-Policy', `default-src 'nonce-${nonce}'`);
      return res.render('saml-form', {
        callbackUrl: this.getAcsUrl(),
        samlResponse: loginResponse?.context,
        relayState,
        nonce,
      });
    }
    const baseRedirectUrl =
      samlApp.customLoginUrl || settings.issuerUrl + LOGIN_ROUTE;
    const redirect =
      baseRedirectUrl +
      '?redirect=' +
      encodeURIComponent(this.getRedirectLocation(settings, samlApp, req));
    return res.redirect(redirect);
  }

  async logout(
    uuid: string,
    req: Request,
    res: Response,
    binding = samlify.Constants.wording.binding.redirect,
  ) {
    const samlApp = await this.getValidSamlApp(uuid);
    const settings = await this.settings.find();
    if (req.user) {
      this.idp = await this.getIdp(samlApp, settings);
      const result = await this.idp.parseLogoutRequest(this.idp, binding, req);
      this.inResponseTo = result?.extract?.request?.id;
      const relayState =
        req.body.RelayState || req.query.RelayState || samlApp.relayState || '';
      await this.setNameIDAndProfile(req, samlApp);
      this.sessionIndex = req.session.id;
      this.inResponseTo = result?.extract?.request?.id;
      const logoutResponse = await this.idp.createLogoutResponse(
        this.idp,
        result,
        samlify.Constants.wording.binding.post,
        relayState,
        this.tagReplacement.bind(this),
      );
      if (!logoutResponse?.context) {
        throw new BadRequestException({ InvalidContext: uuid });
      }

      const nonce = randomBytes(TWENTY_FOUR_NUMBER).toString('base64');
      res.set('Content-Security-Policy', `default-src 'nonce-${nonce}'`);
      return res.render('saml-form', {
        callbackUrl: this.getSloUrl(),
        samlResponse: logoutResponse?.context,
        relayState,
        nonce,
      });
    }
    throw new BadRequestException({ UserAlreadyLoggedOut: uuid });
  }

  getRedirectLocation(
    settings: ServerSettings,
    samlApp: SAMLApp,
    req: Request,
  ) {
    if (req.body.SAMLRequest) {
      const relayState =
        req.body.RelayState || req.query.RelayState || samlApp.relayState || '';
      return (
        settings.issuerUrl +
        req.originalUrl +
        (relayState ? `?RelayState=${relayState}` : '')
      );
    }
    return settings.issuerUrl + req.originalUrl;
  }

  getMetaAttributes(samlApp: SAMLApp) {
    const attributes = [];

    for (const property of USER_PROPERTIES) {
      const nameFormat =
        property.field === EMAIL
          ? samlify.Constants.namespace.format.emailAddress
          : samlify.Constants.namespace.format.unspecified;

      attributes.push({
        name: property.field,
        nameFormat,
        valueTag: `user.${property.mapTo}`,
        valueXsiType: 'xs:string',
      });
    }

    for (const claimType of samlApp.claimTypes) {
      attributes.push({
        name: claimType.name,
        valueTag: claimType.valueTag,
        nameFormat: claimType.nameFormat,
        valueXsiType: claimType.valueXsiType,
      });
    }

    return attributes;
  }

  getSamlify() {
    samlify.setSchemaValidator({ validate: r => Promise.resolve() });
    return samlify;
  }

  async getIdp(samlApp: SAMLApp, settings: ServerSettings) {
    const attributes = this.getMetaAttributes(samlApp);
    return this.getSamlify().IdentityProvider({
      metadata: this.renderMetadata(samlApp, settings),
      privateKey: samlApp.key,
      privateKeyPass: samlApp.privateKeyPass
        ? this.passwordCrypto.decrypt(samlApp.privateKeyPass)
        : undefined,
      loginResponseTemplate: {
        context: SAML_LOGIN_RESPONSE_XML_TEMPLATE,
        attributes,
      },
    });
  }

  async getIdpMetadata(uuid: string, res: Response) {
    const settings = await this.settings.find();
    const samlApp = await this.getValidSamlApp(uuid);
    const idp = await this.getIdp(samlApp, settings);
    return res
      .set(CONTENT_TYPE_HEADER, APPLICATION_XML)
      .send(idp.getMetadata());
  }

  renderMetadata(samlApp: SAMLApp, settings: ServerSettings) {
    const issuer = this.getIssuer(settings, samlApp);
    const pem = samlApp?.cert
      ?.split('\n')
      ?.filter(
        m =>
          ![
            '-----BEGIN CERTIFICATE-----',
            '-----END CERTIFICATE-----',
          ].includes(m),
      )
      ?.join('');

    return this.renderTemplate(SAML_METADATA_XML_TEMPLATE, {
      issuer,
      pem,
      redirectLogoutEndpoint:
        settings.issuerUrl + SAML_LOGOUT_ENDPOINT + samlApp.uuid,
      postLogoutEndpoint:
        settings.issuerUrl + SAML_LOGOUT_ENDPOINT + samlApp.uuid,
      redirectEndpoint: settings.issuerUrl + SAML_LOGIN_ENDPOINT + samlApp.uuid,
      postEndpoint: settings.issuerUrl + SAML_LOGIN_ENDPOINT + samlApp.uuid,
    });
  }

  getIssuer(settings: ServerSettings, samlApp: SAMLApp) {
    return settings.issuerUrl + SAML_METADATA_ENDPOINT + samlApp.uuid;
  }

  getSp(samlApp: SAMLApp) {
    return this.getSamlify().ServiceProvider({
      metadata: samlApp.spMetadataXml,
    });
  }

  async getValidSamlApp(uuid: string) {
    const samlApp = await this.samlApp.findOne({ uuid });
    if (!samlApp) {
      throw new NotFoundException({ SAMLAppNotFound: uuid });
    }
    if (samlApp.disable) {
      throw new BadRequestException({ SAMLAppDisabled: uuid });
    }
    return samlApp;
  }

  renderTemplate(template: string, data: unknown) {
    const renderer = handlebars.compile(template);
    return renderer(data);
  }

  tagReplacement(template: string) {
    const now = new Date();
    const spEntityID = this.sp.entityMeta.getEntityID();
    const idpSetting = this.idp.entitySetting;
    const fiveMinutesLater = new Date(now.getTime());
    fiveMinutesLater.setMinutes(fiveMinutesLater.getMinutes() + 5);
    const acl = this.getAcsUrl();
    const id = uuidv4();
    const tValue = {
      ID: id,
      AssertionID: idpSetting.generateID ? idpSetting.generateID() : uuidv4(),
      Destination: acl,
      Audience: spEntityID,
      SubjectRecipient: acl,
      NameIDFormat: this.nameIDFormat,
      NameID: this.nameID, // set as per claim map
      Issuer: this.idp.entityMeta.getEntityID(),
      IssueInstant: now.toISOString(),
      ConditionsNotBefore: now.toISOString(),
      ConditionsNotOnOrAfter: fiveMinutesLater.toISOString(),
      SubjectConfirmationDataNotOnOrAfter: fiveMinutesLater.toISOString(),
      AssertionConsumerServiceURL: acl,
      EntityID: spEntityID,
      InResponseTo: this.inResponseTo,
      SessionIndex: this.sessionIndex,
      StatusCode: samlify.Constants.namespace.statusCode.success,
      attrUserUuid: this.profile.uuid,
      attrUserDisabled: this.profile.disabled,
      attrUserName: this.profile.name,
      attrUserPhone: this.profile.phone,
      attrUserEmail: this.profile.email,
      attrUserRoles: ((this.profile.roles as string[]) || []).join(','),
      attrUserEnable2Fa: this.profile.enable2Fa || 'false',
      attrUserDeleted: this.profile.deleted || 'false',
      attrUserEnablepasswordless: this.profile.enablePasswordLess,
      attrUserEnableotp: this.profile.enableOtp || 'false',
    };

    // User Claim attribute
    for (const element of Object.keys(this.profile)) {
      const userProperties = USER_PROPERTIES.map(prop => prop.field);
      if (!userProperties.includes(element)) {
        const value =
          typeof this.profile[element] === 'undefined' ||
          this.profile[element] === null
            ? 'false'
            : this.profile[element].toString();
        tValue[`attrUser${this.capitalizeFirstChar(element)}`] = value;
      }
    }

    const context = samlify.SamlLib.replaceTagsByValue(template, tValue);
    // replace tag
    return { id, context };
  }

  async getUserClaims(uuid: string, scope: string) {
    return await this.userClaims.find({ uuid, scope });
  }

  async setNameIDAndProfile(req: Request, samlApp: SAMLApp) {
    this.profile = req.user as unknown as { [key: string]: unknown };
    if (!this.profile.deleted) {
      this.profile.deleted = 'false';
    }
    const userClaims = await this.getUserClaims(
      this.profile.uuid as string,
      samlApp.scope || SCOPE_OPENID,
    );
    for (const claim of userClaims) {
      if (claim.value) {
        this.profile[claim.name] = claim.value;
      }
    }
    this.nameIDFormat = samlify.Constants.namespace.format.unspecified;

    if (this.profile[samlApp.nameIDAttribute] as string) {
      this.nameID = this.profile[samlApp.nameIDAttribute] as string;
    }
    if (!this.nameID) {
      throw new BadRequestException({
        NoDataForNameIDAttribute: samlApp.nameIDAttribute,
      });
    }
    if (isEmail(this.nameID)) {
      this.nameIDFormat = samlify.Constants.namespace.format.emailAddress;
    }
  }

  capitalizeFirstChar(input: string) {
    return input.charAt(0).toUpperCase() + input.toLowerCase().slice(1);
  }

  async addSAMLApp(payload: CreateSAMLAppDTO, createdBy: string) {
    try {
      payload.privateKeyPass = this.passwordCrypto.encrypt(
        payload.privateKeyPass,
      );
      const params = {
        ...payload,
        ...{
          uuid: uuidv4(),
          createdBy,
          creation: new Date(),
        },
      } as SAMLApp;
      this.apply(new SAMLAppAddedEvent(params));
      return params;
    } catch (error) {}
  }

  async modifySAMLApp(payload: CreateSAMLAppDTO, uuid: string) {
    const samlApp = await this.samlApp.findOne({ uuid });
    if (!samlApp) {
      throw new NotFoundException({ SAMLApp: uuid });
    }
    payload.privateKeyPass = this.passwordCrypto.encrypt(
      payload.privateKeyPass,
    );
    Object.assign(samlApp, payload);
    this.apply(new SAMLAppModifiedEvent(samlApp));
    return samlApp;
  }

  async removeSAMLApp(uuid: string, userUuid: string) {
    const samlApp = await this.samlApp.findOne({ uuid });
    if (!samlApp) {
      throw new NotFoundException({ SAMLApp: uuid });
    }
    this.apply(new SAMLAppRemovedEvent(userUuid, samlApp));
  }

  getAcsUrl() {
    return (
      this.sp.entityMeta.getAssertionConsumerService(
        samlify.Constants.wording.binding.redirect,
      ) ||
      this.sp.entityMeta.getAssertionConsumerService(
        samlify.Constants.wording.binding.post,
      )
    );
  }

  getSloUrl() {
    return (
      this.sp.entityMeta.getSingleLogoutService(
        samlify.Constants.wording.binding.redirect,
      ) ||
      this.sp.entityMeta.getSingleLogoutService(
        samlify.Constants.wording.binding.post,
      )
    );
  }
}
