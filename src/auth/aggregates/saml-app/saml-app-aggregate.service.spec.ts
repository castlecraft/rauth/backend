import { Test, TestingModule } from '@nestjs/testing';
import { SamlAppAggregateService } from './saml-app-aggregate.service';

describe('SamlAppAggregateService', () => {
  let service: SamlAppAggregateService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SamlAppAggregateService],
    })
      .overrideProvider(SamlAppAggregateService)
      .useFactory({ factory: () => jest.fn() })
      .compile();
    service = module.get<SamlAppAggregateService>(SamlAppAggregateService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
