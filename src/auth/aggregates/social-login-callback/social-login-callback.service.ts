import { HttpService } from '@nestjs/axios';
import {
  ForbiddenException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { stringify } from 'querystring';
import { forkJoin, from, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';
import { OAuth2TokenRequest } from '../../../auth/controllers/social-login/oauth2-token-request.interface';
import { ACCOUNTS_ROUTE, EMAIL } from '../../../constants/app-strings';
import { SOCIAL_LOGIN_CALLBACK_ENDPOINT } from '../../../constants/url-strings';
import { i18n } from '../../../i18n/i18n.config';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { UpdateOpenIDClaimsCommand } from '../../../user-management/commands/update-openid-claims/update-openid-claims.command';
import { User } from '../../../user-management/entities/user/user.interface';
import { UserService } from '../../../user-management/entities/user/user.service';
import { OpenIDClaimsDTO } from '../../../user-management/policies/openid-claims/openid-claims-dto';
import { SignUpSocialLoginUserCommand } from '../../commands/sign-up-social-login-user/sign-up-social-login-user.command';
import { UpdateUserClaimsCommand } from '../../commands/update-user-claims/update-user-claims.command';
import { SocialLogin } from '../../entities/social-login/social-login.interface';
import { SocialLoginService } from '../../entities/social-login/social-login.service';
import { IDTokenClaims } from '../../middlewares/interfaces';
import { PasswordCryptoService } from '../../../auth/policies/password-crypto/password-crypto.service';

@Injectable()
export class SocialLoginCallbackService {
  constructor(
    private readonly userService: UserService,
    private readonly socialLoginService: SocialLoginService,
    private readonly settingsService: ServerSettingsService,
    private readonly http: HttpService,
    private readonly commandBus: CommandBus,
    private readonly passwordCryptoService: PasswordCryptoService,
  ) {}

  requestTokenAndProfile(
    code: string,
    state: string,
    socialLogin: string,
    redirect: string,
    storedState: string,
    done: (...args) => any,
  ) {
    return forkJoin({
      socialLoginObject: from(
        this.socialLoginService.findOne({ uuid: socialLogin }),
      ),
      settings: from(this.settingsService.find()),
    }).pipe(
      switchMap(({ socialLoginObject, settings }) => {
        if (!socialLoginObject) {
          return done(new UnauthorizedException());
        }

        if (socialLoginObject.disabled) {
          return done(new UnauthorizedException());
        }

        if (!redirect) {
          redirect = settings.issuerUrl + ACCOUNTS_ROUTE;
        }

        // Create state with redirect Uri and
        // unique identifier and store it in request.
        const uuid = uuidv4();
        const stateData = JSON.stringify({ redirect, uuid });
        const builtState = Buffer.from(stateData).toString('base64');

        const separator = socialLoginObject.authorizationURL?.includes('?')
          ? '&'
          : '?';

        const redirectURI =
          settings.issuerUrl +
          SOCIAL_LOGIN_CALLBACK_ENDPOINT +
          socialLoginObject.uuid;

        const confirmationURL =
          socialLoginObject.authorizationURL +
          separator +
          'client_id=' +
          socialLoginObject.clientId +
          '&response_type=code' +
          '&state=' +
          builtState +
          '&scope=' +
          socialLoginObject.scope.join('%20') +
          '&redirect_uri=' +
          encodeURIComponent(redirectURI);

        if (!code) {
          // Redirect to confirmationURL
          return done(null, null, confirmationURL, {
            state: builtState,
          });
        }

        // Check incoming state and stored state
        if (!state || !storedState) {
          return done(new ForbiddenException());
        }

        const parsedStoredState = JSON.parse(
          Buffer.from(storedState, 'base64').toString(),
        );

        const parsedState = JSON.parse(Buffer.from(state, 'base64').toString());

        if (parsedStoredState.uuid !== parsedState.uuid) {
          return done(new ForbiddenException());
        }

        // Create payload for token request
        const payload: OAuth2TokenRequest = {
          code,
          grant_type: 'authorization_code',
          redirect_uri: redirectURI,
          client_id: socialLoginObject.clientId,
          scope: socialLoginObject.scope.join(' '),
        };

        // add client_secret to payload if specified
        if (socialLoginObject.clientSecretToTokenEndpoint) {
          payload.client_secret = this.passwordCryptoService.decrypt(
            socialLoginObject.clientSecret,
          );
        }

        return this.http
          .post(socialLoginObject.tokenURL, stringify(payload), {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
            },
          })
          .pipe(
            switchMap((tokenResponse: { data: { access_token: string } }) => {
              const token = tokenResponse.data;
              // TODO: OIDC and read id_token

              // Check Profile Endpoint
              return this.http.get(socialLoginObject.profileURL, {
                headers: {
                  authorization: 'Bearer ' + token.access_token,
                },
              });
            }),
            switchMap((profileResponse: { data: unknown }) => {
              const profile = profileResponse?.data;
              const emailAttribute = socialLoginObject.emailAttribute || EMAIL;
              if (!profile) {
                return done(new ForbiddenException(i18n.__('Invalid User')));
              }
              if (!profile[emailAttribute]) {
                return done(new ForbiddenException(i18n.__('Invalid User')));
              }
              // Check Profile and set user.
              // TODO: Store Upstream sub claim on local server
              return forkJoin({
                user: from(
                  this.userService.findOne({
                    email: profile[emailAttribute],
                  }),
                ),
                profile: of(profile),
              });
            }),
            switchMap(({ user, profile }) => {
              if (!user) {
                if (socialLoginObject.disableUserCreation) {
                  return done(
                    new ForbiddenException(i18n.__('User Creation Disabled')),
                  );
                }
                return from(
                  this.signUpSocialLoginUser(profile, socialLoginObject),
                );
              }

              if (user.disabled) {
                return done(new ForbiddenException(i18n.__('User Disabled')));
              }
              this.updateUserClaims(profile, user, socialLoginObject);
              return of(user);
            }),
          );
      }),
    );
  }

  async signUpSocialLoginUser(
    profile: IDTokenClaims,
    socialLogin: SocialLogin,
  ) {
    return await this.commandBus.execute(
      new SignUpSocialLoginUserCommand(profile, socialLogin),
    );
  }

  async updateUserClaims(
    profile: IDTokenClaims,
    user: User,
    socialLogin: SocialLogin,
  ) {
    return await this.commandBus.execute(
      new UpdateUserClaimsCommand(profile, user, socialLogin),
    );
  }

  async updateOpenIDClaims(userUuid: string, claims: OpenIDClaimsDTO) {
    return await this.commandBus.execute(
      new UpdateOpenIDClaimsCommand(userUuid, claims),
    );
  }
}
