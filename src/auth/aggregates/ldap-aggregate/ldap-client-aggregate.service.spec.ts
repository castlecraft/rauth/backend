import { Test, TestingModule } from '@nestjs/testing';
import { OnlyAllowValidScopeService } from '../../../client-management/policies';
import { UserManagementService } from '../../../user-management/aggregates/user-management/user-management.service';
import { AuthService } from '../../controllers/auth/auth.service';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';
import { LDAPClientService } from '../../entities/ldap-client/ldap-client.service';
import { UserClaimService } from '../../entities/user-claim/user-claim.service';
import { OTPAggregateService } from '../otp-aggregate/otp-aggregate.service';
import { LDAPClientAggregateService } from './ldap-client-aggregate.service';
import { ConfigService } from '../../../config/config.service';
import { AuthDataService } from '../../../user-management/entities/auth-data/auth-data.service';
import { PasswordCryptoService } from '../../../auth/policies/password-crypto/password-crypto.service';

describe('LDAPAggregateService', () => {
  let service: LDAPClientAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        LDAPClientAggregateService,
        { provide: UserManagementService, useValue: {} },
        { provide: LDAPClientService, useValue: {} },
        { provide: KerberosRealmService, useValue: {} },
        { provide: OnlyAllowValidScopeService, useValue: {} },
        { provide: AuthService, useValue: {} },
        { provide: OTPAggregateService, useValue: {} },
        { provide: UserClaimService, useValue: {} },
        { provide: AuthDataService, useValue: {} },
        { provide: PasswordCryptoService, useValue: {} },
        {
          provide: ConfigService,
          useValue: {
            get: (key: string) => key,
          },
        },
      ],
    }).compile();

    service = module.get<LDAPClientAggregateService>(
      LDAPClientAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
