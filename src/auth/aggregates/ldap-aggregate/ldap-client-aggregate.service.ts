import {
  BadGatewayException,
  BadRequestException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { isEmail } from 'class-validator';
import { Client, ClientOptions, Entry, SearchResult } from 'ldapts';
import { v4 as uuidv4 } from 'uuid';

import { UserClaim } from '../../../auth/entities/user-claim/user-claim.interface';
import { PasswordCryptoService } from '../../../auth/policies/password-crypto/password-crypto.service';
import { OnlyAllowValidScopeService } from '../../../client-management/policies';
import { isMobileE164 } from '../../../common/decorators/is-mobile-e164.decorator';
import {
  AdminPasswordException,
  InvalidUserException,
} from '../../../common/filters/exceptions';
import { RAuthLogger } from '../../../common/services/logger/r-auth-logger';
import { ConfigService } from '../../../config/config.service';
import {
  LDAP,
  LOGIN,
  OTP_ENABLED,
  RAUTH_SOURCE_ID,
  RAUTH_SOURCE_TYPE,
  SCOPE_OPENID,
  SCOPE_SELF_SERVICE,
  SUCCESS,
  USER_ADDED,
  USER_CLAIMS,
  VERIFY_USER,
} from '../../../constants/app-strings';
import { i18n } from '../../../i18n/i18n.config';
import {
  SystemLogCode,
  SystemLoginType,
} from '../../../system-settings/entities/system-log/system-log.interface';
import { UserManagementService } from '../../../user-management/aggregates/user-management/user-management.service';
import { AuthDataType } from '../../../user-management/entities/auth-data/auth-data.interface';
import { AuthDataService } from '../../../user-management/entities/auth-data/auth-data.service';
import {
  User,
  UserStatus,
} from '../../../user-management/entities/user/user.interface';
import { USER } from '../../../user-management/entities/user/user.schema';
import { UserAccountAddedEvent } from '../../../user-management/events/user-account-added/user-account-added.event';
import { UserAccountModifiedEvent } from '../../../user-management/events/user-account-modified/user-account-modified.event';
import { AuthService } from '../../controllers/auth/auth.service';
import { CreateLDAPClientDto } from '../../controllers/ldap-client/ldap-client-create.dto';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';
import { LDAPClient } from '../../entities/ldap-client/ldap-client.interface';
import { LDAPClientService } from '../../entities/ldap-client/ldap-client.service';
import { UserClaimService } from '../../entities/user-claim/user-claim.service';
import { LDAPClientAddedEvent } from '../../events/ldap-client-added/ldap-client-added.event';
import { LDAPClientModifiedEvent } from '../../events/ldap-client-modified/ldap-client-modified.event';
import { LDAPClientRemovedEvent } from '../../events/ldap-client-removed/ldap-client-removed.event';
import { LDAPUserVerifiedEvent } from '../../events/ldap-user-verified/ldap-user-verified.event';
import { UserClaimsAddedEvent } from '../../events/user-claims-added/user-claims-added.event';
import { UserLogInHOTPGeneratedEvent } from '../../events/user-login-hotp-generated/user-login-hotp-generated.event';
import { OTPAggregateService } from '../otp-aggregate/otp-aggregate.service';

export interface LDAPUser {
  dn: string;
  [key: string]: string | string[] | undefined;
}

export interface SearchResultEntry {
  type: string;
  objectName: string;
  attributes: {
    type: string;
    values: string[];
  }[];
}

export interface AuthenticationOptions {
  ldapOpts: ClientOptions;
  userDn?: string;
  adminDn?: string;
  adminPassword?: string;
  userSearchBase?: string;
  usernameAttribute?: string;
  username?: string;
  verifyUserExists?: boolean;
  startTLS?: boolean;
  groupsSearchBase?: string;
  groupClass?: string;
  groupMemberAttribute?: string;
  groupMemberUserAttribute?: string;
  userPassword?: string;
  attributes?: string[];
}

@Injectable()
export class LDAPClientAggregateService extends AggregateRoot {
  private readonly logger = new RAuthLogger(this.config);
  constructor(
    private readonly userClaim: UserClaimService,
    private readonly userManager: UserManagementService,
    private readonly ldap: LDAPClientService,
    private readonly kerberosRealm: KerberosRealmService,
    private readonly scopePolicy: OnlyAllowValidScopeService,
    private readonly authService: AuthService,
    private readonly otp: OTPAggregateService,
    private readonly config: ConfigService,
    private readonly authData: AuthDataService,
    private readonly passwordCryptoService: PasswordCryptoService,
  ) {
    super();
  }

  searchResultToUser(result: SearchResult) {
    if (!result.searchEntries.length) {
      return;
    }
    return result.searchEntries[0];
  }

  async ldapBind(
    dn: string,
    password: string,
    startTLS: boolean,
    ldapOpts: ClientOptions,
  ): Promise<Client> {
    ldapOpts.connectTimeout = ldapOpts.connectTimeout || 5000;
    const client = new Client(ldapOpts);
    try {
      if (startTLS) {
        await client.startTLS(ldapOpts.tlsOptions);
      }
    } catch (error) {
      await client.unbind();
      throw new BadGatewayException(error);
    }
    try {
      await client.bind(dn, password);
    } catch (error) {
      await client.unbind();
      throw new UnauthorizedException(error);
    }
    return client;
  }

  async searchUser(
    ldapClient: Client,
    baseDN: string,
    usernameAttribute: string,
    username: string,
    attributes: string[] = [],
  ) {
    try {
      const result = await ldapClient.search(baseDN, {
        filter: `(${usernameAttribute}=${username})`,
        scope: 'sub',
        attributes,
      });
      return this.searchResultToUser(result);
    } catch (error) {
      await ldapClient.unbind();
      throw new UnauthorizedException(error);
    }
  }

  async searchUserGroups(
    ldapClient: Client,
    searchBase: string,
    user: Entry,
    groupClass: string,
    groupMemberAttribute: string = 'member',
    groupMemberUserAttribute: string = 'dn',
  ) {
    try {
      const result = await ldapClient.search(searchBase, {
        filter: `(&(objectclass=${groupClass})(${groupMemberAttribute}=${user[groupMemberUserAttribute]}))`,
        scope: 'sub',
      });
      return this.searchResultToUser(result);
    } catch (error) {
      await ldapClient.unbind();
      throw new UnauthorizedException(error);
    }
  }

  async authenticateWithAdmin(
    adminDn: string,
    adminPassword: string,
    userSearchBase: string,
    usernameAttribute: string,
    username: string,
    userPassword: string,
    startTLS: boolean,
    ldapOpts: ClientOptions,
    groupsSearchBase: string,
    groupClass: string,
    groupMemberAttribute: string = 'member',
    groupMemberUserAttribute: string = 'dn',
    attributes: string[] = [],
  ) {
    // Bind as the admin to perform user search
    const ldapAdminClient = await this.ldapBind(
      adminDn,
      adminPassword,
      startTLS,
      ldapOpts,
    );

    // Search for the user
    const user: Entry & { groups?: Entry } = await this.searchUser(
      ldapAdminClient,
      userSearchBase,
      usernameAttribute,
      username,
      attributes,
    );

    // Unbind the admin client
    ldapAdminClient.unbind();

    // Check if user was found
    if (!user || !user.dn) {
      this.logger.error(
        `Admin did not find user (${usernameAttribute}=${username})`,
        this.constructor.name,
      );
      throw new UnauthorizedException(
        'User not found or username attribute is wrong',
      );
    }

    // Bind as the user to validate credentials
    const userDn = user.dn;
    const ldapUserClient = await this.ldapBind(
      userDn,
      userPassword,
      startTLS,
      ldapOpts,
    );
    await ldapUserClient.unbind();

    // Optionally search for user groups
    if (groupsSearchBase && groupClass && groupMemberAttribute) {
      // Re-bind as admin to fetch user groups
      const ldapAdminClient = await this.ldapBind(
        adminDn,
        adminPassword,
        startTLS,
        ldapOpts,
      );

      const groups = await this.searchUserGroups(
        ldapAdminClient,
        groupsSearchBase,
        user,
        groupClass,
        groupMemberAttribute,
        groupMemberUserAttribute,
      );

      // Assign groups to the user object
      user.groups = groups;
      await ldapAdminClient.unbind();
    }

    return user;
  }

  async authenticateWithUser(
    userDn: string,
    userSearchBase: string,
    usernameAttribute: string,
    username: string,
    userPassword: string,
    startTLS: boolean,
    ldapOpts: ClientOptions,
    groupsSearchBase: string,
    groupClass: string,
    groupMemberAttribute: string = 'member',
    groupMemberUserAttribute: string = 'dn',
    attributes: string[] = [],
  ) {
    const ldapUserClient = await this.ldapBind(
      userDn,
      userPassword,
      startTLS,
      ldapOpts,
    );
    if (!usernameAttribute || !userSearchBase) {
      // if usernameAttribute is not provided, no user detail is needed.
      await ldapUserClient.unbind();
      return true;
    }
    const user: Entry & { groups?: Entry } = await this.searchUser(
      ldapUserClient,
      userSearchBase,
      usernameAttribute,
      username,
      attributes,
    );
    if (!user || !user.dn) {
      await ldapUserClient.unbind();
      this.logger.error(
        `User logged in, but user details could not be found. (${usernameAttribute}=${username}). Probably wrong attribute or searchBase?`,
        this.constructor.name,
      );
      throw new UnauthorizedException(
        'user logged in, but user details could not be found. Probably usernameAttribute or userSearchBase is wrong?',
      );
    }
    if (groupsSearchBase && groupClass && groupMemberAttribute) {
      const ldapUserClient = await this.ldapBind(
        userDn,
        userPassword,
        startTLS,
        ldapOpts,
      );
      const groups = await this.searchUserGroups(
        ldapUserClient,
        groupsSearchBase,
        user,
        groupClass,
        groupMemberAttribute,
        groupMemberUserAttribute,
      );
      user.groups = groups;
      await ldapUserClient.unbind();
    }
    return user;
  }

  async verifyUserExists(
    adminDn: string,
    adminPassword: string,
    userSearchBase: string,
    usernameAttribute: string,
    username: string,
    startTLS: boolean,
    ldapOpts: ClientOptions,
    groupsSearchBase: string,
    groupClass: string,
    groupMemberAttribute: string = 'member',
    groupMemberUserAttribute: string = 'dn',
    attributes: string[] = [],
  ) {
    const ldapAdminClient = await this.ldapBind(
      adminDn,
      adminPassword,
      startTLS,
      ldapOpts,
    );
    const user: Entry & { groups?: Entry } = await this.searchUser(
      ldapAdminClient,
      userSearchBase,
      usernameAttribute,
      username,
      attributes,
    );
    ldapAdminClient.unbind();
    if (!user || !user.dn) {
      this.logger.error(
        `admin did not find user! (${usernameAttribute}=${username})`,
        this.constructor.name,
      );
      throw new UnauthorizedException(
        'user not found or username attribute is wrong',
      );
    }
    if (groupsSearchBase && groupClass && groupMemberAttribute) {
      const ldapAdminClient = await this.ldapBind(
        adminDn,
        adminPassword,
        startTLS,
        ldapOpts,
      );
      const groups = await this.searchUserGroups(
        ldapAdminClient,
        groupsSearchBase,
        user,
        groupClass,
        groupMemberAttribute,
        groupMemberUserAttribute,
      );
      user.groups = groups;
      await ldapAdminClient.unbind();
    }
    return user;
  }

  async authenticate(options: AuthenticationOptions) {
    if (options.verifyUserExists) {
      return await this.verifyUserExists(
        options.adminDn,
        options.adminPassword,
        options.userSearchBase,
        options.usernameAttribute,
        options.username,
        options.startTLS,
        options.ldapOpts,
        options.groupsSearchBase,
        options.groupClass,
        options.groupMemberAttribute,
        options.groupMemberUserAttribute,
        options.attributes,
      );
    }
    if (!options.userPassword) {
      throw new UnauthorizedException('userPassword must be provided');
    }
    if (options.adminDn) {
      return await this.authenticateWithAdmin(
        options.adminDn,
        options.adminPassword,
        options.userSearchBase,
        options.usernameAttribute,
        options.username,
        options.userPassword,
        options.startTLS,
        options.ldapOpts,
        options.groupsSearchBase,
        options.groupClass,
        options.groupMemberAttribute,
        options.groupMemberUserAttribute,
        options.attributes,
      );
    }
    if (!options.userDn) {
      throw new UnauthorizedException(
        'adminDn/adminPassword OR userDn must be provided',
      );
    }
    return await this.authenticateWithUser(
      options.userDn,
      options.userSearchBase,
      options.usernameAttribute,
      options.username,
      options.userPassword,
      options.startTLS,
      options.ldapOpts,
      options.groupsSearchBase,
      options.groupClass,
      options.groupMemberAttribute,
      options.groupMemberUserAttribute,
      options.attributes,
    );
  }

  async getValidClient(uuid: string) {
    const client = await this.ldap.findOne({ uuid });

    if (!client) {
      throw new BadRequestException({ InvalidLDAPClient: uuid });
    }

    if (client.disabled) {
      throw new BadRequestException({ InvalidLDAPClient: uuid });
    }

    return client;
  }

  async loginViaLDAP(
    ldapClient: string,
    username: string,
    userPassword: string,
    code?: string,
  ) {
    const client = await this.getValidClient(ldapClient);
    const verifiedLdapUser = (await this.verifyUser(
      client,
      username,
    )) as LDAPUser;
    const user = await this.getLocalUser(verifiedLdapUser, client);

    if (!user) {
      throw new InvalidUserException();
    }
    if (user.disabled) {
      throw new UnauthorizedException(i18n.__('User Disabled'));
    }

    let failedAttempts: number;
    if (!code) {
      failedAttempts = await this.authService.validateLoginAttempts(
        user,
        client.allowedFailedLoginAttempts,
      );
    }

    try {
      const ldapUser = (await this.verifyUser(
        client,
        username,
        userPassword,
      )) as LDAPUser;

      const localUser = await this.getLocalUser(ldapUser, client);

      if (!localUser) {
        return this.createUser(ldapUser, client);
      }

      await this.loginWith2Fa(localUser, code);

      if (failedAttempts) {
        await this.authService.clearAuthFailLogs(localUser.uuid);
      }

      if (localUser.status === UserStatus.Unverified) {
        localUser.status = UserStatus.Verified;
        this.apply(new UserAccountModifiedEvent(localUser));
        this.logger.log(`${LOGIN}- ${localUser.uuid}`, this.constructor.name);
      }

      await this.authService.saveAuthLog(
        user.uuid,
        SystemLogCode.Success,
        { loginType: SystemLoginType.LDAP },
        SUCCESS,
        false,
      );

      return localUser;
    } catch (error) {
      const err = new UnauthorizedException(error);
      this.logger.error(err, err, this.constructor.name);
      await this.authService.saveAuthLog(
        user.uuid,
        SystemLogCode.Failure,
        { loginType: SystemLoginType.LDAP },
        error.message,
      );
      throw err;
    }
  }

  async verifyUser(
    client: LDAPClient,
    username: string,
    userPassword?: string,
  ) {
    const attributes = this.setAttributes(client);
    const ldapOpts: ClientOptions = {
      url: client.url,
      connectTimeout: client.timeoutMs,
    };

    if (client.disableUserCreation) {
      throw new UnauthorizedException(i18n.__('User creation is disabled'));
    }

    if (client.isSecure) {
      ldapOpts.tlsOptions = {
        cert: client.cert,
        key: this.passwordCryptoService.decrypt(client.key),
        ca: client.ca,
        passphrase: client.keyPassphrase
          ? this.passwordCryptoService.decrypt(client.keyPassphrase)
          : undefined,
      };
    }

    return await this.authenticate({
      username,
      ldapOpts,
      adminDn: client.adminDn,
      adminPassword: this.passwordCryptoService.decrypt(client.adminPassword),
      userSearchBase: client.userSearchBase,
      usernameAttribute: client.usernameAttribute,
      userPassword,
      attributes,
      verifyUserExists: userPassword ? false : true,
    });
  }

  async verifyViaLDAP(
    ldapClient: string,
    username: string,
    password?: string,
    shouldSendOtp = true,
  ) {
    const client = await this.getValidClient(ldapClient);
    const ldapUser = (await this.verifyUser(client, username)) as LDAPUser;

    if (!ldapUser) {
      throw new InvalidUserException();
    }

    const localUser = await this.getLocalUser(ldapUser, client);

    if (!localUser) {
      const isUnverified = true;
      const user = this.createUser(ldapUser, client, isUnverified);
      this.logger.log(
        `${VERIFY_USER}- ${user.uuid}`,
        USER_ADDED,
        this.constructor.name,
      );
      return this.emitLDAPUserVerifiedEvent(client, ldapUser, user);
    }

    if (localUser && password) {
      try {
        await this.authService.validateLoginAttempts(
          localUser,
          client.allowedFailedLoginAttempts,
        );
        await this.verifyUser(client, username, password);
        if (shouldSendOtp) {
          await this.sendOTP(localUser);
        }
        this.updateUserClaims(client, localUser, ldapUser);
      } catch (error) {
        const err = new UnauthorizedException(error);
        this.logger.error(err, err, this.constructor.name);
        await this.authService.saveAuthLog(
          localUser.uuid,
          SystemLogCode.Failure,
          {},
          error.message,
        );
        throw err;
      }
    }

    return this.emitLDAPUserVerifiedEvent(client, ldapUser, localUser);
  }

  emitLDAPUserVerifiedEvent(
    client: LDAPClient,
    ldapUser: LDAPUser,
    user: User,
  ) {
    client.adminPassword = undefined;
    this.apply(new LDAPUserVerifiedEvent(ldapUser, client));
    this.logger.log(`${VERIFY_USER}- ${user.uuid}`, this.constructor.name);
    return user;
  }

  async updateUserClaims(
    client: LDAPClient,
    localUser: User,
    ldapUser: LDAPUser,
  ) {
    const getDbClaim = await this.userClaim.find({ uuid: localUser.uuid });
    const claims = this.getClaimsToBeAdded(
      { uuid: localUser.uuid } as User,
      client,
      ldapUser,
    );

    const updateClaims = [];
    for (const claim of claims) {
      const matchingAttribute = getDbClaim.find(
        attribute => attribute.name === claim.name,
      );
      if (matchingAttribute) {
        if (matchingAttribute.value !== claim.value) {
          updateClaims.push(claim);
        }
      } else {
        updateClaims.push(claim);
      }
    }
    this.apply(new UserClaimsAddedEvent(updateClaims));
  }

  createUser(
    ldapUser: LDAPUser,
    client: LDAPClient,
    isUnverified: boolean = false,
  ) {
    const user = this.authService.userModel({
      email: ldapUser[client.emailAttribute],
      phone: ldapUser[client.phoneAttribute],
      name: ldapUser[client.fullNameAttribute],
      isEmailVerified: true,
      uuid: uuidv4(),
    } as User);
    user.email = user.email?.trim().toLowerCase();

    if (isUnverified) {
      user.status = UserStatus.Unverified;
    }
    this.apply(new UserAccountAddedEvent(user));
    this.logger.log(`${USER_ADDED} - ${user.uuid}`, this.constructor.name);
    const claims = this.getClaimsToBeAdded(user, client, ldapUser);
    if (claims.length > 0) {
      this.apply(new UserClaimsAddedEvent(claims));
    }
    const rauthSourceTypeClaim = claims.find(c => c.name === RAUTH_SOURCE_TYPE);
    user.source_type = rauthSourceTypeClaim
      ? (rauthSourceTypeClaim.value as string)
      : undefined;

    const rauthSourceIdClaim = claims.find(c => c.name === RAUTH_SOURCE_ID);
    user.source_id = rauthSourceIdClaim
      ? (rauthSourceIdClaim.value as string)
      : undefined;
    this.logger.log(
      `${USER_CLAIMS}-${rauthSourceTypeClaim} ${rauthSourceIdClaim}`,
      this.constructor.name,
    );

    return user;
  }

  async getLocalUser(ldapUser: LDAPUser, client: LDAPClient) {
    const email = ldapUser[client.emailAttribute] as string;
    if (email && !isEmail(email?.trim().toLowerCase())) {
      throw new UnauthorizedException(`Invalid Email: ${email}`);
    }
    const phone = ldapUser[client.phoneAttribute] as string;
    if (!client.skipMobileVerification && phone && !isMobileE164(phone)) {
      throw new UnauthorizedException(`Invalid Phone: ${phone}`);
    }
    if (!phone && !email) {
      throw new UnauthorizedException(
        'email and phone attributes missing from AD user',
      );
    }
    const userEmail = await this.userManager.findByEmail(
      email?.trim().toLowerCase(),
    );
    const userPhone = await this.userManager.findByPhone(phone as string);

    if (userEmail && userPhone) {
      // Found phone and email locally

      if (userEmail.uuid === userPhone.uuid) {
        // phone and email belong to same user

        return userEmail;
      }

      if (userEmail.uuid !== userPhone.uuid) {
        // phone and email do not belong to same user
        // set priority to email
        // (TODO: can check from `client: LDAPClient`)

        return userEmail;
      }
    } else if (userEmail) {
      // Select email if only email found

      return userEmail;
    } else if (userPhone) {
      // Select phone if only phone found

      return userPhone;
    }
  }

  async removeLDAPClient(uuid: string, userUuid: string) {
    const ldapClient = await this.ldap.findOne({ uuid });
    if (!ldapClient) {
      throw new NotFoundException({ LDAPClientNotFound: uuid });
    }
    const kerberosRealm = await this.kerberosRealm.findOne({
      ldapClient: ldapClient.uuid,
    });
    if (kerberosRealm) {
      throw new BadRequestException({
        KerberosRealm: {
          uuid: kerberosRealm.uuid,
          ldapClient: kerberosRealm.ldapClient,
        },
      });
    }
    this.apply(new LDAPClientRemovedEvent(userUuid, ldapClient));
  }

  async addLDAPClient(payload: CreateLDAPClientDto, createdBy: string) {
    if (!payload.adminPassword) {
      throw new AdminPasswordException();
    }
    payload.adminPassword = this.passwordCryptoService.encrypt(
      payload.adminPassword,
    );
    if (
      payload.isSecure &&
      (!payload.cert || !payload.key || !payload.ca?.length)
    ) {
      throw new BadRequestException(
        'Cert and Key are required for secure LDAP',
      );
    }
    if (payload.key) {
      payload.key = this.passwordCryptoService.encrypt(payload.key);
    }
    if (payload.keyPassphrase) {
      payload.keyPassphrase = this.passwordCryptoService.encrypt(
        payload.keyPassphrase,
      );
    }
    if (payload.scope) {
      await this.scopePolicy.validate([payload.scope]);
    }
    const ldapClient = {
      ...payload,
      ...{
        uuid: uuidv4(),
        createdBy,
        creation: new Date(),
      },
    } as LDAPClient;
    this.apply(new LDAPClientAddedEvent(ldapClient));
    return ldapClient;
  }

  async modifyLDAPClient(
    payload: CreateLDAPClientDto,
    uuid: string,
    modifiedBy: string,
  ) {
    const ldapClient = await this.ldap.findOne({ uuid });
    if (!ldapClient) {
      throw new NotFoundException({ LDAPClient: uuid });
    }
    if (payload.adminPassword) {
      payload.adminPassword = this.passwordCryptoService.encrypt(
        payload.adminPassword,
      );
    }
    if (
      payload.isSecure &&
      !payload.cert &&
      (!payload.key || !ldapClient.key)
    ) {
      throw new BadRequestException(
        'Cert and Key are required for secure LDAP',
      );
    }
    if (payload.key) {
      payload.key = this.passwordCryptoService.encrypt(payload.key);
    }
    if (!payload.key) {
      delete payload.key;
    }
    if (payload.keyPassphrase) {
      payload.keyPassphrase = this.passwordCryptoService.encrypt(
        payload.keyPassphrase,
      );
    }
    if (!payload.keyPassphrase) {
      delete payload.keyPassphrase;
    }
    if (payload.scope) {
      await this.scopePolicy.validate([payload.scope]);
    }
    Object.assign(ldapClient, payload);
    ldapClient.modified = new Date();
    ldapClient.modifiedBy = modifiedBy;
    this.apply(new LDAPClientModifiedEvent(ldapClient));
    return ldapClient;
  }

  async findOne(params) {
    return await this.ldap.findOne(params);
  }

  getClaimsToBeAdded(user: User, client: LDAPClient, ldapUser: LDAPUser) {
    const claims = [
      {
        scope: SCOPE_SELF_SERVICE,
        name: RAUTH_SOURCE_TYPE,
        uuid: user.uuid,
        value: LDAP,
      } as UserClaim,
      {
        scope: SCOPE_SELF_SERVICE,
        name: RAUTH_SOURCE_ID,
        uuid: user.uuid,
        value: client.uuid,
      } as UserClaim,
    ];
    for (const attribute of client.attributes) {
      if (ldapUser[attribute.claim]) {
        claims.push({
          scope: client.scope || SCOPE_OPENID,
          name: attribute.mapTo,
          uuid: user.uuid,
          value: ldapUser[attribute.claim],
        } as UserClaim);
      }
    }
    return claims;
  }

  async loginWith2Fa(user: User, code?: string) {
    if (user.enable2fa || user.enableOTP) {
      this.logger.log(`${OTP_ENABLED}`, this.constructor.name);
      await this.authService.validate2FaCode(user, code, SystemLoginType.LDAP);
      await this.authService.removeLoginHOTP(user);
    }
  }

  sanitizeUser(user: User) {
    return this.authService.sanitizeUser(user);
  }

  setAttributes(client: LDAPClient) {
    const attributes = [
      client.emailAttribute,
      client.phoneAttribute,
      client.fullNameAttribute,
    ];
    if (client.attributes?.length) {
      for (const attr of client.attributes) {
        attributes.push(attr.claim);
      }
    }
    return attributes;
  }

  async sendOTP(localUser: User) {
    const hotp = await this.otp.generateHOTP(localUser);

    const otp = await this.getUserOTP(localUser);

    if (
      otp?.entityUuid === localUser.uuid &&
      otp?.metaData?.resendOTPAfterSec === 0
    ) {
      this.otp.sendEmail(localUser, hotp);
      this.apply(new UserLogInHOTPGeneratedEvent(localUser, hotp));
    }
  }

  getUserOTP(user: User) {
    return this.authData.findOne({
      entity: USER,
      entityUuid: user?.uuid,
      authDataType: AuthDataType.LoginOTP,
    });
  }
}
