import { Injectable } from '@nestjs/common';
import cron from 'node-cron';
import { AuthorizationCodeService } from '../../entities/authorization-code/authorization-code.service';

@Injectable()
export class AuthCodeSchedulerService {
  constructor(private readonly authCode: AuthorizationCodeService) {}

  start() {
    // Every 15 minutes
    cron.schedule('*/15 * * * *', async () => {
      await this.authCode.deleteMany({ deleteAfter: { $lt: new Date() } });
    });
  }
}
