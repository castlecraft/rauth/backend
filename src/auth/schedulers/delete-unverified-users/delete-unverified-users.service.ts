import { Injectable } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import cron from 'node-cron';
import { RemoveUnverifiedUserCommand } from '../../../user-management/commands/remove-unverified-user/remove-unverified-user.command';
import { UserStatus } from '../../../user-management/entities/user/user.interface';
import { UserService } from '../../../user-management/entities/user/user.service';

export const TWENTY_FOUR_HOURS_MS = 24 * 60 * 60 * 1000;

@Injectable()
export class DeleteUnverifiedUsersService {
  constructor(
    private readonly user: UserService,
    private readonly commandBus: CommandBus,
  ) {}

  start() {
    // Every hour
    cron.schedule('0 * * * *', async () => {
      // Delete unverified emails created 24 hours ago, user must also be disabled.
      const users = await this.user.find({
        status: UserStatus.Unverified,
        creation: { $lt: new Date(Date.now() - TWENTY_FOUR_HOURS_MS) },
      });

      for (const user of users) {
        await this.commandBus.execute(new RemoveUnverifiedUserCommand(user));
      }
    });
  }
}
