import { CqrsModule } from '@nestjs/cqrs';
import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from '../../../user-management/entities/user/user.service';
import { DeleteUnverifiedUsersService } from './delete-unverified-users.service';

describe('DeleteUnverifiedEmailsService', () => {
  let service: DeleteUnverifiedUsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        DeleteUnverifiedUsersService,
        { provide: UserService, useValue: {} },
      ],
    }).compile();

    service = module.get<DeleteUnverifiedUsersService>(
      DeleteUnverifiedUsersService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
