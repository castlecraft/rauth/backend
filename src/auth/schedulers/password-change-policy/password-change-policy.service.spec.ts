import { CqrsModule } from '@nestjs/cqrs';
import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from '../../../user-management/entities/user/user.service';
import { PasswordChangePolicyService } from './password-change-policy.service';
import { UserClaimService } from '../../../auth/entities/user-claim/user-claim.service';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { AuthDataService } from '../../../user-management/entities/auth-data/auth-data.service';

describe('PasswordChangePolicyService', () => {
  let service: PasswordChangePolicyService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        PasswordChangePolicyService,
        { provide: UserService, useValue: {} },
        { provide: UserClaimService, useValue: {} },
        { provide: AuthDataService, useValue: {} },
        { provide: ServerSettingsService, useValue: {} },
      ],
    }).compile();

    service = module.get<PasswordChangePolicyService>(
      PasswordChangePolicyService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
