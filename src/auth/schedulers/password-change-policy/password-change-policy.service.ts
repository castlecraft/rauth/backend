import { Injectable } from '@nestjs/common';
import cron from 'node-cron';
import { CommandBus } from '@nestjs/cqrs';
import { UserService } from '../../../user-management/entities/user/user.service';
import { UserClaim } from '../../../auth/entities/user-claim/user-claim.interface';
import {
  RAUTH_EMAIL_SENT,
  RAUTH_PASSWORD_CHANGE,
  SCOPE_SELF_SERVICE,
} from '../../../constants/app-strings';
import { UserClaimService } from '../../../auth/entities/user-claim/user-claim.service';
import { AuthDataService } from '../../../user-management/entities/auth-data/auth-data.service';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { UpdateUserClaimCommand } from '../../../user-management/commands/update-user-claim/update-user-claim.command';
import { SendPasswordExpiryAlertCommand } from '../../../user-management/commands/send-password-expiry-alert/send-password-expiry-alert.command';

@Injectable()
export class PasswordChangePolicyService {
  constructor(
    private readonly user: UserService,
    private readonly commandBus: CommandBus,
    private readonly userClaim: UserClaimService,
    private readonly authDataService: AuthDataService,
    private readonly serverSettings: ServerSettingsService,
  ) {}

  start() {
    // Every hour
    cron.schedule('0 * * * *', async () => {
      const daysToSent = [30, 15, 7, 6, 5, 4, 3, 2, 1];
      const settings = await this.serverSettings.find();
      const userPasswordClaim = await this.userClaim.find({
        name: RAUTH_PASSWORD_CHANGE,
      });

      for (const passwordClaim of userPasswordClaim) {
        const daysSincePasswordChanged = this.getDaysFromDate(
          passwordClaim.value as string,
          settings.passwordResetIntervalDays,
        );
        if (daysSincePasswordChanged <= 0) {
          await this.removeUserPasswordAndAuthData(passwordClaim.uuid);
        } else if (daysToSent.includes(daysSincePasswordChanged)) {
          const userEmailClaim = await this.userClaim.findOne({
            uuid: passwordClaim.uuid,
            name: RAUTH_EMAIL_SENT,
          });

          let claim;
          if (userEmailClaim) {
            const emailSentDaysAgo = this.getDaysFromDate(
              userEmailClaim.value as string,
              settings.passwordResetIntervalDays,
            );
            const currentDay = this.getDaysFromDate(
              new Date() as unknown as string,
              settings.passwordResetIntervalDays,
            );
            if (emailSentDaysAgo === currentDay) {
              return;
            }
            await this.commandBus.execute(
              new SendPasswordExpiryAlertCommand(
                await this.user.findOne({ uuid: passwordClaim.uuid }),
              ),
            );
            userEmailClaim.value = new Date();
            claim = userEmailClaim;
          } else {
            await this.commandBus.execute(
              new SendPasswordExpiryAlertCommand(
                await this.user.findOne({ uuid: passwordClaim.uuid }),
              ),
            );
            claim = {
              scope: SCOPE_SELF_SERVICE,
              name: RAUTH_EMAIL_SENT,
              uuid: passwordClaim.uuid,
              value: new Date(),
            } as UserClaim;
          }
          await this.commandBus.execute(new UpdateUserClaimCommand(claim));
        }
      }
    });
  }

  getDaysFromDate(date: string, passwordResetIntervalDays: number) {
    const currentDate = new Date();
    const passwordChangedDate = new Date(date as string);
    const timeDifference =
      currentDate.getTime() - passwordChangedDate.getTime();
    const daysDifference = Math.floor(timeDifference / (1000 * 3600 * 24));
    const remainingDays = passwordResetIntervalDays - daysDifference;

    return remainingDays;
  }

  async removeUserPasswordAndAuthData(user) {
    const password = await this.authDataService.findOne({
      uuid: user.password,
    });
    user.password = null;
    await this.user.update(user);
    await this.authDataService.remove(password);
  }
}
