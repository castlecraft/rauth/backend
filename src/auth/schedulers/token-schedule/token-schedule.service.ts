import { Injectable } from '@nestjs/common';
import cron from 'node-cron';
import { OAuth2Service } from '../../controllers/oauth2/oauth2.service';
import { BearerTokenService } from '../../entities/bearer-token/bearer-token.service';

export const TOKEN_DELETE_QUEUE = 'token_delete';

@Injectable()
export class TokenSchedulerService {
  constructor(
    private readonly token: BearerTokenService,
    private readonly oauth2: OAuth2Service,
  ) {}

  start() {
    // Every hour
    cron.schedule('0 * * * *', async () => {
      const tokens = await this.token.find({
        deleteAfter: { $lt: new Date() },
      });

      for (const token of tokens) {
        const accessToken = token.accessToken;
        await this.oauth2.tokenRevoke(accessToken);
      }
    });
  }
}
