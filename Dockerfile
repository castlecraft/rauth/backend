FROM node:22-bookworm AS builder

# Copy and build app
COPY . /home/rauth/backend
WORKDIR /home/rauth/backend
RUN yarn \
    && yarn build \
    && yarn workspaces focus --production

FROM node:22-bookworm-slim

# Install dependencies
RUN apt-get update && \
    apt-get install -y krb5-user && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Setup entrypoint
COPY container/entrypoint.sh /usr/local/bin/entrypoint.sh

# Add non root user and make entrypoint executable.
RUN useradd -ms /bin/bash rauth && chmod +x /usr/local/bin/entrypoint.sh
WORKDIR /home/rauth/backend

RUN chown -R rauth:rauth /home/rauth
COPY --from=builder --chown=rauth:rauth /home/rauth/backend .

USER rauth

ENTRYPOINT ["entrypoint.sh"]
CMD ["start"]
