#!/bin/bash

create_services() {
  kadmin -r EXAMPLE.ORG -p admin@EXAMPLE.ORG -q "addprinc -randkey HTTP/localhost" <<EOF
admin
admin
EOF

  kadmin -r EXAMPLE.COM -p admin@EXAMPLE.COM -q "addprinc -randkey HTTP/localhost" <<EOF
admin
admin
EOF

  sudo kadmin -r EXAMPLE.COM -p admin@EXAMPLE.COM -q "ktadd HTTP/localhost@EXAMPLE.COM" <<EOF
admin
admin
EOF

  sudo kadmin -r EXAMPLE.ORG -p admin@EXAMPLE.ORG -q "ktadd HTTP/localhost@EXAMPLE.ORG" <<EOF
admin
admin
EOF

  sudo chown -R vscode:root /etc/krb5.keytab
}

if [ ! -f /etc/krb5.keytab ]; then
  create_services
else
  echo "Keytab already exists"
fi
