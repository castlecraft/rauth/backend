#!/bin/bash

create_user() {
  kadmin.local -q "addprinc -x dn=cn=${LDAP_PRINCIPAL},ou=Users,${LDAP_DC} ${LDAP_UID}" <<EOF
password1
password1
EOF
}

if [ ! -f /kerberos_principal_added ]; then
  create_user

  touch /kerberos_principal_added
else
  echo "Principal: $LDAP_PRINCIPAL from $LDAP_DC already added with uid $LDAP_UID"
fi
