const { ConsoleLogger } = require('@nestjs/common'); // eslint-disable-line
const { writeFileSync } = require('fs'); // eslint-disable-line

const allowedEnvVars = [
  { key: 'NODE_ENV', value: process.env.NODE_ENV },
  { key: 'ADMIN_SET_USER_PWD', value: process.env.ADMIN_SET_USER_PWD },
  { key: 'AUTH_MAX_REQUESTS', value: process.env.AUTH_MAX_REQUESTS },
  { key: 'AUTH_WINDOW', value: process.env.AUTH_WINDOW },
  { key: 'COOKIE_MAX_AGE', value: process.env.COOKIE_MAX_AGE },
  { key: 'DB_HOST', value: process.env.DB_HOST },
  { key: 'DB_NAME', value: process.env.DB_NAME },
  { key: 'DB_PASSWORD', value: process.env.DB_PASSWORD },
  { key: 'DB_USER', value: process.env.DB_USER },
  { key: 'ENABLE_CORS', value: process.env.ENABLE_CORS },
  { key: 'ENABLE_CRON', value: process.env.ENABLE_CRON },
  { key: 'ENABLE_RATE_LIMIT', value: process.env.ENABLE_RATE_LIMIT },
  { key: 'ENABLE_SWAGGER', value: process.env.ENABLE_SWAGGER },
  { key: 'EVENTS_CLIENT_ID', value: process.env.EVENTS_CLIENT_ID },
  { key: 'EVENTS_GROUP', value: process.env.EVENTS_GROUP },
  { key: 'EVENTS_HEALTHCHECK', value: process.env.EVENTS_HEALTHCHECK },
  {
    key: 'EVENTS_HEALTHCHECK_TIMEOUT',
    value: process.env.EVENTS_HEALTHCHECK_TIMEOUT,
  },
  { key: 'EVENTS_HOST', value: process.env.EVENTS_HOST },
  { key: 'EVENTS_PASSWORD', value: process.env.EVENTS_PASSWORD },
  { key: 'EVENTS_PORT', value: process.env.EVENTS_PORT },
  { key: 'EVENTS_PROTO', value: process.env.EVENTS_PROTO },
  { key: 'EVENTS_TOPIC', value: process.env.EVENTS_TOPIC },
  { key: 'EVENTS_USER', value: process.env.EVENTS_USER },
  { key: 'LOG_LEVEL', value: process.env.LOG_LEVEL },
  { key: 'MONGO_OPTIONS', value: process.env.MONGO_OPTIONS },
  { key: 'MONGO_URI_PREFIX', value: process.env.MONGO_URI_PREFIX },
  {
    key: 'PASSWORD_ENCRYPTION_KEY',
    value: process.env.PASSWORD_ENCRYPTION_KEY,
  },
  { key: 'PROD_ENV_VALUE', value: process.env.PROD_ENV_VALUE },
  { key: 'SESSION_NAME', value: process.env.SESSION_NAME },
  { key: 'SESSION_SECRET', value: process.env.SESSION_SECRET },
  { key: 'TOKEN_LENGTH', value: process.env.TOKEN_LENGTH },
  { key: 'TOKEN_VALIDITY', value: process.env.TOKEN_VALIDITY },
];

function generateDotEnv() {
  const context = `GenerateDotEnv`;
  const logger = new ConsoleLogger();
  logger.setLogLevels(process.env.LOG_LEVEL?.split(/\s*,\s*/) || []);

  const generatedEnvVars = [];
  for (const env of allowedEnvVars) {
    if (env?.value) {
      const envLine = env.value.includes('#')
        ? `${env.key}="${env.value}"`
        : `${env.key}=${env.value}`;
      generatedEnvVars.push(envLine);
      logger.log(`Added ${env.key}`, context);
    }
  }
  const envData = generatedEnvVars.join('\n') + '\n';
  writeFileSync(process.env.ENV_FILE_PATH || '.env', envData);
  logger.debug(`Following env file generated\n${envData}`, context);
}

generateDotEnv();
