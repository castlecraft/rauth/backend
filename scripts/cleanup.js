const mongoose = require('mongoose'); // eslint-disable-line

async function cleanupDatabase() {
  try {
    const client = await mongoose.connect(
      process.env.TEST_MONGO_URI ||
        'mongodb://authorization-server:admin@mongo:27017/test_authorization-server',
      { authSource: process.env.AUTH_SOURCE },
    );
    const isDropped = await client.connection.db.dropDatabase();
    await client.disconnect();
    if (isDropped) {
      return 'Database Deleted';
    }
  } catch (error) {
    return error;
  }
}

cleanupDatabase().then(console.log).catch(console.error);
