const { Logger } = require('@nestjs/common'); // eslint-disable-line
const { from, switchMap } = require('rxjs'); // eslint-disable-line

const subscription = from(
  fetch((process.env.SERVER_URL || 'http://localhost:3000') + '/api/setup', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      fullName: process.env.FULL_NAME || 'R-Auth Administrator',
      organizationName: process.env.ORGANIZATION_NAME || 'Example Inc',
      email: process.env.ADMIN_EMAIL || 'admin@example.com',
      issuerUrl: process.env.ISSUER_URL || 'http://accounts.localhost:4210',
      adminPassword: process.env.ADMIN_PASSWORD || '14CharP@ssword',
      phone: process.env.ADMIN_PHONE || '+919876543210',
    }),
  }),
)
  .pipe(switchMap(res => from(res.json())))
  .subscribe({
    next: res => {
      Logger.log(res, 'R-Auth Setup');
      subscription.unsubscribe();
    },
    error: error => {
      Logger.error(error.toString(), error, 'R-Auth Setup');
      subscription.unsubscribe();
    },
  });
