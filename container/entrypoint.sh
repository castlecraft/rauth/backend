#!/bin/bash

if [ "$1" = 'start' ]; then
  # Generate .env file
  node scripts/generate-dotenv.js
  # Start server
  node dist/backend/main.js
fi

exec "$@"
